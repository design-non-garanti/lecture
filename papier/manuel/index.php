<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>LICENCE</title>
        <?php include "styles/styles.php"; ?>
        <link rel="stylesheet" type="text/css" href="styles/styles.css">
        <link rel="stylesheet" type="text/css" href="fonts/AGBookFromBerthold/AGBook.css">
        <script type="text/javascript" src="scripts/scripts.js" defer></script>
    </head>
    <body>

        <div id="licence">
            <?php include "LICENCE-MEMOIRE.html"; ?>
        </div>

    </body>
</html>

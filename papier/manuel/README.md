#Version papier

- Manuel d'utilisation / mode d'emploi
  * plan du mémoire
    * Inscription (titre temporaire)
	  * .html .js .json >formulaires
    * L'ordinateur du 21e siècle
      * .html .js .mp4 .vtt >commentaires
    * frere dominic (titre temporaire)
	  * .html .js >youtubeAPI
    * Une entreprise de la disparition
	  * .html .js
    * À la recherche du design perdu
	  * .html .js <iframe>
    * Pourquoi le texte à l'écran c'est moche ? (titre temporaire)
	  * .html .js .py >TUI >buffer
    * Automisation, réglage, transparence (titre temporaire)
      * .html .js
    * Stop + code (titre temporaire)
      * html .js >outil-développeur pour navigateur
    * Index
      * .html .js .json >items
    * SAV
      * .html .js .json >chat
    * answer of natural language (titre temporaire)
      * .html .js
    * cadeau (titre temporaire)
      * .html .js .papier >drm

<!-- 
item contient info-box et espace ecriture md/html, info box contient keys, keys créent automatiquement un item 
##index
GPS
ver de pomme
camembert, charte
homard
clavier
mise en page de bible illustrée (book anatomy)
silhouettes de bonhomme
01010101010101
bureau du moine
ed fella
typo anatomy (chasse, délié etc.)
photo de fantômes
arbre généalogique
des gens qui font du yoga
indesign interface guide
workflow (scrum, agile) etc.
icônes menus material, flat
data vis hardcore (nuage de fils)
pissenlit
bougie
moteur
tatouage dans prison break
maquette de locomotive
huarache
home d'un site mockup
pdf de site avec indesign
 -->
  * licence design non garanti <!-- trouver une licence qui garanti les usages et écrire l'inverse -->
    * qu'est ce que la non garantie ?
    * qu'est ce que cela implique
  * Qu'est ce qu'un mémoire non garanti ?
    * rédaction implicite
    * work in progress
    * quend il est rendu ce n'est plus un mémoire (temporalité statut)
  * mode d'emploi d'un site internet
    * comment lire un site web <!-- steves krug dont make think et katerine hayles -->
    * browser conseillé
    * liste des raccourcis clavier ex : ouvrir la console, afficher le code source
  * où est ce mémoire ?
    * adressage
    * serveur
  * Comment à été fait ce mémoire ?
    * par qui ?
    * comment ?
      * travailler à deux
      * git
    * qui fait quoi ?







Nous pensons qu'une approche du design qui cherche seulement à rendre simple est absurde / impossible.
En effet, le mythe de la simplification 
tel un 

D'après la théorie solaire, Sisyphe représente le soleil qui s'élève chaque jour pour plonger à nouveau le soir sous l'horizon

Pourtant la recherche de la simplicité est un élément conducteur majeur aujourd'hui.

- McLuhan
- Flusser

que dans la tentative d'achever un travail interminable.

non seulement
insuffisante mais même impossible. 

Pour avoir osé défier les dieux, Sisyphe fut condamné, dans le Tartare, à faire rouler éternellement
jusqu'en haut d'une colline un rocher qui en redescendait chaque fois avant de parvenir au sommet
(Odyssée, chant XI).

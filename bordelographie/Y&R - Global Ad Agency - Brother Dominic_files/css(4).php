/*==================================================================================*/
/*===========NV.CSS Original Content================================================*/
/*==================================================================================*/

/* Droppics Carousel Restyling */
/* Navigation Buttons */
.theme-default .nivo-controlNav a.active {
    background: #2959f8 !important;
}

.theme-default .nivo-controlNav a {
    width: 10px;
    height: 10px;
    background: #a2a2a2!important;
    margin: 0 7px;
    border-radius: 5px;
}

.theme-default .nivo-controlNav {
    padding: 35px 0 20px 0;
}



/* Late Breaking Style Changes */
/* Turn off biography below the Leadership scroller */
.yr-about .mnwall_media_scroller.YRAboutScroller p {display: none;}
/* Turn off intro text in Pulse Item view so can use for FP tagging */
.YRPulseItem  .itemBody .itemIntroText {display:none;}
/* Remove space between images on Leadership images */
@media (min-width: 768px) {
.yrLeadership .row-fluid [class*="span"] {margin-left: 0;}
.yrLeadership .row-fluid .span3 {width: 25%;}
}
}

/* Turn off alert message on BAV Email Success Page */
.BAVEmailSuccess .alert-message {
    display: none;
}

/* ========== Scrollbar Styling =========== */
::-webkit-scrollbar-button{ display: block; height: 13px; border-radius: 0px; background-color: #AAA; } 
::-webkit-scrollbar-button:hover{ background-color: #AAA; } 
::-webkit-scrollbar-thumb{ background-color: #1275E0; } 
::-webkit-scrollbar-thumb:hover{ background-color: #2190ff; } 
::-webkit-scrollbar-track{ background-color: #efefef; } 
::-webkit-scrollbar-track:hover{ background-color: #CCC; } 
::-webkit-scrollbar{ width: 12px; }
::-webkit-scrollbar-thumb {border-radius: 0;-webkit-box-shadow: none;}
::-webkit-scrollbar-track {border-radius: 0;-webkit-box-shadow: none;}
/* ======== End Scrollbar Styling ========== */


/*------------------------------------------------------------------------------
	Sitewide Styling
--------------------------------------------------------------------------------*/
/* Sidewide Button Styling */
div.itemIsFeatured:before, div.catItemIsFeatured:before, div.userItemIsFeatured:before {display:none;}
.yrbtn, .yrbtn:focus, .yrbtn:active {color: #fff; text-shadow:none;margin: 0;font-family: yrthree_bold, sans-serif;text-transform: uppercase;letter-spacing: 3px;font-size: 12px;}

.btn, .btn:active, .btn:focus {
	text-transform:uppercase;
	text-shadow:none;
	box-shadow:none;
	font-family:yrthree_bold,arial,helvetica,sans-serif;
	color:white;
	background:#2959f8!important;
	border:2px solid #2959f8!important;
	letter-spacing:3px;
	padding:12px 20px!important;
}
.btn:hover, .btn.hover {
	-moz-transition:all 150ms;
	-webkit-transition:all 150ms;
	transition:all 150ms;
	background: #fff!important;
	color:#2959f8;
}


/* .btn {border: 2px solid #1275E0!important;box-shadow: none;padding: 8px 20px!important;}
.btn:hover {background: #1275E0!important; color: #fff; text-shadow:none;} */

.btn.active, .btn:active {
    background-image: none;
    outline: 0;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}
.btn:focus {
    outline: none;
    outline: none;
    outline-offset: 0;
}

.YROnGrey.yrbtn, .YROnGrey.yrbtn:focus, .YROnGrey.yrbtn:active {background: rgba(255, 255, 255, 0)!important;color: #1275E0;text-shadow: none;text-transform: uppercase;letter-spacing: 1px;font-family: yrthree_book, sans-serif;padding: 8px 20px!important;margin: 5px 20px 0 0;}
.YROnGrey.yrbtn:hover {color: #fff;background:#1275E0!important;}

@media (max-width: 767px) {
.btn {padding: 8px 20px!important;}
.YROnGrey.yrbtn, .YROnGrey.yrbtn:focus, .YROnGrey.yrbtn:active {padding: 8px 20px!important;margin: 0px 0px 20px 0px;}
}
/*------------------------------------------------------------------------------
	Terms & Conditions and Privacy Styling
--------------------------------------------------------------------------------*/
.yr-terms #region3, .yr-privacy #region3 {padding: 100px 15% 0;text-align:left;}

@media (max-width: 768px) {
.yr-terms #region3, .yr-privacy #region3 {padding: 80px 8%;text-align:left;}
}


/*------------------------------------------------------------------------------
	Locations Page Styling
--------------------------------------------------------------------------------*/
/* Contacts in the header part */
.locationsContacts .span6, .locationsContacts p {margin:0}
.locationsNB {
    display: block;
	padding: 50px 50px;
	float: right;
}
.locationsPR {
    display: block;
	padding: 50px 50px;
	float: left;
}
@media (max-width: 768px) {
.locationsNB {
    display: block;
	padding: 20px 50px;
	float: none;
}
.locationsPR {
    display: block;
	padding: 20px 50px;
	float: none;
}
}


/*------------------------------------------------------------------------------
	News (Pulse and External Link) Page Styling
--------------------------------------------------------------------------------*/
/* Pulse ITEM Page Styling */
.YRPulseItem.itemView .itemImageBlock {padding: 0;margin: 0 0 16px 0;}
.YRPulseItem.itemView .itemBody {padding: 0;margin: 0;}
#k2Container.YRPulseItem.itemView {padding: 0;margin: 0;}
.YRPulseItem.itemView .itemRelated ul {padding: 0;margin: 0;}
.YRPulseItem.itemView .itemImage {padding: 0;text-align:center;}
.YRPulseItem.itemView .itemHeader {padding: 0 15% 80px;text-align:center;}
.YRPulseItem.itemView .itemIntroText {margin: 35px 15% 20px 15%;text-align:left;}
.YRPulseItem.itemView .itemFullText {margin: 60px 15% 50px 15%;text-align:left;}
.YRPulseItem.itemView .itemVideoBlock {margin:0;padding:0;background: #303030;color: #fff;}
.YRPulseItem.itemView .itemLinks, .YRPulseItem.itemView .itemRelated {background: #303030;color: #fff;margin: 0;padding: 30px 15%;}
.YRPulseItem.itemView .itemTagsBlock span {color: #fff;}
.YRPulseItem .itemRelated h3 {padding: 20px 0;color: #fff;margin:0;}
.YRPulseItem .itemFullText h4 {color: #07002D;margin: 0 0 30px 0;}
.com_k2.item div.itemHeader h2.itemTitle {text-transform: uppercase;letter-spacing: 3px;}
.com_k2.item #component {margin-bottom: 0;}

.YRPulseItem .catItemDateCreated {
    display: block;
    text-align: left;
    padding: 0;
    margin: 0;
    color: #999;
    border-top: 0;
	font-size: 10px;
}

.YRPulseItem .catItemIntroText {
    font-size: inherit;
    font-weight: normal;
    line-height: inherit;
    padding: 4px 0 12px 0;
}

.YRPulseItem.itemListView {text-align: left;}

.YRPulseItem h3 {
	font-size: 24px;
    line-height: 150%!important;
	margin: 5px 0 10px 0;
}

.YRPulseItem .catItemImageBlock {
    padding: 5px 0;
    margin: 0;
}

.YRPulseItem .catItemBody {
    padding: 8px 15px;
    margin: 0;
}

.YRPulseItem .catItemImage {
    overflow: hidden;
    max-height: 225px;
}
/* Styling the header of External Link Item page */
.YRPulseItem div.itemExtraFields ul li span.itemExtraFieldsLabel {display: none;}
.YRPulseItem div.itemExtraFields {border-top: none;}
/* End styling the header of External Link Item page */
/* Styling the image of External Link Item page */
.YRPulseItem.itemView .itemImageBlock {margin: 0;}
.YRPulseItem span.itemImage {margin: 0;}
/* End styling the image of External Link Item page */


@media (max-width: 768px) {
.YRPulseItem.itemView .itemHeader {padding: 20px 8% 30px;text-align:center;}
.YRPulseItem.itemView .itemRelated {padding: 30px 4%;}
}

/* Pulse ITEM Related Items Page Styling */
.YRPulseItem.itemView .itemRelated h4 {color:#fff;margin-bottom:30px;}
.YRPulseItem.itemView .itemRelated h5 a {color:#fff;}
.YRPulseItem.itemView .itemRelated h5 a:hover{color:#1275E0;}
.YRPulseItem.itemView .itemRelatedImageYR {display: block;overflow: hidden;}
.YRPulseItem.itemView  .itemRelatedTextYR {margin:30px 10%;}
.YRPulseItem.itemView img.itemRelImg {object-fit: cover;width: 170px!important;height: 170px!important;}
.YRPulseItem .itemRelated .span4  .itemRelatedImageYR {
    width:170px;
    height:170px;
    background-repeat: no-repeat;
    background-size:cover;
}

/* Search Box */
.yr-thepulse .default-theme .mls-results h3 {font-family: yrthree_bold, sans-serif;color: #07002D;}
.yr-thepulse .default-theme .mls-results, .yr-thepulse .default-theme .mls-tooltip {border: none;border-radius: 0;}
.yr-thepulse .default-theme .mls-results-cont.expanded, .yr-thepulse .default-theme .instance-selector-cont.expanded {box-shadow: none;border-radius: 0px;}
.yr-thepulse .mls-item-title {font-size: 12px;font-family: yrthree_bold, sans-serif;color: #07002D;}
.yr-thepulse .mls-results-cont {left:auto;right:0;}
.yr-thepulse .mls-results-cont:after, .instance-selector-cont:after {left: 260px;}
.yr-thepulse .mls-results-cont:before, .instance-selector-cont:before {left: 258px;}


.yr-thepulse #region2.xtc-fluidwrapper {padding: 75px 15% 50px;}
.yr-thepulse #region3.xtc-fluidwrapper {padding: 0;}
.yr-thepulse .module.PulseSectionHeader {margin: 0;}

@media (max-width: 768px) {
.yr-thepulse #region2.xtc-fluidwrapper {padding: 75px 8% 0;}
}

/*------------------------------------------------------------------------------
	Careers Page Styling
--------------------------------------------------------------------------------*/
/* Main Careers Page 
.yr-careers .recruitmentButtons {padding-bottom: 70px;}
.yr-careers #component {margin-bottom: 0px;}
.yr-careers #k2Container {padding: 0;}
.yr-careers div.itemView {margin: 0;}
.yr-careers div.itemBody {padding:0;}
.yr-careers .yrCareersIntro {padding:50px 15% 35px 15%;}
.yr-careers .careersYr {padding: 0;}
.yr-careers .imagesRight .textBlock {padding-top: 0;}
.yr-careers .imagesRight .imageBlock {float: right;}
.yr-careers #region3.xtc-fluidwrapper {padding: 0;}
.yr-careers .zAcademyText {position: absolute;top: 50%;-webkit-transform: translate(0,-50%);transform: translate(0,-50%);}

@media (max-width: 767px) {
.yr-careers .yrCareersIntro {padding:40px 8%;}
}
*/

input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {height: 20px!important;min-width: 60%;text-align: center;}
.contact-form textarea {min-width: 60%;}
.contact-form .form-horizontal .controls {margin-left: 0;}
.contact-form h3 {display:none;}
.com_contact input[type="checkbox"] {float: none;margin-right: 0;}
.form-horizontal .control-label {float: none;width: 100%;text-align: center;}
.contact-form .btn-primary {background: #1275E0!important;color: #fff;text-shadow: none;margin: 0;font-family: yrthree_bold, sans-serif;text-transform: uppercase;letter-spacing: 3px;font-size: 12px;}
.contact-form .btn:hover {background: transparent!important;color: #1275E0;text-shadow: none;}
dl.contact-address.dl-horizontal {display: none;}

.yr-careers #component {margin-bottom: 0;}
.yr-careers-emea #region3.xtc-fluidwrapper, .yr-careers-na #region3.xtc-fluidwrapper, .yr-contact-asia #region3.xtc-fluidwrapper, .yr-contact-latinamerica #region3.xtc-fluidwrapper, .yr-contact-australia #region3.xtc-fluidwrapper  {padding: 0 15%;}
.yr-careers-emea #region2.xtc-fluidwrapper, .yr-careers-na #region2.xtc-fluidwrapper, .yr-contact-asia #region2.xtc-fluidwrapper, .yr-contact-latinamerica #region2.xtc-fluidwrapper, .yr-contact-australia #region2.xtc-fluidwrapper {padding: 130px 15% 0;}

.yr-contact-latinamerica .page-header, .yr-contact-asia .page-header, .yr-contact-australia .page-header  {text-align: center;}

.yr-contact-latinamerica h2, .yr-contact-asia h2, .yr-contact-australia h2 {font-size: 20px;line-height: 100%!important;}

.dropdown-menu>li>a:focus {outline: none;outline-offset: 0;}

@media (max-width: 768px) {
.yr-careers #region3.xtc-fluidwrapper .yrCareersIntro {padding: 40px 8%;}

.yr-careers-emea #region3.xtc-fluidwrapper, .yr-careers-na #region3.xtc-fluidwrapper, .yr-contact-asia #region3.xtc-fluidwrapper, .yr-contact-latinamerica #region3.xtc-fluidwrapper, .yr-contact-australia #region3.xtc-fluidwrapper  {padding: 0 10px;}

.yr-contact-asia input[type="text"], .yr-contact-latinamerica input[type="text"], .yr-contact-australia input[type="text"] {height: 15px!important;}
.yr-contact-asia input[type="email"], .yr-contact-latinamerica input[type="email"], .yr-contact-australia input[type="email"] {height: 15px!important;}

.yr-contact-asia .form-horizontal .control-label, .yr-contact-latinamerica .form-horizontal .control-label, .yr-contact-australia .form-horizontal .control-label {text-align: center;}
.yr-contact-asia input[type="checkbox"], .yr-contact-latinamerica input[type="checkbox"], .yr-contact-australia input[type="checkbox"] {float: none;margin-right: auto;margin-right: auto;}
}

/*------------------------------------------------------------------------------
	About Page Styling
--------------------------------------------------------------------------------*/
/* Main About Page 
.yr-about #component {margin-bottom: 0px;}
.yr-about .imagesRight .AboutText {position: absolute;top: 50%;-webkit-transform: translate(0,-50%);transform: translate(0,-50%);}
.yr-about .imagesLeft .AboutText {position: absolute;top: 50%;-webkit-transform: translate(100%,-50%);transform: translate(100%,-50%);}
.yr-about .imageBlock {float: right;}
.yr-about .imagesRight .imageBlock {float: right;}
.yr-about .imagesLeft .imageBlock {float: left;}*/


/* Landscape phone to portrait tablet 
@media (max-width: 767px) {
.yr-about .mnwall-scr-media-db {padding: 40px 8%;}
.yr-about #mnwall_scr_4 .flickity-prev-next-button {margin:-25px 25px;color: #07002D;}
.yr-about .mnwall_media_scroller.YRAboutScroller h3.mnwall-title {font-size: 15px;}
.yr-about .mnwall-desc h4 {font-size: 12px;}
}*/
/* Adjustment small devices landscape
@media (max-height: 414px) {
.yr-about .mnwall_media_scroller.YRAboutScroller .mnwall-scr-detail-box {width: 100%;}
.yr-about #mnwall_scr_4 .mnwall-scr-media-db {padding: 20px 10px;}
.yr-about #mnwall_scr_4 .flickity-prev-next-button {margin: -45px 25px;}
} */
@media (max-width: 767px) {
.yr-about .flickity-prev-next-button {top: 70%;}
.yr-about .flickity-prev-next-button.previous {left: 0px;}
.yr-about .flickity-prev-next-button.next {right: 0px;}
}
/*------------------------------------------------------------------------------
	Home Page Styling
--------------------------------------------------------------------------------*/
.YRhomepageH1 h1 {font-size: 20px;padding-bottom: 10px;line-height: 150%!important;letter-spacing: 1px;}

.yr-homepage #region2.xtc-fluidwrapper {padding: 130px 15%;}
.yr-homepage .homepagetext p {padding-bottom:20px;}


@media (max-width: 768px) {
.yr-homepage #region2.xtc-fluidwrapper {padding: 40px 8%;}
.yr-homepage #region4.xtc-fluidwrapper {padding: 40px 8%;}
.yr-homepage #region3 .module {margin: 0;}
}
/* Adjustment small devices landscape */
@media (max-height: 414px) {
.yr-homepage .resistText {top: 40%;max-width: 60%;}
.yr-homepage .arrow {bottom: -10px;}
}

/*------------------------------------------------------------------------------
	Work Page Styling
--------------------------------------------------------------------------------*/
/* Work Main Page Styling */
.yr-work  #region3.xtc-fluidwrapper {padding: 0;}
.yr-work  #component {display: none;}


@media (max-width: 767px) {
    .yr-work #user7 .module {margin:0;}
    .yr-work #user7 .modulecontent {padding:0;}
}


/*------------------------------------------------------------------------------
	Styling for Minitek Smart Search on The Pulse page
--------------------------------------------------------------------------------*/
/* Hide tabs in Results page */
.com_miniteklivesearch .mls_comp_results_filters {display: none;}
/* Reduce bottom padding of title in Results page */
.com_miniteklivesearch #region2.xtc-fluidwrapper {padding: 110px 15% 50px 15%;}
/* Remove keyline around box */
.yr-thepulse .default-theme input.mls-query {font-family: yrthree_book, sans-serif;text-transform: uppercase;}

/* Tweaks to Search Results page */
.com_miniteklivesearch div#component {margin:0 15%;}
.com_miniteklivesearch #region3.xtc-fluidwrapper {padding: 0;}
.com_miniteklivesearch .mls_comp_results_lists h3 {color: #07002D;display:none;}

/* Styling once search is positioned inside the template default.php */
.yr-thepulse .yrsearch {display: inline-block;vertical-align: top;}
.yr-thepulse .yrsearch button.button.btn.btn-primary.mod-mls-btn {height: 24px;}

/*===End Styling for Minitek Smart Search=================================*/


/*------------------------------------------------------------------------------
	Styling for Minitek K2 Responsive Grid on The Pulse page
--------------------------------------------------------------------------------*/
.yr-thepulse .mix-date {font-size: 10px;line-height: 1.2;margin: 0 0 5px;}
.yr-thepulse .regridk2 .inner-mix {padding: 10px 0;}
.yr-thepulse a.mix-title {color: #07002D;}
.yr-thepulse .mix-controls {margin: 0;}
.yr-thepulse .mix-sorts .button.active, .yr-thepulse .inner-mix .button.active {background: #07002D;}
.yr-thepulse .mix-sorts .button.active i.toggle_layout:before, .yr-thepulse .mix-sorts .button.active i.toggle_layout:after {background: #07002D;}
.yr-thepulse .mix-controls .button {
    background: none repeat scroll 0 0 #C3BDB8;
    border-radius: 0;
    color: #F9F6F0;
    cursor: pointer;
    display: inline-block;
    height: 32px;
    line-height: 34px;
    margin: 0 4px 0 0;
    padding: 0 8px;
    text-transform: none;
    border: 0;
    box-shadow: none;
    text-shadow: none;
    font-size: 14px;
}

.yr-thepulse .mix-controls {margin: 30px 0 0 0;}


@media (max-width: 760px) {
  .yr-thepulse #region3.row-fluid.xtc-fluidwrapper {padding: 0 10px 50px 10px;}
  .yr-thepulse .mix-filters .drop_down.drop_down1470.mix-tags1470 {margin: 0;}
  .yr-thepulse .mix-sorts {display: none;}
  .yr-thepulse .mls-results-cont {top: 55px;}
  .yr-thepulse .mix-filters {width: 100%;}
}

@media (max-width: 759px) and (min-width: 451px) {
  .yr-thepulse #regridk21470.mix-grid .mix, .gap {width: 100%;max-width: 100%;}
}

@media (max-width: 450px) {
  .yr-thepulse input.mls-query {width: 135px!important;}
  .yr-thepulse .mix-filters .drop_down {width: 150px;}
  .yr-thepulse .mls-results-cont {width: 335px;}
  .yr-thepulse #regridk21470.mix-grid .mix  {width: 100%;max-width: 100%;}
  .yr-thepulse #region2 .modulecontent {padding: 0;}
}

@media (max-width: 320px) {
  .yr-thepulse input.mls-query {width: 110px ;}
  .yr-thepulse .mix-filters .drop_down {width: 120px;}
  .yr-thepulse .mls-results-cont {width: 300px;}
}

.yr-thepulse .btn {background: #07002D;text-shadow: none;}


.yr-thepulse .drop_down .label:hover i.fa, .yr-thepulse div.drop_down .label:hover {
    color: #07002D;
}

.yr-thepulse .drop_down .label div {
    padding: 10px 10px 10px;
    font-family: yrthree_book, sans-serif;
    text-transform: uppercase;
    color: #aaa;
    font-size: 12px;
}

.yr-thepulse .mix-filters .drop_down.expanded .label {
    border-radius: 0;
    background: #07002D;
    border-color: #07002D;
}
.yr-thepulse .drop_down li.active {
    color: #07002D;
}
.yr-thepulse .drop_down .label:hover div {
    color: #07002D;
}
.mix-filters .drop_down.expanded ul {
    border-top-color: #07002D;
    box-shadow: none;
    border-radius: 0;
}
.yr-thepulse .drop_down .label {
    max-height: 32px;
    background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 0;
}

.yr-thepulse .mix-filters .drop_down {
    margin: 0 4px 0 10px;
    padding: 0;
    height: 34px;
}

.yr-thepulse .regridk2 .inner-mix h3 {
    font-size: 13px;
    letter-spacing: 1px;
}

/*===End Styling for Minitek K2 Responsive Grid=================================*/


/*------------------------------------------------------------------------------
	Custom YR Styles for K2 "The Pulse Item" Template
--------------------------------------------------------------------------------*/
.yr-thepulse #region3.xtc-fluidwrapper {padding: 0 15% 50px 15%;}

@media (max-width: 768px){
.yr-thepulse #region3.row-fluid.xtc-fluidwrapper {padding: 0 20px;}
}

.YRLatestNews .mnwall-hover-box-content h3.mnwall-title a, .YRLatestNews .mnwall-hover-box-content h3.mnwall-title span {
    font-size: 20px;
    font-weight: 800;
    line-height: 0;
}



.mnwall-s-desc, .mnwall-desc, .dark-text .mnwall-desc {
    font-size: 13px;
    line-height: 18px;
    padding: 0 15px 15px;
    text-transform: none;
}

div.mnwall-s-desc:before, div.mnwall-desc:before {
    content:' ';
    display:block;
    border:2px solid #1275E0;
    width: 50px;
    margin-left:auto;
    margin-right:auto;
    margin-bottom: 22px;
    margin-top: 20px;
}




	/* CS CHANGES 2017-05-15 */
	.yrLeadership img {width:100%;}
	.wallview.columns-4 .jxtc_dnpwall_tmp1, .wallviewbootstrap.columns-4 .jxtc_dnpwall_tmp1 {border-width:0!important;max-width:100%!important;width:100%!important;}
	@media (max-width: 768px) {
		.wallviewbootstrap .span3 {width:50%;float:left;}
		.wallviewbootstrap.columns-4 .jxtc_dnpwall_tmp1 p {display:none;}
	}


/*==================================================================================*/
/*===========yrtypography.CSS Original Content======================================*/
/*==================================================================================*/
/*------------------------------------------------------------------------------
	Custom YR typography amends in main template.
    Element and style amends are held in template.css file
    Check the template framework in Joomla backend for many/most typography
--------------------------------------------------------------------------------*/
/* Sitewide line height tweaks */
p {line-height: 190%;}
.introtext li, .fulltext li {line-height: 190%;}
h4, h3, h5 {line-height: 150%!important;letter-spacing: 1px;}
h4 {padding-bottom: 10px;}
h2 {line-height: 120%!important;}

h1.moduletitle span.first_word, h1.moduletitle span.rest, h2.moduletitle span.first_word, h2.moduletitle span.rest, h3.moduletitle span.first_word, h3.moduletitle span.rest, h4.moduletitle span.first_word, h4.moduletitle span.rest, h5.moduletitle span.first_word, h5.moduletitle span.rest, h6.moduletitle span.first_word, h6.moduletitle span.rest {
    font-size: 34px;
    line-height: 112%;
    font-weight: 700;
    letter-spacing: 0;
    font-family: yrthree_bold, sans-serif;
    color: #07002D;
    letter-spacing: 4px;
}
@media (max-width: 760px) {
h1.moduletitle span.first_word, h1.moduletitle span.rest, h2.moduletitle span.first_word, h2.moduletitle span.rest, h3.moduletitle span.first_word, h3.moduletitle span.rest, h4.moduletitle span.first_word, h4.moduletitle span.rest, h5.moduletitle span.first_word, h5.moduletitle span.rest, h6.moduletitle span.first_word, h6.moduletitle span.rest {
    font-size: 25px;
    line-height: 112%;
    font-weight: 700;
    letter-spacing: 0;
    font-family: yrthree_bold, sans-serif;
    color: #07002D;
    letter-spacing: 1px;
}
}


/*==================================================================================*/
/*===========Template File Original Content=========================================*/
/*==================================================================================*/

.YRLatestNews  .dark-text h3.mnwall-title a, .YRLatestNews  .dark-text h3.mnwall-title span, .YRLatestNews .dark-text h3.mnwall-title a:hover {color: #07002D;font-size:20px;}

/* CAREERS page layout */
.yr-contact #component {padding: 0 10%;}
.yr-careers  #region3 {padding: 40px 0 0 0;!important}
@media (max-width: 767px) {
.yr-careers  #region3, .yrlocations  #region3.xtc-fluidwrapper {padding: 0;!important}
}

/* ABOUT page layout */
.yr-about #region3.xtc-fluidwrapper {padding: 0;!important}
.yr-about div.itemBody {padding: 0;}

/* LOCATIONS page layout */
.yrlocations  #region3.xtc-fluidwrapper {padding: 40px 0 0 0;!important}

/* CASE STUDIES page layout */
div#k2Container.YRCaseStudy {padding: 0;}
div#k2Container.YRCaseStudy.itemView {margin: 0;}

/* Adjustments to Title In Module */
.TitleInModuleCode {padding: 0 80px 60px 80px;}

/* Turn Off K2 Featured Star in all K2 templates */
div.itemIsFeatured:before, div.catItemIsFeatured:before, div.userItemIsFeatured:before {display: none;}

/* Bouncing scroll down arrow in top block of homepage */
.arrow {text-align: center;position: absolute;bottom: 10px;width: 100%;}
.bounce {-moz-animation: bounce 3s infinite;-webkit-animation: bounce 3s infinite;animation: bounce 3s infinite;}
@keyframes bounce {
	0%, 20%, 50%, 80%, 100% {transform: translateY(0);}
	40% {transform: translateY(-30px);}
	60% {transform: translateY(-15px);}
}

/* White Margin AB testing */
body {margin: 0!important;padding: 0!important;}
#header {transform: translateX(0px)!important;-webkit-transform: translateX(0px)!important;}
body.frontpage #region1 {height: calc(100vh)!important;}
.yr-homepage #region3.xtc-fluidwrapper, .yr-homepage #region6.xtc-fluidwrapper {padding: 0!important;}
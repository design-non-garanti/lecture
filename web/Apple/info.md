
## iPhone

« So, three things: a widescreen iPod with touch controls; a revolutionary mobile phone; and a
breakthrough Internet communications device.  An iPod, a phone, and an Internet communicator. An
iPod, a phone … are you getting it?  These are not three separate devices, this is one device, and
we are calling it iPhone. » *iPhone Introduction*, 09/01/2007, 00:24:02, consultable à l'adresse :
<https://www.youtube.com/watch?v=-3gw1XddJuc> 

« I can look at my e-mail with a split view just like I do on my computer [...] So it’s real
e-mail, just like you’re used to on your computer, right here on your phone », Ibid., 1:00:00

« So, today, we’ve added to the Mac and the iPod. We’ve added Apple TV and now iPhone.  And
you know, the Mac is the only one that you think really of as a computer. Right?  And so we’ve
thought about this and we thought, you know, maybe our name should reflect this a little bit more
than it does.  So we’re announcing today we’re dropping the computer from our name, and from this
day forward, we’re gonna be known as Apple Incorporated, to reflect the product mix that we have
today. », Ibid., 1:37:42

« I don’t want people to think of this as a computer », John Markoff, *Steve Jobs Walks the
Tight Rope Again*, 12/01/2007, consultable à l'adresse :
<http://www.nytimes.com/2007/01/12/technology/12apple.html>



<div class="rc">
    <img src="http://via.placeholder.com/320x150">
</div>

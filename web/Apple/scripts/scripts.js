console.clear();

// UTILS
function rdm(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min)
	}
}

function secondify(hms) {
    var a = hms.split(':');
    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
    return seconds;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


// BROWSERINFO
browser = {};
browser.clientHeight = document.documentElement.clientHeight;
browser.clientWidth = document.documentElement.clientWidth;
browser.scrollHeight = document.documentElement.scrollHeight;
browser.scrollWidth = document.documentElement.scrollWidth;
browser.offsetHeight = document.documentElement.offsetHeight;
browser.offsetWidth = document.documentElement.offsetWidth;
browser.innerWidth = window.innerWidth;
browser.innerHeight = window.innerHeight;
//console.log(browser);

// FIND
function find(basename) {
    var obj;
    for (var i in desktop.files) {
        if (desktop.files[i].basename.substr(0,1) === basename) {
            obj = desktop.files[i];
        }
    }
    return obj;
}

// CALL
Call = function(el, window) {
    this.el = el;
    this.window = window;
   
    var options = ['n','x','y','w','h','t'];
    for (var i in options) {
        if (this.el.getAttribute('data-'+options[i])) {
            this[options[i]] = this.el.getAttribute('data-'+options[i]);
        }
    }

    var tmp = this;
    this.el.onclick = function() {
        var file = find(tmp.n);
        file.window.open();
        console.log(file.type); 
        if (file.type === 'video') {
            if (tmp.t) {
                seconds = secondify(tmp.t);
                file.window.content.currentTime = seconds;
            }
            file.window.content.play();
        }
    }
}


// WINDOW
Window = function(file) {
    this.file = file;
}

Window.prototype.focus = function() {
    this.el.classList.add('focus');
    var max = 100;
    for (var i in this.file.desktop.files) {
        max = Math.max(max, this.file.desktop.files[i].window.el.style.zIndex)
    }
    this.el.style.zIndex = max + 1;
}

Window.prototype.shuffle = function() {
    var w = rdm(300,400).int + "px";
    this.el.style.width = w;
    this.el.style.height = rdm(200,300).int + "px";
    this.el.style.left = rdm(0,browser.clientWidth-400).int + "px";
    this.el.style.top = rdm(0,browser.clientHeight-300).int + 'px'; 
    
}

Window.prototype.position = function(param) {
    if (param) {
        console.log(param);
    }
}

Window.prototype.open = function() {
    this.el.classList.add('open');
    this.status = 1;
    this.focus();
}

Window.prototype.close = function() {
    console.log('close');
}


Window.prototype.getContent = function() {
    var tmp; 
    var data = {};
    data.file = this.file.basename;
    var request = $.ajax({
        url: "php/getContent.php",
        type: "POST",
        data: data,
        async: false, // grado mais permet d'assigner result
        success: function(result){
            tmp = result;
        }
    });
    return tmp;
}

Window.prototype.build = function() {

    this.el = document.createElement('div');
    this.el.classList.add('window');

    this.header = {};
    this.header.el = document.createElement('div');
    this.header.el.classList.add('header');
    this.header.el.innerHTML = this.file.basename;
    this.el.appendChild(this.header.el);
   
    this.container = {};
    this.container.el = document.createElement('div');
    this.container.el.classList.add('container');
    this.el.appendChild(this.container.el);
    
    switch(this.file.extension) {
        case "png":
        case "jpg":
        case "svg":
        case "jpeg":
        case "gif":
            this.el.classList.add('img');
            this.content = {};
            this.content.el = document.createElement('img');
            this.content.el.src = desktop.path + this.file.basename;
            break;
        case "mp4":
        case "webm":
            this.el.classList.add('video');
            this.content = {};
            this.content.el = document.createElement('video');
            this.content.el.src = desktop.path + this.file.basename;
            this.content.el.controls = 'controls';
            break;
        case "txt":
        case "html":
        case "md":
            this.el.classList.add('text');
            this.content = {};
            this.content.el = document.createElement('div');
            this.content.el.classList.add('text');
            this.content.el.innerHTML = this.getContent();
            var calls = this.content.el.getElementsByClassName('call');
            for (var i = 0; i < calls.length; i ++) {
                var call = new Call(calls[i],this);
            }
            break
    } 
    console.log(this);
    this.container.el.appendChild(this.content.el);
    
    this.file.desktop.el.appendChild(this.el);
   
    this.shuffle();
    $(this.el).draggable({handle: ".header", containment: "parent"});
    
    var tmp = this;
    this.el.onmousedown = function() {
        tmp.focus();
    }
}

// FILE
File = function(desktop,info) {
    this.desktop = desktop;
    this.basename = info.basename;          //todo
    this.dirname = info.dirname;            //todo
    this.extension = info.extension;        //todo
    this.filename = info.filename;          //todo
}

File.prototype.focus = function() {
    this.desktop.unfocus();
    this.el.classList.add('focus');
    var max = 0;
    for (var i in this.desktop.files) {
        max = Math.max(max, this.desktop.files[i].el.style.zIndex)
    }
    this.el.style.zIndex = max + 1;
}

File.prototype.shuffle = function() {
    this.el.style.position = 'absolute';
    //console.log(this.el);
    //console.log(this.el.offsetHeight);
    this.el.style.top = rdm(0,browser.clientHeight-this.el.offsetHeight-20).int + "px";
    this.el.style.left = rdm(0,browser.clientWidth-this.el.offsetWidth).int + "px";
}

File.prototype.build = function() {

    this.el = document.createElement('div');
    this.el.classList.add('file');

    switch(this.extension) {
        case "png":
        case "jpg":
        case "svg":
        case "jpeg":
        case "gif":
            this.el.classList.add('img');
            this.type = 'img';
            var icon = document.createElement('img');
            icon.src = desktop.path + this.basename;
            break;
        case "mp4":
        case "webm":
            this.el.classList.add('video');
            this.type = 'video';
            var icon = document.createElement('video');
            icon.src = desktop.path + this.basename;
            break;
        case "txt":
        case "html":
        case "md":
            this.el.classList.add('text');
            this.type = 'text'
            var icon = document.createElement('img');
            //icon.classList.add('nobackground');'
            icon.src = 'img/textfile2.png'
            break
    } 
    icon.classList.add('icon');
    this.el.appendChild(icon);

    var name = document.createElement('p');
    name.classList.add('name');
    name.innerHTML = this.basename;
    this.el.appendChild(name);

    this.desktop.el.appendChild(this.el);
    
    this.window = new Window(this);
    this.window.build();
    
    $(this.el).draggable({containment: "parent"});
    this.shuffle();

    var tmp = this;
    this.el.onmousedown = function() {
        tmp.focus();
    }
    this.el.ondblclick = function() {
        tmp.window.open();
    }
}

// DESKTOP
Desktop = function(){
    this.el = document.getElementById('desktop');       //as argument todo
    this.path = 'desktop/';                             //as argument todo
    this.files = [];
}

Desktop.prototype.get = function() {
    var request = $.ajax({
        url: "php/get.php",
        type: "GET",
        async: false, // grado mais permet d'assigner result
        success: function (result){
            data = JSON.parse(result);
        }
    });
    this.build(data);
};

Desktop.prototype.build = function(files){
    for (var i in files) {
        var file = new File(this,files[i]);
        this.files.push(file);
        
        file.build();
    }
}

Desktop.prototype.unfocus = function() {
    for (var i in this.files) {
        this.files[i].el.classList.remove('focus');
    }
}

// TASKBAR

//var clock = setInterval(function(){ myTimer() }, 1000);
//
//function myTimer() {
//    var d = new Date();
//    var t = d.toLocaleTimeString();
//    document.getElementById("clock").innerHTML = t;
//}


// INIT
var desktop = new Desktop();
desktop.get();

setTimeout(function(){
    document.getElementById('spinner').classList.remove('visible');
    document.getElementById('spinner').classList.add('hidden');
}, 4000);  
setTimeout(function(){
    document.getElementById('logo').classList.remove('hidden');
    document.getElementById('logo').classList.add('visible');
}, 6000);  
setTimeout(function(){
    document.getElementById('title').classList.remove('hidden');
    document.getElementById('title').classList.add('visible');
}, 8000);  
setTimeout(function(){
    document.getElementById('logo').classList.remove('visible');
    document.getElementById('logo').classList.add('hidden');
    document.getElementById('title').classList.remove('visible');
    document.getElementById('title').classList.add('hidden');
}, 11000);  
setTimeout(function(){
    document.getElementById('loading').classList.remove('visible');
    document.getElementById('loading').classList.add('hidden');
}, 12000);  
setTimeout(function(){
    document.getElementById('loading').style.display = 'none';
}, 13000);


// LAYOUT

function layout() {
    var file = find('t');
    file.window.el.style.top = "100px";
    file.window.el.style.left = "100px";
    file.window.container.el.style.width = "360px";
    file.window.container.el.style.height = "500px";
    
    var file = find('1');
    file.window.el.style.top = "500px";
    file.window.el.style.left = "500px";
    file.window.el.style.width = "374px";
    file.window.el.style.height = "275px";
}

layout();


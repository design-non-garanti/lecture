<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Apple</title>
        <link rel="stylesheet" type="text/css" href="styles/styles.css">
        <link rel="stylesheet" type="text/css" href="fonts/SanFrancisco/SanFrancisco.css">
        <script type="text/javascript" src="scripts/scripts.js" defer></script>
        <script src="assets/jquery-2.1.4.min.js"></script>
        <script src="assets/jquery-ui.js"></script>
    </head>
    <body>
        
        <?php include "inc/loading.php"; ?>

        <?php include "inc/taskbar.php"; ?>

        <div id="desktop">

        </div>

    </body>
</html>


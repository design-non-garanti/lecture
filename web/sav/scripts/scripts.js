var sms = document.getElementById('simple-messages-system');
var root = document.getElementById('root');
var newme = document.getElementById('newme');
newme.addEventListener('click', postData);

// récupère les data json contenant les messages et appelle createMessages
function getData() {
    console.log('getData');
        
    var request = $.ajax({
        url: "php/get.php",
        type: "GET",
        async: false, // grado mais permet d'assigner result
        success: function(result){
            while (root.hasChildNodes()) {
                root.removeChild(root.lastChild);
            }
            var data = JSON.parse(result);
            var level = 0;
            createMessages(root,level,data);
        }
    });
}

// parse les messages json et créer les éléments dans le dom
function createMessages(container,level,data,path) {
    console.log('createMessages');
        
    if (path === undefined) { 
        var path = []; 
    } 
    
    for (item in data) {
        
        var ipath = [];
        ipath = ipath.concat(path);
        ipath.push(item);
        
        var message = document.createElement('div');
        message.classList.add('message');
        message.dataset.level = level;
        message.dataset.key = item;
        message.dataset.path = ipath.join('-');
        
        var text = document.createElement('div');
        text.classList.add('text');
        text.innerText = data[item]['text'];
        message.appendChild(text);
       
        var p = document.createElement('p');
        
        var name = document.createElement('span');
        name.classList.add('name');
        name.innerText = data[item]['name'];
        p.appendChild(name);
        p.appendChild(document.createTextNode("-"));
      
        if (data[item]['mail']) {
            var mail = document.createElement('span');
            mail.classList.add('mail');
            mail.innerText = data[item]['mail'];
            p.appendChild(mail);
            p.appendChild(document.createTextNode("-"));
        }

        var datetime = document.createElement('span');
        datetime.classList.add('datetime');
        datetime.innerText = data[item]['date'] + ' à ' + data[item]['time'];
        p.appendChild(datetime);

        message.appendChild(p);
        
        var reply = document.createElement('button');
        reply.addEventListener("click", createPost);
        reply.classList.add('reply');
        reply.innerText = 'Message';
        message.appendChild(reply);
       
        var messages = document.createElement('div');
        messages.classList.add('messages');
        message.appendChild(messages);
        createMessages(messages,level+1,data[item]['messages'], ipath);

        container.appendChild(message);
    }
    console.clear;
}

// créer les champs pour répondre
function createPost() {
    console.log('createPost');

    var message = this.parentNode;
    
    if (message.childNodes[4]) {
        message.childNodes[3].childNodes[0].focus();
    
    } else {
        
        var post = document.createElement('div');
        post.classList.add('post');
            
        var text = document.createElement('textarea');
        text.placeholder = '...';
        post.appendChild(text);

        var p = document.createElement('p');
        
        var name = document.createElement('input');
        name.type = 'text';
        name.placeholder = 'Nom';
        var nameCookie = cookies.get('name');
        if (nameCookie) {
            name.value = nameCookie;
        }
        p.appendChild(name);
        
        var mail = document.createElement('input');
        mail.type = 'mail';
        mail.placeholder = 'Mail';
        var mailCookie = cookies.get('mail');
        if (mailCookie) {
            mail.value = mailCookie;
        }
        p.appendChild(mail);

        post.appendChild(p);
        
        var send = document.createElement('button');
        send.addEventListener("click", postData);
        send.innerText = 'Ok';
        post.appendChild(send);
        
        //var cancel = document.createElement('button');
        //cancel.addEventListener("click", postCancel);
        //cancel.innerText = 'Cancel';
        //post.appendChild(cancel);

        message.insertBefore(post, message.getElementsByClassName('messages')[0]);
        text.focus();
    }
}

console.log(document.cookie);

// post data to json
function postData() {
    console.log('postData');
    
    var post = this.parentNode;
    var message = post.parentNode;
    var dataPath = message.getAttribute('data-path');
    var message = post.parentNode;
    if (dataPath) {
        var path = dataPath.split('-');
    }

    console.log(post.childNodes[0]);
    
    var data = {};
    data.text =  post.childNodes[0].value;
    data.name =  post.childNodes[1].childNodes[0].value;
    data.mail =  post.childNodes[1].childNodes[1].value;
    data.messages = [];
    if (dataPath) {
        data.path =  path;
    }

    cookies.set('name',data.name);
    cookies.set('mail',data.mail);

    if (!data.text) {
        alert('Texte est vide');
    } else if (!data.name) {
        alert('Nom est vide');
    } else {
        var request = $.ajax({
            url: "php/post.php",
            type: "POST",
            data: data,
            success: function(result){
                console.log(result);
                getData();
            }
        });
    }
}



// BUTTONS
buttons = {};
buttons.el = document.createElement('div');
buttons.el.id = 'buttons';

buttons.zoom = {};
buttons.zoom.el = document.createElement('div');
buttons.zoom.value = 100;

buttons.zoom.in = {};
buttons.zoom.in.el = document.createElement('button');
buttons.zoom.in.el.innerText = '+';
buttons.zoom.in.f = function () {
    if (buttons.zoom.value < 100) {
        buttons.zoom.value += 10;
        root.style.transform = 'scale('+buttons.zoom.value/100+')';
    }
}

buttons.zoom.out = {};
buttons.zoom.out.el = document.createElement('button');
buttons.zoom.out.el.innerText = '-';
buttons.zoom.out.f = function () {
    if (buttons.zoom.value > 10) {
        buttons.zoom.value -= 10;
        root.style.transform = 'scale('+buttons.zoom.value/100+')';
    }
}

buttons.zoom.in.el.addEventListener('click', buttons.zoom.in.f);
buttons.zoom.out.el.addEventListener('click', buttons.zoom.out.f);

buttons.zoom.el.appendChild(buttons.zoom.in.el);
buttons.zoom.el.appendChild(buttons.zoom.out.el);
buttons.el.appendChild(buttons.zoom.el);
sms.appendChild(buttons.el);



// COOKIES
cookies = {};
cookies.set = function (name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
cookies.get = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
cookies.erase = function (name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}


getData();

<style type="text/css">
<?php
$dir = "../fonts/";
$fonts = scandir($dir);

foreach ($fonts as $key => $value) {
	if($value == "." || $value == ".."){}else{
		echo "@font-face{\r\nfont-family: \"".$value."\";\r\nsrc: url(\"".$dir.$value."\");\r\n}\r\n";
	}
}
?>
*{
	font-family: "Roboto-Regular.ttf" !important;
	font-weight: normal !important;
}
span{
	text-indent: 2rem;
	display: block;
	margin: 0 !important;
}
body, html{
	height: 100%;
	overflow: hidden;
}

header, nav{
	overflow: hidden;
}
header .btn{
	font-family: "Roboto-Regular.ttf" !important; 
	text-transform: none !important;
	height: fit-content !important;
	line-height: 1rem !important;
	margin: 0rem 0rem 0rem 0rem !important;
	padding: 0.75rem 1rem 0.75rem 1rem !important;
	font-size: 1rem;
}
a.btn{
	font-family: "Roboto-Regular.ttf" !important; 
	text-transform: none !important;
	height: fit-content !important;
	min-height: 1.75rem !important;
	line-height: 0.5rem !important;
	margin: 0rem 0rem 0rem 0rem !important;
	padding: 0.75rem 0.75rem 0.5rem 0.75rem !important;
	font-size: 1rem;
}
img, video{
	max-width: 100%;
	margin-bottom: -0.9rem !important;
}
video{
		margin-top: -1rem !important;
}
h1,h2,h3 {
	font-family: "Roboto-Light.ttf" !important;
}
h3{
	margin-bottom: -0.5rem !important;
}
blockquote{
	font-size: 1.2rem;
	margin-top: -1rem !important;
	margin-bottom: -0.5rem !important;
}

#container > section {
	box-sizing: border-box;
	display: inline-block;
	vertical-align: top;
	width: 50%;
	height: calc(100vh - 64px);
}
#enquete{
	box-sizing: border-box;
	padding: 0rem 2rem 0rem 2rem;
	overflow-y: scroll;
	overflow-x: hidden;
}
#enquete *{
	margin-bottom: 1rem;
}
#google_archive .toast.pinned{
	right: 3.5rem;
}
p{
	/* width: 640px; */
	font-size: 1.2rem;
	max-width: 100%;
	margin:auto;
}
article > section .card-panel:not(h2):not(h3){
	  padding: 24px 3rem 24px 3rem !important;
}

#google_archive div, #google_archive iframe{
	width: 100%;
	height: 100%;
	border:none;
}
iframe *{
	background-color: white;
}

article{
	margin-top: 2rem !important;
}

.holds-the-iframe {
	background-color: rgb(247,244,248);
  background:url('assets/giphy.webp') center center no-repeat;
 }

 .article{
 	display: none;
 }
</style>
 * [X] L'enquête
    * [X] Pourquoi cette enquête ?
      - [X] expérience utilisateur
      - [X] Résumé des courants de design graphique du web (s'il existent)
      <!--
      - [ ] design moderne & informatique
      - [ ] design moderne & web -->
    * [X] Comment ?
      - [X] Compagny Info
      - [X] Web archives
      <!--
    * [ ] Google ?
      - [ ] culture du design d’expérience, culture google ?
    -->

* [ ] 05may1999
  * [ ] Le site de google
    - [ ] un moteur de recherche 
    - [ ] où est le design ?
    - [ ] The Name

* [ ] 08nov1999
  - [ ] changement de logo
  - [ ] complexification de la page (8 parties)
  - [ ] apparition du terme experience (3)
  - [ ] apparition du terme user (19)
  - [ ] apparition du terme design (5)
  - [ ] le design comme une approche, une mission
  - [ ] Jakob Nielsen dans la team

* [ ] 2001
  - [ ] complexification de la page (11 parties) <!-- le contenu principal précédant est séparé dans une page google today de la section in Depth, en profondeur -->
  - [ ] changement d'adresse <!-- (compagny -> corporate/today) -->
  - [ ] formule user experience (1)
  - [ ] apparition terme people (3)
  - [ ] terme experience (2)
  - [ ] terme user (20)
  - [ ] disparition du terme design

* [ ] 2008
  - [ ] complexification de la page (15 parties) <!-- la page s'appelle désormais Design principles dans la section Our Philosophy -->
  - [ ] changement d'adresse, apparition du terme UX <!-- (corporate/today -> corporate/ux) -->
  - [ ] 10 vérités deviennent 10 principes résumés en dix mots <!-- top 10, mojo, dieter rams, bible -->
  - [ ] formule user experience (8) <!-- titre -->
  - [ ] terme people (22) <!-- explosion -->
  - [ ] terme experience (10)  <!-- montée en force -->
  - [ ] terme user (33) <!-- montée stable -->
  - [ ] retour en force du terme design (27) <!-- associé au terme principe, nom de cette page dans l'index -->

* [ ] 2010
  - [ ] re-design même architecture
  - [ ] changement de logo
  - [ ] année d'embauche de Mathias Duarte
  - [ ] même texte
  …

* [ ]  2011
  - [ ] version française (tardive, a vérifier) google.fr/corporate/ux
  - [ ] pas de terme people car ça veux rien dire en français donc utilisateur
  - [ ] google traduction ?
    …

* [ ] 25jun2014
  - [ ] création d'une page design <!-- qui existait depuis des années mais vide -->
  - [ ] veille de l'officialisation du material design
  - [ ] formules sont déplacées 
  - [ ] Praise to be Duarte
    …

* [ ] 29jun2014
  - [ ] material
    …

* [ ] 2017
  - [ ] 
    …

 
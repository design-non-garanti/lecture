<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Google : d'un esprit d'entreprise à un courant de design</title>
	<?php 
	include 'styles/styles.php'
	?>
	<link rel="stylesheet" type="text/css" href="styles/materialize.css">
	<script type="text/javascript" src="scripts/jquery-3.3.1.min" ></script>
	<script type="text/javascript" src="scripts/materialize.js"></script>
</head>
<body>
	<header id="Timeline">
		<nav >
			<ul>
				<!--<li class="btn" href="">intro</li>-->
				<li class="btn" timeline=true url="../" ref="../index">
					Intro
				</li>
				<li class="btn" timeline=true url="google.com/company" ref="08may1999">
					a.1999
				</li>
				<li class="btn" timeline=true url="google.com/company" ref="05nov1999">
					b.1999
				</li>
				<li class="btn" timeline=true url="google.com/corporate/today" ref="13dec2001">
					2001
				</li>
				<li class="btn" timeline=true url="google.com/corporate/ux" ref="30apr2008">
					2008
				</li>
				<!--<li class="btn" timeline=true url="google.com/corporate/ux" ref="30oct2010">
					2010
				</li>-->
				<li class="btn" timeline=true url="google.com/about/company/tenthings<br>google.com/intl/en/about/company/ux<br>google.com/corporate/ux<br>google.fr/corporate/ux<br>google.com/about/company/philosophy<br>" ref="../index">
					2008-2016
				</li>
				<!--
				<li class="btn" timeline=true url="google.fr/corporate/ux" ref="29may2011">
					fr.2011
				</li>
				
				<li class="btn" timeline=true url="google.com/corporate/ux" ref="27apr2011.ux.corporate">
					ux.corp.2011
				</li>

				<li class="btn" timeline=true url="google.com/corporate/tenthings" ref="08may2011.ten.corporate">
					ten.corp.2011
				</li>
				<li class="btn" timeline=true url="google.com/intl/en/about/company/tenthings" ref="13feb2012.ten.compagny">
					ten.comp.2012
				</li>
				<li class="btn" timeline=true url="google.com/intl/en/about/company/ux" ref="13feb2012.ux.compagny">
					ux.comp.2012
				</li>
				<li class="btn" timeline=true url="google.com:80/about/company/philosophy" ref="19jun2012.ten.compagny">
					φ.comp.2012
				</li>-->
				<li class="btn" timeline=true url="google.com/design" ref="25jun2014">
					25 juin 2014
				</li>
				<li class="btn" timeline=true url="google.com/design/spec/
				material-design/introduction" ref="29jun2014">
				29 juin 2014
			</li>
			<!--
			<li class="btn" timeline=true url="google.com/about/company/philosophy" ref="31jan2016.10.compagny">
				2016
			</li>-->
			<li class="btn" timeline=true url="material.io" ref="29aug2017">
				2017
			</li>
		</ul>
	</nav>
</header>
<main id="container">
	<section id="enquete" class="indigo">
		<article class="card-panel article pink lighten-5" id="../index">
			<aside class="toast pinned" ></aside>
			<h1 class="card-panel" >Le material design, à la recherche du design perdu.</h1>
			<section class="card-panel lime">
				<h2 id="une-investigation">Une investigation</h2>
				<h3 id="mais-qui-a-tu-l-utilisateur-">Mais qui a tué l’utilisateur ?</h3>
				<p>Usagers, utilisateurs, consommateurs, acheteurs, cibles, masses, peuple, homme ordinaire, (Mr)tout le monde, ménagères, chacun et personne… Un terme qui permet de classer un ensemble d’individu est le reflet d’une stratégie élaborée par d’autres individus. Bien qu’universels – hérités de l’aube de la modernité¹ – ces termes ne témoignent pas des mêmes idéologies, des mêmes enjeux. <em>Turing Complete User</em>¹ d’Olia Lialina raconte une histoire de l’utilisateur, ou plutôt de comment le terme ’user’ est progressivement remplacé par le terme ’people’ dans l’industrie de l’ordinateur depuis la fin des années 90. Dans une industrie où les produits manufacturés sont camouflés au maximum, Olia Lialina rappelle qu’être appelé utilisateur manifeste la présence de l’ordinateur. La présence d’un système programmé que l’on est en train d’utiliser. Cette disparition résulte directement de l’influence de théoriciens du design proches des acteurs de la Silicon Valley. Un milieu dans lequel le terme utilisateur va jusqu’à devenir un tabou (’U’ word). Pour Donald Norman le terme “people” (les gens, le peuple, les citoyens) est préférable à “user“. “We design for people, we don’t design for users“. Pour lui est également préférable de substituer les différentes opérations proposées au destinataire d’une interface par ’expérience’. L’ordinateur n’est plus utilisé : il est ressenti, donné. Quand bien même l’UX dans le discours est défini comme un design centré sur l’usager, il tend à assimiler le contenu de l’ordinateur à ceux d’autres médias (le livre, la télévision, le cinéma, le spectacle)²(Voir Jakob Nielsen). Pour Olia Lialina cette substitution est un problème ; c’est une disparition. Cette disparition des machines implique la disparition de ceux qui les manipulent et vice-versa. Il n’y a rien à voir donc rien n’est fait, ni à faire. Le destinataire de l’industrie de l’ordinateur n’est plus considéré comme acteur de celle-ci. L’autorité sur ces objets n’est plus partagée entre ceux qui les inventent et ceux qui en ont l’usage.</p>
				<h3 id="courte-histoire-du-design-des-interfaces-dites-graphiques">Courte histoire du design des interfaces dites graphiques</h3>
				<p>l’UX est un design qui apparaît dans la Silicon Valley à la fin des années 80 suite aux premières interfaces dites “graphiques” et à l’élaboration de concepts tels que l’<em>information appliance</em> et l’ordinateur ubiquitaire. Le terme <em>user-experience</em>  – généralement traduit en français par expérience-utilisateur, nous le désignerons ici par sa traduction ou par son acronyme <em>UX</em> – est formulé par Don Norman à la suite de la parution de son livre <em>Design of every day things</em>. Concernant le design de ce qui s’affiche sur les écrans (<em>ses graphismes</em>³) l’UX flirt avec le <em>flat-design</em>¹ et le <em>skeuomorphisme</em>¹ qui sont les deux modes graphiques (esthétiques) dominant des interfaces graphiques. Ces deux courants s’incarnent et se suivent dans les années 2000. Bien que cela ne soit pas clairement de notoriété publique, ces deux courants sont largement conditionnés par les acteurs du design de quelques entreprises de l’industrie de l’ordinateur, liés à la sortie de générations d’appareils ou d’OS¹. Exemple : skeumorphisme avec l’Apple-store, le flat-design avec la sortie de windows 10, le Material Design avec la version Lollipop d’android. Ce qui pose un contexte de quasi monopole d’influence sur le graphisme à l’écran par deux ou trois entreprises depuis les années 70 au fil des mises jour systèmes et sorties d’appareils. Le skeuomorphisme est en quelque sorte l’apport des années Don Norman chez Apple¹ alors que la période Jonathan Ive amène le flat-design influencé par Dieter Rams, le style suisse international. Susan Kare est peut-être l’influence la plus forte avant l’émergence du flat et du skeuomorphisme. </p>
				<p>Il ne s’agit pas ici de composer l’historiographie exhaustive des différents acteurs qui ont influencé le design des interfaces dites graphiques mais plutôt de proposer un regard critique sur ces design qui n’en sont pas, du fait de l’aspect superficiel de leur champ de modification qui ne s’en tient qu’à l’apparence. En effet la structure des interfaces n’est pratiquement jamais modifiée ou prise en compte dans ces courants : ces structures sont le bureau, la page web, formulaires, buttons, dossiers etc.  Il est également important de rappeler que ce ne sont pas les seuls «courant de design» des interfaces graphiques. On peut penser au retro (nitrome.com), au brutalisme(<a href="http://brutalistwebsites.com/">http://brutalistwebsites.com/</a>), au flash(ref deep sites) ou à de nombreux genres hybrides ( plus ou moins éloignés du flat et du skeuomorphisme avec par exemple la mode des effets parralax fin 2000, ni skeuomorphique ni flat ). Puis en 2014 ces deux courant forment une sorte de synthèse avec le Material design, revendiqué de l’idéologie UX. Le material Design est un produit Google. Qu’une entreprise seule soit propriétaire d’un courant de design, c’est peut-être une première dans l’histoire du graphisme. Il s’agit bien d’un courant voire d’une discipline dans la mesure où le Material Design est largement réapproprié, accrédité, transmis etc. par des professionnels, des experts du design mais également par des amateurs, des disciples, des fidèles (exemple : la mise en page de ce site faite avec une une feuille de style css faite par un amateur du Material Design destinée à rendre Material une page web <a href="http://materializecss.com/">http://materializecss.com/</a>). Son influence est très visible : le design des grandes plates-formes web en est souvent inspiré (quand elles ne sont pas directement propriétés de google). Google l’a impulsé et continue régulièrement de le mettre à jour voire à en changer radicalement le style¹. Un produit pensé en Material Design est un produit Google. A l’image des produits Apple, c’est un design qui transforme un objet en marque. </p>
				<p>Ainsi le prétexte de cette investigation est le suivant : </p>
				<ol>
					<li><p>Olia Lialina associe mort de l’utilisateur et UX. </p>
					</li>
					<li><p>l’UX est un design, le Material Design fabriqué par Google est une forme récente et influente d’UX appliqués aux interfaces dites «graphiques». </p>
					</li>
					<li><p>Le Material Design serait donc un design qui tue l’utilisateur et qui émerge d’une entreprise. La question est la suivante : comment une entreprise peut-elle fabriquer un design, et comment ce processus est relié à la négation de l’usager ?</p>
					</li>
				</ol>
				<p>En parcourant les différents sites web de Google, on se rend rapidement compte que le terme design fait partie intégrante du discours/esprit de l’entreprise (cette remarque vaut pour d’autres sites qui ne sont pas ici traités).</p>
				<h3 id="le-discours-d-entreprise">Le discours d’entreprise</h3>
				<p>Sur le site internet d’un groupe ou d’une entreprise, consacrer un ensemble de pages à décrire sa structure et ses enjeux est une tradition. Ces pages sont généralement indexés sous le terme de “compagny”, “corporate”, (la société, l’entreprise) “compagny info”, “about” “about us” (à propos, qui sommes nous). Au prétexte, à première vue purement factuel, de décrire et de renseigner se dégage l’occasion d’assurer l’authenticité d’un discours soit d’une stratégie.</p>
				<h3 id="comment-se-servir-de-ce-contenu">Comment se servir de ce contenu</h3>
				<p>En raison de sa duplicité, à l’échelle de sa formulation, le contenu même de ces pages n’est pas très scrupuleux et il serait hasardeux de s’en servir pour argumenter sur les réelles ambitions de l’entité qu’il concerne ou de son auteur. Une observation contextualisée plus largement, lorsque ce contenu évolue dans le temps, varie, s’adapte, est corrigé dans la durée, fait apparaître à la fois ce qu’il a de régulier ou d’instable. Il est alors révélateur en ce qu’il fait trace d’une trajectoire, d’une direction : une trame plus profonde ; la structure d’un système, d’une stratégie. La méthode d’investigation a été la suivante : surfer via les archives de Waybackmachine (<a href="https://archive.org/web/">https://archive.org/web/</a>) sur les pages indexées “corporate” sur le site de Google, pour voir quand et comment apparaît le terme design dans le discours de l’entreprise. Comment le Material Design à t-il été préparé, de quelle pensée provient-il ?</p>
			</section>
		</article>





		<article class="card-panel article pink lighten-5" id="08may1999">
			<h1 class="card-panel">* [ ] 05may1999</h1>
			<aside class="toast pinned" ></aside>
			<section class="card-panel lime">
				<h2 id="05may1999">05may1999</h2>
				<h3 id="le-site-de-google">Le site de Google</h3>
				<p>Google.com existe depuis 1998. La page “compagny” apparaît sur le site en février 1999. Avant novembre 1999 Google est encore en version beta et s’appelle “Google! Inc.”.</p>
				<h3 id="un-moteur-de-recherche">un moteur de recherche</h3>
				<p>A cette époque Google est uniquement un moteur de recherche, ce moteur est présenté comme un projet dans lequel on peut investir :</p>
				<p>Le terme design est absent de la page mais on apprend que les co-fondateurs Sergey Brin et Larry Page sont rattachés au Stanford University Computer Science Department, attestant de la scientificité de la démarche de l’entreprise courte histoire de la sillicon . Il est précisé que l’entreprise émerge suite à trois ans de recherche en Data-mining c’est-à-dire l’exploration de quantités massives de données pour en extraire des motifs (patterns). Cette démarche rationalisante est déjà proche du discours lequel ?  d’un design qui cherche à maîtriser ou à conditionner un environnement.</p>
				<p>La première chose que Google a designé c’est peut-être leur nom, il nous est dévoilé que le terme “Google” est une «meilleure» prononciation du nombre googol (10^100, un nombre gigantesque). Il est suggéré que ce nom a été choisi car il est la métaphore de l’objectif des créateurs de Google : rendre accessible à tous d’énormes quantités d’information (énormes comme un googol). Il est ajouté que Google sonne cool (meilleur et cool ?) et n’a que six lettres (peu de lettres). Le mot Google est sûrement «meilleur» que Googol car sa forme est proche de celle que l’on attend d’un mot (anglais en l’occurence), “google” rappelle “example”, “doodle”, “poodle”, “feeble”, “bubble”… il apparaît familier. Googol n’est pas cool car il demande un effort : étranger, il ne livre pas dans sa graphie le mode d’emploi de sa prononciation qui devient plus lente. À l’inverse Google est cool car il est exotique, inhabituel il reste néanmoins très familier. Googol devient d’autant plus familier que sa reproduction en Google vient redoubler ou «masquer» un sens trop complexe ou trop abstrait pour être «facile d’accès» (comme ces énormes quantités de données).</p>
			</section>

		</article>
		<article class="card-panel article pink lighten-5" id="05nov1999">
			<aside class="toast pinned" ></aside>
			<h1 class="card-panel" >* [ ] 08nov1999</h1>
			<section class="card-panel lime">
				<h2 id="08nov1999">08nov1999</h2>
				<h3 id="update">Update</h3>
				<h4 id="changement-de-logo">changement de logo</h4>
				<p>Google ne s’appelle plus « Google! inc. » mais « Google », outre la formulation, Google possède maintenant un nouveau logo, toujours serif, toujours bleu rouge jaune bleu vert rouge, mais beaucoup moins gras, aux serifs plus pointus ( mécane avant ), le ’e’ est incliné.</p>
				<p> ####complexification de la page (8 parties)<br>La page compagny info possède désormais un contenu bien plus volumineux (environ 10000 caractères pour 2200 précédemment ) et sa structure se complexifie et contient désormais huit parties distinctes.<br>Ce qui était autrefois divisé en the compagny, the name et contact est désormais réparti entre Our Mission, Our Company, Google’s Approach to Searching the Internet, Google Search Services, Special Searches, Our Business, About Google Inc., The Meaning of Google.<br>Ce qui étais autrefois dans the compagny est désormais dispatché entre Our compagny et About Google Inc., le contenu de The name est désormais dans the Meaning of Googl.</p>
				<h4 id="apparition-du-terme-design-5-">apparition du terme design (5)</h4>
				<p>Le terme design apparaît dans Googles’s Approach to Searching the Internet.<br>Une partie largement consacré à la conception alternant conception au sens choix de fabrication et conception au sens de point de vue.<br>Sa première occurrence est associée au terme ’elegant’ et est dissociée du terme ’innovative’ qui est associé au terme ’search technology’.<br>Dans “An Elegant, Easy-to-Use Interface” le design concerne l’interface de Google. Le design dans cette interface est qualifié d’“épuré“, “clair” et son rôle est de faciliter l’entrée de requêtes pour les utilisateurs.donc?<br>Le terme design apparaît également dans Googles Search Services, en tant que conception (Google SiteSearch est conçu pour rechercher de l’information contenu au sein d’un site spécifique).<br>Le terme design apparaît également dans la section Our Business, de nouveau pour rappeler la fonction de Google SiteSearch puis associé au terme “graphiquement” concernant la publicité de Google qui serait donc graphiquement designée pour améliorer l’expérience de recherche de l’utilisateur. Puis est associé à au terme publicité les mêmes adjectifs que le design cad épuré, clair.donc ?</p>
				<h4 id="apparition-du-terme-experience-3-">apparition du terme experience (3)</h4>
				<p>Le terme expérience-utilisateur (user-experience) n’existe pas encore dans le compagny-info de google, néanmoins on peux trouver trois occurrences du terme expérience seuls.<br>Dans Our Compagny Google expliquent qu’ils sont à la recherche de la meilleure expérience de recherche sur le net.<br>On retrouve le terme dans la section Our Buisiness, également associé au terme “search“, “search-experience” semble être un mot-valise pour désigner l’acte d’être en train de rechercher. On pourrait penser à un voyage touristique ou un spectacle.</p>
				<h4 id="apparition-du-terme-user-19-">apparition du terme user (19)</h4>
				<p>On retrouve également le user seul en grande quantité, pour l’instant aucune trace du terme people.<br>Le terme user est employé sur toute la page, présent dans pratiquement toutes les sections du texte. Il est systématique associé au terme aidé offre, facile ou facilitation, Google helps its users, designed to make it easy for users, Google helps users, Google offres its advanced search technology to individual users, Google offers special seaches for users.<br>Il est aussi également très souvent associé au terme désir, besoin, effort, devoir :  users must rely on search technology, as users strive to find information, users can easily tell if the corresponding web pages will satisfy their need, more relevant to user, users who want to find information, users who want to search the web, more likely to be relevant to a users needs.<br>Google ne se contente pas de satisfaire les besoins des utilisateurs, il les définit.</p>
				<h4 id="le-design-comme-une-approche-une-mission">le design comme une approche, une mission</h4>
				<p>Sur google.com google parle d’user interface dans sa page compagny info.</p>
				<p>5 novembre 1999, google emploie le terme “design” dans la sous-rubrique “Google’s Approach to Searching the Internet” en même temps que celui de “mission” (première sous-partie de l’article “Our Mission”). La mission de google est d’organiser l’information mondiale, la rendre universellement accessible et utile. L’objectif est de délivrer la meilleure expérience de recherche sur internet “World Wide Web”. Les avancées innovantes en matière de technologie de recherche doivent aider les utilisateurs “users” à trouver l’information qu’ils recherchent avec une pertinence et une facilité sans précédents. La technologie de recherche est qualifiée d’innovante, le design d’interface-utilisateur lui est dit élégant. L’interface est designée pour être élégante, facile à l’utilisation, propre, épurée pour mieux inscrire la recherche et en interpréter les résultats. Les résultats sont délivrés par ordre d’importance et de pertinence. La pertinence est calculée par la quantité de liens interdépendants sur une page, un lien allant de la page A à la page B est un “vote”. L’honnêteté est pertinence, l’honnêteté est importante, c’est un élément quantifiable, la démocratie est objective car automatique. La démocratie est un élement de conception, de design pragmatique. Google est moral car il est démocratique et la démocratie est la morale. La morale est quantifiable.</p>
				<h3 id="jakob-nielsen-dans-la-team">Jakob Nielsen dans la team</h3>
				<p>Sur le site de Google 1999 on peux également apprendre la présence de Jakob Nieslen au sein de l’équipe Google en tant que Conseiller technique.<br>Spécialiste de la facilitation du web (alors comment qu’il fait ? Il déplace les anciens médiums dans le nouveau).</p>
				<p>Il est l’avocat des utilisateurs pour google. Jakob Nielsen nous intéresse pour plusieurs raisons. 1 Jakob Nielsen est le maître en la matière de concevoir des règles fondamentales de design pour les sites web.exemple Dans les années 90 il tient un blog intitulé alertbox dans lequel il décrit ce qui va et ce qui ne va pas dans le web, ce dont le web à besoin.exemple Son site est composés d’articles traitant de la manière dont les utilisateurs font ou ne font pas certaines choses et la manière dont il faut exploiter ces “faits“.<br>exempleJakob Nieslen propose également des articles particuliers intitulés “Mes prédictions” dans lesquels il annonce ce qu’il va se passer sur le web l’année suivante mais également ce qui est advenu de ses prédictions précédentes.exemple<br>Mais ce qui nous intéresse tout particulièrement avec Jakob Nielsen est qu’il est co-fondateur avec Don Norman du N&amp;N group (Norman and Nielsen group, Evidence-Based User Experience Research, Training, and Consulting) qui sont largement aux origines du concept d’experience utilisateur, fondé la même année que le site de google apparaît (1998). Le fait qu’il travail chez google n’est pas anodin, outre sa propre influence, il fait le lien entre la pensée de Don Norman et celle de google c’est à dire entre la pensée utilisateur expérience et celle de Google.<br>La pensée de Don Norman peut peut-être nous éclairer sur certains points abordés par google.</p>
				<h4 id="pens-e-ux-et-google-design-corporate">Pensée Ux et Google/Design corporate</h4>
				<h5 id="penser-le-web-comme-un-outil-marketing">Penser le web comme un outil marketing</h5>
				<h5 id="le-marketing-au-service-de-l-homme">Le marketing au service de l’homme</h5>
				<p> si le marketing existe pour servir l’homme, nul doute que la publicité améliore l’usage d’un objet</p>
				<p>Dans Design of everyday things ¹ Donald Norman déclare en introduction que le commerce existe pour servir “the people” un terme exclusivement pluriel désignant tout à la fois l’homme, l’individu, le citoyen, le peuple. Cette vision humaniste, voir idyllique de l’activité commerçante est-elle naïve ou cache t-elle une attitude mystificatrice, positiviste dans un monde où commerce rime avec pouvoir ou simplement est-ce un bon moyen de promouvoir sa propre recherche dans un pays où le secteur privé assure la subsistance des universités ? Je ne dis pas que le commerce ou n’importe quels activités, secteurs ou disciplines soient bons ou mauvais par «nature», comme si le monde pouvait s’offrir avec une telle simplicité. Mais affirmer que le commerce, dans toute son histoire, ses origines, ses ramifications et dérivés existe pour servir le peuple me parait être un fantasme, un mensonge voir même une réduction de ce que sont ou peuvent être l’ensemble des activités commerciale. Donald Norman affirme cela comme un état de fait sans plus d’argumentation sinon que ce point (servir l’homme) est un point qui est compris et pratiqué par les commerces ayant réussi (serait-ce prophétique de la carrière de google ?). Mais quelle est la différence entre être au service et se servir de ? Voire existe-t-il une différence ? Goethe du temps des Lumières enjoint l’homme à être «noble, secourable et bon»². Cette forme inaccomplie du potentiel moral de l’homme est déplaçée au statut de fonction acquise du capital. Servir, se mettre au service de, être secourable, cela devient dans l’affirmation de Donald Norman la «nature» du “Business” ainsi que de l’ensemble de ses acteurs (les designers) qui jouissent ainsi exclusivement de ce pouvoir, un pouvoir disposé à être librement distribué, donné au reste des individus “people” dans la constante détresse d’en être dépourvu. Le comble dans la pensée de Donald Norman c’est qu’à force de décrire les utilisateurs comme des victimes de l’objet, c’est à dire des victimes du designer (voir The Invisible Computer) il se fait le représentant d’un système, d’un mouvement de design qui effectivement victimise son destinataire, qui prend toute la dimension de «l’usager comme esclave post-moderne par excellence» que Jean Baudrillard décrit dans les premiers chapitres de L’Échange symbolique et la mort ¹. Que le “business” existe pour servir la société (people) cela rappelle la «définition la plus rigoureuse» du capital comme «forme du rapport social»¹. Avec la différence que la forme du capital, l’organisation sociale actuelle, est déplacée par Donald Norman au statut de fonction. Affirmer que servir est la fonction de l’exploitation économique et non sa forme revient à masquer la violence de l’exercice du don sous une forme stratégique. Le don place son destinataire dans l’obligation symbolique d’une contre-partie, d’une dette. Dans le cadre du design d’interface c’est une dette d’usage qui est constituée. Les designers évoqués par Donald Norman (c’est à dire les acteurs du “business”, capital) ont ainsi toute légitimité à jouir de nos usages, à transformer nos interactions avec la machine «secourable» en bases de données (don-nées). «People» est la matière première des nouvelles industries. Que Donald Norman décrive le fait de servir l’individu comme condition d’existence de l’activité économique et par extension comme condition d’existence à toute entreprise (leur réussite), n’est pas un mensonge mais un rappel à la prise du pouvoir par l’exercice unilateral du don. C’est à dire la valeur du monopole de la dette de ce qui est rendu en échange du don ; nos données personnelles. Designer pour servir l’individu, pour donner revient à designer pour endetter c’est à dire dominer. Que le capital organise les rapports entre individus ne signifie pas qu’il existe pour cela. J’aimerais agrémenter ces parties avec des exemples d’interfaces suivant ce dogme et analyser le déroulé de leur utilisation (contrôle de drônes par la «pensée», casque de réalité virtuelle, scroll sur écran tactile ect…)<br> à revoir </p>
				<h4 id="je-pense-donc-je-quitte">Je pense donc je quitte</h4>
				<p>La renommée de Jakob Nielsen est basé sur plusieurs «découvertes» qui s’avèrent être les fondamentaux de l’expérience utilisateur.<br>Les ne sont pas patients, les gens ne veulent pas lire, il ne faut pas les écouter. à préciser<br>C’est sûrement lui qui fait que katerine Hayles se tire les cheveux dans How we read: Close, Hyper, Machine“(<a href="http://nkhayles.com/how_we_read.html">http://nkhayles.com/how_we_read.html</a>) Katerine explique l’impact négatif sur la lecture que provoque le ramassage des informations en haut ou à gauche de l’écran tandis que Jakob Nielsen en fait une loi de design basée sur l’évidence. à préciser<br>Jakob Nielsen est une des références principales d’un livre intitulé “Don’t make me think” littéralement “ne me faites pas réfléchir” qui est un des livres de références en matière de web design<br>M. Frederic Marand<br>5,0 sur 5 étoilesLe seul livre sur le Web que je relis régulièrement<br>8 mars 2017<br>Format: Broché<br>Découvert il y a des années lors de sa sortie, il a été un choc parce qu’il se situait à la fois hors de la technologie et de la théorie pour se focaliser sur la perception par les utilisateurs. Depuis, je le relis régulièrement quand je m’interroge sur un point ou un autre en matière d’UX : c’est le fondamental, avant de plonger sur les textes plus complexes... et je l’ai fait acheter plusieurs fois à des clients ou des collègues.</p>
				<p>Comme d’autres l’ont dit dans les commentaires, la technologie a changé, et les codes du design aussi, mais le texte est presque intemporel, alors que les collections de Jakob Nielsen sont presque illisibles aujourd’hui.</p>
				<p>Incontournable.<br>Commentaire| Une personne a trouvé cela utile. Ce commentaire vous a-t-il été utile ?<br>Oui<br>Non<br>Signaler un abus<br><a href="https://www.amazon.fr/Dont-Make-Me-Think-Usability/dp/0321344758">https://www.amazon.fr/Dont-Make-Me-Think-Usability/dp/0321344758</a></p>
				<p>Ce livre explique en 200 pages illustrés toutes les méthodes pour éviter à l’utilisateur d’avoir à réfléchir sur une page car c’est le principe de base de l’usabilité comme le dit si bien Nielsen : First Rule of Usability? Don’t Listen to Users. C’est à dire que L’ux est centrée sur l’user mais qu’il ne faut surtout pas l’écouter car il ne sais pas ce qu’il veux, mais nous designers savons bien pour lui. User centered = centré sur la domination de l’user.<br>« Ce que nous essayons de faire c’est de construire une humanité augmentée, nous construisons des machines pour aider les individus à mieux faire les choses qu’ils n’arrivent pas à bien faire eux-mêmes.»<br>Eric Schmidt, PDG de Google<br>C’est projet aporétique, effectivement on peux toujours enlever des choses, mais il y en aura toujours ( ça défi les lois de la physique) c’est le rêve quoi, le comble du marketing (technologie qui mettent à ses pieds le monde et ses contraintes)</p>
				<h4 id="la-t-l-du-web">La télé du web</h4>
				<p>Exemple sur ma théorie du déplacement d’un ancien médium dans un nouveau pour faciliter l’utilisation ( c’est à dire faire disparaître toute différence ou inattendu, cad toute perte de contrôle ) : la télé du web de Nielsen :<br><a href="https://web.archive.org/web/19970218113557/http://www.useit.com:80/alertbox/9702a.html">https://web.archive.org/web/19970218113557/http://www.useit.com:80/alertbox/9702a.html</a><br> chercher des exemples<br>Fascination pour la webtv et état de youtube (tf1) youtube étant google, coïncidence ?</p>
			</section>
  <!--
			<h1 class="card-panel" >Novembre 1999</h1>
			<h2 class="card-panel" >google.com/company</h2>
			<section class="card-panel lime">
				<p class="card-panel" >
					5 novembre 1999, google emploie le terme “design” dans la sous-rubrique “Google's Approach to Searching the Internet” en même temps que celui de “mission” (première sous-partie de l'article 
					“Our Mission”).

					La mission de google est d'organiser l'information mondiale, la rendre universellement accessible et utile.

					L'objectif est de délivrer la meilleure expérience de recherche sur internet “World Wide Web”. Les avancées innovantes en matière de technologie de recherche doivent aider les utilisateurs “users” à trouver l'information qu'ils recherchent avec une pertinence et une facilité sans précédents.

					La technologie de recherche est qualifiée d'innovante, le design d'interface-utilisateur lui est dit élégant.

					L'interface est designée pour être élégante, facile à l'utilisation, propre, épurée pour mieux inscrire la recherche et en interpréter les résultats.

					Les résultats sont délivrés par ordre d'importance et de pertinence.

					La pertinence est calculée par la quantité de liens interdépendants sur une page, un lien allant de la page A à la page B est un “vote”.

					L'honnêteté est pertinence, l'honnêteté est importante, c'est un élément quantifiable, la démocratie est objective car automatique.

					La démocratie est un élement de conception, de design pragmatique.

					Google est moral car il est démocratique et la démocratie est la morale.
					La morale est quantifiable.
				</p>
			-->
		</article>
		<article class="card-panel article pink lighten-5" id="13dec2001">
			<aside class="toast pinned" ></aside>
			<h1 class="card-panel" >* [ ] 2001</h1>
			<section class="card-panel lime">
				<h2 id="2001">2001</h2>
				<h4 id="complexification-de-la-page-11-parties-le-contenu-principal-pr-c-dant-est-s-par-dans-une-page-google-today-de-la-section-in-depth-en-profondeur">complexification de la page (11 parties) le contenu principal précédant est séparé dans une page google today de la section in Depth, en profondeur</h4>
				<p>Tandis que le contenu about continue à se complexifier, (12000 caractères, 3 grandes parties et 11 sous-parties) chaque sous-partie possède une adresse propre google.com/corporate/[sous-partie].</p>
				<h4 id="changement-d-adresse-compagny-corporate-today-">changement d’adresse  (compagny -&gt; corporate/today)</h4>
				<p>Nous allons nous cibler particulièrement la page Google today google.com/corporate/today qui reprend le contenu Google’s Approach to Searching the Internet de la pge de 1999 particulièrement attaché aux problématique de conception de l’entreprise.<br>Tandis que l’url google.com/compagny va continuer d’exister parallèlement.<br>Ce qui était formulé au travers de cinq points An Elegant, Easy-to-Use Interface: , Sophisticated Text-Matching, Patent-Pending PageRankTM Technology,Results are delivered in order of importance and relevance:  et Integrity in Search Results revêt désormais la forme d’une liste de 10 items<br>1) Focus on the user and all else will follow.<br>2) It’s best to do one thing really, really well.<br>3) Fast is better than slow.<br>4) Democracy on the web works.<br>5) You don’t need to be at your desk to need an answer.<br>6) You can make money without doing evil.<br>7) There’s always more information out there.<br>8) The need for information crosses all borders.<br>9) You can be serious without a suit.<br>10) Great just isn’t good enough.<br>intitulés les 10 vérités que google à découvert. N’allait n’est-ce pas sans rappeler les 10 commandements.<br>Google insiste sur son importance dans la définition des besoins, des vérités.<br>Il ne se contente pas de satisfaire les besoins il les définit (si différence il y a).</p>
				<h4 id="disparition-du-terme-design">disparition du terme design</h4>
				<p>Chose assez surprenante, le terme design disparaît du discours, peut-être n’a t’il pas sa place dans une liste de vérités ou bien est-il trop enclin à faire réfléchir l’utilisateur.</p>
				<h4 id="terme-experience-2-">terme experience (2)</h4>
				<p>on retrouve toujours le terme expérience (une occurrence en moins),<br>toujours best : providing the best user experience possible, rapide et harmonieuse pour des millions d’utilsateurs : a fast and seamless experience for millions of users. Uniformiser la naviguation sur le web est donc l’objectif ?</p>
				<h4 id="terme-user-20-">terme user (20)</h4>
				<p>user toujours similaire, l’utilisateur à toujours besoin d’aide en 2001 : help users , users seeking answers. Mais ce n’est plus rendu facile pour lui (easy une seule occurence) mais maintenant Google connait parfaitement les besoins de l’user car lis dans l’esprit de l’user : One engineer saw a need and created a spell checker that seems to read a user’s mind.<br>L’utilisateur est la préoccupation principale de google (le client est roi) : benefit to the users, one satisfied user to another,  always placing the interests of the user first, Google, wireless users (le smartphone entre en jeu), users living outside the United States (google semble étendre sa cible), user’s preference,  content available to users regardless of their native tongue, offer users in even the most far flung corners of the globe. ( on va aller chercher des client PARTOUT, sans fil au bout du monde), This process has greatly improved both the variety and quality of service we’re able to offer users in even the most far flung corners of the globe, Google puts users first. Cette fois le nombre d’users est insistant, une horde, une armée : a user base in the millions, millions of users.<br>L’utilisateur est rapide grâce à google : Google may be the only company in the world whose stated goal is to have users leave its website as quickly as possible. </p>
				<h4 id="apparition-terme-people-3-">apparition terme people (3)</h4>
				<p>Le terme people apparait pour la première fois avec le même rôle qu’un user, tout comme l’user il a besoin d’information (autrefois reservée aux users) : people want information to come to them, mais contrairement à l’user ils ne sont pas dans le besoin, ils sont une force autonome ce ne sont pas les users de drogue de twitter  Jack’s<br>#<br>Let’s reconsider our “users”<br>us·er<br>/ˈyo͞ozər/</p>
				<p>Noun</p>
				<ol>
					<li>A person who uses or operates something, esp. a computer or other machine.</li>
					<li>A person who takes illegal drugs; a drug user.</li>
				</ol>
				<p>Synonyms<br>consumer</p>
				<p>During a Square Board meeting, our newest Director Howard Schultz, pulled me aside and asked a simple question.</p>
				<p>“Why do you all call your customers ‘users’?”</p>
				<p>“I don’t know. We’ve always called them that.” Jack Dorsey, executive chairman of Twitter:<br>Let’s reconsider our “users” <a href="http://jacks.tumblr.com/post/33785796042/lets-reconsider-our-users">http://jacks.tumblr.com/post/33785796042/lets-reconsider-our-users</a><br>: This highly communicative environment fosters a productivity and camaraderie fueled by the realization that millions of people rely on Google results. Give the proper tools to a group of people who like to make a difference, and they will.</p>
				<h4 id="formule-user-experience-1-">formule user experience (1)</h4>
				<p>La formule user experience apparait aussi pour la première fois (tiens, tiens) et elle est une reformulation très basique du “the best search experience on the World Wide Web.” de 99 en “the best user experience possible” le terme contiendrai donc les enjeux du web à lui seul.</p>
				<p>La page compagny info de google devient “google today” et google parle de “user experience” et affirme “focus on the user and all else will follow”.</p>
				<p>10 things Google has found to be true</p>
				<p>“10 Things Google has found to be true” est un paragraphe sur la page “google today” sur le site google.com apparue en décembre 2001 sur l’adresse google.com/corporate/today.</p>
				<p>En décembre 2001, la section “corporate information” est divisée en trois parties : “Profile”, “In Depth”, “The Difference”.</p>
				<p>“Profile” contient les informations élémentaires, une section concerne le terme “google” et son origine, une section esquisse l’entreprise “en un clin d’œil” par le biais de statistiques ; nombre de requêtes quotidiennes : plus de 150 millions, nombres de pages web indexées : plus de 2 billions […] Google figure parmi les dix sites les plus populaires sur internet etc. Cette page contient également les noms et les fonctions des acteurs principaux de l’entreprise sous forme de crédits (on y retrouve notamment Jakob Nielsen annoncé comme représentant du Nielsen Norman Group en tant que conseillé technique). Une dernière contient les adresses des différents bureaux de l’entreprise ainsi que des conseils pour y accéder.</p>
				<p>À partir du 13 décembre 2001 la section compagny info du site de google se complexifie, la section “Google’s Approach to Searching the Internet” s’appelle maintenant “Google Today” et le discours n’est plus tout à fait le même. Les 5 points décrivant les choix de google concernant les méthodes d’indexation du web – qu’il s’agisse des choix de design d’interfaces ou de la description de leur système de notation démocratique de sites – sont remplacés par «10 vérités», google ne se contente plus de décrire ses méthodes il en propose une, il propose des lois. En parallèle le terme “user experience” apparait pour la première ce qui dévoile la proximité des idées entre google et Donald Norman.</p>
			</section>
		<!--
		<h1 class="card-panel" >2001</h1>
		<h2 class="card-panel">google.com/corporate/today</h2>
		<section class="card-panel lime">
			<p class="card-panel" >
				À partir du 13 décembre 2001 la section compagny info du site de google se complexifie, la section “Google's Approach to Searching the Internet” s'appelle maintenant “Google Today” et le discours n'est plus tout à fait le même. Les 5 points décrivant les choix de google concernant les méthodes d'indexation du web – qu'il s'agisse des choix de design d'interfaces ou de la description de leur système de notation démocratique de sites – sont remplacés par «10 vérités», google ne se contente plus de décrire ses méthodes il en propose une, il propose des lois. En parallèle le terme “user experience” apparait pour la première ce qui dévoile la proximité des idées entre google et Donald Norman.

				10 things Google has found to be true

				“10 Things Google has found to be true” est un paragraphe sur la page “google today” sur le site google.com apparue en décembre 2001 sur l'adresse google.com/corporate/today.

				En décembre 2001, la section “corporate information” est divisée en trois parties : “Profile”, “In Depth”, “The Difference”.

				“Profile” contient les informations élémentaires, une section concerne le terme “google” et son origine, une section esquisse l'entreprise “en un clin d’œil” par le biais de statistiques ; nombre de requêtes quotidiennes : plus de 150 millions, nombres de pages web indexées : plus de 2 billions […] Google figure parmi les dix sites les plus populaires sur internet etc. Cette page contient également les noms et les fonctions des acteurs principaux de l'entreprise sous forme de crédits (on y retrouve notamment Jakob Nielsen annoncé comme représentant du Nielsen Norman Group en tant que conseillé technique). Une dernière contient les adresses des différents bureaux de l'entreprise ainsi que des conseils pour y accéder.
			</p>
		</section>
	-->
</article>
<article class="card-panel article pink lighten-5" id="30apr2008">
	<aside class="toast pinned" ></aside>
	<h1 class="card-panel" >* [ ] 2008</h1>
	<section class="card-panel lime">
		<h2 id="2008">2008</h2>
		<h3 id="update">Update</h3>
		<h4 id="complexification-de-la-section-corporate-15-parties-la-page-s-appelle-d-sormais-design-principles-dans-la-section-our-philosophy">complexification de la section corporate (15 parties)  la page s’appelle désormais Design principles dans la section Our Philosophy</h4>
		<p>La structure de la section corporate continue à se complexifier, 3 parties et 15 sous-parties et les noms des parties et sous parties sont de nouveau remaniées (tous) : Profile devient Corporate Overview (Compagny, Features, Technology, Business, Culture, Green Energy(green washing ?)), In depth devient At a Glance (l’inverse ?) avec : Quick Profile, Address (autrefois dans Profile), Management, Miles stones. The Difference devient Our Philosophy ( se démarquer, cad faire de la com de la pub = notre philosophie ?) avec : Ten Things, Software principles, Design principles, No pop-ups, Security.<br>Les 10 vérités deviennent 10 choses (things) tandis que sa structure/contenu est copiée modifiée en Design principles, section que l’on va désormais suivre.<br>Le contenu précédent est rebaptisé 10 things et existe aussi dans Our philosophy.<br>Le Design à donc un espace privilégier désormais, il s’agit également d’une philosophie mais une philosophie de la vérité (autrefois 10 truths).<br>Le contenu est plus court (10000)</p>
		<h4 id="changement-d-adresse-apparition-du-terme-ux-corporate-today-corporate-ux-">changement d’adresse, apparition du terme UX  (corporate/today -&gt; corporate/ux)</h4>
		<p>Le design est donc associé avec une philosophie de la vérité, mais pas seulement car l’url est désormais google.com/corporate/ux, Design et Ux se confondent. Est-donc associé à ux les valeurs que pour le design, philosophie publicitaire de la vérité.</p>
		<h4 id="10-v-rit-s-deviennent-10-principes-r-sum-s-en-dix-mots-top-10-mojo-dieter-rams-bible">10 vérités deviennent 10 principes résumés en dix mots  top 10, mojo, dieter rams, bible</h4>
		<p>Les dix vérités deviennent 10 principes de design applicables, “Ten principles that contribute to a Googley user experience” c’est une invitation à quiconque à produire des objets à l’image de google. Car l’application à un projet, un objet de ces 10 vérités n’aura pas seulement pour effet de produire un design dont les utilisateurs ont besoin, cela produira un objet Google, une sorte de brevet sur la vérité ?</p>
		<h4 id="retour-en-force-du-terme-design-27-associ-au-terme-principe-nom-de-cette-page-dans-l-index">retour en force du terme design (27)  associé au terme principe, nom de cette page dans l’index</h4>
		<p> Le design comme principes de vérités ? de la vérité ?<br>Cette fois Le terme design est omniprésent. Dans tout les principes. Il s’agit donc bien de principes de design.<br>Un petit paragraphe Our aspirations (notre idéal ?) associe 10 termes au terme design, chaque terme correspondant à un principe, à une vérité découverte par Google. The Google User Experience team aims to create designs that are useful, fast, simple, engaging, innovative, universal, profitable, beautiful, trustworthy, and personable.Le design de la vérité est : utile, rapide, simple, accessible, innovant, universel, rentable, élégant, sécuritaire et convivial.<br>Dans le premier paragraphe Utilité (Focus on people – their lives, their work, their dreams), le well-designed est useful in daily life, utile dans la vie de tout les jours. Plus utile est donc ce qui est utilisé le plus de fois, avec le plus de régularité.<br>Dans rapidité (Every millisecond counts.), le terme design n’apparait pas.<br>Dans Simplicité (simplicity is powerful), le design est associé aux termes: good design is : ease of use, visual appeal, accessibility, les uns validant les autres et inversement. Ce qui est puissant est simple et inversement (Less is more) moins c’est plus, plus c’est moins (la loi de l’équivalence ?) c’est à dire que l’exercice de la force produit de la simplicité (appauvrissement des choix, des alternatives) tandis que l’appauvrissement des choix, des alternatives définit l’exercice d’une force.<br>Dans accessibilité (Engage beginners and attract experts.). Le design est destiné à tous (Designing for many people) permis, offert à tous (A well-designed Google product lets new users jump in, offers help when necessary) et est de nouveau associé à la force et la simplicité : designs appear quite simple on the surface but include powerful features. Nous rappelant encore l’équivalence entre moins et plus. L’exercice de la force est donc destiné à tous, la simplification (la raison ?) est destiné à elle-même (toujours plus) dialectique de la raison.</p>
		<p>“Cette permission donnée au consommateur…“, il faut permettre aux hommes d’être des enfants sans en avoir honte. “Libre d’être soi-même” signifie en clair : libre de projeter ses désirs dans des biens de production. […] Cette “philosophie” de la vente ne s’embarrasse guère du paradoxe : elle se réclame d’un but rationnel (éclairer les gens sur ce qu’ils veulent) et de méthodes scientifiques, afin de promouvoir chez l’homme un comportement irrationnel (accepter de n’être qu’un être complexe de pulsions immédiates et se satisfaire de leur satisfaction).[…] L’irrationalité toujours plus “libre” des pulsions à la base ira de pair avec un contrôle toujours plus strict au sommet” j b le systeme des objets 1968 p260</p>
		<p>Dans innovation (Dare to innovate) le terme design est associé à celui de confiance : Design consistency builds a trusted foundation. Le terme design est également associé à des antinomies, il peut être ho-hum (disgracieux, déplaisant) tout comme il peut être Delightfull (attirant, charmant, plaisant), l’imagination (innovation) permettant de faire la différence.<br>Il est également associé à une prise de risque : risk-taking design, mais il est encore rappelé à son assujettissement aux besoins des utilisateurs (besoin que google definit, donc assujetti à google)<br>Google encourages innovative, risk-taking designs whenever they serve the needs of users.<br>Est encourager l’initiative qui va dans le sens de la raison, vérité (définit, découvert par google).<br>Tout est bon à condition que cela répande l’exercice de la force (la force de la vérité, celle de google) dans son équivalence avec la faiblesse (les usages, les utilisateurs), que le même schéma soit reproduit sans cesse, inceste ?<br>Dans universel (Design for the world), design est associé à relevant, available (pertinent, disponible), à exactitude (to design the right products), ce qui est évoqué plus haut est repdroduit, re-dit<br>La mise en scène de la force et de la faiblesse destiné à tous, destiné à sa reproduction.<br>Dans rentable (Plan for today’s and tomorrow’s business.) Le design est de nouvelle fois rappelé à son assujettissement aux besoins définit par google mais également au profit de google (sa reproduction) “designers work with product teams to ensure that business considerations integrate seamlessly with the goals of users.” Il est encadré, surveillé par la “product team” équipe commerciale. Les mêmes principes sont à nouveau reproduits avec la différence que les besoins des utilisateurs définit par google sont associés aux besoins commerciaux de profit de reproduction. Le terme design est directement associé à profit et à user “If a profitable design doesn’t please users“. Un double rôle ou un double contrôle donc, le design s’assure à la fois de la correspondance au profit et au besoin. À l’adéquation avec la définition et à sa reproduction conforme.<br>Dans élégant (Delight the eye without distracting the mind.).<br>Design est de-nouveau associé à une forme reproduite de simplicité : clean, clutter-free design. On retrouve aussi form follow fonction, “<br>Design est de-nouveau associé à une forme reproduite de simplicité : clean, clutter-free design. On retrouve aussi form follow fonction, ” Google à une conception du design très traditionnelle (1 siècle) à moins qu’il s’agisse d’une modification : la forme c’est la fonction (Medium is the message ?) à moins que tout cela soit une reproduction de la même force simplificatrice (manger les autres, la mort?).<br>Less is more<br>Form follow function<br>ressemble à ” La guerre c’est la paix “, ” La liberté c’est l’esclavage “, ” L’ignorance c’est la force “<br>(war is peace, freedom is slavery, ignorance is strength)<br>. 1984<br>Dans sécurité (Be worthy of people’s trust.) l’objectif du design est de mériter (ou d’obtenir) la confiance des utilisateurs (qui utilisent déjà google ?)<br>Good design can go a long way to earn the trust of the people who use Google products.<br>Dans convivialité (Add a human touch.) le design est associé avec personnalité, individualité (our designs have personality) mais également dissocié “And Google doesn’t let fun or personality interfere with other elements of a design, especially when people’s livelihood, or their ability to find vital information, ” C’est encore la reproduction du Tout est bon, tout le monde est bon à condition que cela répande l’exercice de la force (la force de la vérité, celle de google). Ici personnalité désigne la totalité des individus dans leur diversité “Google includes a wide range of personalities, and our designs have personality” Google s’adresse directement à eux (simplement) “Google text talks directly to people and offers the same practical, informal assistance that anyone would offer to a neighbor who asked a question.” Il prend la diversité et la converti en uniformité.<br>Aussi text et design sont des éléments disctincts “Text and design elements are friendly, quirky, and smart – and not boring, close-minded, or arrogant. ” tandis qu’ils sont unis dans la convivialité, l’accessibilité, l’intelligence, c’est à dire qu’ils sont simples.<br>On pourrait croire que ces dix termes tendent à signifier la même chose, l’inceste ?</p>
		<p>“Finie l’apocalypse, auj c’est la précession du neutre, des formes du neutre et de l’indifférence. Je laisse à penser s’il peut y avoir un romantisme, une esthétique du neutre. Je ne le crois pas – tout ce qui reste, c’est la fascination pour les formes désertiques et indifférentes, pour l’opération même du système qui nous annule. Or la fascination […] est une passion nihiliste propre au mode de disparition. Nous sommes fascinés par toutes les formes de disparition, de notre disparition“</p>
		<h4 id="formule-user-experience-8-titre">formule user experience (8)  titre</h4>
		<p>Le terme user experience est également présent sur toute la page (7 de plus) même s’il n’est pas présent dans tout les paragraphes répéter de nombreuses fois au début (en titre) il revient régulièrement dans la deuxième moitié du texte. Au singulier il est constamment attaché à team, l’équipe : The Google User Experience team aims to , The Google User Experience team works to , The User Experience team researches,  the User Experience team would cheer, the Google User Experience team seeks.<br>Google disparait dans l’user experience team. Ux est google. Googley user experience</p>
		<h4 id="terme-people-22-explosion">terme people (22)  explosion</h4>
		<p>le terme people explose sur la page (22 occurences)<br>Tandis que son utilisation précédente s’opposait clairement à l’user (people : force autonome, user : dans le besoin)<br>Le terme est ici mitigé,<br>features that people need to accomplish their goals (people need help)<br>many people (army of people)<br>encourages people to expand their usage (people need help)<br>people with complex online lives (people autonomes),<br>people who share data (idem),<br>people everywhere (army),<br>Google allows people to choose (need help),<br>meet people’s needs. (need help<br>)<br>people looked at a Google product and said “Wow, that’s beautiful!” (army/autonome)<br>encourages people to make the product their own (need help / autonome)<br>Be worthy of people’s trust. (people army)<br>earn the trust of the people who use Google (army)<br>Google text talks directly to people and offers (need help)<br>especially when people’s livelihood, or their ability to find vital information, is at stake. (people need google own)</p>
		<h4 id="terme-experience-10-mont-e-en-force">terme experience (10) montée en force</h4>
		<p>experience quand il n’est pas avec user est une experience standard partagée “provide a useful and enjoyable experience for everyone“<br>experience conforme car bonne avant même son acte ?  great initial experience</p>
		<h4 id="terme-user-33-mont-e-stable">terme user (33)  montée stable</h4>
		<p>Le terme user seul reste très présent, toujours plus que people, et se retrouve utilisé de la même manière que people<br>It doesn’t try to impress users with its whizbang technology (user need or don’t need)<br>users who want to explore(user wants users need)<br>Speed is a boon to users.(this is good for users, google know)<br>easily accessible to those users who want them (users want, users need help)<br>attracting power users whose excitement and expertise will draw others to the product. pour la 1er fois user prend le sens d’individus autonomes et forts ( le client est roi)<br>lets new users jump in (users needs)<br>ensures that users can (users needs figure de l’enfant)<br>makes users comfortable, and speeds their work. (users needs)<br>they serve the needs of users (users needs)<br>many users (army)<br>methods that make sense to users (google know for users)<br>helpful to users(users needs)<br>the goals of users(google know)<br>the number of Google users (army)<br>profitable design doesn’t please users (users needs, user don’t know )<br>A positive first impression makes users comfortable (users needs)<br>doesn’t distract users from their goals (users needs, google know)<br>visual design should please its users and improve usability (google define, users needs)<br>terminology is consistent, and users are never unhappily surprised (no surprises, inceste)<br>etc.</p>
		<h3 id="l-influence-de-don-norman">L’influence de Don Norman</h3>
		<p>explosion du terme people même année de sa déclaration, adressage ux, L’UX les origines de la peur</p>
		<p>Angie Li<br>Ux Specialist, Nielsen Norman Group<br>fake diversity<br>jeunes cool<br>jeune fille<br>tailleur<br>oriflame</p>
		<p>Hi i’m Angie Li and im here  at te ux conference in san fransisco<br>i got to ask Don Norman what he thinks about the term ux.</p>
		<p>ton distinct anglais simple<br>vidéo éducative, “universelle“<br>contée par Don Norman</p>
		<p>logo N N /g<br>Donc on a une interview de Don Norman sur le site de Don Norman où on lui demande ce qu’il pense du terme qu’il a inventé.</p>
		<p>Agé, occidental<br>fort contraste avec Angie Li<br>Don Norman ressemble au père noël, avec un bleu de travail (populiste démagogue, col bleu pas col blanc != tailleur)</p>
		<p>once upon a time, a very long time ago, </p>
		<p>il raconte autant avec les gestes que avec l’intenation ou les mots<br>tout est très clar simple voir caricatural<br>think emoji</p>
		<p>il s’affiche 1993-1997</p>
		<p>i was at apple </p>
		<p>eye contact emoji</p>
		<p>and you know we said </p>
		<p>more thinking emoji</p>
		<p>the experience of using this computers is weak…</p>
		<p>emoji dégout<br>ect...<br>Ah, the experience when you first discover it<br>when you see it in the store<br>when you buy it<br>When you ouh it cant fit into the car ! Its in this great big box, it doesn’t fit into the car. When you finaly do get at home<br>you opening the box up and uuuuh ouh<br>oh it looks scary i dont know if i dare put this computer togever.<br>All of that is user experience.<br>Its everything that touches upon your experience with the product<br>and it may not even be you and the product, its maybe when you tell somebody else about it.</p>
		<p>thas what we meant<br>when we device the term user experience and set up what we call the user experience architects office at apple,<br>to try to, enhance things.<br>now apple product is allready pretty good so we’ve been started with a good product<br>make it even better<br>:( Today that term is been horribly misused<br>its use by people to say<br>im a user experience designer<br>i design websites so i design apps<br>so they have no clues to what they’re doing and they think the experience of that<br>simple device<br>the websites or the apps<br>or who know what<br>NOOO<br>its everything<br>its the way you experience the<br>world<br>// your life<br>// the service<br>or yeah an app or a computer system<br>but its a system thats everything<br>got it ?</p>
		<p>En 2008, la même année où Donald Norman avoue publiquement sa répulsion pour le terme “user” et explique préférer “people” citoyen, personne, indivu le 30 avril cette section est carrément renommée «design principles» c’est à dire «principes de design», et il ne s’agit plus pour google de se focaliser sur l’utilisateur mais sur l’individu “people” «we design for people we dont design for users» nous désignons des expériences et non des utilisations, nous designons des objets en sorte que leurs consommateurs en aient une expérience et non une prise en main, qu’ils n’en fassent rien si ce n’est recevoir leur message.<br>La page google today devient “design principles” et Google demande “focus on people – their work, their dreams”.</p>
	</section>
	<!--
	<h1 class="card-panel" >2008</h1>
	<h2 class="card-panel">google.com/corporate/ux</h2>
	<section class="card-panel lime">
		<p class="card-panel" >
			En 2008, la même année où Donald Norman avoue publiquement sa répulsion pour le terme “user” et explique préférer “people” citoyen, personne, indivu le 30 avril cette section est carrément renommée «design principles» c'est à dire «principes de design», et il ne s'agit plus pour google de se focaliser sur l'utilisateur mais sur l'individu “people” «we design for people we dont design for users» nous désignons des expériences et non des utilisations, nous designons des objets en sorte que leurs consommateurs en aient une expérience et non une prise en main, qu'ils n'en fassent rien si ce n'est recevoir leur message. 
		</p>
		<video class="card-panel light-blue lighten-4" controls src="Don Norman The term UX.mp4"></video>
		<p class="card-panel" >
			paragraphe
		</p>
	</section>-->
</article>
<article class="card-panel article pink lighten-5" id="8-16">
	<aside class="toast pinned" ></aside>
	<h1 class="card-panel" >2008-2016</h1>
	<h2 class="card-panel">google.com/undefined</h2>
	<section class="card-panel lime">
		<p>À partir de 2008 il n’y aura plus de grands changements dans le discours. Le contenu se stabilise en grosso-modo deux formes rédactionnelles définitives.<br>Les dix choses (Dix vérités) et les principes de Design.<br>Néanmoins ces deux formes vont exister parallèlement au travers de nombreuses urls.<br>Premièrement le contenu “corporate” est doublé au travers des url /compagny et /corporate avec notamment des variations de mise en page et d’architecture néanmoins le contenu rédactionnel est le même. Au sein même des branches /compagny et /corporate vont exister de nombreux doublons avec par exemple company/philosophy clone de company/tenthings et corporate/tenthings, company/ux clone de corporate/ux …<br>Le seul contenu rédactionnel significativement nouveau est la formule We first wrote these “10 things” several years ago. From time to time we revisit this list to see if it still holds true. We hope it does–and you can hold us to that.<br>en (September 2009) qui en soit confirme la stabilisation du contenu rédactionnel.<br>On peux également trouver des versions traduites de ces pages par exemple google.fr/corporate/ux, première apparition de la version traduite en français de design principles (Principes applicables à la conception de sites), traduit très étrangement, peut-être principalement avec google traduction. On peut remarquer qu’il n’y pas de substitution ou d’assimilation au terme utilisateur à une traduction du terme people un terme exclusivement pluriel désignant tout à la fois l’homme, l’individu, le citoyen, le peuple selon le contexte, intraduisible, n’a pas de sens en français.</p>
		<p>People et users bien qu’utilisés de la même manière dans les design principles et les 10 things ne sont pas réparti de la même manière entre les deux formes.<br>(voir shéma)<br>dans design principles 33 occurences du terme user pour 22 occurences du terme people, dans 10 things 11 occurences du terme people pour 5 occurences du terme user.<br>On peut supposer que 10 things est une version de la philosophie de google déstinée au publique le plus large possible tandis que design principles bien que très large également est plus destiné à des “utilisateurs expérimentés“.</p>
		<p>Dernière chose, à partir de 2016 les pages design principles disparaissent, 10 things devient l’unique et définitive version du discours de google.</p>
		<h3 id="dix-v-rit-s">dix vérités</h3>
		<h3 id="dix-principes-de-design">dix principes de design</h3>
		<h3 id="la-philosophie-l-emporte">la philosophie l’emporte</h3>
		<h2 id="2011">2011</h2>
		<h3 id="traduction">traduction</h3>
		<h4 id="version-fran-aise-tardive-a-v-rifier-google-fr-corporate-ux">version française (tardive, a vérifier) google.fr/corporate/ux</h4>
		<h4 id="pas-de-terme-people-car-a-veux-rien-dire-en-fran-ais-donc-utilisateur">pas de terme people car ça veux rien dire en français donc utilisateur</h4>
		<p>En 2011 cette page est traduite en français et le terme de citoyen ou de personne ou d’individu n’apparait nullement, la page s’intitule<br>«les utilisateurs avant tout» marquant encore plus cette difficulté de traduction du terme people, et le caractère peut-être exclusivement anglophone voir californien de l’utilisation du terme “people” concernant le design d’interface.</p>
		<h4 id="google-traduction-">google traduction ?</h4>
	</section>
</article>
<article class="card-panel article pink lighten-5" id="30oct2010">
	<aside class="toast pinned" ></aside>
	<h1 class="card-panel" >2010</h1>
	<h2 class="card-panel">google.com/corporate/ux</h2>
	<section class="card-panel lime">
		<h3 class="card-panel" >partie</h3>
		<p class="card-panel" >
			paragraphe
		</p>
	</section>
</article>
<article class="card-panel article pink lighten-5" id="29may2011">
	<aside class="toast pinned" ></aside>
	<h1 class="card-panel" >2011</h1>
	<h2 class="card-panel">google.fr/corporate/ux</h2>
	<section class="card-panel lime">
		<p class="card-panel" >
			En 2011 cette page est traduite en français et le terme de citoyen ou de personne ou d'individu n'apparait nullement, la page s'intitule 
			«les utilisateurs avant tout» marquant encore plus cette difficulté de traduction du terme people, et le caractère peut-être exclusivement anglophone voir californien de l'utilisation du terme “people” concernant le design d'interface.
		</p>
	</section>
</article>
<article class="card-panel article pink lighten-5" id="25jun2014">
	<aside class="toast pinned" ></aside>
	<h1 class="card-panel" >25 juin 2014</h1>
	<h2 class="card-panel">google.com/design</h2>
	<section class="card-panel lime">
		<p>Le “Material Design” est officialisé le 25 Juin 2014 simultanément durant la Google I/O conférence et en ligne à l’adresse www.google.com/design/spec/material-design. C’est également à cette date que pour la première fois du contenu sur la page google.com/design apparaît, la page existe depuis 20** mais vide ou redirigée.</p>
		<h4 id="cr-ation-d-une-page-design-qui-existait-depuis-des-ann-es-mais-vide">création d’une page design, qui existait depuis des années mais vide</h4>
		<p>Avant de nous pencher sur la page material design nous allons dabord observer celle-ci.</p>
		<p>Malgré une url très différente, et une mise en page drastiquement revue (loué soit Duarte, on y reviendra plus tard) cette page s’inscrit dans une continuité avec les pages 10 things et design principles.</p>
		<p>Titré Google Design cette page s’ouvre avec un paragraphe rappel de la première vérité découverte par google : “Focus on the user and all else will follow“</p>
		<p>le bouton ? Google guidelines renvoi à l’adresse /design/spec/material-design, le design était formulés en principes il est maintenant formulés par des directives.</p>
		<p>Le contenu de cette page est très peu textuel, ce qui saute aux yeux c’est le caractère résolu « graphique » de cette nouvelle mise en page.</p>
		<p>Faisons rapidement la comparaison formelle avec la page 10 things 2016,<br>même si cette page est plus récente cela n’a pas d’importance, puisqu’elle suit clairement les principes de mise en page des pages précédentes qui ont précéder la page design qui contient ce “nouveau” principe de mise en page qui en soit introduit ce qu’est le material design. Le fait qu’elle soit plus récent permet de faire une comparaisons de choix et non de contraintes techniques ou de mode. Car les technologies web sont les mêmes et sensiblement les mêmes modes.<br>Pour voir ces modes nous allons aussi comparer avec la mise en forme de 1999.<br>Quand bien même 10 things hérite des contraintes formelles historiques des pages que nous venons d’étudier. Il s’agit bien de choix de conservation de méthodes, une esthétique ?<br>Ces trois pages montrent bien la différence entre l’ancien design google qui est toujours présent et cohabite avec le “nouveau” design google, que l’on va appeler le Material Google (plutôt que material design, comme on l’a vu le design se confond largement avec la philosophie de google, cad son identité).</p>
		<p>pour plus de “fidelité” j’ai re-telecharger ces pages avec wget, qui telecharge l’ensemble des liens qui construisent la page contrairement aux autres pages pages que vous avez pu voir qui était juste enregistrer sous avec explorer windows.</p>
		<p>Le Material Google met l’accent sur les formes géométriques.<br>Les couleurs vives.</p>
		<p>L’association des deux donne une tonalité “ludique“, simpliste ou plutôt enfantine qui va dans la continuité de l’infantilisation du discours. lego playdo, jeu vidéo, flat design, otto neurath.</p>
		<p>Un fort accent est mis sur les “effets“, d’ombres, de transition, d’agrandissement, responsive etc. Ce qui va dans la continuité du spectaculaire, web tv (), les moments du site ressemble à des mini compagny intro video.</p>
		<p>L’ancien Google est très proche de l’esthétique du « document » malgré les différences (choix de typo, encadrements, colonnages, couleurs de fonds) l’influence esthétique par les contraintes techniques et les attributs de mise en page par défaut du HTML reste forte même en 2016, assez 90, environnement de bureau.<br>De l’article, du format papier corporate. </p>
		<p>C’est la simplicité économique dans tout les sens du terme, du web as a marketing tool de Nielsen.</p>
		<p>exemple poussé à l’extrême : <a href="http://motherfuckingwebsite.com/">http://motherfuckingwebsite.com/</a></p>
		<p>(vous remarquerez qu’en observant le code source, on tombe sur un script google analytics)</p>
		<p>Ces choix, ces manières de faire “graphiques” se remarque également dans le code</p>
		<p>ci joint le code html de la mise en page d’un paragraphe sur les 4 différents sites</p>
		<h5 id="2014-material-google">2014 Material Google</h5>
		<pre><code>&lt;section class=“intro section-wrapper“&gt;
			&lt;a class=“gweb-smoothscroll-control paper-button btn-next” data-g-event=“nav” data-g-label=“scrolldown” href=“https://web.archive.org/web/20140625223441/http://www.google.com/design/#resources“&gt;&lt;span&gt;Down&lt;/span&gt;&lt;/a&gt;
			&lt;div class=“span-9” id=“intro“&gt;
			&lt;p&gt;
			At Google we say, “Focus on the user and all else will follow.” We embrace that principle
			in our design by seeking to build experiences that surprise and enlighten our users in
			equal measure. This site is for exploring how we go about it. You can read our design
			guidelines, download assets and resources, meet our team, and learn about job and
			training opportunities.
			&lt;/p&gt;
			&lt;/div&gt;
			&lt;/section&gt;
		</code></pre><h5 id="2016-ancien-google">2016 ancien Google</h5>
		<pre><code>&lt;li&gt;
			&lt;h2&gt;
			Focus on the user and all else will follow.
			&lt;/h2&gt;
			&lt;p&gt;
			Since the beginning, we’ve focused on providing the best user experience
			possible. Whether we’re designing a new Internet browser or a new tweak to the
			look of the homepage, we take great care to ensure that they will ultimately
			serve &lt;strong&gt;you&lt;/strong&gt;, rather than our own internal goal or bottom line. Our
			homepage interface is clear and simple, and pages load instantly. Placement in
			search results is never sold to anyone, and advertising is not only clearly
			marked as such, it offers relevant content and is not distracting. And when we
			build new tools and applications, we believe they should work so well you don’t
			have to consider how they might have been designed differently.
			&lt;/p&gt;
			&lt;/li&gt; 
		</code></pre><h5 id="1999-ancien-google">1999 ancien Google</h5>
		<pre><code>&lt;li&gt;
			&lt;p&gt;&lt;b&gt;An Elegant, Easy-to-Use Interface&lt;/b&gt;: Google’s clean,
			uncluttered interface is designed to make it easy for users to
			enter search queries and interpret results.  Results are
			presented with context sensitive summaries so users can easily
			tell if the corresponding web pages will satisfy their need.
			Users can also enter a query and click the “I’m Feeling
			Lucky&lt;sup&gt;TM&lt;/sup&gt;” button, which takes users directly to the
			website of the first search result. For example, entering
			&lt;code&gt;smithsonian&lt;/code&gt; into Google search field and clicking
			the “I’m Feeling Lucky” button takes the user directly to
			www.si.edu, the official homepage of the Smithsonian
			Institution.
			&lt;/p&gt;
			&lt;/li&gt;
		</code></pre><h5 id="2008-ancien-google">2008 ancien Google</h5>
		<pre><code>&lt;h3&gt;Ten principles that contribute to a Googley user experience&lt;/h3&gt;
			&lt;p&gt;
			&lt;a href=“https://web.archive.org/web/20080430190246/http://www.google.com:80/corporate/ux.html” id=“useful“&gt;&lt;/a&gt;&lt;strong&gt;1. Focus on people&lt;/strong&gt; – &lt;strong&gt;their lives, their work, their dreams.&lt;/strong&gt;
			&lt;br&gt;
			&lt;br&gt;
			&lt;span&gt;The Google User Experience team works to discover people’s actual needs, including needs they can’t always articulate. Armed with that information, Google can create products that solve real-world problems and spark the creativity of all kinds of people. Improving people’s lives, not just easing step-by-step tasks, is our goal.&lt;/span&gt;
			&lt;br&gt;
			&lt;br&gt;
			&lt;span&gt;Above all, a well-designed Google product is useful in daily life. It doesn’t try to impress users with its whizbang technology or visual style&lt;/span&gt; &lt;span&gt;–&lt;/span&gt; &lt;span&gt;though it might have both. It doesn’t strong-arm people to use features they don’t want&lt;/span&gt; &lt;span&gt;–&lt;/span&gt; &lt;span&gt;but it does provide a natural growth path for those who are interested. It doesn’t intrude on people’s lives&lt;/span&gt; &lt;span&gt;–&lt;/span&gt; &lt;span&gt;but it does open doors for users who want to explore the world’s information, work more quickly and creatively, and share ideas with their friends or the world.&lt;/span&gt;
			&lt;/p&gt;
		</code></pre><p>Ce qui saute aux yeux dans le code c’est la “nudité” du html dans les codes 2016 2008 1999 </p>
		<pre><code>&lt;p&gt;
			paragraphe
			&lt;/p&gt;
		</code></pre><p>La mise en page est « sémantique » avec les “tics” de programmations spécifiques à chaque “époque” du web</p>
		<pre><code> &lt;b&gt;texte en gras&lt;/b&gt;
		</code></pre><p>En 1999 la balise <code>&lt;b&gt;</code> pour rendre gras, cette méthode est dépréciée dans les années 2000</p>
		<pre><code>&lt;strong&gt;texte “intensifié“, gras et légèrement plus grand&lt;/strong&gt;
			&lt;br&gt;saut de ligne
			&lt;br&gt;
			&lt;span&gt;?&lt;span&gt;
		</code></pre><p>En 2008 la balise <code>&lt;strong&gt;</code> pour intensifié le texte (plus sémantique, accessible, compréhensible par les néophytes que le rendre gras ?).<br>La balise <code>&lt;br&gt;</code> pour sauter une ligne déprécié la décennie suivante.<br>La balise <code>&lt;span&gt;</code> pour créer des divisions dans le texte idem car peu sémantique idem.</p>
		<pre><code>&lt;li&gt;
			&lt;h2&gt;
			titre
			&lt;/h2&gt;
			&lt;p&gt;
			paragraphe &lt;strong&gt;mot en gras&lt;/strong&gt; paragraphe
			&lt;/p&gt;
			&lt;/li&gt; 
		</code></pre><p>La version de 2016 est en fait la plus proche du html brut de base</p>
		<p>Le Material Google de 2014 est bourré d’informations liés à d’autres langages, css, javascript, on retrouve de nombreux style et id tout au long du code </p>
		<pre><code>class=“gweb-smoothscroll-control paper-button btn-next“
		</code></pre><p>ces marques témoignant de la machine à gaz qui tourne derrière chaque paragraphe</p>
		<pre><code>&lt;section class=“intro section-wrapper“&gt;
			&lt;div class=“span-9” id=“intro“&gt;
			&lt;p&gt;
			paragraphe
			&lt;/p&gt;
			&lt;/div&gt;
			&lt;/section&gt;
		</code></pre><p>La programmation suit une logique « d’emballage », les éléments sémantiques sont entourés de couches abstraites qui servent de lien avec de nombreux scripts contrôlant dynamiquement la mise en page à l’échelle de l’élément, on approche d’une conception « atomique »¹ de la mise en page très éloignée de la pensée de la page web en tant que « document ». La page devient une sorte de bac à sable ou les éléments s’organisent par les interactions d’autres élements de leur échelle tandis que dans le document on suit une logique “journalistique” hiérachisée Article &gt; Titre &gt; sous-titre &gt; paragraphe.</p>
		<h3 id="praise-to-be-duarte">Praise to be Duarte</h3>
		<h4 id="un-messie-chez-google">Un messie chez google</h4>
		<p>Pour comprendre cette nouvelle mise en page il est nécessaire de intéresser à Mathias Duarte’s </p>
		<p>environement ikea, aseptisé ? Feutres trias, street photography now, crappy design book, design know, illustration know.</p>
		<p>innovative ambiance ?<br>work space<br>trendy co-working spaces</p>
		<p>type resto ?<br>mac book</p>
		<p>musique piano emotionnelle, espoir, fascination, calme, concentration, nostalgie.</p>
		<pre><code>fonds boisé design modern nordic ? Rires exagérés, chemise desigual
		</code></pre><p>the intersting thing about, design for software, design for pixels, is : its one of the most difficult material to work with. </p>
		<pre><code>Mathias joue avec des feutres et des post-it (bac à sable ?) atelier hyper clean lavabo, on vois un imac plein de post-it
			Google Headquarters, Mountain View, California
		</code></pre><p>To be an artist you have to understand your material, you have to understand what its good at doing or what is its bad at doing.</p>
		<pre><code> on vois des livres: Sketching user experience, design &amp; identity, designing with ? SP- FREE ? la musique s’intensifie progressivement
		</code></pre><p>There is not decades or hundreds of years of making haem software or apps or operatings systems the way there is for making shoes or spoons or ah even a car. </p>
		<pre><code> workspace loft, ourlets au pantalon, adidas, nike sport, pantalon toile, pull uni, casquette en jean, luentter monture noir epaisse
		</code></pre><p>So what is the right way to design, what is the right way to build ?<br>I’ve allways insisted we have to keep an open mind.<br>Where i can know the irhgt way to do this ? For decades still.</p>
		<pre><code>(tape la tete en riant) ha la bonne blague blow minding
		</code></pre><p>I know ive tell this story before but its stick so much in my mind. I remember the </p>
		<pre><code> Sorte de cuisine google, couleurs assorties jaune citron, vert beige, materiaux mi-récup mi luxe. Plan sur des vélos (luxe ?) whaou on va au travail en vélo
		</code></pre><p>first day at google thinking. Wouah this is really exiting, thats google orientation, they got that hole process, they tell you everything about how great google is and you know all the parts of google, and the scale and…! </p>
		<pre><code> musique emotionnelle de plus en plus intense 
		</code></pre><p>There was kind of like, a list of things that i knew where gonna be important to get that haem. What good for users and how/had good design can be respected and recognize trhougt design. </p>
		<p>There was problably not until… what was it ? Lollipop ? </p>
		<pre><code> mathias semble travailler avec plaisir, il soulève une feuille vierge immaculée puis la repose sur sa table en bois vernie si propre qu’elle reflete comme un miroir, des shémas au feutre la parcours (whaou écrire sur la table, think outside the box)
		</code></pre><p>I think when we rolled out material design. It happend a time when people where exited about this tech nostalgia. Lets get everybody to rally around somthing, that is </p>
		<pre><code> emotionaly connection correspond avec un plan sur un puzzle en bois enigmatique, jouet interdit ? Kubrick ?
		</code></pre><p>extactic ! They emotionaly have a connection to. We thought ok, lets work together, lets go all in on this, let make it a project and a initiative. So what it is it ? what its unifiyng idea, we need to get everybody toghever, we need to figure out how to extend material design not just to the phone and desktop but also to the new plateforms.</p>
		<pre><code> mathias regard interessé et mystérieux sur table de travail, prise de recul ?
			très très gros plan sur des feutres stabilos (special roughs)
		</code></pre><p>We’re trying to represent and think about all of google, and kind of provide an infrasrtructre and a tool kit cross all of google.<br>The google design website is a perfect example of that, the websites is run by designers from material design team.</p>
		<pre><code> mathias est fimé entrain d’être pris en photo, puis il rit de bon coeur , re pose
		</code></pre><p> i want to have a more human... design process, and one that respect kind of the individual diognity of every person by creating a safe place  for people to contribute stuff.</p>
		<pre><code> zoom sur tete (vidéo est très coupée, le discours ressemble à un collage toutes les deux phrases)
		</code></pre><p>enable some to do there best work, to learn faster, to unlock their full potential.<br>Well at the same time taking full advantage of the peers and the senior designer mastery and maintain and set a high bar and a high quality standard and constently keep pushing.</p>
		<p>La plus grande réalisation de Duarte a été le développement du langage visuel de Material Design-Google pour ses multiples plateformes, un ensemble de règles qui guident tous les produits de l’entreprise à travers le mobile et le web. Le projet a débuté vers 2013, lorsque l’équipe de Google Search tentait de dépasser le statu quo des ” 10 liens bleus ” qui existait depuis une décennie. Au même moment, une équipe de Chrome cherchait à faire progresser certaines technologies avec les équipes d’applications Google. Et l’équipe Android de Duarte se préparait à travailler sur sa prochaine sortie, Lollipop. Pour unifier l’expérience utilisateur à travers les équipes Search, Chrome et Android, Duarte a développé un langage cohérent et unifié, capable de passer du téléphone au portatif en passant par la télé et la voiture.</p>
		<p>Traduit avec www.DeepL.com/Translator </p>
		<p>Duarte’s largest achievement was developing Material Design—Google’s visual language for its myriad platforms, a set of rules that guide all of the company’s products across mobile and web. The project began around 2013, when the Google Search team was trying to break out beyond the “10 blue links”’ status quo that had existed for a decade. At that same time, a team within Chrome was looking to advance some technologies with the Google apps teams. And Duarte’s Android team had been preparing to work on its next release, Lollipop. To unify the user experience across the Search, Chrome, and Android teams, Duarte developed a cohesive, unified language that could scale from phones to wearables to TVs to cars.</p>
		<p>The impact Duarte has had on user-experience design is deep, and not just at Google, where he has been a vice president of design since 2015. He once oversaw the design team that created the Danger Hiptop, known to us consumers as the T-Mobile Sidekick. In the late aughts, he was a VP at Palm, where he led development of the company’s WebOS platform with its revolutionary “card” system. At Google, Duarte convinced the notoriously engineering-first company to embrace design, developing a single visual language applied to all of its products, from email to home-automation software, quietly affecting the experiences of millions of people every day.</p>
		<p>In 2010, following Palm’s acquisition by HP, Duarte started at Android, working for Andy Rubin, who was CEO of the company—which Google had acquired in 2005—and had previously worked with Duarte at Danger. Before his first day, though, Duarte wanted to make it clear that he did not want to get sucked into the then-engineer-heavy Google vortex. “I never thought I’d work for Google” he says. “I had zero ambition to work for Google. Everybody knew Google was a terrible place for design.”</p>
	</section>
</article>
<article class="card-panel article pink lighten-5" id="29jun2014">
	<aside class="toast pinned" ></aside>
	<h1 class="card-panel" >29 juin 2014</h1>
	<h2 class="card-panel">google.com/design/spec/<br>material-design/introduction</h2>
	<section class="card-panel lime">
		<h2 id="29jun2014">29jun2014</h2>
		<h3 id="un-document-en-avant-premi-re">Un document en avant-première</h3>
		<p>Le Material Design </p>
		<p>Savez-vous ce qu’est le Material Design ?<br>Décrire le Material Design n’est pas simple.</p>
		<p>il est surpenant de constater la quantité d’articles ou de commentaires soulignant le caractère incompréhensible du Material Design, sous tout ses aspects, dans le discours, la théorie et la pratique.<br>Notamment sur ce qui le distingue du ’flat-design’ ou du ’skeuomorphisme’</p>
		<h3 id="mathias-le-roi-de-la-surface">Mathias le roi de la surface</h3>
		<h4 id="mathias-le-moderne">Mathias le moderne</h4>
		<h4 id="un-ph-nix-en-papier">Un phénix en papier</h4>
		<p>« […] Nous nous sommes mis au défi de définir les principes physiques sous-jacents à nos interfaces pour élaborer un langage visuel qui synthétise les principes classiques de ce qui fait un bon design avec une solide compréhension des principes physiques les plus fondamentaux. Au début, nous avons pensé comme des designers, comment le présenter, quel apparence lui donner. Puis nous avons pensé comme des scientifiques, pourquoi se comporte il de cette façon ? Enfin après de nombreuses expérimentations et de nombreuses observations nous avons écrit tout ce que nous avons appris. C’est ça le principe du Material. […] » </p>
		<p>puis ainsi par Mathias Duarte :</p>
		<p>« Le tout premier principe dans le Material Design est celui de la métaphore. […] Les métaphores sont vraiment puissantes car elles sont profondes de sens. Elles communiquent plus richement que le langage verbal le permet. […]<br>La métaphore est une forme de transmission de la connaissance qui dépend des expériences partagées, de fait cette capacité, la transmission de la connaissance et de l’apprentissage font parties des choses qui définissent l’humanité et de fait défini l’intelligence. Donc pour nous, l’idée de la métaphore est une histoire de fond pour le design. Elle unifie et façonne le design. Elle a deux fonctions, elle fonctionne pour notre audience, nous voulons présenter une métaphore qu’ils peuvent comprendre avec laquelle ils peuvent se connecter, qu’ils peuvent utiliser pour aller plus vite dans la compréhension de comment on utilise les choses. C’est aussi une métaphore pour nous-même, pour les designers et les développeurs et les project managers, et les gens de Quality assurance. Tout ces gens travaillent ensemble car quand vous avez une métaphore que tout le monde comprend, comprend intuitivement, vous n’avez pas à expliquer comment ils ont violé la sous-section C clause 2 de votre charte graphique, ils sentent tout simplement que c’est faux, hors-sujet. Donc pourquoi cette métaphore particulière, pourquoi avons-nous imaginé un matériau qui à une forme de papier suffisamment avancée pour se confondre avec de la magie, et bien d’une part il est évident que nous avons une grande expérience de la communication sur papier, elle est juste riche dans une histoire traversant l’ensemble de nos cultures de la transmission d’information et cela naturellement permet tant de différentes manières d’interagir avec. Mais l’autre aspect du papier est que c’est quelque chose de physique qui existe dans le monde, et cette idée que les surfaces parce qu’elles sont tangibles sont une métaphore que nous pouvons utiliser pour accélérer la compréhension de notre UI est vraiment importante. Vous avez cette perception des objets et des surfaces qui se passe dans les parties les plus primitives du cerveau, ça arrive dans ces cortex visuels qui sont dans la partie arrières et basses de votre cerveau et cela signifie simplement que … »</p>
		<p>comme un «langage visuel synthétisant les principes classiques d’une bonne conception, de l’innovation visuelle, les possibilité technologiques et scientifiques».</p>
		<p>sur la page google.com/design/spec/material-design le Material Design est défini ainsi :</p>
		<p>« Nous nous sommes mis au défi de créer un langage visuel pour nos utilisateurs qui synthétise les principes classiques d’une bonne conception (“good design”, dans le texte) avec l’innovation et les possibilité offertes par la technologie et la science. Ceci est le material design »</p>
		<p>Pendant la conférence une personne demande pourquoi il y a des cercles en quantités, la personne dans une sorte d’ironie mêlée d’un sincère engouement s’excuse par avance en disant qu’elle est développeur et que sa question peux paraître bête pour un designer. Néanmoins cette personne demande de parler de ces cercles, où sont sont ils appropriés ? Que permettent ils de transmettre à l’utilisateur.</p>
		<p>Mathias Duarte réponds à propos du travail de simplification qu’ils ont menés, la tentative de conserver uniquement des formes primaires élémentaires de «bas niveau» (dialectique de la raison). </p>
		<p>Que ce qu’ils font c’est de tout ramener aux formes géométriques les plus simple et basiques. Que nous pouvons y voir des cercles, des carrés, des rectangles, des divisions basiques de l’espace (structuralisme, bauhaus, nouvelle typographie). Qu’ils utilisent le cercle car naturellement il contraste avec l’espace qui est divisé, qu’il est un très bon moyen d’attirer l’attention de l’oeil sans effectuer de mouvements, qu’il peux indiquer une action primaire, qu’il créé du rythme par lui-même. Que le cercle est un outil à penser comme n’importe quel autre outil visuel. Que c’est une forme qui se propage de son point d’origine, qu’ainsi il correspond en termes d’affordance avec le toucher sur une surface et la propagation de sa force.(gestalt théorie)</p>
		<p>Plus tard une autre question, la personne se présente comme UX UI designer, sa question concerne la manière dont s’animent les formes, il commence par poser un contexte (sans en faire la critique) où nous savons tous que le travail d’animation sert principalement à faire sortir du lot une application et à conserver sa popularité, que dans les agences de design les animations sont élaborés sur fireworks, after effects, origami ou même à la main, et que la principale des difficultés lors de l’élaboration d’une app est d’arriver à transmettre aux développeurs l’explication de comment on va interagir avec l’application et qu’il se demande si Google va fournir un outil permettant d’aider les designers à réaliser leurs animations, si dans les directives du guide que sera le material design il y aura en plus des explications sur les animations sur lequels se concentrer par défaut un outil permettant aux designers de transmettre aux développeurs les points nécessaires concernant les animations prévues …</p>
		<p>Une autre personne pose la question des couleurs, son constat est le suivant on peux voir partout beaucoup de couleurs éclatantes pour ne pas dire criardes ces derniers temps, et elles se retrouvent partout dans la présentation, qu’il y a beaucoup de combinaisons colorées, comment ses couleurs vives sont-elles choisis dans le contexte du material Design, comment charment elles l’utilisateur, quel sens doit-on en tirer, et ne sont elles pas si brillantes qu’elles deviennent désagréables. </p>
		<p>La réponse immédiate Jon Wiley sous forme d’une blague entre collègues designers est qu’ils ont testés 41 nuances différentes. Puis Johnatan Lee explique qu’ils ont intentionnellement choisis des couleurs pour leur exubérance, afin de caractériser clairement les produits de la firme. Mais que à coté de cela les directives proposées concernant les couleurs ont été pensées pour être souple, de proposer des conseils d’associations de couleurs concernant des gammes colorées très diverses, même très discrètes afin que chacun puisse s’y sentir chez soi.</p>
		<p>En 2008, la même année où Donald Norman avoue publiquement sa répulsion pour le terme “user” et explique préférer “people” citoyen, personne, indivu le 30 avril cette section est carrément renommée «design principles» c’est à dire «principes de design», et il ne s’agit plus pour google de se focaliser sur l’utilisateur mais sur l’individu “people” «we design for people we dont design for users» nous désignons des expériences et non des utilisations, nous designons des objets en sorte que leurs consommateurs en aient une expérience et non une prise en main, qu’ils n’en fassent rien si ce n’est recevoir leur message. </p>
		<h4 id="veille-de-l-officialisation-du-material-design">veille de l’officialisation du material design</h4>
		<h4 id="un-design-pour-tous-les-guider">Un Design pour tous les guider</h4>
		<p>Le Material Design est présenté comme un ensemble de directives destinées aux designers et développeurs d’interfaces graphiques. Les directives proposées insistent sur l’utilisation de formes géométriques élémentaires aux couleurs vives contrastées et complémentaires au comportement imitant de façon minimaliste les propriétés physiques du papier, de la page.</p>
		<p>le 25 juin 2014 Google introduit sur son site une section design et dévoile son propre courant/discipline (discipline car ayant des disciples) de design, le “Material Design”. Le “Material Design” est une transposition graphique de leurs précédentes règles/philosophies/pensées, les mêmes arguments viennent alors commenter des formes, des mouvements, couleurs, choix de composition et de typographie.<br>Le “Material Design” apparait comme une synthèse de ce dont le «citoyen» est habitué à observer sur son écran. Lors de ce dévoilement de nombreux articles établissent une parenté entre les deux grosses tendances graphiques ( en oubliant une troisième, le texte ) présentes sur le web c’est à dire le skeuomorphisme (voir les potentiomètres de Reason (jecrois) ou le micro de Apple) et le flat (sorte de style suisse international appliqué au web design) ce qui revient à la classique dichotomie figuration–abstraction et décrivent le “Material Design” comme la synthèse des deux modes de pensées, des mouvements s’ils existent. Pour ma part je pense que ce qui se joue dans le “Material Design” est plus proche du travail sur la norme telle qu’on pu le faire les suisses… à voir. Cette «méthode» de design s’appuie sur le matériau comme métaphore et propose d’envisager tout élément d’interface comme une matière «concrète» ayant des caratéristiques physiques (poids, texture, position, exposition à la lumière). Le matérial design se fait alors passer pour une sorte d’abstraction de la matière, métaphore abstraite qui serait la raison de son caractère innovant et unificateur, mais cette métaphore ne serait-elle pas simplement celle de la feuille de papier ?</p>
		<h4 id="formules-sont-d-plac-es">formules sont déplacées</h4>
		<p>formules des principes et des 10 things </p>
		<h4 id="une-arme-pour-rivaliser-avec-l-app-store">une arme pour rivaliser avec l’app store</h4>
		<p>Depuis 2014 Google publie des directives destinées à la conception d’objets visuels, interactifs et animés. La stratégie étant d’unifier l’ensemble de leurs services tout en poussant les développeurs passant par la plate-forme de distribution de l’entreprise (Google Play) à respecter certaines règles d’ergonomie et d’apparence, Google ne soumettant pas les produits du “play store” à un processus de validation pour se différencier de l’App Store d’Apple</p>
		<p>Topolsky hails the system as game-changing. “His concept of compartmentalizing information inside of cards—he was so ahead of the game on that,” he says. “I want to be very clear, when Apple introduces iOS 7 [in 2013] and Jony Ive leads the redesign, they are all over cards and the concept of what Matías had introduced both in WebOS and later with Android. They are full-scale lifting from his design, in my opinion. And not doing that great a job of it, by the way. Matías’s design, to me, had a much more coherent underlying system. You see with Apple the lack of a clearly thought-out system.” Apple did not respond to a request for comment.</p>
		<p>John Maeda, the global head of computation design and inclusion at Automattic, the web development company behind WordPress.com and Jetpack, among other products, disagrees with Topolsky’s Apple thesis, but does credit Google with software mastery. When asked if he thinks Apple has ever followed Duarte’s lead, he says, “No, I think Apple has played its playbook over and over. And it kind of works, but it’s less appealing than it was in the past. It’s kind of like Sony in its heyday.” He continues, “On the computational design side, Google is winning because it has better machine-intelligence chops. That’s where the world will be won, and that kind of design is frightening and interesting.”</p>
		<h4 id="la-soumission-graphique-librement-consentie">la soumission graphique librement consentie</h4>
		<h3 id="la-nouvelle-typographie-du-web">La nouvelle typographie du web</h3>
		<h4 id="do-don-t">Do / Don’t</h4>
		<p>Le Document</p>
		<p>Dans sa première version en 2014 le document se compose de 41 chapitres comme autant d’aspects définissant l’interface graphique en tant qu’objet, regroupés en 8 parties comme autant d’aspects définissant le travail de design d’interface graphique.</p>
		<ul>
			<li>Nouvelle Typograhie de Tschihold </li>
			<li>le sommaire des guidelines du material design est tellement exaustif qu’il décrit un interface</li>
		</ul>
	</section>
</article>
<article class="card-panel article pink lighten-5" id="29aug2017">
	<aside class="toast pinned" ></aside>
	<h1 class="card-panel" >2017</h1>
	<h2 class="card-panel">material.io</h2>
	<section class="card-panel lime">
		<h2 id="2017">2017</h2>
		<h3 id="le-material-design-le-nouveau-bauhaus">Le Material Design, le nouveau Bauhaus</h3>
		<p>le design semble changer drastiquement, plus plat plus géomtrique, couleurs changent beaucoup aussi, beaucoup plus de noir, fini le papier? pourtant les guidelines restent très similaire, protéiforme ? </p>
		<h4 id="l-szl-moholy-nagy-com">László Moholy-Nagy.com</h4>
		<p>logo ressemble énormement à celui du bauhaus dessiné par LMN, aucune reference n’est explicité, comme dans la conférence io</p>
		<h4 id="une-r-volte-devenue-tradition">Une révolte devenue tradition</h4>
		<p>se referé au design moderne avant 2 guerre c’est se référer à la reblelion moderne , inspiré du futurisme, manifeste, tschichold réclame clairement une sission avec le passé.<br>Ce fond progressiste disparait completement au profit des formes produites dans ce contexte.<br>Completement decontextualisé réutilisées par “habitude” = facilité, ces formes prennetn toute la valeur traditionnelle à laquelle ell étaient censées s’opposer.</p>
		<p>En allant sur cette adresse nous accédons à la page index du site.</p>
		<p>Le texte est en langue anglaise, la fonte utilisée est une monospace avec serif.</p>
		<p>La page se divise en trois parties horizontales placées l’une au dessus de l’autre.</p>
		<p>Les deux premières parties possèdent un fond d’un gris très sombre, proche du noir , et occupent 6 divisions sur 7 de la hauteur. Des marges latérales les ramène plus au centre. La dernière partie, beaucoup moins haute, possède un fonds très clair et occupe intégralement la largeur de la page. Elle forme une sorte de bande claire collée en bas de la page.</p>
		<p>La première de ces parties tout en haut, comporte dans son premier tiers horizontal principalement du texte tandis que le reste de sa surface est occupé par des figures géométriques en mouvement permanent.</p>
		<p>La deuxième partie, au centre, est divisée horizontalement en trois blocs verticaux dont l’essentiel de la hauteur est en hors-champ, en partie dissimulés par la bande  du bas de la page. Le premier bloc possède un fond gris sombre et comprend du texte. Le deuxième sur fond blanc comprend du texte et ce qui semble être le haut d’une forme géométrique. Le troisième sur fond jaune orangé comprend aussi du texte et ce qui semble être le haut d’une forme géométrique.</p>
		<p>La dernière partie, la bande, est elle même verticalement divisée en deux bandes comprenant toutes deux une forme géométrique et du texte, le fond de la première est blanc, le fonds de la deuxième est d’un gris très clair.<br>Dans l’ensemble de la page, lorsque le texte est sur un fond sombre il apparaît blanc, lorsqu’il est sur un fond clair il apparaît du même gris très sombre que le fonds des deux premières parties. </p>
		<p>Dans la première partie, en haut à gauche de la page est inscrit en lettres capitales “MATERIAL DESIGN“. Il s’agit du titre, du nom du site. Celui-ci est accompagné d’un symbole, un logo, positionné à sa gauche. Il occupe une surface équivalente à quatre de ses signes, deux de haut et deux de large. Ce symbole est constitué d’un triangle équilatéral blanc pointant vers le bas, superposé à un carré gris clair lui-même superposé à un cercle d’un gris plus sombre. Les trois figures s’assemblent en partageant des mesures équivalentes. Le coté supérieur du triangle et du carré se confondent, tandis que la pointe du triangle correspond au milieu du coté inférieur du carré, les quatre points du carré atteignent le périmètre du cercle sans le dépasser. Cette combinaison forme une sorte de ’M’.</p>
		<p>Juste en dessous de ce titre il y a un petit paragraphe de quatre lignes<br>“Material Design is a unified system that combines theory, resources, and tools for crafting digital experiences.” Juste en dessous de ce paragraphe est inscrit une note, soulignée en petites capitales “WATCH THE VIDEO“. Au survol de la souris le soulignement fais un léger mouvement vers le bas.<br>Le titre et son logo, le paragraphe et sa note occupent la partie<br>supérieure gauche de la page. La partie supérieure droit</p>
		<p>À droite Les trois figures du logo sont répétées et agrandies de façon à occuper un tiers de la hauteur et un peu plus d’un tiers de la largeur de la partie supérieure de la page. Tracées par un contour gris légèrement épais leur surface laisse entrevoir la couleur sombre du fond de la page. Le Triangle, le carré et le cercle du logo ne se superposent plus mais produisent une sorte de chorégraphie combinatoire. Les trois figures glissent horizontalement, parfois tour à tour, parfois au même moments. Échangeant leurs précédentes positions. Tandis que le cercle et le carré se contentent de glisser, le triangle procède en plus à des rotations inattendues. Il ne pivote jamais vraiment mais change de sens en déplaçant ses points le long des différentes mesures des autres formes. Parfois il épouse même entièrement la forme du carré en se rajoutant brièvement un quatrième point. Les trois figures arrêtent leur chorégraphie à intervalles réguliers, semblant choisir une position pour nous montrer pendant un cours moment un signe puis changent de nouveau leur configuration et reprennent une pause etc.</p>
	</section>
</article>
<article class="card-panel article pink lighten-5" id="27apr2011.ux.corporate">
	<aside class="toast pinned" ></aside>
</article>
<article class="card-panel article pink lighten-5" id="08may2011.ten.corporate">
	<aside class="toast pinned" ></aside>
</article>
<article class="card-panel article pink lighten-5" id="13feb2012.ten.compagny">
	<aside class="toast pinned" ></aside>
</article>
<article class="card-panel article pink lighten-5" id="13feb2012.ux.compagny">
	<aside class="toast pinned" ></aside>
</article>
<article class="card-panel article pink lighten-5" id="19jun2012.ten.compagny">
	<aside class="toast pinned" ></aside>
</article>
<article class="card-panel article pink lighten-5" id="31jan2016.10.compagny">
	<aside class="toast pinned" ></aside>
</article>
</section><section id="google_archive">
	<div class="holds-the-iframe">
		<aside class="toast pinned"></aside>
		<iframe id="iframe" src="">

		</iframe>
	</div>
</section>
</main>
<script type="text/javascript">
	window.onload=function(){
		var btn = document.getElementsByClassName('btn');
		var iframe= document.getElementById("iframe");
		var enquete= document.getElementById("enquete");
		for (var i in btn){
			btn[i].onclick=function(){
				var ref=this.getAttribute("ref");
				if(this.getAttribute("timeline")){
					window.ref=this.getAttribute("ref");
					window.refText=this.textContent;
					window.refUrl=this.getAttribute("url");
					var articles = document.getElementsByClassName('article');
					for(var i in articles){
						if(articles[i] instanceof HTMLElement)articles[i].style.display='none';
					}
					document.getElementById(ref).style.display="block";
					document.getElementById(ref).getElementsByClassName('toast pinned')[0].innerHTML=window.refText+'<br>'+this.getAttribute("url");
					iframe.src="iframes/"+ref+".html";
					iframe.parentNode.getElementsByClassName('toast pinned')[0].innerHTML=window.refText+'<br>'+this.getAttribute("url");
					enquete.scrollTo(0,0);
				}else{
					if(this.textContent!=window.refText){
						window.buttonRef=this.getAttribute("ref");
						window.buttonText=this.textContent;
						window.buttonUrl=this.getAttribute("ref");
						this.setAttribute('ref',window.ref);
						this.textContent=window.refText;
						this.style['background-color']='#ee6e73';
						iframe.src="iframes/"+window.buttonRef+".html";
						iframe.parentNode.getElementsByClassName('toast pinned')[0].innerHTML=window.buttonText+'<br>'+this.getAttribute("url");
					}else{
						this.setAttribute('ref',window.buttonRef);
						this.textContent=window.buttonText;
						this.style['background-color']='#26a69a';
						iframe.src="iframes/"+window.ref+".html";
						iframe.parentNode.getElementsByClassName('toast pinned')[0].innerHTML=window.refText+'<br>'+window.refUrl;
					}
				}

			}
		}
		btn[0].click();
	}
</script>
</body>
</html>
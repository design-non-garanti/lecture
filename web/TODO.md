# TODO

## autre

- [X] www.design-non-garanti.pw
- [?] scrapping adress mail avec envoi code du robot scrappeur et renvoi sur le mémoire

## interlude

- [ ] cadeau DRM
- [~] bonne position

##Index

- [~] design page 
- [ ] intégrer les courts articles

##version papier

introduire site, interragir avec le site ( ex drm random words ), introduire le mémoire sur la question de design non garanti ( simondon ) ( voir lundi avec annick ? commencer dimanche )

- [ ] design page <!-- manuel d'utilisation, telecharger manuels jeux video -->
- [?] écrire une introduction contextualisante <!-- voir lire à l'écran -->
- [~] plan du site
- [~] plan du mémoire
- [~] licence design non garanti <!-- trouver une licence qui garanti les usages et écrire l'inverse -->
- [ ] Qu'est ce qu'un mémoire non garanti ?
- [ ] mode d'emploi d'un site internet
- [ ] où est ce mémoire ?
- [ ] Comment a été fait ce mémoire ?

##Inscription

formulaire d'inscription questionnaire qui tend à ouvrir sur références rencontrées durant le mémoire + préparer l'utilisateur à Think (inverse de Don't make me think, katerine hayles)

- [~] refs <!-- completer et integrer dans l'inscription + index -->
- [ ] clean code
	
##21e

détournement du texte de Weiser + commentaire, introduction à la pensée de l'inattention 

- [~] rédiger commentaires
- [~] mettre des trucs video pour dialogues part 2
- [ ] revoir timming sous-titre
- [X] couper la vidéo

##Frere Dominic

étude de cas sur publicité xerox, xerox et pensée universelle

- [X] histoire de xerox
- [ ] revoir la rédaction / mise en page <!--(michelet & moyen age) -->
- [ ] part 2 dominic 2017 <!-- ( éventuellement le mélanger à la part 1, comparer ) -->

##Enquête Google

étude de cas du material design, comment un courant de design emerge d'un esprit d'entreprise

- [X] design page
- [~] rédaction / mise en page <!-- texte sur ux don norman, analyse par page -->

##Apple : une entreprise de la disparition

analyse de l'esprit de la marque à travers pubs conf et objet

- [~] design page
- [ ] rédaction / mise en page

##Pourquoi le texte c'est moche

analyse religieuse, esthétique des interfaces

- [~] scroller <!-- revoir matrix syst pour ça, matrix dans matrix -->
- [ ] rédaction / mise en page

## Une automatisation non débridable

une montre facile à régler, simplicité vers automatisation non-débridable, simplicité discours du contrôle

- [~] design page
- [ ] rédaction / mise en page

## SAV

- [X] design page

##Architecture du site

- [ ] transitions
- [ ] hyperliens
- [ ] intégrer les interludes ( en faire d'autres )

180126 : 4/26 – il reste 14 jours – faire environ 1,8 point par jour

180127 : 4/28 – il reste 13 jours – faire environ 1,8 point par jour

180129 : 4/29 (6 en cours) – il reste 11 jours – faire environ 2,3 point par jour

180130 : 4/29 (6 en cours) - il reste 10 jours - faire environ 2,5 point par jour

180131 : 4/35 (6 en cours) - il reste 9 jours - faire environ 3,4 point par jour

180201 : 4/35 (10 en cours) - il reste 8 jours - faire environ 3,9 point par jour

180202 : 4/35 (10 en cours) - il reste 7 jours - faire environ 4,4 point par jour



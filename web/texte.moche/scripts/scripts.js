		var nWrite=0;

	/*#Generic functions*/

	var style=document.createElement('style');
	document.children[0].children[0].appendChild(style);

	var styleSheet = style.sheet;
	var rulesIndex=0;

	function newCssRule(objetName,CSS){
		var str='{'
		for(var i in CSS){
			str+=' '+i+':'+CSS[i]+'; ';
		}
		str+='}';
		console.log(str);
		styleSheet.insertRule(objetName+" "+str, 0);
		var output = styleSheet.cssRules[rulesIndex].style;
		rulesIndex++;
		return output;	
	}

	function define(objet,from,along){
		for(var i in along){

			objet[i]=check(i,from,along[i]);
		}
	}

	function check(what,where,otherwise){

		if (where instanceof Object) {

			for (var i in where){
				if(i==what){

					return where[i];
				}
				if(where[i]==what){
					return i;
				}
			}
		}

		if (where instanceof Array) {
			var found = [];
			for (var i in where){
				found.push(check(what,where[i],otherwise));
			}
			return found;
		}

		if(otherwise!=undefined)
			return otherwise;
	}

	Keys = function(){

	}

	Keys.prototype.trigger = function(key,down){
		this[key]=down;	
	}

	function sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}


	/*#Debussick variables*/

	var speed=100;

	/*#Debussick functions*/

	/*##Matrix*/

	Matrix = function(options){
		define(this,options,{
			lines:[],
			dom:document.createElement('table')
		});
	}

	Matrix.prototype.clear = async function(){
        for(var l= 1 ; l < this.lines.length-1 ; l++){
            await sleep(17);
            for(var t=2 ; t < this.lines[l].types.length-1 ; t++){

                this.lines[l].types[t].fill({face:' '});
            }
        }
    }

	Matrix.prototype.build = function(size){
		this.x=size[0];
		this.y=size[1];
		for(var y = 0 ; y < size[1] ; y++){

			var line = new Line({matrix:this,id:y});

			for(var x = 0 ; x < size[0] ; x++){

				var type = new Type({line:line,id:x});
				type.fill();
				line.types.push(type);
				line.dom.appendChild(type.dom);

			}

			this.lines.push(line);
			this.dom.appendChild(line.dom);

		}

	}

	Matrix.prototype.rect = async function(string){
		nWrite++;
		for(var j = 0 ; j< this.y ; j++){

			for(var i = 0 ; i< this.x ; i++){
				await sleep();
				var type = this.lines[j].types[i];

				if(i==0){

					if(j==0){
						if(type.face==''){
							type.face=lolfactor.charAt(parseInt(Math.random()*lolfactor.length));
							type.dom.style.position='absolute';
							type.fill();
							await sleep(17);
							type.dom.style.position='inherit';
							type.face='┌';
						}
					}
					if(j>0 && j<this.y-1){
						if(type.face==''){
							type.face=lolfactor.charAt(parseInt(Math.random()*lolfactor.length));
							type.dom.style.position='absolute';
							type.fill();
							await sleep(17);
							type.dom.style.position='inherit';
							type.face='│';
						}
					}
					if(j==this.y-1){
						if(type.face==''){
							type.face=lolfactor.charAt(parseInt(Math.random()*lolfactor.length));
							type.dom.style.position='absolute';
							type.fill();
							await sleep(17);
							type.dom.style.position='inherit';
							type.face='└';
						}
					}
				}
				if(i==this.x-1){
					if(j==0){
						if(type.face==''){
							type.face=lolfactor.charAt(parseInt(Math.random()*lolfactor.length));
							type.dom.style.position='absolute';
							type.fill();
							await sleep(17);
							type.dom.style.position='inherit';
							type.face='┐';
						}
					}
					if(j>0 && j<this.y-1){
						if(type.face==''){
							type.face=lolfactor.charAt(parseInt(Math.random()*lolfactor.length));
							type.dom.style.position='absolute';
							type.fill();
							await sleep(17);
							type.dom.style.position='inherit';
							type.face='│';
						}
					}
					if(j==this.y-1){
						if(type.face==''){
							type.face=lolfactor.charAt(parseInt(Math.random()*lolfactor.length));
							type.dom.style.position='absolute';
							type.fill();
							await sleep(17);
							type.dom.style.position='inherit';
							type.face='┘';
						}
					}
				}
				if(i>0 && i<this.x-1){
					if(j==0 || j==this.y-1){
						if(type.face==''){
							type.face=lolfactor.charAt(parseInt(Math.random()*lolfactor.length));
							type.dom.style.position='absolute';
							type.fill();
							await sleep(17);
							type.dom.style.position='inherit';
							type.face='─';
						}
					}
				}
				type.fill();
			}

		}

		matrix.write(0,1,'<8 to scroll up>','white',parseInt(Math.random()*25+25));
		matrix.write(nLines-1,1,'<2 to scroll down>','white',parseInt(Math.random()*25+25));
		nWrite--;
	}
	var yellow_chars='0123456789';
	var lolfactor='━│┃┄┅┆┇┈┉┊┋┌┍┎┏┐┑┒┓└┕┖┗┘┙┚┛├┝┞┟┠┡┢┣┤┥┦┧┨┩┪┫┬┭┮┯┰┱┲┳┴┵┶┷┸┹┺┻┼┽┾┿╀╁╂╃╄╅╆╇╈╉╊╋╌╍╎╏═║╒╓╔╕╖╗╘╙╚╛╜╝╞╟╠╡╢╣╤╥╦╧╨╩╪╫╬╭╮╯╰╱╲╳╴╵╶╷╸╹╺╻╼╽╾╿▀▁▂▃▄▅▆▇█▉▊▋▌▍▎▏▐░▒▓▔▕■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹►▻▼▽▾▿◀◁◂◃◄◅◆◇◈◉◊○◌◍◎●◐◑◒◓◔◕◖◗◘◙◚◛◜◝◞◟◠◡◢◣◤◥◦◧◨◩◪◫◬◭◮◯☀☁☂☃☄★☆☇☈☉☊☋☌☍☎☏☐☑☒☓☔☕☖☗☘☙☚☛☜☝☞☟☠☡☢☣☤☥☦☧☨☩☪☫☬☭☮☯☰☱☲☳☴☵☶☷☸☹☺☻☼☽☾☿♀♁♂♃♄♅♆♇♈♉♊♋♌♍♎♏♐♑♒♓♔♕♖♗♘♙♚♛♜♝♞♟♠♡♢♣♤♥♦♧♨♩♪♫♬♭♮♯✁✂✃✄✅✆✇✈✉✊✋✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✨✩✪✫✬✭✮✯✰✱✲✳✴✵✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❌❍❎❏❐❑❒❓❔❕❖❗❘❙❚❛❜❝❞❟❠❡❢❣❤❥❦❧';

	Matrix.prototype.write = async function(line,margin,string,color,speed){
		nWrite++;
		var xIndex= margin;
		var yIndex= line;

		for(var i = margin ; i< string.length+margin ; i++){

			if(xIndex>this.x-4){
				this.lines[yIndex].types[xIndex].fill();
				xIndex=margin;
				yIndex++;
			}

			var type = this.lines[yIndex].types[xIndex];

			type.ink=color;


			for(var j = 0 ; j < yellow_chars.length ; j++){
				if(string.charAt(i-margin)==yellow_chars.charAt(j)){
					type.ink='yellow';
				}
			}



			if(string.charAt(i+margin)!=' '){
				type.face=lolfactor.charAt(parseInt(Math.random()*lolfactor.length));
				type.dom.style.position='absolute';
				type.fill();
				await sleep(speed);
				type.face=string.charAt(i-margin);
			}
			type.dom.style.position='inherit';
			type.fill();
			await sleep(speed);
			xIndex++;

		}
		nWrite--;
		console.log('nWrite',nWrite);
	}

	/*##Line*/

	Line = function(options){
		define(this,options,{
			id:undefined,
			types:[],
			matrix:undefined,
			dom:document.createElement('tr')
		});
	}

	Line.prototype.content = function(){
		var content=''
		for(var i=2 ; i<this.types.length-3 ; i++){
			content+=this.types[i].face;
		}
		return content;
	}

	/*##Type*/

	Type = function(options){
		define(this,options,{
			id:undefined,
			shoulder:'transparent',
			face:'',
			ink:[255,255,255],
			line:undefined,
			input:undefined,
			border:[
			[1,'transparent'],
			[1,'transparent'],
			[1,'transparent'],
			[1,'transparent'],
			],
			dom:document.createElement('td')
		});
	}

	function update(dom,type){
		dom.textContent=type.face;
		dom.style.color=type.ink;
		dom.style.backgroundColor=type.shoulder;
	}

	Type.prototype.fill = function(options){
		define(this,options,this);
		if(this.input!=undefined){
			update(this.dom,this.input);
		}else{
			update(this.dom,this);
		}

	}


	/*##Text*/

	function text(type,string,color){ /*quelle est la couleur*/
		for (var i = 0; i < string.length; i++) {
			letter(type.line.types[type.id+i*8],string.charAt(i),color)
		}
	}

	function letter(type,character,color) {
		var dec = character.charCodeAt(0);
		for (var i = 0; i < font8x8[dec].length; i ++) {
			for (var j = 0; j < font8x8[dec][i].length; j ++) {
				if (font8x8[dec][i][j] == 1) {
					type.line.matrix.lines[type.line.id+i].types[type.id+j].fill({shoulder:color});
				}
			}
		}
	}



	var nLines;
	var fontsize;
	var textContent = "Le fait est que ce qui s'affiche sur les écrans des systèmes informatiques est divisé entre le graphique et le textuel. La présence même minime d'éléments visuels hors d'une matrice de signes indique que nous sommes en présence de ce qui s'appelle un *environnement graphique* ou une *interface graphique* – de l'anglais GUI, graphical user interface. À l'inverse c'est lorsque que l'ensemble de ce qui nous est donné à voir est conditionné par une grille que nous nous trouvons devant un *environnement textuel*. L'emploi du terme *graphique* – du grec ancien *graphein* (« écrire ») – en opposition à ce qui est de l'ordre du texte est contradictoire mais ce n'est pas un accident. ☺Un environnement en mode texte (TUI, de l'anglais « Text User Interface », « Textual User Interface » ou encore «Terminal User Interface » est un rétronyme introduit dans le jargon informatique après l'invention des environnements graphiques pour se distinguer des interfaces en ligne de commande.☻ À l'écran *graphique* tend à signifier hors de la grille du texte. La distinction environnement graphique, environnement textuel symbolisait la division entre les utilisateurs d'ordinateurs Mac — un système d'exploitation à interface graphique – et les utilisateurs d'ordinateurs compatibles MS-DOS – un système d'exploitation à interface textuelle. En 1994 Umberto Eco décrit cette opposition comme un déplacement méconnu de l'éthique catholique et protestante. ";
			var height=60;
		var fontsize=(window.innerWidth/height)*1.5;
	window.onload= async function(){

		nLines=parseInt(window.innerHeight/(fontsize*1.2));
		matrix = new Matrix();
		style.td=newCssRule('td',{
			'min-width':window.innerWidth/height+'px !important',
			'max-width':window.innerWidth/height+'px !important',
			color:'white',
		});

		style.tr=newCssRule('tr',{


		});
		matrix.build([height-1,nLines]);
		document.body.appendChild(matrix.dom);
		var html=document.getElementsByTagName('html');
		html[0].style.fontSize=fontsize+'px';
		matrix.rect();

		matrix.write(2,3,'Pourquoi le texte à l\'écran c\'est moche ?','rgb(255,128,128)',parseInt(Math.random()*25+25));

		


		var i = 0;
		var char = 0;
		var line=4;
		var previousSlice=0;
		var color='white';
		for (;;) {
			if (i > textContent.length) break;
			if(textContent.charAt(char)=='☺'){

				if(line<nLines-2){
					matrix.write(line,3,textContent.slice(previousSlice,char),color,parseInt(Math.random()*25+25));
				}
				char++;
				previousSlice=char;
				color='rgb(64,255,128)';
				line+=2;
			}
			if(textContent.charAt(char)=='☻'){

				if(line<nLines-2){
					matrix.write(line,3,textContent.slice(previousSlice,char),color,parseInt(Math.random()*25+25));
				}
				char++;
				previousSlice=char;
				color='white';
				line+=2;
			}
			if(char-previousSlice>height-7){
				console.log(textContent.charAt(char));
				while(textContent.charAt(char)!=' '){
					char--;
				}
				if(textContent.slice(previousSlice,char).charAt(0)==' '){
					previousSlice++;
				}
				if(line<nLines-2){
					matrix.write(line,3,textContent.slice(previousSlice,char),color,parseInt(Math.random()*25+25));
				}
				previousSlice=char;
				line++;

			}else{
				char++;
				i++;
			}
		}
	}

	document.onkeydown=function(e){
		if(e.key == '2'){
			
			if(!nWrite)scrollDown();
		}
	}

	function scrollDown(){
		textContent=textContent.slice(textContent.indexOf(matrix.lines[matrix.lines.length-3].content())-1);
		matrix.clear();
		var i = 0;
		var char = 0;
		var line=3;
		var previousSlice=0;
		var color='white';

		for (;;) {
			console.log(i);
			if (i > textContent.length) break;
			if(textContent.charAt(char)=='☺'){

				if(line<nLines-2){
					matrix.write(line,3,textContent.slice(previousSlice,char),color,parseInt(Math.random()*25+25));
				}
				char++;
				previousSlice=char;
				color='rgb(64,255,128)';
				line+=2;
			}
			if(textContent.charAt(char)=='☻'){

				if(line<nLines-2){
					matrix.write(line,3,textContent.slice(previousSlice,char),color,parseInt(Math.random()*25+25));
				}
				char++;
				previousSlice=char;
				color='white';
				line+=2;
			}
			if(char-previousSlice>height-7){
				while(textContent.charAt(char)!=' '){
					char--;
				}
				if(textContent.slice(previousSlice,char).charAt(0)==' '){
					previousSlice++;
				}
				if(line<nLines-2){
					matrix.write(line,3,textContent.slice(previousSlice,char),color,parseInt(Math.random()*25+25));
				}
				previousSlice=char;
				line++;

			}else{
				char++;
				i++;
			}
		}

	}
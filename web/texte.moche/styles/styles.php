<style type="text/css">
<?php
$dir = "../fonts/";
$fonts = scandir($dir);

foreach ($fonts as $key => $value) {
	if($value == "." || $value == ".."){}else{
		echo "@font-face{\r\nfont-family: \"".$value."\";\r\nsrc: url(\"".$dir.$value."\");\r\n}\r\n";
	}
}
?>


	html,body{
		font-family: "JRUPUNK.ttf";
		margin:0;
		padding:0;
		overflow: hidden;
		background-color: rgb(25,20,30);
		
		width: 100%;
	}
body{

	}

	table{
		position: absolute;
		top: 0; left: 0; right: 0; bottom: 0;
		margin: auto;
	}

	table,tr,td{
		border-collapse: collapse;
		padding:0;
		border:none;
	}

	td{

		background-size:100% 100%;
	}

	#overlay{
		position: absolute;
		z-index: 999;
		width: 100%;
		height: 100%;
		background-image: url("overlay.png");
		opacity: 1;
	}

</style>
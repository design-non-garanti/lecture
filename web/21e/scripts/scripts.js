var sms = document.getElementById('simple-messages-system');
var root = document.getElementById('root');
//var newme = document.getElementById('newme');
//newme.addEventListener('click', postData);

// récupère les data json contenant les messages et appelle createMessages
function getData() {
    console.log('getData');

    var request = $.ajax({
        url: "php/get.php",
        type: "GET",
        async: false, // grado mais permet d'assigner result
        success: function(result){
            while (root.hasChildNodes()) {
                root.removeChild(root.lastChild);
            }
            var data = JSON.parse(result);
            var level = 0;
            createMessages(root,level,data);
        }
    });
}

// parse les messages json et créer les éléments dans le dom
function createMessages(container,level,data,path) {
    console.log('createMessages');

    if (path === undefined) { 
        var path = []; 
    } 
    
    for (item in data) {

        var ipath = [];
        ipath = ipath.concat(path);
        ipath.push(item);
        
        var message = document.createElement('div');
        message.classList.add('message');
        message.dataset.level = level;
        message.dataset.key = item;
        message.dataset.path = ipath.join('-');
        
        var text = document.createElement('div');
        text.classList.add('text');
        //console.log(data[item]);
        text.innerText = data[item]['text'];
        

        var p = document.createElement('p');
        p.className="meta";
        
        var name = document.createElement('span');
        name.classList.add('name');
        name.innerText = data[item]['name'];
        p.appendChild(name);
        

        if(data[item]['mate']){
            p.appendChild(document.createTextNode(":"));
            var mate = document.createElement('span');
            mate.classList.add('mate');
            mate.innerText = '+'+data[item]['mate'];
            p.appendChild(mate);
        }
        p.appendChild(document.createTextNode("-"));

        if (data[item]['mail']) {
            var mail = document.createElement('span');
            mail.classList.add('mail');
            mail.innerText = data[item]['mail'];
            p.appendChild(mail);
            p.appendChild(document.createTextNode("-"));
        }

        var datetime = document.createElement('span');
        datetime.classList.add('datetime');
        datetime.innerText = data[item]['date'] + ' à ' + data[item]['time'];
        p.appendChild(datetime);

        message.appendChild(p);
        message.appendChild(text);
        
        var reply = document.createElement('button');
        reply.addEventListener("click", createPost);
        reply.classList.add('reply');
        reply.innerText = 'Répondre';
        message.appendChild(reply);

        var messages = document.createElement('div');
        messages.classList.add('messages');
        message.appendChild(messages);
        createMessages(messages,level+1,data[item]['messages'], ipath);


        container.appendChild(message);
        
    }
    console.clear;
}

// créer les champs pour répondre
function createPost() {
    console.log('createPost');

    var message = this.parentNode;
    
    if (message.childNodes[4]) {
        message.childNodes[3].childNodes[0].focus();

    } else {

        var post = document.createElement('div');
        post.classList.add('post');

        var text = document.createElement('textarea');
        text.placeholder = '...';
        post.appendChild(text);

        var p = document.createElement('p');
        
        var name = document.createElement('input');
        name.type = 'text';
        name.placeholder = 'Nom';
        var nameCookie = cookies.get('name');
        if (nameCookie) {
            name.value = nameCookie;
        }
        p.appendChild(name);
        
        var mail = document.createElement('input');
        mail.type = 'mail';
        mail.placeholder = 'Mail';
        var mailCookie = cookies.get('mail');
        if (mailCookie) {
            mail.value = mailCookie;
        }
        p.appendChild(mail);

        post.appendChild(p);
        
        var send = document.createElement('button');
        send.addEventListener("click", postData);
        send.innerText = 'Ok';
        post.appendChild(send);
        
        //var cancel = document.createElement('button');
        //cancel.addEventListener("click", postCancel);
        //cancel.innerText = 'Cancel';
        //post.appendChild(cancel);

        message.insertBefore(post, message.getElementsByClassName('messages')[0]);
        text.focus();
    }
}

console.log(document.cookie);

// post data to json
function postData() {
    console.log('postData');
    
    var post = this.parentNode;
    var message = post.parentNode;
    var dataPath = message.getAttribute('data-path');
    var message = post.parentNode;
    if (dataPath) {
        var path = dataPath.split('-');
    }
    
    var data = {};
    var mate = false;
    if(this.parentNode.parentNode.getElementsByClassName('meta')[0]){
        if(this.parentNode.parentNode.getElementsByClassName('meta')[0].getElementsByClassName('name')[0]){
          mate=this.parentNode.parentNode.getElementsByClassName('meta')[0].getElementsByClassName('name')[0];
      }
  }

  if(mate)data.mate = mate.innerText;

  data.text =  post.childNodes[0].value;
  data.name =  post.childNodes[1].childNodes[0].value;
  data.mail =  post.childNodes[1].childNodes[1].value;
  data.messages = [];

  if (dataPath) {
    data.path =  path;
}

cookies.set('name',data.name);
cookies.set('mail',data.mail);

if (!data.text) {
    alert('Texte est vide');
} else if (!data.name) {
    alert('Nom est vide');
} else {
    var request = $.ajax({
        url: "php/post.php",
        type: "POST",
        data: data,
        success: function(result){
            console.log(result);
            getData();
        }
    });
}
}



// BUTTONS
buttons = {};
buttons.el = document.createElement('div');
buttons.el.id = 'buttons';

buttons.zoom = {};
buttons.zoom.el = document.createElement('div');
buttons.zoom.value = 100;

buttons.zoom.in = {};
buttons.zoom.in.el = document.createElement('button');
buttons.zoom.in.el.innerText = '+';
buttons.zoom.in.f = function () {
    if (buttons.zoom.value < 100) {
        buttons.zoom.value += 10;
        root.style.transform = 'scale('+buttons.zoom.value/100+')';
    }
}

buttons.zoom.out = {};
buttons.zoom.out.el = document.createElement('button');
buttons.zoom.out.el.innerText = '-';
buttons.zoom.out.f = function () {
    if (buttons.zoom.value > 10) {
        buttons.zoom.value -= 10;
        root.style.transform = 'scale('+buttons.zoom.value/100+')';
    }
}

buttons.zoom.in.el.addEventListener('click', buttons.zoom.in.f);
buttons.zoom.out.el.addEventListener('click', buttons.zoom.out.f);

//buttons.zoom.el.appendChild(buttons.zoom.in.el);
//buttons.zoom.el.appendChild(buttons.zoom.out.el);
//buttons.el.appendChild(buttons.zoom.el);
sms.appendChild(buttons.el);



// COOKIES
cookies = {};
cookies.set = function (name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
cookies.get = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
cookies.erase = function (name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}


getData();




// video events


var vid = document.getElementById('video');
var sun = document.getElementById('gros_sunny-sun');
var un = document.getElementById('un');
var deux = document.getElementById('deux');
var trois = document.getElementById('trois');
var quatre = document.getElementById('quatre');
var cinq = document.getElementById('cinq');
vid.un =false;
vid.deux =false;
vid.trois =false;
vid.qutre =false;
vid.cinq =false;

vid.onended=function(){
    console.log('test');
    document.getElementById('simple-messages-system').style.display = "block";
    document.getElementById('simple-messages-system').scrollIntoView({behavior: "smooth"});
};


vid.addEventListener('timeupdate',function(){
    //console.log(vid.currentTime);
    if(this.currentTime > 252 && this.currentTime < 253 && !this.sunrise){ //4:12
        sun.style.display = "block";
        sun.style.top = "100vh";
        sunrise();
        this.sunrise=true;
    } else if(this.currentTime > 630 && this.currentTime < 631 && !this.sunset){ // 10:30
        sun.style.display = "block";
        sun.style.top = "-100vw";
        sunset();
        this.sunset=true;
    } 
    else if(this.currentTime > 339 && this.currentTime < 349 && !this.un) { //5:39
        un.style.display = "block";
        this.un=true;
    } else if (this.currentTime < 339 && this.un || this.currentTime > 349 && this.un) {
        un.style.display = "none";
        this.un=false;
    } else if(this.currentTime > 359 && this.currentTime < 363 && !this.deux) { //5:59
        deux.style.display = "block";
        this.deux=true;
    } else if (this.currentTime < 359 && this.deux || this.currentTime > 363 && this.deux) {
        deux.style.display = "none";
        this.deux=false;
    } else if(this.currentTime > 449.5 && this.currentTime < 453.5 && !this.trois) { //7:29.5
        trois.style.display = "block";
        this.trois=true;
    } else if (this.currentTime < 449.5 && this.trois || this.currentTime > 453.5 && this.trois) {
        trois.style.display = "none";
        this.trois=false;
    } else if(this.currentTime > 508 && this.currentTime < 532 && !this.quatre) { //8:38 (630)
        quatre.style.display = "block";
        this.quatre=true;
    } else if (this.currentTime < 508 && this.quatre || this.currentTime > 532 && this.quatre) {
        quatre.style.display = "none";
        this.quatre=false;
    } else if(this.currentTime > 532 && this.currentTime < 581 && !this.cinq) { //8:52
        cinq.style.display = "block";
        this.cinq=true;
    } else if (this.currentTime < 532 && this.cinq || this.currentTime > 581 && this.cinq) {
        cinq.style.display = "none";
        this.cinq=false;
    }
})

//#gros_sunny-sun event



function sunrise(){
    var sun = document.getElementById('gros_sunny-sun');
    var sunRect = sun.getClientRects();
    //console.log(sunRect[0]);
    if(sunRect[0].y+sunRect[0].height>0){
        sun.style.top=sunRect[0].y-1+'px';
        window.setTimeout(function(){
            sunrise();
        },17);
    }else{
        sun.style.display='none';
    }
    
}

function sunset(){
    var sun = document.getElementById('gros_sunny-sun');
    var sunRect = sun.getClientRects();
    console.log(sunRect[0]);
    if(sunRect[0].y<sunRect[0].height){
        sun.style.top=sunRect[0].y+1+'px';
        window.setTimeout(function(){
            sunset();
        },17);
    }else{
        sun.style.display='none';
    }
}
<?php
$dataFile = "../data/data.json";
include "data.php";

# date and time
$date=date("Y/m/d");
$time=date("H:i:s");
$_POST['date'] = $date;
$_POST['time'] = $time;

# new
$dataJson[$itemIdNew] = $_POST;

# save
$dataJsonNew = json_encode($dataJson, JSON_PRETTY_PRINT);
file_put_contents($dataFile, $dataJsonNew) or print_r(error_get_last());

# return id
#echo $itemIdNew;


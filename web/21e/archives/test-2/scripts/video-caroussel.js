// CAROUSSEL

caroussel = {};

caroussel.init = function(objvid, buttons) {
    if (buttons) {
        var prev = document.createElement('button');
        prev.innerText = 'prev';
        prev.onclick = function() {
            caroussel.prev(objvid);
        }
        var next = document.createElement('button');
        next.innerText = 'next';
        next.onclick = function() {
            caroussel.next(objvid);
        }
    }

    var video = document.createElement('video');
    //video.controls = 'controls';        
    video.preload = 'metadata';        
    objvid.video = video;

    if (objvid.sub) {
        var track = document.createElement('track');
        track.kind = 'subtitles';
        track.default = 'default';
        objvid.track = track;
        video.appendChild(track);
    }

    objvid.el.appendChild(video);

    if (buttons) {
        objvid.el.appendChild(prev);
        objvid.el.appendChild(next);
    }
    
    caroussel.update(objvid);
}

caroussel.prev = function(objvid) {
    if (objvid.i == 0) {
        objvid.i = objvid.src.length-1;
    } else {
        objvid.i -= 1;
    }
    caroussel.update(objvid);
    objvid.video.play();
}

caroussel.next = function(objvid) {
    if (objvid.i === objvid.src.length-1) {
        objvid.i = 0;
    } else {
        objvid.i += 1;
    }
    caroussel.update(objvid);
}

caroussel.update = function(objvid) {
    objvid.video.src = objvid.src[objvid.i];
    objvid.video.onended = function() {
        caroussel.next(objvid);
    }
    if (objvid.sub) {
        objvid.track.src = objvid.sub[objvid.i];
        objvid.video.textTracks[0].mode = 'showing';
    }
    if (objvid.i !== 0) {
        objvid.video.play();
    }
}

// VIDEOS

// part_1
part_1 = {};
part_1.i = 0;
part_1.el = document.getElementById('part_1');
part_1.video = undefined;
part_1.track = undefined;
part_1.src = ['videos/Walking tour of Times Square in Midtown Manhattan.mp4']
//part_1.src = [
//    'videos/Vue aérienne de la forêt.mp4',
//    'videos/CU SLO MO Man hitting on wood by axe for splitting wood.mp4',
//    'videos/A woman uses her finger to create riples in water reflected in a blue sky.mp4'
//    ]
//part_2.sub = undefined;

// part_1
part_2 = {};
part_2.i = 0;
part_2.el = document.getElementById('part_2');
part_2.video = undefined;
part_2.track = undefined;
part_2.src = ['videos/Walking tour of Times Square in Midtown Manhattan.mp4']
part_2.src = [
    'videos/Vue aérienne de la forêt.mp4',
    'videos/Dove volant sur ciel bleu.mp4',
    'videos/A woman uses her finger to create riples in water reflected in a blue sky.mp4',
    'videos/Lever du soleil sur la mer, en boucle.mp4',
    'videos/Milford Sound, de Fjordland, Nouvelle-Zélande.mp4'
    ]
part_2.sub = ['videos/un.vtt'];

// part_2
//part_2 = {};
//part_2.i = 0;
//part_2.el = document.getElementById('part_2');
//part_2.video = undefined;
//part_2.track = undefined;
//part_2.src = [
//    'videos/People waking up - Young woman opens her eyes on a sunny morning_2.mp4',
//    'videos/MS Shot of man in bed checking mobile phone and smiling.mp4',
//    'videos/Young women reads text while drinking morning coffee.mp4',
//    'videos/Close up Man using smart phone.mp4',
//    'videos/Young adult female having breakfast and checking smartphone.mp4'
//    ]
//part_2.sub = [
//    'videos/reveille-odeur.vtt',
//    'videos/ipresso-cafe.vtt',
//    'videos/fenetre-itineraires.vtt',
//    'videos/doigt-ecran.vtt',
//    'videos/message-smartcar.vtt'
//    ];



// INIT
//caroussel.init(part_1, 0);
//part_1.video.autoplay = 'autoplay';        
//caroussel.init(part_2, 0);
//part_2.video.controls = 'controls';        
    

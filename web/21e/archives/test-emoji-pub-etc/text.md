Les technologies les plus profondes sont celles qui disparaissent. Elles s'entremêlent dans le tissu
de la vie quotidienne jusqu'à s'y confondre. 

Songez à l'écriture, peut-être la première technologie de l'information. Cette technologie fameuse
pour avoir libéré l'information des limites de la mémoire individuelle en nous donnant la capacité
de représenter symboliquement le langage parlé et de le stocker à long terme. L'écrit est
ubiquitaire.  Les livres, les magazines et les journaux transmettent de l'information écrite tout
comme les panneaux de signalisation, les panneaux publicitaires, les enseignes et devantures de
magasins, les graffitis. Les emballages sont recouverts d'écriture. La présence constante de ces
produits issus des technologies de l'écrit ne nécessite pas une attention active, mais pourtant
l'information à transmettre est prête à être reçue en un coup d'œil. Il est difficile d'imaginer la
vie moderne autrement. 

À l'image de l'écrit, les technologies de l'information à base de silicium, les ordinateurs, font
parti intégrante de l'environnement. L'importance de l'ordinateur ubiquitaire est comparable à celle
de l'invention de l'écriture. Quiconque ne possède pas d'ordinateur a au moins en poche un
smartphone ou chez lui une tablette posée au hasard sur une table basse ou un canapé. Les
ordinateurs sont présents dans les interrupteurs, les thermostats, les chaînes stéréo, les fours.
Ces *objets connectés* sont d'innombrables petits appareils programmés présent dans
l’électroménager, les installations publiques, divers guichets, caméras, bornes, capteurs etc. Ils
permettent d'activer le monde. La plupart des ordinateurs sont en fait invisibles aussi bien dans la
réalité que dans la métaphore. Ces machines sont interconnectées dans un réseau ubiquitaire ; on n'y
pense même plus et cela va de soi, l'ordinateur est largement ordinaire. 

>Souvent emprunté à la langue anglaise, aux mathématiques ou aux sciences son jargon est celui de la
>vie courante : données, mails, likes, algorithmes, crowndfunding, crowdsourcing, cloud, hashtag,
>coworking, e-commerce, data-center, interaction, visualisation, tags, smart university etc. La
>situation est peut-être comparable à une époque où la population largement rurale et imprégnée de
>son environnement en savait autant sur les plantes et les animaux. 

Plus important encore, l'omniprésence des ordinateurs aide à surmonter le problème de la surcharge
d'information. Il y a autant d'informations à la disposition de nos sens lors d'une ballade dans les
bois que dans n'importe quel système informatique. Certaines personnes trouvent même que
l'utilisation de l'ordinateur est aussi relaxante qu'une promenade parmi les arbres. Les machines
sont adaptées à l'environnement humain, elles ne forcent pas les humains à entrer dans le leur. 

Des centaines d'ordinateurs dans une pièce peuvent sembler intimidants au premier abord, tout comme
des centaines de volts circulant à travers les fils dans les murs ont pu le faire. Mais comme les
fils dans les murs, ces centaines d'ordinateurs sont invisibles aux yeux de tous. Nous les utilisons
inconsciemment pour accomplir nos tâches quotidiennes. 

Cette informatique ubiquitaire est graduellement devenue le mode dominant d'accès aux ordinateurs au
cours des vingt dernières années. Comme l'ordinateur personnel, l'informatique ubiquitaire ne permet
rien de fondamentalement nouveau, mais rend tout plus rapide et plus facile à vivre. Avec moins de
tension et de gymnastique mentale, elle transforme les possibles. La publication assistée par
ordinateur, par exemple, n'est pas fondamentalement différente de la composition typographique
classique ou de la photocomposition. Mais la facilité d'utilisation fait toute la différence. 

---

Ce commentaire des principes de l'informatique ubiquitaire retrace ce qu'est la vie dans un monde
plein de gadgets invisibles. Pour appuyer cette présentation je propose de revenir par le biais du
récit sur cet ensemble élaboré qui constitue notre quotidien  :

---

Michel se réveille : il sent l'odeur du café. Il y a quelques minutes, son *iGoogle©* – équipé de
l'application *iPresso : quoi d'autre ?©* – alerté par l'agitation caractéristique de l'éveil, lui
avait discrètement demandé « café? » et il avait marmonné « oui ».

Michel regarde son quartier par la fenêtre. Sur son *iGoogle©* s'affiche une carte indiquant les
itinéraires de ses voisins, reliés en tant que *iPotes* à l'application *coLiving©*. Comme tous les
jours ces trajets semblent très similaires au sien, ça le rassure.

<!-- je trouve ça pas mal : "Privacy conventions and practical data rates prevent displaying video footage, but time markers and electronic tracks on the neighborhood map let Sal feel cozy in her street." comme aujourd'hui il y a bien des conventions de respect de la vie privée etc. mais c'est un peu une blague parce qu'on peut ni les modifier ni les vérifier. -->

Au petit-déjeuner Michel consulte les actualités, il glisse machinalement son doigt sur l'écran de
son *iGoogle©* pendant quelques minutes et fait défiler environ une centaine d'articles. Il n'en lit
aucun mais prend soin de partager les articles qui affichent le plus de votes positifs pour le moins
de vues afin de trouver quelque chose à dire à ses collègues de start-up (reliés en tant que
*workPotes©*).  

Il reçoit un message sur son *iGoogle©* c'est l'entreprise *smartCar©* qui a fabriqué son
ouvre-porte de garage. Il a perdu son manuel d'instruction et leur a demandé de l'aide. Leur réponse
est la suivante : « Les produits *smartCar©* sont si simples d'utilisation qu'ils ne sont pas livrés
avec un manuel d'instruction. Grâce à leur design intuitif, n'importe qui peut les installer en un
clin d'oeil ;). Néanmoins suite à votre demande, pour plus de confort et de sécurité, nous avons
automatiquement pris rendez-vous avec un technicien expert qui viendra vérifier votre produit
*smartCar©*. Nous avons sélectionné le créneau horaire idéal en nous basant sur votre
*iGoogleAgenda©*. Merci de votre fidélité et passez un smartDay :) »

Dans son *smartCar©* qui le conduit sur le chemin du co-working, Michel actualise son profil
*coLiving©* avec un commentaire sur sa situation : « j'ai cru qu'il y avait encore besoin d'un
manuel pour la porte d'un garage lol #smartCar ». Tout *feedback* – aussi succinct soit-il – est
précieux pour le bon design d'un produit et ajoute en échange des points de fidélité au compte
*smartShop©* de son *iGoogle©*. Tandis que son *smartCar©* roule automatiquement sur le trajet le
plus populaire des trajets les plus courts, il ne remarque pas la jeune joggeuse et le vieil homme
obèse qui traversent illégalement la route juste devant son véhicule-intelligent ! **Heureusement
celui-ci évite soigneusement la joggeuse grâce à son système de reconnaissance *moralDriving©*.**

Son *smartCar©* trouve instantanément la meilleure place de parking disponible, la mieux notée des
places parmi les plus proches de l'entrée. Michel arrive au co-working. Alors qu'il entre dans le
bâtiment, il est automatiquement enregistré. Pour plus de confort et de sécurité et pour ne pas
perdre en productivité, le simple fait de rentrer dans le bâtiment fait guise de signature des
conditions d'utilisation des données. En chemin, il s'arrête dans les bureaux de quatre ou cinq
*workPotes©* pour échanger des salutations et des nouvelles. Eux aussi ont soigneusement épluché
leur fil d'actualité au petit-déjeuner et il est bien agréable d'échanger sur les sujets les plus
populaires du moment.  

Michel jette un coup d'oeil à la fenêtre : une journée grise dans *l'inovallée*. Son *iGoogle©* indique 75 % d'humidité et 40 % de chance de se prendre la pluie dans l'après-midi. Pendant ce temps, c'est une matinée tranquille au bureau. Habituellement, l'application *iGoogleWork©* signale au moins une réunion spontanée urgente par matinée. À la place sur son *iGoogle©* s'affiche le commentaire d'un *workPote©* qui a voté contre son dernier profil *coLiving©* et a ajouté un : « Mais tout le monde sait que ça n'existe plus les manuels d'utilisation #BOLOSS ».
Il choisit de jouer à *angryCandy©* sur son *iGoogle©* mais il en connaît d'autres qui auraient surenchéri : habituellement des gens qui ne reçoivent jamais de commentaires, mais qui veulent simplement se sentir impliqués.  

Le témoin près de la porte auquel s'est abonné Michel via l'application *activateWorld©* clignote : le café est prêt. Il se dirige vers la machine à café.

De retour à son bureau, Michel ouvre l'application *coWork©* et *poke* son *workPote©* Michelle du groupe *design*, avec qui il partage un bureau virtuel pendant quelques semaines. Ils travaillent ensemble sur le dernier projet d'innovation. Le partage de bureaux virtuels peut prendre de nombreuses formes - dans ce cas, les deux parties se sont mutuellement données accès à leurs détecteurs d'emplacement, ainsi qu'au contenu et à l'emplacement de leurs *iGoogle©*. Pour plus de sécurité *iGoogle©* conserve toutes les données. Michel choisit de mettre en miniatures sur son *iGoogle©* ce qui s'affiche sur le *iGoogle©* de Michelle. Il n'y prête pas attention, mais il se sent plus en co-working lorsqu'il remarque que les miniatures changent du coin de l'œil ; il peut facilement agrandir si nécessaire. 

Une notification apparaît sur le *iGoogle©* de Michel indiquant que Michelle veut discuter avec lui à propos de la section *guidelines* du rapport d'activité *inovallée2028*
 d'un seul clic Michel accepte et le visage de Michelle s'affiche sur son *iGoogle©*

« J'ai lutté avec ce troisième paragraphe toute la matinée et il a toujours le mauvais ton :( ; tu veux bien y jeter un coup d'oeil ? »

« Pas de problème :) »

Michel s'assoit et analyse le paragraphe grâce à l'application *textExperience©* de son *iGoogle©*. Un mot est automatiquement surligné en rouge : signe qu'il induit une expérience de lecture anormale. Michel en touchant le mot sur l'écran de son *iGoogle©* transmet l'information à Michelle, qu'il commente :

« Je pense que c'est ce terme «ubiquitaire». Il n'est tout simplement pas assez commun, et ça rend à l'ensemble un ton institutionnel. Peut-on reformuler la phrase pour s'en débarrasser? »

"Je vais essayer ça. Au fait, Michel, tu as eu des nouvelles de Marie Hausdorf?"  

"Non. Qui est-ce?"  

"Tu te souviens, elle était à la réunion spontanée urgente de la semaine dernière. Elle m'a dit qu'elle allait te contacter."  

Michel ne se souvient pas de Marie, mais cette réunion spontanée urgente lui dit quelque chose. 

Grâce à l'application *iRemember©* il recherche rapidement dans son *cloud©* à partir des *tags* : rencontre, deux dernières semaines, plus de 6 personnes, nouvelle personne. Différents noms apparaissent dont celui de Marie. Comme c'est souvent le cas dans les réunions spontanées urgentes, Marie a mis à la disposition des autres participants un lien public sur son *profilPro©*.

Son *profilPro* semble très similaire au sien, ça le rassure. Il enverra un mot à Marie pour voir ce qui se passe. Michel est bien content que Marie rende accessible son *profilPro©* en dehors de la rencontre, beaucoup de gens n'ont pas ce réflexe…  

Dans notre monde de l'ordinateur ubiquitaire, les portes ne s'ouvrent qu'aux bons porteurs de badges, les chambres accueillent les gens par leur nom, les réceptionnistes savent réellement où se trouvent les gens, les ordinateurs récupèrent les préférences de ceux qui les utilisent, et les agendas de rendez-vous s'écrivent tout seuls.
L'agenda automatique montre comment une chose aussi simple que de savoir où se trouvent les gens peut entraîner à la connaissance profonde d'une situation.
Révolutionner l'intelligence artificielle, a d'abord consisté à faire entrer les ordinateurs dans la vie quotidienne. 

Dans l'informatique ubiquitaire deux points sont d'importance cruciale: l'emplacement et l'échelle. Il n'y a rien de plus fondamental pour la perception humaine que la géolocalisation, et les ordinateurs ubiquitaires savent où ils se trouvent. Un ordinateur sachant simplement dans quelle pièce il se trouve, adapte son comportement de manière significative c'est ce que l'on nomme l'intelligence artificielle.

De telles machines on fait de l'informatique une partie intégrante et invisible de la façon dont les gens vivent leur vie. Les ordinateurs sont présent dans le monde comme un arrière-plan de l'environnement humain naturel. 

Une telle disparition est une conséquence induite non pas par la nature de la technologie, mais par celle de la psychologie humaine. 

Le philosophe Michael Polanyi l'appelle la "dimension tacite"; 

le psychologue TK Gibson l'appelle "invariants visuels"; 

les philosophes Georg Gadamer et Martin Heidegger l'appellent "l'horizon" et le "à portée de main", 

Le chercheur John Seely Brown l'appelle la "périphérie". 

Chaque fois que les gens apprennent suffisamment bien quelque chose, ils cessent d'en être conscients. 

Lorsque vous regardez une publicité dans la rue, par exemple, vous absorbez son information sans l'avoir consciemment lu… 

Tous nous rappelle que ce n'est que lorsque les choses disparaissent que nous sommes libres de les utiliser sans y penser et donc que nous pouvons nous concentrer sur de nouveaux objectifs.

<html>
<head>
	<title></title>
	<style type="text/css">

	<?php
	$dir = "fonts/";
	$fonts = scandir($dir);


	foreach ($fonts as $key => $value) {
		if($value == "." || $value == ".."){}else{
			echo "@font-face{\r\nfont-family: \"".$value."\";\r\nsrc: url(\"".$dir.$value."\");\r\n}\r\n";
		}
	}
	?>
	*{
		font-family: sans-serif;
		font-weight: normal;
	}
	html, body{
		margin : 0;
		padding: 0;

	}

	html{
		color: white;
		width: 100%;
		height: 100%;
		overflow-x: hidden;
	}

	body{
		width: 100%;
		height: 100%;
		display: flex;
		flex-direction: column;
		justify-content: center;
	}

	main{
		width: 100%;
		top: -4rem;
		position: absolute;
	}
	main article {

		width: 800px;
		max-width: 80%;
		margin: auto;
		
	}
	sup{
		/*font-family: "WorkSans-thin.ttf";*/
		text-transform: lowercase;
	}
	h1{
		margin: 2rem;
		line-height: 5rem;
		/*font-family: "WorkSans-thin.ttf";*/
		font-size: 6rem;
	}
	#intro p{
		margin:2rem;
		margin-top: 5rem;
		margin-bottom: 1rem;
		/*font-family: "WorkSans-Regular.ttf";*/
		font-size: 2rem;
		text-shadow: 0px -1px 0px black, 0px 1px 0px black, 1px 0px 0px black, -1px 0px 0px black;
	}
	aside{
		margin-left: 2rem;
		/*font-family: "WorkSans-Medium.ttf";*/
	}
	i{
		/*font-family: */
	}
	#part_2{
		padding: 2rem;
		text-align: center;
		left: 0;
		width: calc(100% - 4rem);
		background-color: rgb(240,240,240);
	}
	hr{
		background-color: white;
		position: absolute;
		left: calc(50vw - 400px);
		width:calc(800px - 1rem);
		height: 2rem;
		outline: none;
		border-style: none;
		border-bottom: solid;
		border-top: solid;
		border-width: 1px;
		border-color: black;
	}
	.encart{
		margin-top: 2rem;
		margin-bottom: 2rem;
		outline: solid;
		padding:1rem;
		/*font-family: "WorkSans-Medium.ttf";*/
	}
	#left_button{
		display: inline;
		vertical-align: middle;
		background-color: red;
	}



	#cartoon_overlay{
		pointer-events: none;
		z-index: 999;
		left: -4950vw;
		width: 10000vw;
		height: 10000vw;
		position: fixed;
		background-image: radial-gradient(transparent 1%, black 1%);
		/*background-color:'black';*/
	}
	#backvideo{

		height: 100vh;
		z-index: -999;
		position: fixed;
	}
	#left_panel{
		border-right: solid;
		border-width: 1px;
		border-color: black;
		width: calc(50vw - 400px);
		background-color: white;
		position: fixed;
		height: 100vh;
		left:0;
		overflow: hidden;
	}
	#right_panel{
		border-left: solid;
		border-width: 1px;
		border-color: black;
		width: calc(50vw - 400px);
		background-color: white;
		position: fixed;
		height: 100vh;
		right:0;
		overflow: hidden;
	}
	#end{
		background-color: white;
		color: black;
	}
	#end *{
		background-color: white;
	}
	main article video{
		outline: solid;
		outline-width: 1px;
		outline-color: black;
		width: 100%;
	}
	#video_overlay{
		pointer-events: none;
		z-index: 999;
		position: fixed;
		width: 100%;
		height: 100%;
	}
	#story{
		position: absolute;
		margin:auto;
		left:0;
		right:0;
		top:0;
		bottom: 0;
		width: 50vw;
		min-width: 800px;
		height: 50vh;
	}
	#story video{
		width: 100%;
	}
	#left_panel video{
		float: left;
	}
	#right_panel video{
		float: left;
	}
	#left_panel video,#right_panel video{
		height: 100vh;
	}

#descr{
	background-color: white;
	text-shadow: none;
	color: black;
	font-size: 12pt;
}
</style>
</head>
<body>
	<div id="video_overlay">	
		<div id="story">
			<!--
			<video loop="true" controls src="485544086.mp4"></video>
		-->
	</div>
</div>
<div id="cartoon_overlay"></div>

<div id="left_panel">
	<!--
	<video src="481953611.mp4" loop="true" autoplay="true"></video>
-->
</div>
<div id="right_panel">
	<!--
	<video src="481953611.mp4"  loop="true" autoplay="true"></video>
-->
</div>
<video id="backvideo" loop="true" autoplay="true" src="481953611.mp4"></video>

	<!--
	<header><img src="logo_petit.png">
	
		<section>
			

		</section>
		
		<section>
			<nav>
				<ul>
					<li>
					</li>
					<li>
					</li>
					<li>
					</li>
					<li>
					</li>
					<li>
					</li>
					<li>
					</li>
					<li>
					</li>
				</ul>
			</nav>
		</section>
	</header>
-->

<main>

	<article>

		<!--
		<aside>
			Wark Meiser 
			<time>17.01.22 17:20 PM</time>.
		
		
		</aside>

		
	-->
	<section id="intro">
	<p>Les technologies les plus profondes sont celles qui disparaissent, celles qui s'entremêlent dans le tissu de la vie quotidienne jusqu'à s'y confondre. </p><hr><p>
		Songez à l'écriture, peut-être la première technologie de l'information. Cette technologie fameuse pour avoir libéré l'information des limites de la mémoire individuelle en nous donnant la capacité de représenter symboliquement le langage parlé et de le stocker à long terme. L'écrit est ubiquitaire. 
	</p><hr><p>
		<!-- Bien sûr --> Les livres, les magazines et les journaux transmettent <!--de--> l'information écrite tout comme les <!-- mais aussi les --> panneaux de signalisation, les panneaux publicitaires, les enseignes et devantures de magasins, les graffitis. Les emballages sont recouverts d'écriture. La présence constante de ces produits <!-- issus --> des technologies de l'écrit ne nécessite pas une attention active, mais pourtant l'information à transmettre est prête à être reçue en un coup d'œil. Il est difficile d'imaginer la vie moderne autrement.
	</p><hr><p>
		À l'image de l'écrit, les technologies de l'information à base de silicium – les ordinateurs – font parti intégrante de l'environnement. L'importance de l'ordinateur ubiquitaire est comparable à celle de l'invention de l'écriture. <!-- GR propose : «dans les pays développés --> Quiconque ne possède pas d'ordinateur <!-- personnel --> a au moins en poche un smart-phone ou chez lui une tablette posée au hasard sur une table basse ou un canapé. <!-- GR propose : «Maintenant» --> Les ordinateurs sont présents dans les interrupteurs, les thermostats, les chaînes stéréo, les fours. Ces <i>objets connectés</i> sont d'innombrables petits appareils programmés présents dans l’électroménager, les installations publiques, divers guichets, caméras, bornes, capteurs etc. Ils permettent d'activer le monde. La plupart des ordinateurs sont <!-- en fait, GR «devenus» ou ' '? --> invisibles aussi bien dans la réalité que dans la métaphore. Ces machines sont interconnectées dans un réseau ubiquitaire ; on n'y pense même plus et cela va de soi, l'ordinateur est largement ordinaire.
	</p><hr><p>
		Souvent emprunté à la langue anglaise, aux mathématiques ou aux sciences, son jargon est celui de la vie courante : data, mails, likes, algorithmes, crowndfunding, crowdsourcing, cloud, hashtag, coworking, e-commerce, data-center, interaction, visualisation, tags, smart university etc. La situation est peut-être comparable à une époque où la population largement rurale et imprégnée de son environnement en savait autant sur les plantes et les animaux. Plus important encore, l'omniprésence des ordinateurs aide à surmonter le problème de la surcharge d'information. <!-- GR tombe dans le panneau : « la provoque et l'intensifie surtout ! -->  Il y a autant d'informations à la disposition de nos sens lors d'une ballade dans les bois que dans n'importe quel système informatique. Certaines personnes trouvent même que l'utilisation de l'ordinateur est aussi relaxante qu'une promenade parmi les arbres. </p><hr><p>Les machines sont adaptées à l'envirnt humain, elles ne forcent pas les humains <!-- GR : «réfractaires» (pas mal) --> à entrer dans le leur.</p><hr><p> Des centaines d'ordinateurs dans une pièce peuvent sembler intimidants au premier abord, tout comme des centaines de volts circulant à travers les fils dans les murs ont pu le faire. Mais comme les fils dans les murs, ces centaines d'ordinateurs sont invisibles aux yeux de tous. Nous les utilisons inconsciemment pour accomplir nos tâches quotidiennes. Cette informatique ubiquitaire est graduellement devenue le mode dominant d'accès aux ordinateurs au cours des vingt dernières années. Comme l'ordinateur personnel, l'informatique ubiquitaire ne permet rien de fondamentalement nouveau, mais rend tout plus rapide et plus facile à vivre. <!-- GR tombe dans le panneau : « discours positiviste … depuis la diffusion massive des smartphones et ordis portables (?) --> Avec moins de tension et de gymnastique mentale, elle transforme les possibles. La publication assistée par ordinateur, par exemple, n'est pas fondamentalement différente de la composition typographique classique ou de la photocomposition. <!-- GR (dur à lire) : «les utilisateurs changent? et transforment? chacun en "secrétaire-imprimeur-editeur"» --> Mais la facilité d'utilisation fait toute la différence. 
		</p><hr><p>
			Ce commentaire des principes de l'informatique ubiquitaire retrace ce qu'est la vie dans un monde plein de gadgets invisibles. Pour appuyer cette présentation je propose de revenir par le biais du récit sur cet ensemble élaboré qui constitue notre quotidien  :
		</p>
	</section>
			<section id="descr">
		<video src="blank.mp4" controls></video>

			<p>
				Dans notre <!-- Gr : «le» --> monde de l'ordinateur ubiquitaire, les portes ne s'ouvrent qu'aux bons porteurs de badges, les chambres accueillent les gens par leur nom, les réceptionnistes savent réellement où se trouvent les gens, les ordinateurs récupèrent les préférences de ceux qui les utilisent, et les agendas de rendez-vous s'écrivent tout seuls.
			</p><p>
				L'agenda automatique montre comment une chose aussi simple que de savoir où se trouvent <!-- Gr : les abonnés || adhérents || utilisateurs, Mb : les citoyens || le peuple --> peut entraîner à la connaissance profonde d'une situation.
				Révolutionner l'intelligence artificielle, a d'abord consisté à faire entrer les ordinateurs dans la vie quotidienne. 
			</p><p>
				Dans l'informatique ubiquitaire deux points sont d'importance cruciale: l'emplacement et l'échelle. Il n'y a rien de plus fondamental pour la perception humaine que la géolocalisation, et les ordinateurs ubiquitaires savent où ils se trouvent. Un ordinateur sachant simplement dans quelle pièce il se trouve, adapte son comportement de manière significative c'est ce que l'on nomme l'intelligence artificielle.
			</p><p>
				De telles machines on fait de l'informatique une partie intégrante et invisible de la façon dont <!-- les citoyens || le peuple --> vivent leur vie. Les ordinateurs sont présent dans le monde comme un arrière-plan de l'environnement humain naturel. 
			</p><p>
				Cette disparition est une conséquence induite non pas par la nature de la technologie, mais par celle de la psychologie humaine. 
			</p><p>
				Nous n'avons pas conscience des pratiques ou des objets auxquelles nous sommes habitués. Lorsque vous regardez <!-- un panneau de signalisation || un panneau publicitaire -->, par exemple, vous en absorbez l'information sans l'avoir consciemment lu.
			</p><p>
				Le philosophe Michael Polanyi l'appelle la "dimension tacite"; 

				le psychologue TK Gibson parle d'"invariants visuels"; 

				les philosophes Georg Gadamer et Martin Heidegger évoquent "l'horizon" et le "à portée de main", 

				Pour le chercheur John Seely Brown il s'agit de la "périphérie". 
			</p><p>
				Ce constat nous rappelle qu'il est nécessaire que les objets disparaissent pour que nous soyons libres de les utiliser sans réfléchir et pouvoir ainsi nous concentrer sur de nouveaux objectifs.
			</p>
		</section>
	</article>
</main>
<script type="text/javascript">
	function sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}
	async function cartoon_transition(){

		var dom = document.getElementById('cartoon_overlay');
		var size = dom.getClientRects()[0].width;
		var left = dom.getClientRects()[0].x;

		var size_0=window.innerWidth;
		var left_0=0;

		console.log(dom,size,left,size_0,left_0);

		dom.style.left=parseInt(window.innerWidth/2)+'px';
		dom.style.width=window.innerWidth+'px';
		dom.style.height=window.innerWidth+'px';
		dom.style.backgroundColor='transparent';

		while(size_0<size){
			await sleep(17);

			size_0+=2000;
			left_0-=1000;

			if(size_0>size){
				dom.style.backgroundImage=false;

			}

			dom.style.left=left_0+'px';
			dom.style.width=size_0+'px';
			dom.style.height=size_0+'px';

		}
		dom.style.display='none';
		document.firstElementChild.style.overflowY='visible';
		window.cartoonEvent=true;
            //window.location.assign("entravaux.html");

        }
        window.onload=function(){
        	cartoon_transition();
        }
    </script>
</script>
</body>
</html>

# À la recherche du design perdu

## L'investigation d’un utilisateur sur lui-même

### Mais qui a tué l’utilisateur ?

Usagers, utilisateurs, consommateurs, acheteurs, cibles, masses, peuple, homme ordinaire, (Mr)tout le monde, ménagères, chacun et personne… Un terme qui permet de classer un ensemble d’individu est le reflet d’une stratégie élaborée par d’autres individus. Bien qu’universels – hérités de l’aube de la modernité<span class="link" data-id="certeau-modernite">&#42;</span> – ces termes ne témoignent pas des mêmes idéologies, des mêmes enjeux. <span class="link" data-id="turing-complete-user">*Turing Complete User*</span> d’Olia Lialina raconte une histoire de l’utilisateur, ou plutôt de comment le terme *user* est progressivement remplacé par le terme *people* dans l’industrie de l’ordinateur depuis la fin des années 90. Dans une industrie où les produits manufacturés sont camouflés au maximum, Olia Lialina rappelle qu’être appelé utilisateur manifeste la présence de l’ordinateur. La présence d’un système programmé que l’on est en train d’utiliser. Cette disparition résulte directement de l’influence de théoriciens du design proches des acteurs de la Silicon Valley. Un milieu dans lequel le terme utilisateur va jusqu’à devenir un tabou <span class="link" data-id="u-word">*the U word*</span>. Pour Donald Norman le terme *people* (les gens, le peuple, les citoyens) est préférable à *user*. <span class="link" data-id="design-for-people">« We design for people, we don’t design for users »</span>. Pour lui est également préférable de substituer l’appellation des différentes opérations proposées au destinataire d’une interface par le terme *experience*. L’ordinateur n’est plus utilisé : il est ressenti, donné. Quand bien même l’UX dans le discours est défini comme un design centré sur l’usager, il tend à assimiler le contenu de l’ordinateur à ceux d’autres médias <span class="link" data-id="pc-as-book-as-tv">(le livre, la télévision, le cinéma, le spectacle)</span>. Pour Olia Lialina cette substitution est un problème ; c’est une disparition. Cette disparition des machines implique la disparition de ceux qui les manipulent et vice-versa. Il n’y a rien à voir donc rien n’est fait, ni à faire. Le destinataire de l’industrie de l’ordinateur n’est plus considéré comme acteur de celle-ci. l’autorité sur ces objets n’est plus partagée entre ceux qui les inventent et ceux qui en ont l’usage.

### Courte histoire du design des interfaces dites graphiques

l’UX est un design qui apparaît dans la Silicon Valley à la fin des années 80 suite aux premières interfaces dites “graphiques” et à l’élaboration de concepts tels que l’*information appliance* et l’ordinateur ubiquitaire<span class="link" data-id="ux-genesis">&#42;</span>. Le terme *user-experience* – généralement traduit en français par “expérience-utilisateur”, nous le désignerons ici par sa traduction ou par son acronyme UX – est formulé par Don Norman à la suite de la parution de son livre *Design of every day things*<span class="link" data-id="ux-genesis">&#42;</span>. Concernant le design de ce qui s’affiche sur les écrans “ses graphismes”<span class="link" data-id="obeissance-graphique-resistance-textuelle">&#42;</span> l’UX flirt avec le *flat-design*<span class="link" data-id="flat-ux">&#42;</span> et le *skeuomorphisme*<span class="link" data-id="skeu-ux">&#42;</span> qui sont les deux modes “graphiques” dominant des interfaces graphiques. Ces deux courants s’incarnent et se suivent dans les années 2000<span class="link" data-id="flat-skeu-history">&#42;</span>. Bien que cela ne soit pas clairement de notoriété publique, ces deux courants sont largement conditionnés par les acteurs du design de quelques entreprises de l’industrie de l’ordinateur, liés à la sortie de générations d’appareils ou d’OS<span class="link" data-id="flat-skeu-history">&#42;</span>. Exemple : skeumorphisme avec l’Apple-store, le flat-design avec la sortie de windows 10, le Material Design avec la version Lollipop d’android. Ce qui pose un contexte de quasi monopole d’influence sur le graphisme à l’écran par deux ou trois entreprises depuis les années 70 au fil des mises jour systèmes et sorties d’appareils. Le skeuomorphisme est en quelque sorte l’apport des années Don Norman chez Apple<span class="link" data-id="flat-skeu-history">&#42;</span> alors que la période Jonathan Ive amène le flat-design influencé par Dieter Rams, le style suisse international<span class="link" data-id="flat-skeu-history">&#42;</span>. Susan Kare est peut-être l’influence la plus forte avant l’émergence du flat et du skeuomorphisme<span class="link" data-id="flat-skeu-history">&#42;</span>. 

Il ne s’agit pas ici de composer l’historiographie exhaustive des différents acteurs qui ont influencé le design des interfaces dites “graphiques”<span class="link" data-id="obeissance-graphique-resistance-textuelle">&#42;</span> mais plutôt de proposer un regard critique sur ces design qui n’en sont pas vraiment, du fait de l’aspect superficiel de leur champ de modification qui ne s’en tient bien souvent qu’à l’apparence. En effet la structure des interfaces n’est pratiquement jamais modifiée ou prise en compte dans ces courants : les structures restent les mêmes sont le bureau, la page web, formulaires, buttons, dossiers etc.<span class="link" data-id="obeissance-graphique-resistance-textuelle">&#42;</span> Il est également important de rappeler que ce ne sont pas les seuls «courant de design» des interfaces graphiques. On peut penser au [retro](nitrome.com)<span class="link" data-id="retro">&#42;</span>, au [brutalisme](http://brutalistwebsites.com/)<span class="link" data-id="brutalisme">&#42;</span>, au *flash*<span class="link" data-id="deep-sites">&#42;</span> ou à de nombreux genres hybrides ( plus ou moins éloignés du flat et du skeuomorphisme avec par exemple la mode des effets parralax<span class="link" data-id="parralax">&#42;</span> fin 2000, ni skeuomorphique ni flat ). En 2014 le *flat-design* et le *skeuomorphisme* forment une sorte de synthèse avec le Material design<span class="link" data-id="a-la-recherche-du-design-perdu">&#42;</span>, revendiqué de l’idéologie UX. Le Material Design est un produit Google. Qu’une entreprise seule soit propriétaire d’un courant de design, c’est peut-être une première dans l’histoire du graphisme. Il s’agit bien d’un courant voir d’une discipline dans la mesure où le Material Design est largement réapproprié, accrédité, transmis etc. par des professionnels, des experts du design mais également par des amateurs, des disciples, des fidèles (exemple : la mise en page de ce site faite avec une une feuille de style css faite par un amateur du Material Design destinée à rendre Material une page web [materialize](http://materializecss.com/)<span class="link" data-id="material-fans">&#42;</span>. Son influence est très visible : le design des grandes plates-formes web en est souvent inspiré (quand elles ne sont pas directement propriétés de Google)<span class="link" data-id="material-fans">&#42;</span>. Google l’a impulsé et continue régulièrement de le mettre à jour voire à en changer radicalement le style<span class="link" data-id="a-la-recherche-du-design-perdu">&#42;</span>. Un produit pensé en Material Design est un produit Google. A l’image des produits Apple, c’est un design qui transforme un objet en marque. 

Ainsi le prétexte de cette investigation est le suivant : 

1. Olia Lialina associe mort de l’utilisateur et UX. 

2. l’UX est un design, le Material Design fabriqué par Google est une forme récente et influente d’UX appliqués aux interfaces dites «graphiques». 

3. Le Material Design serait donc un design qui tue l’utilisateur et qui émerge d’une entreprise. La question est la suivante : comment une entreprise peut-elle fabriquer un design, et comment ce processus est relié à la négation de l’usager ?

En parcourant les différents sites web de Google, on se rend rapidement compte que le terme design fait partie intégrante du discours/esprit de l’entreprise (cette remarque vaut pour d’autres sites qui ne sont pas ici traités).

### Le discours d’entreprise

Sur le site internet d’un groupe ou d’une entreprise, consacrer un ensemble de pages à décrire sa structure et ses enjeux est une tradition. Ces pages sont généralement indexés sous le terme de *compagny*, *corporate*, (la société, l’entreprise) *compagny info*, *about* *about us* (à propos, qui sommes nous). Au prétexte, à première vue purement factuel, de décrire et de renseigner se dégage l’occasion d’assurer l’authenticité d’un discours soit d’une stratégie.

### Comment se servir de ce contenu

En raison de sa duplicité, à l’échelle de sa formulation, le contenu même de ces pages n’est pas très scrupuleux et il serait hasardeux de s’en servir pour argumenter sur les réelles ambitions de l’entité qu’il concerne ou de son auteur. Une observation contextualisée plus largement, lorsque ce contenu évolue dans le temps, varie, s’adapte, est corrigé dans la durée, fait apparaître à la fois ce qu’il a de régulier ou d’instable. Il est alors révélateur en ce qu’il fait trace d’une trajectoire, d’une direction : une trame plus profonde ; la structure d’un système, d’une stratégie. 

La méthode d’investigation a été la suivante : surfer via les archives de [Waybackmachine](https://archive.org/web/) sur les pages indexées “corporate” sur le site de Google, pour voir quand et comment apparaît le terme design dans le discours de l’entreprise. Comment le Material Design à t-il été préparé, de quelle pensée provient-il ?

## 05may1999

### Le site de Google

Google.com existe depuis 1998. La page “compagny” apparaît sur le site en février 1999. Avant novembre 1999 Google est encore en version beta et s’appelle “Google! Inc”.

### un moteur de recherche

A cette époque Google est uniquement un moteur de recherche, ce moteur est présenté comme un projet dans lequel on peut investir :

Le terme design est absent de la page mais on apprend que les co-fondateurs Sergey Brin et Larry Page sont rattachés au Stanford University Computer Science Department, attestant de la scientificité de la démarche de l’entreprise<!--courte histoire de la sillicon-->. Il est précisé que l’entreprise émerge suite à trois ans de recherche en Data-mining c’est-à-dire l’exploration de quantités massives de données pour en extraire des motifs (patterns). Cette démarche rationalisante est déjà proche du discours <!--lequel ?--> d’un design qui cherche à maîtriser ou à conditionner un environnement.

La première chose que Google design c’est son nom. Il nous est dévoilé que le terme “Google” est une « meilleure » prononciation du nombre *googol*(10^100, un nombre gigantesque). Il est suggéré que ce nom a été choisi car il est la métaphore de l’objectif des créateurs de Google : rendre accessible à tous d’énormes quantités d’information (énormes comme un googol). Il est ajouté que Google sonne cool (meilleur et cool ?) et n’a que six lettres (peu de lettres). Le mot Google est sûrement «meilleur» que Googol car sa forme est proche de celle que l’on attend d’un mot (anglais en l’occurence), “google” rappelle “example”, “doodle”, “poodle”, “feeble”, “bubble”… il apparaît familier. Googol n’est pas cool car il demande un effort : étranger, il ne livre pas dans sa graphie le mode d’emploi de sa prononciation qui devient plus lente. À l’inverse Google est cool car il est exotique, inhabituel il reste néanmoins très familier. Googol devient d’autant plus familier que sa reproduction en Google vient redoubler ou « masquer » un sens trop complexe ou trop abstrait pour être « facile d’accès » (comme ces énormes quantités de données).

## 08nov1999

### Update

#### changement de logo

Google ne s’appelle plus « Google! Inc » mais « Google », outre cette émancipation de l’influence de Yahoo!<span class="link" data-id="yahoo">&#42;</span>, Google<span class="link" data-id="google-logo">&#42;</span> possède maintenant un nouveau logo, toujours dans une font serif, toujours bleu-rouge-jaune-bleu-vert-rouge, mais beaucoup moins gras, aux empattements droits ( autrefois droits à forts congés ), le *e* est maintenant incliné.

#### complexification de la page (8 parties) 

La page *compagny info* possède désormais un contenu bien plus volumineux (environ 10000 caractères pour 2200 précédemment ) et sa structure se complexifie et contient désormais huit parties distinctes. 
Ce qui était autrefois divisé en trois parties – *the compagny*, *the name* et *contact* – est désormais réparti entre *Our Mission*, *Our Company*, *Google's Approach to Searching the Internet*, *Google Search Services*, *Special Searches*, *Our Business*, *About Google Inc* et *The Meaning of Google*.
Le contenu de *the compagny* est désormais dispatché entre *Our compagny* et *About Google Inc*, le contenu de *The name* est déplacé dans *the Meaning of Google.*.

#### apparition du terme _design_ 

(5 occurrences)

[1] Le terme _design_ apparaît dans *Googles’s Approach to Searching the Internet*, une partie largement consacrée à la conception à la fois au sens d’élaboration et de point de vue. Cette première occurrence est associée au terme *elegant* et est dissociée du terme *innovative* qui est associé au terme *search technology*.

[2] Dans *An Elegant, Easy-to-Use Interface* _design_ concerne l’interface du moteur de recherche Google. _Design_ dans cette interface est qualifié _d’épuré_, de _clair_ et son rôle est de _faciliter_ l’entrée de requêtes pour les utilisateurs.

[3] _Design_ apparaît également dans *Googles Search Services* : *Google SiteSearch* est _designé_ pour trouver de l’information contenue au sein d’un site spécifique.

[4,5] _Design_ apparaît de nouveau pour rappeler la fonction de *Google SiteSearch* dans la section *Our Business*, puis il est associé au terme “graphiquement” concernant la publicité de Google qui serait graphiquement _designée_ pour améliorer l’expérience de recherche de l’utilisateur. Est d’ailleurs associé au terme publicité les mêmes adjectifs que pour le _design_ du moteur de recherche Google : _épuré_ et _clair_.

#### apparition du terme _experience_

(3 occurrences)

Le terme _expérience-utilisateur_ est absent en 1999, néanmoins on peut trouver trois occurrences du terme _expérience_ – seul.

[1] Dans *Our Compagny* Il est expliqué que Google est à la recherche de la meilleure _expérience_ de recherche sur le net.

[2] On retrouve le terme dans la section *Our Business*, également associé au terme _search_, _search-experience_ semble être un mot-valise pour désigner l’acte d’être en train de rechercher. Cette association semble assimiler cet action à un voyage touristique ou l’appréciation d’un spectacle.

#### apparition du terme user

(19 occurrences)

On trouve _user_ – seul – en grande quantité, pour l’instant aucune trace du terme _people_.

Le terme _user_ est présent dans pratiquement toutes les sections du texte. Il associé à des termes révélateurs d'une détresse _aidé_, _offre_, _facile_ ou _facilitation_ : “Google helps its users”, “designed to make it easy for users”, “Google helps users”, “Google offres its advanced search technology to individual users”, “Google offers special searches for users”…

Il est également très souvent associé à des termes définissant sa condition – _désir_, _besoin_, _effort_, _devoir_ :  “_users_ *must* rely on search technology”, “as _users_ *strive* to find information”, “_users_ can easily tell if the corresponding web pages will satisfy their *need*”, “more *relevant* to _user_”, “users who want to find information”, “users who want to search the web”, “more likely to be relevant to a users needs”…

Dans sa « mission » Google ne se contente pas de satisfaire les besoins des utilisateurs, il les définit.<span class="link" data-id="jobs-preaching">&#42;</span>

### Jakob Nielsen

<!--
Cette pensée peut-être résumée en trois points :

1. Penser le web comme un outil au service du marketing
2. Penser le design comme un outil au service du marketing
3. Penser l’utilisateur comme un outil au service du marketing

Exemple : 

Je dissimule sur internet un script qui a pour fonction d'observer le temps que passent les utilisateurs sur un site et quels sont les liens sur lesquels ces derniers cliquent.

Ce script peut me renseigner sur un grand nombre de mes questions concernant l'utilisateur :

- Est-il en train de scroller ?

- Quelle est la résolution en pixels de son écran ?

- Quel est son navigateur ?

- Quel est son système d'exploitation ?

- Quel nombre de liens au sein du web renvoient sur le site qu'il utilise ?

Mais il a des limites, Il ne peut pas me renseigner directement sur certaines choses comme :

- La taille d'un écran

- Le modèle ou les modifications d'un appareil

- Il n'est pas capable d'émettre un jugement concernant la qualité informative, esthétique ou littéraire d'un site ni si son utilisateur ressent une émotion ou une sensation particulière

Le script me fait comprendre que :

- en moyenne les utilisateurs ne scrollent pas beaucoup

- qu'ils ne lisent pas beaucoup car ils quittent bien trop rapidement un site au contenu textuel dense pour l'avoir lu

- qu'ils ont tendance à quitter plus rapidement un site à la programmation singulière

Bien-sûr je suis conscient de mon propre désir qui est le suivant : « j'aimerais que les utilisateurs lisent intégralement le contenu informatif de tout les sites web et qu'ils cliquent sur la totalité des liens qu'ils contiennent y-compris les publicités. »

Je sais également certaines choses évidentes ou inhérentes aux 
propriétés techniques d'un site web : 

- Les utilisateurs ne peuvent pas scroller et cliquer

- Les utilisateurs ne cliquent pas lorsqu'ils lisent

Puis-je en conclure que ce n'est pas leur désir de scroller, de lire ou de ne pas consulter un site à la programmation singulière ?

-->

Durant les années 90 Jakob Nielsen<span class="link" data-id="jakob-nielsen">&#42;</span> est très prolifique quant à la conception de règles fondamentales de design concernant l’élaboration de sites web.

Durant ces années il tient un blog intitulé *alertbox*<span class="link" data-id="alert-box">&#42;</span> dans lequel il décrit, commente et explique tout à la fois ce qui va et ce qui ne va pas dans le web ou ce dont le web aurait besoin.

En dehors de ce blog son site personnel se compose d’articles traitant de la manière dont les utilisateurs font ou ne font pas certaines choses et la manière dont il faut exploiter ces « faits ». 

Jakob Nielsen propose également des articles intitulés « Mes prédictions » dans lesquels il annonce ce qu’il va se passer concernant le web l’année suivante mais également ce qui est advenu de ses prédictions précédentes.<span class="link" data-id="surf">&#42;</span>

En 1999, sur le site de Google via l’url [google.com/execbios](iframes/teamoct1999.html) on peut apprendre la présence de Jakob Nieslen au sein de l’équipe Google en tant que conseiller technique.

Sur cette page, il est tout à la fois présenté comme :

- le représentant de la société *Nielsen-Norman group* 

- l'associé de Donald A. Norman l’ancien vice-président de la recherche chez *Apple Computer*

- le porte-parole des utilisateurs pour l’entreprise 

- l’ancien ingénieur et gourou de l’ergonomie (*usability guru*) chez Sun Microsystems

- l’inventeur de la notion “ d’ingénierie ergonomique bon marché ” (*discount usability engineering*)

- l’auteur de nombreuses techniques permettant l’amélioration rapide et peu coûteuse des interfaces-utilisateurs

- étant le propriétaire de 35 brevets état-uniens permettant de rendre le web plus facile à utiliser et enfin 

- l’auteur de nombreux textes décrivant les sites internet les plus réputés

Jakob Nielsen est donc le spécialiste de la « facilitation du web » chez Google mais aussi le co-fondateur avec Don Norman du *N&N group* (Norman and Nielsen group, Evidence-Based User Experience Research, Training, and Consulting). Ce duo, largement aux origines du concept d’expérience-utilisateur, est fondé la même année que Google (1998).

L’influence, la correspondance intellectuelle, idéologique entre ce duo et le discours de Google concernant le design est flagrante et renseigne sur les raisons du rapprochement systématique au terme _design_ de termes comme _facilité_, _besoin_, _offre_ etc. ou la forte proximité qualificative entre design et publicité dans le discours de Google.

Dans la réédition de 1989 de *The design of everyday things*<span class="link" data-id="design-of-every-day-things">&#42;</span> Don Norman a rédigé une préface pour son propre livre. Dans la partie *Business and Industry* après la description du succès de son livre auprès des industriels et commerciaux du monde entier il assimile clairement utilisateur et consommateur.

>Hurrah ! Not for me, but for the positive signs of increased concern for the _customer-user_.

Dans la seconde moitié de cette partie il explique que la problématique de son livre porte sur le manque d’intérêt porté au design concernant les effets qu'il produit sur les « utilisateurs-consommateurs ». Il donne ensuite une sorte de version personnelle de la formule “form follows function” en « la forme ne suffit pas, la fonction est plus importante, si un produit ne peut pas être utilisé facilement et en toute sécurité, son apparence n'a aucune valeur. »

>A large community of designers exist to help improve appearances. But appearances are only part of the story : usability and understandability are more important, for if a product can't be used easily and safely, how valuable is its attractiveness ?

Plus implicitement dans l’ensemble de cette partie il alterne entre les termes utilisateurs, consommateurs, peuple/individus (*people*) sans distinctions, les assimilant les uns aux autres. 

>Business exists to serve _people_, a point that successful businesses understand and practice.

Pour Don Norman le design doit avant tout être un outil au service du business, le business étant lui-même au service du « peuple-utilisateur-consommateur ». À croire que la notion de _peuple_, d’_individus_ ou _d’utilisateurs_ signifient tout autant _masse de consommateurs_ et que c’est ainsi que le business se trouve être au service du peuple, à moins que ce soit l’inverse, en-fait cela ne semble pas avoir d’importance. On en oublie presque que « service » renvoie à « servir » et que, dès lors, la « servitude » ou « l’asservissement » n'est plus très loin.
On peut penser à la formule de Jean Baudrillard à propos de l’usager comme « l’esclave moderne par excellence ».<span class="link" data-id="echange-symbolique-et-la-mort">&#42;</span>

Comme en résonance à cette préface on trouve en 1997 sur le site Jakob Nielsen via l'url www.useit.com/alertbox/9701 une partie intitulée “ Taking the Web Seriously as a Business Tool ” tandis que dans Design of Everyday Things Don Norman se plaint du manque de crédit accordé au design en tant que “ Business Tool ”, Jakob Nielsen regrette le manque de crédits accordé au Web en tant que “ Business Tool ” et prédit que l'année suivante (1998) le vent tournera.

Sur le site du *N&N group* via l'url www.nngroup.com/articles/first-rule-of-usability-dont-listen-to-users/ on trouve un article daté de 2001 rédigé par Jakob Nielsen intitulé *First Rule of Usability? Don't Listen to Users*<span class="link" data-id="dont-think">&#42;</span> littéralement : « Première loi de l'ergonomie ? Ne pas écouter l'utilisateur ». Cet article explique comment le “cool design” a fait perdre des millions de dollars et que pour designer la meilleure expérience-utilisateur il ne faut surtout pas écouter l'utilisateur mais plutôt surveiller le moindre de ces faits et gestes.
On peut penser aux situations panoptiques décrites par Michel Foucault dans surveiller et punir.<span class="link" data-id="surveiller-et-punir">&#42;</span>

>In past years, the greatest usability barrier was the preponderance of cool design.
>[…]
>Watch Users Work *(titre)*
>[…]
>To discover which designs work best, watch users as they attempt to perform tasks with the user interface. This method is so simple that many people overlook it, assuming that there must be something more to usability testing. Of course, there are many ways to watch and many tricks to running an optimal user test or field study.

En 1998 sur le site de Sun microsystems via l'url sun.com/sun-on-net/uidesign on trouve un article<span class="link" data-id="sun">&#42;</span> intitulé *Interface Design for Sun's WWW Site* sous-titré *Fundamental Design Concepts* dans lequel les faits et gestes de l'utilisateur sont décrits comme une matière première extrêmement précieuse.

> We decided to provide value-added information in the form of a monthly magazine cover and to be highly selective in choosing a small number of cover stories. Some people don't understand the value of less is more, but if everything is highlighted, then nothing has prominence. I estimate that it costs the world economy about half a million dollars in lost *user productivity* every time we add one more design element to Sun's home page. It is the responsibility of the Web editor to prioritize the information space for the user and to point out a very small number of recommended information objects. The beauty of hypertext is that the user can then browse the information space further and dive deeper into the specific information of interest to that individual user.

Jakob Nielsen conclue l'article avec « trois découvertes majeures » :

> Three major findings from our extensive usability studies were:

1. Les gens du peuple (*people*) n'ont aucune patience face à un site mal designé

> People have very little patience for poorly designed WWW sites

2. Les utilisateurs ne veulent pas scroller

> Users don't want to scroll

3. Les utilisateurs ne veulent pas lire

> Users don't want to read

Il peut s’avérer difficile de définir la notion de volonté ou de désir d'un individu quand son avis ou sa parole – c'est à dire son libre arbitre – n'est pas pris en compte, voire la notion de volonté ou de désir peut-elle avoir du sens dans un contexte d'absence de libre arbitre ?

Les recherches de Nielsen concernant la manière dont les utilisateurs lisent un site web sont notamment au cœur d'un quiproquo  décrit par Katerine Hayles dans [How we read: Close, Hyper, Machine](http://nkhayles.com/how_we_read.html). Dans un contexte de paranoïa général concernant l'appauvrissement de la qualité de lecture induite par les écrans, Katerine Hayles décrit comment les études censées évaluer la qualité de lecture sur écran sont biaisées par les critères d'évaluations propres à la lecture sur papier. 

exemple emile

Elle prend pour contre argument à cette tendance l'étude de la «lecture en F» de Nielsen (Nielsen, “F-Shaped”), dans cette étude l'équipe de Jakob Nielsen demande aux sujets du test de délivrer des commentaires à voix haute tandis qu'ils parcourent des pages web. Leurs réactions sont enregistrées tandis qu'en simultané un appareil capte les mouvements effectuées par leurs yeux. L'observation est la suivante : sont lues intégralement les premières lignes de la page puis au fur et à mesure l’œil à tendance à se concentrer sur la gauche de l'écran jusqu'à former une ligne verticale. Plus loin dans l'article elle rappelle que suite à l'influence de cette étude les designers se sont mis à systématiquement mettre en page en ‘F’, intensifiant significativement l'automatisme de ce mode de lecture pour l'utilisateur.

>Think, for example, of the F pattern of Web reading that the Nielsen research revealed. Canny Web designers use this information to craft Webpages, and reading such pages further intensifies this mode of reading.

Que signifie une étude qui suite à l'analyse d'un comportement conditionne a posteriori son sujet à la répétition de ce même comportement ?

Si par exemple j'observe qu'en moyenne : 

- les utilisateurs ne scrollent pas beaucoup

- qu'ils ne lisent pas beaucoup car ils quittent bien trop rapidement un site au contenu textuel dense pour l'avoir lu

- qu'ils ont tendance à quitter plus rapidement un site à la programmation singulière

Je n'écoute surtout pas l'avis de l'utilisateur à ce propos mais bien-sûr je suis conscient de mon propre désir qui est le suivant : « j'aimerais que les utilisateurs lisent intégralement le contenu informatif de tout les sites web et qu'ils cliquent sur la totalité des liens qu'ils contiennent surtout les publicités. »

Je sais également certaines choses évidentes ou inhérentes aux 
propriétés techniques d'un site web : 

- Les utilisateurs ne peuvent pas scroller et cliquer

- Les utilisateurs ne cliquent pas lorsqu'ils lisent

Conclure que ce n'est pas leur désir de scroller ne serais pas à tout hasard une manière de pousser l'utilisateur à passer plus de temps à cliquer qu'à lire ? 

Dans l'exemple particulier de l’étude de la lecture en ‘F’ il est évident que les utilisateurs ayant participé à l'étude ont appris à lire de haut en bas et de gauche à droite, pourtant Nielsen ne fait nullement mention de cette particularité. De la même manière qu'il affirme que l'ensemble des utilisateurs ne veulent ni lire, ni scroller, ni consulter un site à la programmation singulière, il affirme que l'ensemble des utilisateurs lisent en ‘F’. Si l’on ajoute l'assimilation entre utilisateur et consommateur de Norman on obtient le conditionnement de l'ensemble des utilisateurs – quel que soit leurs précédentes habitudes – non plus à lire mais à consommer en ‘F’ des sites tous programmés de la même manière non-scrollables et contenant peu d'informations. 

Cette tendance qui transparaît dans le discours de Google consistant à ne pas seulement chercher à répondre à un besoin mais également à le définir se retrouve donc aussi chez Nielsen et Norman.

Heureusement cette volonté de résultats se heurte parfois à des désirs non induits par un discours quelconque.

En ce qui concerne le scroll, il est évident que ce comportement n'a pas disparu, Nielsen à d'ailleurs commenté ce fait en 1998 sur la page “Changes in Web Usability Since 1994” www.useit.com/alertbox/9712a d'une partie sobrement intitulée « Le scroll est désormais autorisé » malheureusement c'est aujourd'hui ce même scroll désormais tactile et nous conditionne à l'usage 

<!--
C’est sûrement lui qui fait que katerine Hayles se tire les cheveux dans How we read: Close, Hyper, Machine"(http://nkhayles.com/how_we_read.html) Katerine explique l’impact négatif sur la lecture que provoque le ramassage des informations en haut ou à gauche de l’écran tandis que Jakob Nielsen en fait une loi de design basée sur l’évidence. à préciser 
Jakob Nielsen est une des références principales d’un livre intitulé "Don’t make me think" littéralement "ne me faites pas réfléchir" qui est un des livres de références en matière de web design 
M. Frederic Marand
5,0 sur 5 étoilesLe seul livre sur le Web que je relis régulièrement
8 mars 2017
Format: Broché
Découvert il y a des années lors de sa sortie, il a été un choc parce qu’il se situait à la fois hors de la technologie et de la théorie pour se focaliser sur la perception par les utilisateurs. Depuis, je le relis régulièrement quand je m'interroge sur un point ou un autre en matière d’UX : c’est le fondamental, avant de plonger sur les textes plus complexes... et je l’ai fait acheter plusieurs fois à des clients ou des collègues.

Comme d’autres l’ont dit dans les commentaires, la technologie a changé, et les codes du design aussi, mais le texte est presque intemporel, alors que les collections de Jakob Nielsen sont presque illisibles aujourd’hui.

Incontournable.
Commentaire| Une personne a trouvé cela utile. Ce commentaire vous a-t-il été utile ?
Oui
Non
Signaler un abus
https://www.amazon.fr/Dont-Make-Me-Think-Usability/dp/0321344758

Ce livre explique en 200 pages illustrés toutes les méthodes pour éviter à l’utilisateur d’avoir à réfléchir sur une page car c’est le principe de base de l’usabilité comme le dit si bien Nielsen : First Rule of Usability? Don’t Listen to Users. c’est à dire que l’ux est centrée sur l’user mais qu’il ne faut surtout pas l’écouter car il ne sais pas ce qu’il veux, mais nous designers savons bien pour lui. User centered = centré sur la domination de l’user.
« Ce que nous essayons de faire c’est de construire une humanité augmentée, nous construisons des machines pour aider les individus à mieux faire les choses qu’ils n’arrivent pas à bien faire eux-mêmes.»
Eric Schmidt, PDG de Google
c’est projet aporétique, effectivement on peux toujours enlever des choses, mais il y en aura toujours ( ça défi les lois de la physique) c’est le rêve quoi, le comble du marketing (technologie qui mettent à ses pieds le monde et ses contraintes)

#### La télé du web

Exemple sur ma théorie du déplacement d’un ancien médium dans un nouveau pour faciliter l’utilisation ( c’est à dire faire disparaître toute différence ou inattendu, cad toute perte de contrôle ) : la télé du web de Nielsen : 
https://web.archive.org/web/19970218113557/http://www.useit.com:80/alertbox/9702a.html
 chercher des exemples 
Fascination pour la webtv et état de youtube (tf1) youtube étant google, coïncidence ?
-->

## 2001

#### complexification de la page (11 parties) le contenu principal précédant est séparé dans une page google today de la section in Depth, en profondeur

Tandis que le contenu about continue à se complexifier, (12000 caractères, 3 grandes parties et 11 sous-parties) chaque sous-partie possède une adresse propre google.com/corporate/[sous-partie].

#### changement d’adresse  (compagny -> corporate/today)

Nous allons nous cibler particulièrement la page Google today google.com/corporate/today qui reprend le contenu Google's Approach to Searching the Internet de la pge de 1999 particulièrement attaché aux problématique de conception de l’entreprise. 
Tandis que l’url google.com/compagny va continuer d’exister parallèlement.
Ce qui était formulé au travers de cinq points An Elegant, Easy-to-Use Interface: , Sophisticated Text-Matching, Patent-Pending PageRankTM Technology,Results are delivered in order of importance and relevance:  et Integrity in Search Results revêt désormais la forme d’une liste de 10 items
1) Focus on the user and all else will follow.
2) It's best to do one thing really, really well.
3) Fast is better than slow.
4) Democracy on the web works.
5) You don’t need to be at your desk to need an answer.
6) You can make money without doing evil.
7) There's always more information out there.
8) The need for information crosses all borders.
9) You can be serious without a suit.
10) Great just isn’t good enough.
intitulés les 10 vérités que google à découvert. n’allait n’est-ce pas sans rappeler les 10 commandements.
Google insiste sur son importance dans la définition des besoins, des vérités.
Il ne se contente pas de satisfaire les besoins il les définit (si différence il y a).

#### disparition du terme design

Chose assez surprenante, le terme design disparaît du discours, peut-être n’a t'il pas sa place dans une liste de vérités ou bien est-il trop enclin à faire réfléchir l’utilisateur.

#### terme experience (2)

on retrouve toujours le terme expérience (une occurrence en moins),
toujours best : providing the best user experience possible, rapide et harmonieuse pour des millions d’utilsateurs : a fast and seamless experience for millions of users. Uniformiser la naviguation sur le web est donc l’objectif ?

#### terme user (20)

user toujours similaire, l’utilisateur à toujours besoin d’aide en 2001 : help users , users seeking answers. Mais ce n’est plus rendu facile pour lui (easy une seule occurence) mais maintenant Google connait parfaitement les besoins de l’user car lis dans l’esprit de l’user : One engineer saw a need and created a spell checker that seems to read a user's mind.
l’utilisateur est la préoccupation principale de google (le client est roi) : benefit to the users, one satisfied user to another,  always placing the interests of the user first, Google, wireless users (le smartphone entre en jeu), users living outside the United States (google semble étendre sa cible), user's preference,  content available to users regardless of their native tongue, offer users in even the most far flung corners of the globe. ( on va aller chercher des client PARTOUT, sans fil au bout du monde), This process has greatly improved both the variety and quality of service we're able to offer users in even the most far flung corners of the globe, Google puts users first. Cette fois le nombre d’users est insistant, une horde, une armée : a user base in the millions, millions of users.
l’utilisateur est rapide grâce à google : Google may be the only company in the world whose stated goal is to have users leave its website as quickly as possible. 

#### apparition terme people (3)

Le terme people apparait pour la première fois avec le même rôle qu’un user, tout comme l’user il a besoin d’information (autrefois reservée aux users) : people want information to come to them, mais contrairement à l’user ils ne sont pas dans le besoin, ils sont une force autonome ce ne sont pas les users de drogue de twitter  Jack's
#
Let’s reconsider our “users”
us·er 
/ˈyo͞ozər/

Noun
1. A person who uses or operates something, esp. a computer or other machine.
2. A person who takes illegal drugs; a drug user.

Synonyms
consumer

During a Square Board meeting, our newest Director Howard Schultz, pulled me aside and asked a simple question.

“Why do you all call your customers ‘users’?”

“I don’t know. We’ve always called them that.” Jack Dorsey, executive chairman of Twitter:
Let’s reconsider our “users” http://jacks.tumblr.com/post/33785796042/lets-reconsider-our-users 
: This highly communicative environment fosters a productivity and camaraderie fueled by the realization that millions of people rely on Google results. Give the proper tools to a group of people who like to make a difference, and they will.

#### formule user experience (1)

La formule user experience apparait aussi pour la première fois (tiens, tiens) et elle est une reformulation très basique du "the best search experience on the World Wide Web." de 99 en "the best user experience possible" le terme contiendrai donc les enjeux du web à lui seul.

La page compagny info de google devient “google today” et google parle de “user experience” et affirme “focus on the user and all else will follow”.

10 things Google has found to be true

“10 Things Google has found to be true” est un paragraphe sur la page “google today” sur le site google.com apparue en décembre 2001 sur l’adresse google.com/corporate/today.

En décembre 2001, la section “corporate information” est divisée en trois parties : “Profile”, “In Depth”, “The Difference”.

“Profile” contient les informations élémentaires, une section concerne le terme “google” et son origine, une section esquisse l’entreprise “en un clin d’œil” par le biais de statistiques ; nombre de requêtes quotidiennes : plus de 150 millions, nombres de pages web indexées : plus de 2 billions […] Google figure parmi les dix sites les plus populaires sur internet etc. Cette page contient également les noms et les fonctions des acteurs principaux de l’entreprise sous forme de crédits (on y retrouve notamment Jakob Nielsen annoncé comme représentant du Nielsen Norman Group en tant que conseillé technique). Une dernière contient les adresses des différents bureaux de l’entreprise ainsi que des conseils pour y accéder.

À partir du 13 décembre 2001 la section compagny info du site de google se complexifie, la section “Google's Approach to Searching the Internet” s’appelle maintenant “Google Today” et le discours n’est plus tout à fait le même. Les 5 points décrivant les choix de google concernant les méthodes d’indexation du web – qu’il s’agisse des choix de design d’interfaces ou de la description de leur système de notation démocratique de sites – sont remplacés par «10 vérités», google ne se contente plus de décrire ses méthodes il en propose une, il propose des lois. En parallèle le terme “user experience” apparait pour la première ce qui dévoile la proximité des idées entre google et Donald Norman.

## 2008

### Update

#### complexification de la section corporate (15 parties)  la page s’appelle désormais Design principles dans la section Our Philosophy

La structure de la section corporate continue à se complexifier, 3 parties et 15 sous-parties et les noms des parties et sous parties sont de nouveau remaniées (tous) : Profile devient Corporate Overview (Compagny, Features, Technology, Business, Culture, Green Energy(green washing ?)), In depth devient At a Glance (l’inverse ?) avec : Quick Profile, Address (autrefois dans Profile), Management, Miles stones. The Difference devient Our Philosophy ( se démarquer, cad faire de la com de la pub = notre philosophie ?) avec : Ten Things, Software principles, Design principles, No pop-ups, Security.
Les 10 vérités deviennent 10 choses (things) tandis que sa structure/contenu est copiée modifiée en Design principles, section que l’on va désormais suivre.
Le contenu précédent est rebaptisé 10 things et existe aussi dans Our philosophy.
Le Design à donc un espace privilégier désormais, il s’agit également d’une philosophie mais une philosophie de la vérité (autrefois 10 truths).
Le contenu est plus court (10000)

#### changement d’adresse, apparition du terme UX  (corporate/today -> corporate/ux)

Le design est donc associé avec une philosophie de la vérité, mais pas seulement car l’url est désormais google.com/corporate/ux, Design et Ux se confondent. Est-donc associé à ux les valeurs que pour le design, philosophie publicitaire de la vérité.

#### 10 vérités deviennent 10 principes résumés en dix mots  top 10, mojo, dieter rams, bible

Les dix vérités deviennent 10 principes de design applicables, "Ten principles that contribute to a Googley user experience" c’est une invitation à quiconque à produire des objets à l’image de google. Car l’application à un projet, un objet de ces 10 vérités n’aura pas seulement pour effet de produire un design dont les utilisateurs ont besoin, cela produira un objet Google, une sorte de brevet sur la vérité ?

#### retour en force du terme design (27)  associé au terme principe, nom de cette page dans l’index

 Le design comme principes de vérités ? de la vérité ? 
Cette fois Le terme design est omniprésent. Dans tout les principes. Il s’agit donc bien de principes de design.
Un petit paragraphe Our aspirations (notre idéal ?) associe 10 termes au terme design, chaque terme correspondant à un principe, à une vérité découverte par Google. The Google User Experience team aims to create designs that are useful, fast, simple, engaging, innovative, universal, profitable, beautiful, trustworthy, and personable.Le design de la vérité est : utile, rapide, simple, accessible, innovant, universel, rentable, élégant, sécuritaire et convivial.
Dans le premier paragraphe Utilité (Focus on people – their lives, their work, their dreams), le well-designed est useful in daily life, utile dans la vie de tout les jours. Plus utile est donc ce qui est utilisé le plus de fois, avec le plus de régularité.
Dans rapidité (Every millisecond counts.), le terme design n’apparait pas.
Dans Simplicité (simplicity is powerful), le design est associé aux termes: good design is : ease of use, visual appeal, accessibility, les uns validant les autres et inversement. Ce qui est puissant est simple et inversement (Less is more) moins c’est plus, plus c’est moins (la loi de l’équivalence ?) c’est à dire que l’exercice de la force produit de la simplicité (appauvrissement des choix, des alternatives) tandis que l’appauvrissement des choix, des alternatives définit l’exercice d’une force.
Dans accessibilité (Engage beginners and attract experts.). Le design est destiné à tous (Designing for many people) permis, offert à tous (A well-designed Google product lets new users jump in, offers help when necessary) et est de nouveau associé à la force et la simplicité : designs appear quite simple on the surface but include powerful features. Nous rappelant encore l’équivalence entre moins et plus. l’exercice de la force est donc destiné à tous, la simplification (la raison ?) est destiné à elle-même (toujours plus) dialectique de la raison.

"Cette permission donnée au consommateur…", il faut permettre aux hommes d’être des enfants sans en avoir honte. "Libre d’être soi-même" signifie en clair : libre de projeter ses désirs dans des biens de production. […] Cette "philosophie" de la vente ne s’embarrasse guère du paradoxe : elle se réclame d’un but rationnel (éclairer les gens sur ce qu’ils veulent) et de méthodes scientifiques, afin de promouvoir chez l’homme un comportement irrationnel (accepter de n’être qu’un être complexe de pulsions immédiates et se satisfaire de leur satisfaction).[…] l’irrationalité toujours plus "libre" des pulsions à la base ira de pair avec un contrôle toujours plus strict au sommet" j b le systeme des objets 1968 p260

Dans innovation (Dare to innovate) le terme design est associé à celui de confiance : Design consistency builds a trusted foundation. Le terme design est également associé à des antinomies, il peut être ho-hum (disgracieux, déplaisant) tout comme il peut être Delightfull (attirant, charmant, plaisant), l’imagination (innovation) permettant de faire la différence.
Il est également associé à une prise de risque : risk-taking design, mais il est encore rappelé à son assujettissement aux besoins des utilisateurs (besoin que google definit, donc assujetti à google) 
Google encourages innovative, risk-taking designs whenever they serve the needs of users.
Est encourager l’initiative qui va dans le sens de la raison, vérité (définit, découvert par google).
Tout est bon à condition que cela répande l’exercice de la force (la force de la vérité, celle de google) dans son équivalence avec la faiblesse (les usages, les utilisateurs), que le même schéma soit reproduit sans cesse, inceste ?
Dans universel (Design for the world), design est associé à relevant, available (pertinent, disponible), à exactitude (to design the right products), ce qui est évoqué plus haut est repdroduit, re-dit
La mise en scène de la force et de la faiblesse destiné à tous, destiné à sa reproduction.
Dans rentable (Plan for today's and tomorrow's business.) Le design est de nouvelle fois rappelé à son assujettissement aux besoins définit par google mais également au profit de google (sa reproduction) "designers work with product teams to ensure that business considerations integrate seamlessly with the goals of users." Il est encadré, surveillé par la "product team" équipe commerciale. Les mêmes principes sont à nouveau reproduits avec la différence que les besoins des utilisateurs définit par google sont associés aux besoins commerciaux de profit de reproduction. Le terme design est directement associé à profit et à user "If a profitable design doesn’t please users". Un double rôle ou un double contrôle donc, le design s’assure à la fois de la correspondance au profit et au besoin. À l’adéquation avec la définition et à sa reproduction conforme.
Dans élégant (Delight the eye without distracting the mind.).
Design est de-nouveau associé à une forme reproduite de simplicité : clean, clutter-free design. On retrouve aussi form follow fonction, "
Design est de-nouveau associé à une forme reproduite de simplicité : clean, clutter-free design. On retrouve aussi form follow fonction, " Google à une conception du design très traditionnelle (1 siècle) à moins qu’il s’agisse d’une modification : la forme c’est la fonction (Medium is the message ?) à moins que tout cela soit une reproduction de la même force simplificatrice (manger les autres, la mort?).
Less is more
Form follow function
ressemble à " La guerre c’est la paix ", " La liberté c’est l’esclavage ", " l’ignorance c’est la force "
(war is peace, freedom is slavery, ignorance is strength)
. 1984
Dans sécurité (Be worthy of people's trust.) l’objectif du design est de mériter (ou d’obtenir) la confiance des utilisateurs (qui utilisent déjà google ?)
Good design can go a long way to earn the trust of the people who use Google products. 
Dans convivialité (Add a human touch.) le design est associé avec personnalité, individualité (our designs have personality) mais également dissocié "And Google doesn’t let fun or personality interfere with other elements of a design, especially when people's livelihood, or their ability to find vital information, " c’est encore la reproduction du Tout est bon, tout le monde est bon à condition que cela répande l’exercice de la force (la force de la vérité, celle de google). Ici personnalité désigne la totalité des individus dans leur diversité "Google includes a wide range of personalities, and our designs have personality" Google s’adresse directement à eux (simplement) "Google text talks directly to people and offers the same practical, informal assistance that anyone would offer to a neighbor who asked a question." Il prend la diversité et la converti en uniformité.
Aussi text et design sont des éléments disctincts "Text and design elements are friendly, quirky, and smart – and not boring, close-minded, or arrogant. " tandis qu’ils sont unis dans la convivialité, l’accessibilité, l’intelligence, c’est à dire qu’ils sont simples.
On pourrait croire que ces dix termes tendent à signifier la même chose, l’inceste ?

"Finie l’apocalypse, auj c’est la précession du neutre, des formes du neutre et de l’indifférence. Je laisse à penser s’il peut y avoir un romantisme, une esthétique du neutre. Je ne le crois pas – tout ce qui reste, c’est la fascination pour les formes désertiques et indifférentes, pour l’opération même du système qui nous annule. Or la fascination […] est une passion nihiliste propre au mode de disparition. Nous sommes fascinés par toutes les formes de disparition, de notre disparition"

#### formule user experience (8)  titre

Le terme user experience est également présent sur toute la page (7 de plus) même s’il n’est pas présent dans tout les paragraphes répéter de nombreuses fois au début (en titre) il revient régulièrement dans la deuxième moitié du texte. Au singulier il est constamment attaché à team, l’équipe : The Google User Experience team aims to , The Google User Experience team works to , The User Experience team researches,  the User Experience team would cheer, the Google User Experience team seeks.
Google disparait dans l’user experience team. Ux est google. Googley user experience

#### terme people (22)  explosion

le terme people explose sur la page (22 occurences)
Tandis que son utilisation précédente s’opposait clairement à l’user (people : force autonome, user : dans le besoin)
Le terme est ici mitigé, 
features that people need to accomplish their goals (people need help)
many people (army of people)
encourages people to expand their usage (people need help)
people with complex online lives (people autonomes),
people who share data (idem),
people everywhere (army),
Google allows people to choose (need help),
meet people's needs. (need help
)
people looked at a Google product and said "Wow, that's beautiful!" (army/autonome)
encourages people to make the product their own (need help / autonome)
Be worthy of people's trust. (people army)
earn the trust of the people who use Google (army)
Google text talks directly to people and offers (need help)
especially when people's livelihood, or their ability to find vital information, is at stake. (people need google own)

#### terme experience (10) montée en force

experience quand il n’est pas avec user est une experience standard partagée "provide a useful and enjoyable experience for everyone"
experience conforme car bonne avant même son acte ?  great initial experience

#### terme user (33)  montée stable

Le terme user seul reste très présent, toujours plus que people, et se retrouve utilisé de la même manière que people
It doesn’t try to impress users with its whizbang technology (user need or don’t need)
users who want to explore(user wants users need)
Speed is a boon to users.(this is good for users, google know)
easily accessible to those users who want them (users want, users need help)
attracting power users whose excitement and expertise will draw others to the product. pour la 1er fois user prend le sens d’individus autonomes et forts ( le client est roi)
lets new users jump in (users needs)
ensures that users can (users needs figure de l’enfant)
makes users comfortable, and speeds their work. (users needs)
they serve the needs of users (users needs)
many users (army)
methods that make sense to users (google know for users)
helpful to users(users needs)
the goals of users(google know)
the number of Google users (army)
profitable design doesn’t please users (users needs, user don’t know )
A positive first impression makes users comfortable (users needs)
doesn’t distract users from their goals (users needs, google know)
visual design should please its users and improve usability (google define, users needs)
terminology is consistent, and users are never unhappily surprised (no surprises, inceste)
etc.

### l’influence de Don Norman

explosion du terme people même année de sa déclaration, adressage ux, l’UX les origines de la peur

Angie Li
Ux Specialist, Nielsen Norman Group
fake diversity
jeunes cool
jeune fille
tailleur
oriflame

Hi i'm Angie Li and im here  at te ux conference in san fransisco
i got to ask Don Norman what he thinks about the term ux.

ton distinct anglais simple
vidéo éducative, "universelle"
contée par Don Norman

logo N N /g
Donc on a une interview de Don Norman sur le site de Don Norman où on lui demande ce qu’il pense du terme qu’il a inventé.

Agé, occidental
fort contraste avec Angie Li
Don Norman ressemble au père noël, avec un bleu de travail (populiste démagogue, col bleu pas col blanc != tailleur)

once upon a time, a very long time ago, 

il raconte autant avec les gestes que avec l’intenation ou les mots
tout est très clar simple voir caricatural
think emoji

il s’affiche 1993-1997

i was at apple 

eye contact emoji

and you know we said 

more thinking emoji

the experience of using this computers is weak…

emoji dégout
ect...
Ah, the experience when you first discover it
when you see it in the store 
when you buy it
When you ouh it cant fit into the car ! Its in this great big box, it doesn’t fit into the car. When you finaly do get at home
you opening the box up and uuuuh ouh
oh it looks scary i dont know if i dare put this computer togever.
All of that is user experience.
Its everything that touches upon your experience with the product
and it may not even be you and the product, its maybe when you tell somebody else about it.

thas what we meant 
when we device the term user experience and set up what we call the user experience architects office at apple,
to try to, enhance things.
now apple product is allready pretty good so we've been started with a good product
make it even better
:( Today that term is been horribly misused
its use by people to say
im a user experience designer 
i design websites so i design apps
so they have no clues to what they're doing and they think the experience of that
simple device 
the websites or the apps
or who know what 
NOOO
its everything
its the way you experience the
world
// your life
// the service
or yeah an app or a computer system
but its a system thats everything
got it ?

En 2008, la même année où Donald Norman avoue publiquement sa répulsion pour le terme “user” et explique préférer “people” citoyen, personne, indivu le 30 avril cette section est carrément renommée «design principles» c’est à dire «principes de design», et il ne s’agit plus pour google de se focaliser sur l’utilisateur mais sur l’individu “people” «we design for people we dont design for users» nous désignons des expériences et non des utilisations, nous designons des objets en sorte que leurs consommateurs en aient une expérience et non une prise en main, qu’ils n’en fassent rien si ce n’est recevoir leur message.
La page google today devient “design principles” et Google demande “focus on people – their work, their dreams”.

## 2008 - 2016

À partir de 2008 il n’y aura plus de grands changements dans le discours. Le contenu se stabilise en grosso-modo deux formes rédactionnelles définitives.
Les dix choses (Dix vérités) et les principes de Design.
Néanmoins ces deux formes vont exister parallèlement au travers de nombreuses urls.
Premièrement le contenu "corporate" est doublé au travers des url /compagny et /corporate avec notamment des variations de mise en page et d’architecture néanmoins le contenu rédactionnel est le même. Au sein même des branches /compagny et /corporate vont exister de nombreux doublons avec par exemple company/philosophy clone de company/tenthings et corporate/tenthings, company/ux clone de corporate/ux …
Le seul contenu rédactionnel significativement nouveau est la formule We first wrote these “10 things” several years ago. From time to time we revisit this list to see if it still holds true. We hope it does–and you can hold us to that. 
en (September 2009) qui en soit confirme la stabilisation du contenu rédactionnel.
On peux également trouver des versions traduites de ces pages par exemple google.fr/corporate/ux, première apparition de la version traduite en français de design principles (Principes applicables à la conception de sites), traduit très étrangement, peut-être principalement avec google traduction. On peut remarquer qu’il n’y pas de substitution ou d’assimilation au terme utilisateur à une traduction du terme people un terme exclusivement pluriel désignant tout à la fois l’homme, l’individu, le citoyen, le peuple selon le contexte, intraduisible, n’a pas de sens en français.

People et users bien qu’utilisés de la même manière dans les design principles et les 10 things ne sont pas réparti de la même manière entre les deux formes.
(voir shéma)
dans design principles 33 occurences du terme user pour 22 occurences du terme people, dans 10 things 11 occurences du terme people pour 5 occurences du terme user.
On peut supposer que 10 things est une version de la philosophie de google déstinée au publique le plus large possible tandis que design principles bien que très large également est plus destiné à des "utilisateurs expérimentés".

Dernière chose, à partir de 2016 les pages design principles disparaissent, 10 things devient l’unique et définitive version du discours de google.

### dix vérités

### dix principes de design

### la philosophie l’emporte

## 2011

### traduction

#### version française (tardive, a vérifier) google.fr/corporate/ux

#### pas de terme people car ça veux rien dire en français donc utilisateur

En 2011 cette page est traduite en français et le terme de citoyen ou de personne ou d’individu n’apparait nullement, la page s’intitule 
«les utilisateurs avant tout» marquant encore plus cette difficulté de traduction du terme people, et le caractère peut-être exclusivement anglophone voir californien de l’utilisation du terme “people” concernant le design d’interface.

#### google traduction ?

## 25jun2014

Le “Material Design” est officialisé le 25 Juin 2014 simultanément durant la Google I/O conférence et en ligne à l’adresse www.google.com/design/spec/material-design. c’est également à cette date que pour la première fois du contenu sur la page google.com/design apparaît, la page existe depuis 20** mais vide ou redirigée.

#### création d’une page design, qui existait depuis des années mais vide

Avant de nous pencher sur la page material design nous allons dabord observer celle-ci.

Malgré une url très différente, et une mise en page drastiquement revue (loué soit Duarte, on y reviendra plus tard) cette page s’inscrit dans une continuité avec les pages 10 things et design principles.

Titré Google Design cette page s’ouvre avec un paragraphe rappel de la première vérité découverte par google : "Focus on the user and all else will follow"

le bouton ? Google guidelines renvoi à l’adresse /design/spec/material-design, le design était formulés en principes il est maintenant formulés par des directives.

Le contenu de cette page est très peu textuel, ce qui saute aux yeux c’est le caractère résolu « graphique » de cette nouvelle mise en page.

Faisons rapidement la comparaison formelle avec la page 10 things 2016,
même si cette page est plus récente cela n’a pas d’importance, puisqu’elle suit clairement les principes de mise en page des pages précédentes qui ont précéder la page design qui contient ce "nouveau" principe de mise en page qui en soit introduit ce qu’est le material design. Le fait qu’elle soit plus récent permet de faire une comparaisons de choix et non de contraintes techniques ou de mode. Car les technologies web sont les mêmes et sensiblement les mêmes modes.
Pour voir ces modes nous allons aussi comparer avec la mise en forme de 1999.
Quand bien même 10 things hérite des contraintes formelles historiques des pages que nous venons d’étudier. Il s’agit bien de choix de conservation de méthodes, une esthétique ? 
Ces trois pages montrent bien la différence entre l’ancien design google qui est toujours présent et cohabite avec le "nouveau" design google, que l’on va appeler le Material Google (plutôt que material design, comme on l’a vu le design se confond largement avec la philosophie de google, cad son identité).

pour plus de "fidelité" j'ai re-telecharger ces pages avec wget, qui telecharge l’ensemble des liens qui construisent la page contrairement aux autres pages pages que vous avez pu voir qui était juste enregistrer sous avec explorer windows.

Le Material Google met l’accent sur les formes géométriques.
Les couleurs vives.

l’association des deux donne une tonalité "ludique", simpliste ou plutôt enfantine qui va dans la continuité de l’infantilisation du discours. lego playdo, jeu vidéo, flat design, otto neurath.

Un fort accent est mis sur les "effets", d’ombres, de transition, d’agrandissement, responsive etc. Ce qui va dans la continuité du spectaculaire, web tv (), les moments du site ressemble à des mini compagny intro video.

l’ancien Google est très proche de l’esthétique du « document » malgré les différences (choix de typo, encadrements, colonnages, couleurs de fonds) l’influence esthétique par les contraintes techniques et les attributs de mise en page par défaut du HTML reste forte même en 2016, assez 90, environnement de bureau.
De l’article, du format papier corporate. 

c’est la simplicité économique dans tout les sens du terme, du web as a marketing tool de Nielsen.

exemple poussé à l’extrême : http://motherfuckingwebsite.com/

(vous remarquerez qu’en observant le code source, on tombe sur un script google analytics)

Ces choix, ces manières de faire "graphiques" se remarque également dans le code

ci joint le code html de la mise en page d’un paragraphe sur les 4 différents sites

##### 2014 Material Google

    <section class="intro section-wrapper">
      <a class="gweb-smoothscroll-control paper-button btn-next" data-g-event="nav" data-g-label="scrolldown" href="https://web.archive.org/web/20140625223441/http://www.google.com/design/#resources"><span>Down</span></a>
      <div class="span-9" id="intro">
        <p>
          At Google we say, “Focus on the user and all else will follow.” We embrace that principle
          in our design by seeking to build experiences that surprise and enlighten our users in
          equal measure. This site is for exploring how we go about it. You can read our design
          guidelines, download assets and resources, meet our team, and learn about job and
          training opportunities.
        </p>
      </div>
    </section>

##### 2016 ancien Google
    
    <li>
      <h2>
        Focus on the user and all else will follow.
      </h2>
      <p>
        Since the beginning, we’ve focused on providing the best user experience
        possible. Whether we’re designing a new Internet browser or a new tweak to the
        look of the homepage, we take great care to ensure that they will ultimately
        serve <strong>you</strong>, rather than our own internal goal or bottom line. Our
        homepage interface is clear and simple, and pages load instantly. Placement in
        search results is never sold to anyone, and advertising is not only clearly
        marked as such, it offers relevant content and is not distracting. And when we
        build new tools and applications, we believe they should work so well you don’t
        have to consider how they might have been designed differently.
      </p>
    </li> 

##### 1999 ancien Google
    
    <li>
    <p><b>An Elegant, Easy-to-Use Interface</b>: Google's clean,
      uncluttered interface is designed to make it easy for users to
      enter search queries and interpret results.  Results are
      presented with context sensitive summaries so users can easily
      tell if the corresponding web pages will satisfy their need.
      Users can also enter a query and click the "I'm Feeling
      Lucky<sup>TM</sup>" button, which takes users directly to the
      website of the first search result. For example, entering
      <code>smithsonian</code> into Google search field and clicking
      the "I'm Feeling Lucky" button takes the user directly to
      www.si.edu, the official homepage of the Smithsonian
      Institution.
    </p>
    </li>

##### 2008 ancien Google
    
    <h3>Ten principles that contribute to a Googley user experience</h3>
    <p>
      <a href="https://web.archive.org/web/20080430190246/http://www.google.com:80/corporate/ux.html" id="useful"></a><strong>1. Focus on people</strong> – <strong>their lives, their work, their dreams.</strong>
      <br>
      <br>
      <span>The Google User Experience team works to discover people's actual needs, including needs they can’t always articulate. Armed with that information, Google can create products that solve real-world problems and spark the creativity of all kinds of people. Improving people's lives, not just easing step-by-step tasks, is our goal.</span>
      <br>
      <br>
      <span>Above all, a well-designed Google product is useful in daily life. It doesn’t try to impress users with its whizbang technology or visual style</span> <span>–</span> <span>though it might have both. It doesn’t strong-arm people to use features they don’t want</span> <span>–</span> <span>but it does provide a natural growth path for those who are interested. It doesn’t intrude on people's lives</span> <span>–</span> <span>but it does open doors for users who want to explore the world’s information, work more quickly and creatively, and share ideas with their friends or the world.</span>
    </p>

Ce qui saute aux yeux dans le code c’est la "nudité" du html dans les codes 2016 2008 1999 

    <p>
      paragraphe
    </p>
La mise en page est « sémantique » avec les "tics" de programmations spécifiques à chaque "époque" du web

     <b>texte en gras</b>
En 1999 la balise `<b>` pour rendre gras, cette méthode est dépréciée dans les années 2000

    <strong>texte "intensifié", gras et légèrement plus grand</strong>
    <br>saut de ligne
    <br>
    <span>?<span>
En 2008 la balise `<strong>` pour intensifié le texte (plus sémantique, accessible, compréhensible par les néophytes que le rendre gras ?).
La balise `<br>` pour sauter une ligne déprécié la décennie suivante.
La balise `<span>` pour créer des divisions dans le texte idem car peu sémantique idem.

    <li>
      <h2>
       titre
      </h2>
      <p>
        paragraphe <strong>mot en gras</strong> paragraphe
      </p>
    </li> 
La version de 2016 est en fait la plus proche du html brut de base

Le Material Google de 2014 est bourré d’informations liés à d’autres langages, css, javascript, on retrouve de nombreux style et id tout au long du code 

    class="gweb-smoothscroll-control paper-button btn-next"
ces marques témoignant de la machine à gaz qui tourne derrière chaque paragraphe

    <section class="intro section-wrapper">
      <div class="span-9" id="intro">
        <p>
          paragraphe
        </p>
      </div>
    </section>
La programmation suit une logique « d’emballage », les éléments sémantiques sont entourés de couches abstraites qui servent de lien avec de nombreux scripts contrôlant dynamiquement la mise en page à l’échelle de l’élément, on approche d’une conception « atomique »¹ de la mise en page très éloignée de la pensée de la page web en tant que « document ». La page devient une sorte de bac à sable ou les éléments s’organisent par les interactions d’autres élements de leur échelle tandis que dans le document on suit une logique "journalistique" hiérachisée Article > Titre > sous-titre > paragraphe.

### Praise to be Duarte
#### Un messie chez google

Pour comprendre cette nouvelle mise en page il est nécessaire de intéresser à Mathias Duarte's 

environement ikea, aseptisé ? Feutres trias, street photography now, crappy design book, design know, illustration know.

innovative ambiance ?
work space
trendy co-working spaces

type resto ?
mac book

musique piano emotionnelle, espoir, fascination, calme, concentration, nostalgie.

    fonds boisé design modern nordic ? Rires exagérés, chemise desigual
the intersting thing about, design for software, design for pixels, is : its one of the most difficult material to work with. 
    
    Mathias joue avec des feutres et des post-it (bac à sable ?) atelier hyper clean lavabo, on vois un imac plein de post-it
    Google Headquarters, Mountain View, California
To be an artist you have to understand your material, you have to understand what its good at doing or what is its bad at doing.

     on vois des livres: Sketching user experience, design & identity, designing with ? SP- FREE ? la musique s’intensifie progressivement
There is not decades or hundreds of years of making haem software or apps or operatings systems the way there is for making shoes or spoons or ah even a car. 

     workspace loft, ourlets au pantalon, adidas, nike sport, pantalon toile, pull uni, casquette en jean, luentter monture noir epaisse
So what is the right way to design, what is the right way to build ?
I've allways insisted we have to keep an open mind.
Where i can know the irhgt way to do this ? For decades still.

    (tape la tete en riant) ha la bonne blague blow minding
I know ive tell this story before but its stick so much in my mind. I remember the 

     Sorte de cuisine google, couleurs assorties jaune citron, vert beige, materiaux mi-récup mi luxe. Plan sur des vélos (luxe ?) whaou on va au travail en vélo
first day at google thinking. Wouah this is really exiting, thats google orientation, they got that hole process, they tell you everything about how great google is and you know all the parts of google, and the scale and…! 

     musique emotionnelle de plus en plus intense 
There was kind of like, a list of things that i knew where gonna be important to get that haem. What good for users and how/had good design can be respected and recognize trhougt design. 

There was problably not until… what was it ? Lollipop ? 

     mathias semble travailler avec plaisir, il soulève une feuille vierge immaculée puis la repose sur sa table en bois vernie si propre qu’elle reflete comme un miroir, des shémas au feutre la parcours (whaou écrire sur la table, think outside the box)
I think when we rolled out material design. It happend a time when people where exited about this tech nostalgia. Lets get everybody to rally around somthing, that is 

     emotionaly connection correspond avec un plan sur un puzzle en bois enigmatique, jouet interdit ? Kubrick ?
extactic ! They emotionaly have a connection to. We thought ok, lets work together, lets go all in on this, let make it a project and a initiative. So what it is it ? what its unifiyng idea, we need to get everybody toghever, we need to figure out how to extend material design not just to the phone and desktop but also to the new plateforms.

     mathias regard interessé et mystérieux sur table de travail, prise de recul ?
     très très gros plan sur des feutres stabilos (special roughs)
We're trying to represent and think about all of google, and kind of provide an infrasrtructre and a tool kit cross all of google.
The google design website is a perfect example of that, the websites is run by designers from material design team.

     mathias est fimé entrain d’être pris en photo, puis il rit de bon coeur , re pose
 i want to have a more human... design process, and one that respect kind of the individual diognity of every person by creating a safe place  for people to contribute stuff.

     zoom sur tete (vidéo est très coupée, le discours ressemble à un collage toutes les deux phrases)
enable some to do there best work, to learn faster, to unlock their full potential.
Well at the same time taking full advantage of the peers and the senior designer mastery and maintain and set a high bar and a high quality standard and constently keep pushing.

La plus grande réalisation de Duarte a été le développement du langage visuel de Material Design-Google pour ses multiples plateformes, un ensemble de règles qui guident tous les produits de l’entreprise à travers le mobile et le web. Le projet a débuté vers 2013, lorsque l’équipe de Google Search tentait de dépasser le statu quo des " 10 liens bleus " qui existait depuis une décennie. Au même moment, une équipe de Chrome cherchait à faire progresser certaines technologies avec les équipes d’applications Google. Et l’équipe Android de Duarte se préparait à travailler sur sa prochaine sortie, Lollipop. Pour unifier l’expérience utilisateur à travers les équipes Search, Chrome et Android, Duarte a développé un langage cohérent et unifié, capable de passer du téléphone au portatif en passant par la télé et la voiture.

Traduit avec www.DeepL.com/Translator 

Duarte’s largest achievement was developing Material Design—Google’s visual language for its myriad platforms, a set of rules that guide all of the company’s products across mobile and web. The project began around 2013, when the Google Search team was trying to break out beyond the “10 blue links”’ status quo that had existed for a decade. At that same time, a team within Chrome was looking to advance some technologies with the Google apps teams. And Duarte’s Android team had been preparing to work on its next release, Lollipop. To unify the user experience across the Search, Chrome, and Android teams, Duarte developed a cohesive, unified language that could scale from phones to wearables to TVs to cars.

The impact Duarte has had on user-experience design is deep, and not just at Google, where he has been a vice president of design since 2015. He once oversaw the design team that created the Danger Hiptop, known to us consumers as the T-Mobile Sidekick. In the late aughts, he was a VP at Palm, where he led development of the company’s WebOS platform with its revolutionary “card” system. At Google, Duarte convinced the notoriously engineering-first company to embrace design, developing a single visual language applied to all of its products, from email to home-automation software, quietly affecting the experiences of millions of people every day.

In 2010, following Palm’s acquisition by HP, Duarte started at Android, working for Andy Rubin, who was CEO of the company—which Google had acquired in 2005—and had previously worked with Duarte at Danger. Before his first day, though, Duarte wanted to make it clear that he did not want to get sucked into the then-engineer-heavy Google vortex. “I never thought I’d work for Google” he says. “I had zero ambition to work for Google. Everybody knew Google was a terrible place for design.”

## 29jun2014

### Un document en avant-première

Le Material Design 

Savez-vous ce qu’est le Material Design ?
Décrire le Material Design n’est pas simple.

il est surpenant de constater la quantité d’articles ou de commentaires soulignant le caractère incompréhensible du Material Design, sous tout ses aspects, dans le discours, la théorie et la pratique.
Notamment sur ce qui le distingue du 'flat-design’ ou du 'skeuomorphisme'

### Mathias le roi de la surface

#### Mathias le moderne

#### Un phénix en papier

« […] Nous nous sommes mis au défi de définir les principes physiques sous-jacents à nos interfaces pour élaborer un langage visuel qui synthétise les principes classiques de ce qui fait un bon design avec une solide compréhension des principes physiques les plus fondamentaux. Au début, nous avons pensé comme des designers, comment le présenter, quel apparence lui donner. Puis nous avons pensé comme des scientifiques, pourquoi se comporte il de cette façon ? Enfin après de nombreuses expérimentations et de nombreuses observations nous avons écrit tout ce que nous avons appris. c’est ça le principe du Material. […] » 

puis ainsi par Mathias Duarte :

« Le tout premier principe dans le Material Design est celui de la métaphore. […] Les métaphores sont vraiment puissantes car elles sont profondes de sens. Elles communiquent plus richement que le langage verbal le permet. […]
La métaphore est une forme de transmission de la connaissance qui dépend des expériences partagées, de fait cette capacité, la transmission de la connaissance et de l’apprentissage font parties des choses qui définissent l’humanité et de fait défini l’intelligence. Donc pour nous, l’idée de la métaphore est une histoire de fond pour le design. Elle unifie et façonne le design. Elle a deux fonctions, elle fonctionne pour notre audience, nous voulons présenter une métaphore qu’ils peuvent comprendre avec laquelle ils peuvent se connecter, qu’ils peuvent utiliser pour aller plus vite dans la compréhension de comment on utilise les choses. c’est aussi une métaphore pour nous-même, pour les designers et les développeurs et les project managers, et les gens de Quality assurance. Tout ces gens travaillent ensemble car quand vous avez une métaphore que tout le monde comprend, comprend intuitivement, vous n’avez pas à expliquer comment ils ont violé la sous-section C clause 2 de votre charte graphique, ils sentent tout simplement que c’est faux, hors-sujet. Donc pourquoi cette métaphore particulière, pourquoi avons-nous imaginé un matériau qui à une forme de papier suffisamment avancée pour se confondre avec de la magie, et bien d’une part il est évident que nous avons une grande expérience de la communication sur papier, elle est juste riche dans une histoire traversant l’ensemble de nos cultures de la transmission d’information et cela naturellement permet tant de différentes manières d’interagir avec. Mais l’autre aspect du papier est que c’est quelque chose de physique qui existe dans le monde, et cette idée que les surfaces parce qu’elles sont tangibles sont une métaphore que nous pouvons utiliser pour accélérer la compréhension de notre UI est vraiment importante. Vous avez cette perception des objets et des surfaces qui se passe dans les parties les plus primitives du cerveau, ça arrive dans ces cortex visuels qui sont dans la partie arrières et basses de votre cerveau et cela signifie simplement que … »

comme un «langage visuel synthétisant les principes classiques d’une bonne conception, de l’innovation visuelle, les possibilité technologiques et scientifiques».

sur la page google.com/design/spec/material-design le Material Design est défini ainsi :

« Nous nous sommes mis au défi de créer un langage visuel pour nos utilisateurs qui synthétise les principes classiques d’une bonne conception (“good design”, dans le texte) avec l’innovation et les possibilité offertes par la technologie et la science. Ceci est le material design »

Pendant la conférence une personne demande pourquoi il y a des cercles en quantités, la personne dans une sorte d’ironie mêlée d’un sincère engouement s’excuse par avance en disant qu’elle est développeur et que sa question peux paraître bête pour un designer. Néanmoins cette personne demande de parler de ces cercles, où sont sont ils appropriés ? Que permettent ils de transmettre à l’utilisateur.

Mathias Duarte réponds à propos du travail de simplification qu’ils ont menés, la tentative de conserver uniquement des formes primaires élémentaires de «bas niveau» (dialectique de la raison). 

Que ce qu’ils font c’est de tout ramener aux formes géométriques les plus simple et basiques. Que nous pouvons y voir des cercles, des carrés, des rectangles, des divisions basiques de l’espace (structuralisme, bauhaus, nouvelle typographie). qu’ils utilisent le cercle car naturellement il contraste avec l’espace qui est divisé, qu’il est un très bon moyen d’attirer l’attention de l’oeil sans effectuer de mouvements, qu’il peux indiquer une action primaire, qu’il créé du rythme par lui-même. Que le cercle est un outil à penser comme n’importe quel autre outil visuel. Que c’est une forme qui se propage de son point d’origine, qu’ainsi il correspond en termes d’affordance avec le toucher sur une surface et la propagation de sa force.(gestalt théorie)

Plus tard une autre question, la personne se présente comme UX UI designer, sa question concerne la manière dont s’animent les formes, il commence par poser un contexte (sans en faire la critique) où nous savons tous que le travail d’animation sert principalement à faire sortir du lot une application et à conserver sa popularité, que dans les agences de design les animations sont élaborés sur fireworks, after effects, origami ou même à la main, et que la principale des difficultés lors de l’élaboration d’une app est d’arriver à transmettre aux développeurs l’explication de comment on va interagir avec l’application et qu’il se demande si Google va fournir un outil permettant d’aider les designers à réaliser leurs animations, si dans les directives du guide que sera le material design il y aura en plus des explications sur les animations sur lequels se concentrer par défaut un outil permettant aux designers de transmettre aux développeurs les points nécessaires concernant les animations prévues …

Une autre personne pose la question des couleurs, son constat est le suivant on peux voir partout beaucoup de couleurs éclatantes pour ne pas dire criardes ces derniers temps, et elles se retrouvent partout dans la présentation, qu’il y a beaucoup de combinaisons colorées, comment ses couleurs vives sont-elles choisis dans le contexte du material Design, comment charment elles l’utilisateur, quel sens doit-on en tirer, et ne sont elles pas si brillantes qu’elles deviennent désagréables. 

La réponse immédiate Jon Wiley sous forme d’une blague entre collègues designers est qu’ils ont testés 41 nuances différentes. Puis Johnatan Lee explique qu’ils ont intentionnellement choisis des couleurs pour leur exubérance, afin de caractériser clairement les produits de la firme. Mais que à coté de cela les directives proposées concernant les couleurs ont été pensées pour être souple, de proposer des conseils d’associations de couleurs concernant des gammes colorées très diverses, même très discrètes afin que chacun puisse s’y sentir chez soi.

En 2008, la même année où Donald Norman avoue publiquement sa répulsion pour le terme “user” et explique préférer “people” citoyen, personne, indivu le 30 avril cette section est carrément renommée «design principles» c’est à dire «principes de design», et il ne s’agit plus pour google de se focaliser sur l’utilisateur mais sur l’individu “people” «we design for people we dont design for users» nous désignons des expériences et non des utilisations, nous designons des objets en sorte que leurs consommateurs en aient une expérience et non une prise en main, qu’ils n’en fassent rien si ce n’est recevoir leur message. 

#### veille de l’officialisation du material design

#### Un Design pour tous les guider

Le Material Design est présenté comme un ensemble de directives destinées aux designers et développeurs d’interfaces graphiques. Les directives proposées insistent sur l’utilisation de formes géométriques élémentaires aux couleurs vives contrastées et complémentaires au comportement imitant de façon minimaliste les propriétés physiques du papier, de la page.

le 25 juin 2014 Google introduit sur son site une section design et dévoile son propre courant/discipline (discipline car ayant des disciples) de design, le “Material Design”. Le “Material Design” est une transposition graphique de leurs précédentes règles/philosophies/pensées, les mêmes arguments viennent alors commenter des formes, des mouvements, couleurs, choix de composition et de typographie. 
Le “Material Design” apparait comme une synthèse de ce dont le «citoyen» est habitué à observer sur son écran. Lors de ce dévoilement de nombreux articles établissent une parenté entre les deux grosses tendances graphiques ( en oubliant une troisième, le texte ) présentes sur le web c’est à dire le skeuomorphisme (voir les potentiomètres de Reason (jecrois) ou le micro de Apple) et le flat (sorte de style suisse international appliqué au web design) ce qui revient à la classique dichotomie figuration–abstraction et décrivent le “Material Design” comme la synthèse des deux modes de pensées, des mouvements s’ils existent. Pour ma part je pense que ce qui se joue dans le “Material Design” est plus proche du travail sur la norme telle qu’on pu le faire les suisses… à voir. Cette «méthode» de design s’appuie sur le matériau comme métaphore et propose d’envisager tout élément d’interface comme une matière «concrète» ayant des caratéristiques physiques (poids, texture, position, exposition à la lumière). Le matérial design se fait alors passer pour une sorte d’abstraction de la matière, métaphore abstraite qui serait la raison de son caractère innovant et unificateur, mais cette métaphore ne serait-elle pas simplement celle de la feuille de papier ?

#### formules sont déplacées 

formules des principes et des 10 things 

#### une arme pour rivaliser avec l’app store

Depuis 2014 Google publie des directives destinées à la conception d’objets visuels, interactifs et animés. La stratégie étant d’unifier l’ensemble de leurs services tout en poussant les développeurs passant par la plate-forme de distribution de l’entreprise (Google Play) à respecter certaines règles d’ergonomie et d’apparence, Google ne soumettant pas les produits du “play store” à un processus de validation pour se différencier de l’App Store d’Apple

Topolsky hails the system as game-changing. “His concept of compartmentalizing information inside of cards—he was so ahead of the game on that,” he says. “I want to be very clear, when Apple introduces iOS 7 [in 2013] and Jony Ive leads the redesign, they are all over cards and the concept of what Matías had introduced both in WebOS and later with Android. They are full-scale lifting from his design, in my opinion. And not doing that great a job of it, by the way. Matías’s design, to me, had a much more coherent underlying system. You see with Apple the lack of a clearly thought-out system.” Apple did not respond to a request for comment.

John Maeda, the global head of computation design and inclusion at Automattic, the web development company behind WordPress.com and Jetpack, among other products, disagrees with Topolsky’s Apple thesis, but does credit Google with software mastery. When asked if he thinks Apple has ever followed Duarte’s lead, he says, “No, I think Apple has played its playbook over and over. And it kind of works, but it’s less appealing than it was in the past. It’s kind of like Sony in its heyday.” He continues, “On the computational design side, Google is winning because it has better machine-intelligence chops. That’s where the world will be won, and that kind of design is frightening and interesting.”

#### la soumission graphique librement consentie

### La nouvelle typographie du web

#### Do / Don’t

Le Document

Dans sa première version en 2014 le document se compose de 41 chapitres comme autant d’aspects définissant l’interface graphique en tant qu’objet, regroupés en 8 parties comme autant d’aspects définissant le travail de design d’interface graphique.
- Nouvelle Typograhie de Tschihold 
- le sommaire des guidelines du material design est tellement exaustif qu’il décrit un interface

## 2017

### Le Material Design, le nouveau Bauhaus

le design semble changer drastiquement, plus plat plus géomtrique, couleurs changent beaucoup aussi, beaucoup plus de noir, fini le papier? pourtant les guidelines restent très similaire, protéiforme ? 

#### László Moholy-Nagy.com

logo ressemble énormement à celui du bauhaus dessiné par LMN, aucune reference n’est explicité, comme dans la conférence io

#### Une révolte devenue tradition

se referé au design moderne avant 2 guerre c’est se référer à la reblelion moderne , inspiré du futurisme, manifeste, tschichold réclame clairement une sission avec le passé.
Ce fond progressiste disparait completement au profit des formes produites dans ce contexte.
Completement decontextualisé réutilisées par "habitude" = facilité, ces formes prennetn toute la valeur traditionnelle à laquelle ell étaient censées s’opposer.

En allant sur cette adresse nous accédons à la page index du site.

Le texte est en langue anglaise, la fonte utilisée est une monospace avec serif.

La page se divise en trois parties horizontales placées l’une au dessus de l’autre.

Les deux premières parties possèdent un fond d’un gris très sombre, proche du noir , et occupent 6 divisions sur 7 de la hauteur. Des marges latérales les ramène plus au centre. La dernière partie, beaucoup moins haute, possède un fonds très clair et occupe intégralement la largeur de la page. Elle forme une sorte de bande claire collée en bas de la page.

La première de ces parties tout en haut, comporte dans son premier tiers horizontal principalement du texte tandis que le reste de sa surface est occupé par des figures géométriques en mouvement permanent.

La deuxième partie, au centre, est divisée horizontalement en trois blocs verticaux dont l’essentiel de la hauteur est en hors-champ, en partie dissimulés par la bande  du bas de la page. Le premier bloc possède un fond gris sombre et comprend du texte. Le deuxième sur fond blanc comprend du texte et ce qui semble être le haut d’une forme géométrique. Le troisième sur fond jaune orangé comprend aussi du texte et ce qui semble être le haut d’une forme géométrique.

La dernière partie, la bande, est elle même verticalement divisée en deux bandes comprenant toutes deux une forme géométrique et du texte, le fond de la première est blanc, le fonds de la deuxième est d’un gris très clair.
Dans l’ensemble de la page, lorsque le texte est sur un fond sombre il apparaît blanc, lorsqu’il est sur un fond clair il apparaît du même gris très sombre que le fonds des deux premières parties. 

Dans la première partie, en haut à gauche de la page est inscrit en lettres capitales "MATERIAL DESIGN". Il s’agit du titre, du nom du site. Celui-ci est accompagné d’un symbole, un logo, positionné à sa gauche. Il occupe une surface équivalente à quatre de ses signes, deux de haut et deux de large. Ce symbole est constitué d’un triangle équilatéral blanc pointant vers le bas, superposé à un carré gris clair lui-même superposé à un cercle d’un gris plus sombre. Les trois figures s’assemblent en partageant des mesures équivalentes. Le coté supérieur du triangle et du carré se confondent, tandis que la pointe du triangle correspond au milieu du coté inférieur du carré, les quatre points du carré atteignent le périmètre du cercle sans le dépasser. Cette combinaison forme une sorte de 'M'.

Juste en dessous de ce titre il y a un petit paragraphe de quatre lignes 
"Material Design is a unified system that combines theory, resources, and tools for crafting digital experiences." Juste en dessous de ce paragraphe est inscrit une note, soulignée en petites capitales "WATCH THE VIDEO". Au survol de la souris le soulignement fais un léger mouvement vers le bas.
Le titre et son logo, le paragraphe et sa note occupent la partie 
supérieure gauche de la page. La partie supérieure droit

À droite Les trois figures du logo sont répétées et agrandies de façon à occuper un tiers de la hauteur et un peu plus d’un tiers de la largeur de la partie supérieure de la page. Tracées par un contour gris légèrement épais leur surface laisse entrevoir la couleur sombre du fond de la page. Le Triangle, le carré et le cercle du logo ne se superposent plus mais produisent une sorte de chorégraphie combinatoire. Les trois figures glissent horizontalement, parfois tour à tour, parfois au même moments. Échangeant leurs précédentes positions. Tandis que le cercle et le carré se contentent de glisser, le triangle procède en plus à des rotations inattendues. Il ne pivote jamais vraiment mais change de sens en déplaçant ses points le long des différentes mesures des autres formes. Parfois il épouse même entièrement la forme du carré en se rajoutant brièvement un quatrième point. Les trois figures arrêtent leur chorégraphie à intervalles réguliers, semblant choisir une position pour nous montrer pendant un cours moment un signe puis changent de nouveau leur configuration et reprennent une pause etc.

# Xerox, la bible et les grecs.

## Aux origines de l’interface graphique ( du graphisme à l’écran ? )

### La culture des interfaces graphiques

#### le bureau

#### le travailleur en col blanc

#### le scribe

À l’écran notre environnement visuel est essentiellement constitué de pages et de fenêtres.
La métaphore du bureau est décrite comme une aide pour les utilisateurs pour interagir plus facilement avec l’ordinateur.

Il fut un temps où les bureaux étaient en réalité des bureaux

http://www.orilliapacket.com/2015/09/04/there-was-a-time-when-offices-were-actually-offices-and-the-technology-can-be-fascinating

Documents, fichiers, pages, cet environnement est celui du travail d’entreprise, bureau, comptable, administratif, économique, commercial, mercantile.
d’une certaine classe sociale, d’un certain héritage idéologique.

c’est l’environnement du travailleur à col blanc, du pouvoir (monastique)

Le moine copiste aussi utilise un bureau.

wikipédia, https://fr.wikipedia.org/wiki/Scribe :

>Un scribe est, au sens historique, une personne qui pratique l’écriture. Son activité consiste à écrire à la main des textes administratifs, religieux et juridiques ou des documents privés, et à en faire des copies. Il peut alors être assimilé à un copiste ou à un écrivain public.

wikipédia, https://fr.wikipedia.org/wiki/Copiste#En_imprimerie :

>Un copiste est un professionnel chargé de la reproduction de documents écrits ou d’œuvres d’arts. Ce métier est né de la nécessité de produire des copies de documents administratifs et de textes destinés à l’enseignement et à la propagation du savoir, bien avant l’invention de l’imprimerie, puis de la photographie et d’autres moyens de reproduction.

En 1991 Mark Weiser dans TCOT2C juge que l’ordinateur personnel comme technologie de l’information (malgré son succès commercial) a du mal à s’intégrer à l’environnement. Abordable uniquement par le biais d’un vocabulaire complexe très éloigné des tâches pour lesquels les gens l’utilise. Il compare cette situation avec la période durant laquelle les scribes devaient en savoir autant sur la fabrication de l’encre que sur l’écriture. Mark Weiser travail chez Xerox.

### Courte histoire de Xerox

#### origines du terme Xerox

#### un papier WYSIWIG aux origines de l’interface graphique

Le mode graphique de la matrice 
Le mode graphique d’une interface représente l’idéal de transparence explicite, langage universel datant de la renaissance et l’on retrouve cette pensée dans l’histoire de Xerox et de sa publicité.

>Inversement, l’IHM réinvente le paradigme du langage pictural universel des signes, d’abord envisagé dans les utopies éducatives de la Renaissance, de la Cité du Soleil de Tommaso Campanella à Jan Amos Comenius, en passant par le livre scolaire illustré "Orbis Pictus". Leurs objectifs de conception étaient similaires:"utilisabilité", fonctionnement explicite à travers différentes langues et cultures humaines, si nécessaire au détriment de la complexité ou de l’efficacité. 

florian cramer $(echo echo) echo $(echo)_ Command Line Poetics — LGRU Reader

Utopies éducatives
Villes nouvelles, villes utopies (années 60-70)
XVIIIe Salines Arc et Senans Claude-Nicolas Ledoux  

points communs dates, grecs, universalité, renaissance réaction à l’entropie de la seconde ?  

Entre 1935 et 1938 Chester Carlson s’intéresse à la photoconductivité et particulièrement à un article de l’ingénieur Pál Selényi (Tungsram entreprise hongroise d’ampoules et électronique), après plusieurs expérimentations ratées, quelques conseils de son épouse, l’utilisation d’un local appartenant à sa belle-mère et l’aide d’un physicien allemand réfugié, il fini par mettre au point un procédé d’impression qu’il nomme électrophotographie. d’abord, il faut écrire à l’encre sur une lame de verre, puis il faut frotter une plaque métallique recouverte de soufre avec un chiffon pour lui donner une charge électrique. Il faut placer la glissière contre la plaque, placer l’ensemble sous une lampe puissante pendant quelques secondes, enlever la glissière et saupoudrer du pigment sur la plaque. l’inscription apparaît. Cette technique est  celle des imprimantes laser, LED et employée  dans la plus part des photocopieuses et le pigment saupoudré sur la plaque est appelée toner. Carlson tente ensuite d’obtenir sans succès des financements de la part d’IBM, Kodak, General Electric et de RCA pour développer son invention.
c’est par le biais d’une association à but non-lucrative qui finit par obtenir un accord avec une entreprise spécialisée dans les services et produits photographiques nommée Haloid en 1947. En 1948 l’électrophotographie est renommée Xerographie du grecque *xeros* pour sec et *graphis* du latin pour écriture, terme plus *intuitif* proche d’une description du procédé de Carlson et le terme Xerox deviens une marque déposée. En 1956 l’ensemble des droits sur les brevets xérographiques sont conférés à Haloid et en 1958 Haloid change officiellement de nom pour Haloid Xerox puis Xerox en 1961 – inspiré par le fait que le nom Kodak est construit autour de la même première et dernière lettre.

>Kodak est un Mot créé arbitrairement en 1888 pour ses possibilités internationales d’emprunt par George Eastman (1854-1932) qui le déposa comme nom pour son appareil photographique portatif.
https://fr.wiktionary.org/wiki/kodak#en 

Dans le magazine fortune en 1961 Xerox insiste sur le fait qu’il n’y a rien d’ancien et de grecque dans l’esprit de l’entreprise soulignant plutôt son engagement à répondre aux exigences de la diffusion de masse de l’information.

Eva Hemmungs, NO TRESPASSING: AUTHORSHIP, INTELLECTUAL PROPERTY RIGHTS, AND THE BOUNDARIES OF GLOBALIZATION, Chapter Three THE DEATH OF THE AUTHOR AND THE KILLING OF BOOKS: ASSAULT BY MACHINE, pp 57-69

Dire qu’il n’y a rien de grec et d’ancien dans Xerox est faux car il y a tout de l’universalité occidentale.

Emmanuel Lévinas écrit: 
>«qu’est-ce que l’Europe? c’est la Bible et les Grecs. [...]  Il faut, par l’amour de l’unique, renoncer à l’unique. Il faut que l’humanité de l’Humain se replace dans l’horizon de l’Universel. Ο messages bienvenus de la Grèce! s’instruire chez les Grecs et apprendre leur verbe et leur sagesse»2. 
À l’heure des nations, Paris, éditions de Minuit, 1988, p. 155-156. dans Vieillard-Baron Jean-Louis. Hegel et la Grèce. In: Le Romantisme et la Grèce. Actes du 4ème colloque de la Villa Kérylos à Beaulieu-sur-Mer du 30 septembre au 3 octobre 1993. Paris : Académie des Inscriptions et Belles-Lettres, 1994. pp. 55-61.

Xerox c’est la bible et les grecs, confondus dans l’universalité.

voir frère Dominique, origines du nom.  

Xerox c’est l’universalité occidentale  marchande ? 

En ce sens, la Grèce est bien l’universalité de l’Occident. 

Car Xerox est marqué par la reproduction de l’écrit et sa diffusion, l’accessibilité et les références bibliques.

La publicité de xerox stigmatise cette pensée universelle occidentale

c’est au Xerox Park que 
Xerox est une entreprise qui vend des imprimantes avant tout et c’est dans cette 

Conçu au Xerox PARC en 1973, Xerox Alto est l’un des premiers ordinateur personnels PC, c’est également le premier ordinateur à comporter une interface dite graphique et un environnement de bureau.
interface textuelle (TUI)

##Dominique 1977

### Le travail manuel, une corvée archaïque

### l’âge sombre

Michelet

Marcel Pagnol, dans ses mémoires (cf. La Gloire de mon père), l’a décrit de manière synthétique comme étant le propagandiste des idées de la IIIe République à travers une relecture complète de l’Histoire de France. De lui découlent certains mythes républicains comme Clovis, François Ier, Henri IV, Louis XIV, la Révolution Française, Napoléon. De lui découlent surtout d’importantes erreurs historiques, notamment sur Louis XI, les guerres de religion et Louis XVI. Cette critique est reprise par plusieurs historiens dont Jacques Heers dans son livre Le Moyen Âge, une imposture qui voient en Michelet un propagandiste motivé non pas par la recherche de vérité mais par son idéologie (notamment anticléricale).

l’historien du haut Moyen Âge Pierre Riché reproche à Michelet d’avoir propagé le mythe des Terreurs de l’an mille dans le premier chapitre de son livre IV de l’Histoire de France « l’an mille » lorsqu’il écrit :

« Cet effroyable espoir du Jugement dernier s’accrut dans les calamités qui précédèrent l’an mille ou suivirent de près. Il semble que l’ordre des saisons se fût interverti, que les éléments suivissent des lois nouvelles. Une peste terrible dévasta l’Aquitaine, la chair des malades semblait frappée par le feu, se détachait de leurs os et tombait en pourriture… Une famine ravagea tout le monde depuis l’Orient, la Grèce, l’Italie, la France, l’Angleterre. »

Michelet décrit ensuite des scènes de cannibalisme et les paysans qui se réfugient dans les églises et qui font donation de leurs terres aux prêtres et aux moines34.

La thèse de Michelet selon laquelle la chute de Constantinople en 1453 aurait provoqué le basculement brutal du Moyen Âge à la Renaissance par un afflux en Occident de savants en provenance de Constantinople est aujourd’hui fortement nuancée par tous les historiens, à la suite des travaux de Charles H. Haskins sur la Renaissance du xiie siècle, publiés en 1927N 2. Haskins a en effet montré qu’un mouvement important de traduction des œuvres scientifiques et philosophiques grecques et arabes a eu lieu dès le xiie siècle, ce qui a été confirmé par tous les médiévistes ultérieurs, par exemple Jacques Verger35.

Dans La Sorcière36, Michelet accrédite la légende du droit de cuissage, bien qu’on n’ait jamais trouvé la trace d’une telle pratique dans le droit positif français ni dans aucune archive37.

#### Représentation pessimiste du moyen-age

Le Moyen Âge pouvait-il exercer un sens critique ? Pouvait-il concilier critique et tradition desautorités ? Un examen de la technique médiévale de compilation montre que, loin de devoir luttercontre la  tradition, le  discours médiéval peut  libérer un  espace critique (avec la  possibilitéd’innovations, d’écarts et de ruptures par rapport à la tradition) quand il assume au contrairel’hétéronomie de sa condition. Pour ne pas être ébloui par les Lumières, le médiéviste doit doncreconstruire les  catégories qu’il  emploie (auteur, individu, sujet, critique) dans l’esprit d’unincessant comparatisme avec la modernité. La réévaluation des pratiques critiques médiévalespasse inévitablement par un retour critique sur la vision héritée des Lumières.

Si les Lumières sont par excellence l’âge de la critique et de la raison, il n’y a qu’un
pas  à  faire  pour  suggérer  que  le  Moyen  Âge  n’est,  lui,  ni  critique  (mais  au  choix  :
conservateur,  dogmatique,  superstitieux,  etc.),  ni  rationnel  (mais  empreint  de
religiosité naïve, populaire ou traditionnelle, traditionaliste, obscurantiste, et pourquoi
pas intégriste). Les travaux de Louis Dumont nous ont mis à de nombreuses reprises en
garde contre de tels raccourcis (Dumont, 1985, 1991). La modernité s’est imposée selon
lui sur un coup de force, non seulement en faisant la promotion de l’individualisme,
mais aussi en dévaluant par la suite ce qui la précédait. Le propos de Dumont est alors
double : d’une part, en dépit de l’individualisme qu’elle met en valeur, la modernité
continue de fonctionner de manière hiérarchique ; il sied d’autre part d’entendre donc,
derrière  les  reconstructions  modernes  du  non  moderne  (irrationalité,  tradition,
hétéronomie,  aliénation,  inégalité,  etc.),  des  termes  plus  neutres  et  heuristiquement
plus  féconds.  Hiérarchie  et  holisme  social  permettent  par  exemple  de  penser  les
relations humaines médiévales comme modernes dans leur asymétrie, sans convoquer
pour  autant  l’idée  d’inégalité  :  avant  même  de  pouvoir  être  considéré  comme  un
individu soustrait à des déterminations, on se trouve socialement positionné comme le
fils de son père, le maître de son esclave, le vassal de son seigneur, le prêtre de sa
paroisse,  etc.  Vincent  Descombes  a  extrait  toute  la  substance  comparatiste  de  cette
leçon élémentaire, en suivant les pas de Tocqueville qu’il commente ici :

Les contemporains valorisent l’égalité. Du coup, ils lui opposent l’inégalité. Mais
comment pourrait-on faire de l’inégalité une valeur ? Elle n’est par elle-même
porteuse d’aucun sens ou d’aucune valeur. Tant qu’on en reste à la vision du sens
commun, les âges aristocratiques font l’effet d’avoir été déficients, immoraux,
incompréhensibles. Une comparaison sociologique permettra de surmonter
l’unilatéralité du sens commun et de ne pas opposer naïvement la présence du
bien (chez nous) et l’absence du même bien (chez eux). Pourtant c’est encore cette
vue du sens commun que reproduit le sociologue wébérien lorsqu’il définit la
modernité par la rationalité et ne peut donc lui opposer qu’une irrationalité
(routines de la tradition, arbitraire du charisme). Et c’est encore le sens commun
que vient fonder en raison le philosophe kantien qui oppose notre moralité de
l’autonomie humaine à la moralité hétéronomique des Anciens : de nouveau, on
cherche le contraste entre eux et nous dans une comparaison où nous incarnons
tout ce qui fait sens, tout ce qui est empreint de valeur, ce qui laisse à l’autre pôle,
pour définir l’autre forme d’humanité, la seule ressource de diviniser la servitude
et le non-sens. (Descombes, 1999, p. 70)

Il est aussi tentant que réducteur de confondre l’autre de la modernité avec l’inverse
de  la  modernité.  Le  procédé  est  banal  et  relève  du  simple  sens  commun.  Hérodotedécrivait  déjà  l’Égypte  comme  une  image  inversée  de  la  civilisation  grecque  :  sadescription de l’autre passait par une traduction de la différence (heteros et allos) eninversion (empalin)2.  La  Renaissance  y  avait  aussi  cédé  à  sa  manière,  si  l’on  pense5simplement  à  l’écolier  limousin  du Pantagruel de François Rabelais, aux propos deVasari sur la rupture picturale que représente Giotto ou aux travaux de Jacques Lefèvred’Étaples  et  de  ses  disciples,  qui  sont  «  autant  de  manifestes  dirigés  contrel’enseignement  scolastique  [...]  fustige[ant]  la barbariacrassa, insulsa, miseranda,inepta, taetra,  la rudis  nimis  et indocta  sermonis  absurditas  de  cette vulgaris,absurda, sterilis gothica plaga » (Balley, 2006, p. 129).

Il  n’est  pas  lieu  de  faire  un  procès  à  Hérodote  ou  à  Rabelais,  cela  va  de  soi.  En
revanche, l’historien peut prévenir ce genre de jugements pour le moins situés, sans
tomber non plus dans les travers d’un excès inverse. Car il convient d’éviter tout autant
un évolutionnisme de sens commun que son extrême opposé, à savoir le culturalisme.
L’historien  ne  doit  ni  se  considérer  comme  l’aboutissement  historique  et
l’accomplissement  qualitatif  de  la  société  ancienne  qu’il  étudie,  ni  accentuer  la
discontinuité entre deux époques au point de les rendre incommensurables. C’est de
cette  tension  entre  culturalisme  et  évolutionnisme  que  naît  le  comparatisme.  Plus
particulièrement le médiéviste se trouve au cœur de ce grand écart : son travail ne
consiste  pas  seulement  à  trouver  quelque  chose  de  passé  et  de  disparu,  mais  à
rechercher en réalité quelque chose que sa position même de moderne masque et voile.
À
 la façon de Louis Dumont, le médiéviste peut (et doit certainement) tendre à une
anthropologie de sa propre modernité et inscrire un principe de comparaison radicale
dans l’opération historique
3
, en construisant deux entités comparables l’une et l’autre
dotées d’une certaine positivité, et qui ne sont pas liées par une relation de symétrie
inverse.

#### Le moyen-age comme anti-thèse de la modernité

Pour la promotion de 

En 1975 Xerox diffuse une publicité mettant en scène un personnage nommé frêre dominique. 

Xerox “It's a Miracle” est un spot publicitaire datant de 1975.

Ce court film met en scène un personnage nommé frêre Dominique qui 

Dans cette publicité frêre Dominique  

Xerox : “Monks”

créé par Allen Kay (à ne pas confondre avec Allan Kay) pour promouvoir la sortie du Xerox 9200 duplicating system, diffusé durant le super-bowl de 1977, frêre dominique devient une icône pour Xerox.

Le très court film s’ouvre dans un scriptorium sombre. Assis face à un bureau, frère Dominique à l’aide d’une plume, reproduit avec soin un ancien manuscrit.
Son environnement comporte lutrin, plumier, encrier et quelques feuilles dont ce qui semble être un *bavoir à encre* sont disposées inégalement sur la surface de la table. Contrairement aux représentations communes, la table ne comporte pas de pupitre ; Dominique dessine à plat sur la table.
Sur fond de ce qu’on identifie comme des chants grégoriens, le narrateur en anglais déclare 
>«Depuis que l’homme à commencé à enregistrer de l’information, il fut nécessaire de la reproduire»
Every since people start to recording information there been a need to duplicate it

Durant cette narration la camera se rapproche du moine jusqu’à découvrir un plan à l’échelle de sa main et des caractères *gothiques* qu’il trace méticuleusement.
Le geste de sa plume semble d’une lenteur et d’une précision infinie, cet effet est accentué par un fondu enchaîné sur le visage du frère grimaçant et se massant l’*entre-yeux* de deux doigts. La tâche est si minutieuse qu’elle donne à Dominique des mots de tête. 

La pénombre, Le filtre sépia, le travail du décor, des costumes ajouté aux propos du narrateur donne le ton d’un certain type de reconstitution historique mettant en scène le moyen-age comme *l’âge sombre* une époque dystopique, mystique, perdue dans le temps abandonnée par la raison et la science proche de l’univers  visuel  du *nom de la rose*. Le jeu d’acteur simple et archétypal contraste avec la lourdeur de la reconstitution tout en renforçant la bêtise ou naïveté attribué aux homme du moyen age ainsi représenté.

Son œuvre achevée, il présente avec fierté son travail à l’abbé. Ce dernier se trouve assis devant un bureau à la configuration similaire, plus luxueux dans une pièce modeste, lumineuse et meublée. Tandis que nous sommes toujours accompagnés de chants grégoriens. En examinant attentivement le manuscrit qui lui a été présenté, l’abbé félicite généreusement Dominique pour la qualité de son travail. c’est avec un entrain à peine dissimulé qu’il fini par demander au scribe 500 copies supplémentaires.

Retournant sur ses pas, le sourire qui habitait le visage de frère Dominique s’estompe instantanément à la seconde il tourne le dos à son supérieur. l’instant suivant alors qu’il revenait penaud sur ses pas traversant le *cloitre* une idée géniale semble lui traverser l’esprit, il rebrousse chemin et s’empresse de passer une lourde porte métallique s’ouvrant sur une lumière blanche surnaturelle.
En passant cette porte frère Dominique se retrouve instantanément dans un magasin de copies *flambant neuf* de la fin du XXe 1975 (contemporain à la sortie du spot). Et les choeurs que l’on entendait continuellement en fond sonore se coupent.

Cette fois, l’environnement est très lumineux. Toujours peu coloré les tons passent du marron noir orange au bleu blanc et beige.

Dominique est reconnu immédiatement, tandis qu’une employée *printer girl* lui tiens la porte en souriant, un employé modèle, sosie de Clark Kent l’accueil chaleureusement un fichier à la main.
>«Hey ! Frère Dominique comment ça va?».

Le plan se ressert sur les deux personnages et l’on remarque clairement que l’employé dépasse le frère Dominique d’environ une tête et demi. 

Dominique lui demande en lui tenant amicalement l’épaule :

>«Peux-tu faire un grand travail pour moi ?»

Tandis que les deux personnages se dirigent vers une machine occupant environ un tiers de la pièce, on peux observer une armoire, contenant des encres et des ramettes de papier, des fiches attachées à des tableaux en liège accrochés aux murs, des plantes et des bureaux dans des pièces voisines, des employés s’activant autour des deux protagonistes, en pleine discussion ou transportant des dossiers sur un *caddie*.

Dans ce mouvement tandis le duo entame une discussion le son se coupe et le narrateur commente 

«le Xerox 9200 duplicating system contrairement à tout ce que nous avons jamais fait, scan et tri automatiquement des originaux grâce à un ordinateur intégré qui contrôle l’ensemble du système et peut dupliquer réduire et assembler un nombre pratiquement illimité des documents complets et le fait à une incroyable vitesse de deux pages par seconde»


the Xerox 9200 duplicating system unlike anything we've ever made feeds and cycles originals has a
computerized programmer that controls the entire system can duplicate reduce
and assemble a virtually limitless number of complete sets and does it all at an incredible two pages per second

arias Ed’s father 500 sets you asked for it's a miracle


le système de duplication Xerox 9200 contrairement à tout ce que nous avons jamais fait des flux et des originaux de cycles a un
programmateur informatisé qui contrôle l’ensemble du système peut dupliquer réduire
et assembler un nombre pratiquement illimité d’ensembles complets et le fait à une vitesse incroyable de deux pages par seconde.

Il retourne au monastère et livre les copies en un rien de temps. "c’est un miracle", dit le père. Le frère Dominic sourit vers le ciel.

1 DigiBarn TV Features: "Xerox Monks" Classic Superbowl ad for Xerox 9200 Duplicating System and other commercials in the "Xerox Monks" series by Allen Kay http://www.digibarn.com/collections/movies/digibarn-tv/xerox-monks/index.html

##Dominique 2017

###Esthétique post-moderne

###Xerox missionnaire

#### diffusion des textes sacrés

Thomas Römer – Il y a toute sorte d’usage, des usages communautaires et des usages individuels. Il y a des gens qui supportent mal que l’on contextualise, c’est-à-dire que l’on dise qu’il n’est pas possible de lire ces textes comme on les a lus il y a deux mille ou trois mille ans. L’on constate en France un certain retour au fondamentalisme, comme dans certains milieux aux États-Unis, où l’on pense que la Bible est un livre de recettes ou de réponses à des questions d’ordre sexuel, économique, ou tout ce que vous voulez. D’un autre côté, il y a aussi un intérêt nouveau pour la naissance de ces textes ; les gens veulent comprendre pourquoi ils ont tellement influencé notre culture dite judéo-chrétienne, et l’on essaie de plus en plus d’enseigner ces textes de manière éclairée et de les confronter à des questions actuelles. C’est une grande chance que nous avons, dans le cadre du Collège de France, de pouvoir faire se rencontrer la pensée chinoise et la pensée biblique, comme on l’a fait à l’époque de Voltaire.

Les textes sacrés dans un monde mondialisé
Anne CHENG et Thomas RÖMER
http://journals.openedition.org/lettre-cdf/2448

apparition du livre, diffusion de la bible 

On trouvera plus loin une rapide chronologie de la conquête de
l’Europe occidentale par le papier ; on verra d’autre part comment
l’apparition du papier et le développement de l’industrie papetière
permirent la naissance de l’imprimerie. En ce qui concerne le
manuscrit, le papier ne présentait pas d’autres avantages sur le
parchemin que son moindre prix et la possibilité que l’on avait d’en
produire une quantité en principe illimitée. Plus fragile, de surface
plus rugueuse (nous ne parlons ici que des papiers médiévaux, bien
entendu), d’une plus grande porosité à l’encre, il se prêtait moins bien
à supporter les pigments utilisés par les enlumineurs. Il avait, en
revanche, l’avantage d’être plus léger - moins cependant qu’on ne
pourrait le croire, car on était arrivé, au XIIIe siècle, à fabriquer un
parchemin d’une finesse et d’une souplesse extrêmes, plus mince
même que le papier du temps. Un grand nombre de petites bibles
latines du XIIIe siècle peuvent ainsi, par un double tour de force du
parcheminier et du copiste, n’atteindre que des dimensions inférieures
à celles des deux volumes qu’occupe par exemple la traduction
moderne de Lemaistre de Sacy. Certes, il faut pour les déchiffrer des
yeux exercés et perçants, mais ces bibles sont sans conteste plus
maniables et moins encombrantes que les premières et célèbres bibles
imprimées ; ce n’est qu’au XVIe siècle que l’imprimerie produira des
bibles portatives. 



On trouvera plus loin une rapide chronologie de la conquête de
l’Europe occidentale par le papier ; on verra d’autre part comment
l’apparition du papier et le développement de l’industrie papetière
permirent la naissance de l’imprimerie. En ce qui concerne le
manuscrit, le papier ne présentait pas d’autres avantages sur le
parchemin que son moindre prix et la possibilité que l’on avait d’en
produire une quantité en principe illimitée. Plus fragile, de surface
plus rugueuse (nous ne parlons ici que des papiers médiévaux, bien
entendu), d’une plus grande porosité à l’encre, il se prêtait moins bien
à supporter les pigments utilisés par les enlumineurs. Il avait, en
revanche, l’avantage d’être plus léger - moins cependant qu’on ne
pourrait le croire, car on était arrivé, au XIIIe siècle, à fabriquer un
parchemin d’une finesse et d’une souplesse extrêmes, plus mince
même que le papier du temps. Un grand nombre de petites bibles
latines du XIIIe siècle peuvent ainsi, par un double tour de force du
parcheminier et du copiste, n’atteindre que des dimensions inférieures
à celles des deux volumes qu’occupe par exemple la traduction
moderne de Lemaistre de Sacy. Certes, il faut pour les déchiffrer des
yeux exercés et perçants, mais ces bibles sont sans conteste plus
maniables et moins encombrantes que les premières et célèbres bibles
imprimées ; ce n’est qu’au XVIe siècle que l’imprimerie produira des
bibles portatives. 

file:///D:/Documents/2017-2018/ECRITURE/ressources/apparition%20du%20livre/apparition_du_livre_pt1.pdf

Très vite aussi, une simple feuille ne suffit plus ; les livrets
xylographiques, formés comme les livres de cahiers d’un format
correspondant généralement à notre petit in-4°, firent alors leur
apparition. Toute une littérature se développa ainsi ; on y retrouvait

65 C. MORTET, op. cit.,11 ; A. BLUM, op. cit., 35 et s., 52 et s. 
 Lucien Febvre et Henri-Jean Martin, L’apparition du livre (Première partie) (1958) 94
les thèmes religieux et moraux les plus populaires de l’époque :
Apocalypses figurées, Bibles des Pauvres, Histoires de la Vierge, ou
encore Miroirs de la Rédemption, Passions du Christ, Vies des Saints,
Arts de mourir, etc. Petits livrets dans lesquels le texte prenait de
l’importance à côté de l’illustration, ils donnaient aux « pauvres
clercs » isolés, des exemples pour la préparation de leurs sermons et
pour l’enseignement de la religion. Surtout, par leur prix et leur
conception, ils rendaient, pour la première fois, le livre accessible aux
classes populaires ; ceux mêmes qui ne savaient pas lire pouvaient
comprendre le sens de ces suites d’images et ceux qui possédaient
quelques rudiments - le succès même de ces livrets dont le texte
prenait une importance croissante semble prouver qu’ils étaient
nombreux - suivaient d’autant plus facilement les explications qu’elles
étaient rédigées en langue vulgaire. 

Mais, dès la fin du XVe siècle, les Alde, désireux de faciliter la
lecture des auteurs classiques, lancent leur célèbre collection
« portative ». Adoptée par le petit monde des humanistes, la mode des
formats réduits se répand de plus en plus au début du XVIe siècle : à
Paris, par exemple, où Simon de Colines, qui crée une collection
analogue à celle des Alde, trouve de nombreux imitateurs, à Lyon
surtout, où l’on copie souvent les modèles vénitiens. Bientôt, on édite
systématiquement les ouvrages littéraires nouveaux dans des éditions
de petit format aisément maniables et consultables. Si les anciens
romans de chevalerie continuent de paraître dans des éditions in-folio
ou in-4°, les poésies latines des humanistes, les œuvres de Marot ou
de Rabelais, celles de Marguerite de Navarre et bientôt celles des
poètes de la Pléiade, sont publiées dans des volumes de petit format.
c’est sous cette forme que les Adages d’Érasme sont répandus dans
toute l’Europe, de même que les multiples pamphlets que Luther et les
réformateurs font imprimer pour diffuser leurs idées. En même temps
cette mode gagne les livres illustrés. Vers 1540, Holbein compose de
petites vignettes pour des éditions in-4° et in-8° des Images de la
Bible, et des Simulacres de la mort qui ont un énorme succès 129. À
Lyon, chez les de Tournes, puis à Paris, chez Denis Janot, et bientôt
un peu partout, paraissent des éditions in-8° des Figures de la Bible,
des Emblèmes d’Alciat ou des Métamorphoses d’Ovide 130.
Cependant, les gens d’études préfèrent toujours pour les livres de
travail les in-folio, peu maniables certes, mais plus clairs et dans
lesquels il est plus facile de retrouver la référence que l’on cherche. 

p160

Lucien Febvre et Henri-Jean Martin
(1958)
L’APPARITION
DU LIVRE 

#### caricature culturelle

otto neurath 5 genres humains

###Un message résolument universel

Créé par l’agence de publicité Y&R ce remake en grandes pompes aux airs de blockbuster dure une minute de plus que son aîné. Au bord de la faillit l’objectif est d«'aider Xerox à embarquer dans une nouvelle ère, celle d’un retour aux sources du document numérique».[voir fuji]

Le court métrage s’ouvre sur le plan aérien d’un monastère ancien (monastère mythique du nom de la rose) haut perché sur une montagne.
Cette fois Frère Dominic ne travail pas dans un scriptorium sombre mais dans sa chambre. Il s’agit d’une pièce austère, aux murs décrépis et au parquet usé. Meublée au minimum, son lit aux allures d’un lit époque Napoléon III en fonte et laiton – iconique des dortoirs d’anciennes casernes ou hôpitaux – côtoie un lutrin, une armoire et un bureau accompagné d’une chaise placée devant une grande double fenêtre tandis que deux icônes religieuses constituent l’intégralité de la décoration.
Sur d’une flûte baroque accompagnée de cordes, assis face à un bureau, frère Dominique à l’aide d’une plume, reproduit avec soin un manuscrit récent (un carnet de notes épais); supposément illustré, pour la première fois, à moins qu’il s’agisse d’un manuscrit personnel. Son environnement comporte plumier, encrier mais également une lampe articulée, pinceaux, tampons, crayons et un pupitre sur lequel Dominique dessine incliné conformément aux représentations communes du moine copiste.

Contrairement à la version 77, aucun narrateur ne commente la situation.
Une nouvelle fois la camera se rapproche du moine jusqu’à découvrir un plan à l’échelle de sa main et des caractères *gothiques* qu’il trace méticuleusement.
c’est cette fois avec calme, patience et une pointe de mélancolie que frère Dominique achève son ouvrage. La flagrante corvée manuelle de Frère Dominic 1977 semble laisser place en 2017 à un pudique amour de l’artisanat.
Après avoir jeté un dernier coup d’œil à son travail pour se rassurer, le moine traverse une cour extérieure, entre dans une église, accède à une galerie en hauteur par un escalier avant de faire face à la porte du bureau de l’abbé qu’il entrouvre timidement sans toquer. 
On aperçoit dans l’entrebâillement une grande pièce aux murs recouverts de fresques pâlies par le temps. Au fond de la pièce est placé un bureau massif derrière lequel l’abbé assis sur un siège en bois exubérant – à la limite du trône – semble absorbé par un document. À ses cotés un moine se tient debout, surveillant les faits et gestes de l’abbé, sans doute un conseiller.
Sur le bureau de l’abbé on peux observer  quelques feuilles, tampons, un téléphone à cadran ainsi qu’une lampe électrique antique type lampe de banquier/lampe de bibliothèque. Sur le mur au dessus du bureau est accroché un tableau de ce qui semble être un clair obscur d’un vieil homme priant. 
À peine la porte entrouverte, le conseiller remarque la présence de Dominique. La musique se coupe tandis que ce dernier annonce sur un ton à l’agacement à peine dissimulé « Frère Dominique, Monseigneur »
Dominique entre, le visage marqué d’appréhension et d’inquiétude.
Il présente immédiatement et sans commentaires son œuvre à l’abbé qui presque instantanément le complimente d’un sincère « beau travail frère Dominique… Très beau ». Dominique le remercie tandis que le moine conseiller l’observe d’un regard noir. Très enthousiaste, l’abbé enchaîne « Maintenant nous aurions juste besoin de 500 copies supplémentaires … » Le visage de frère Dominique se décompose tandis que le père poursuit « … traduit en 45 langues … » (À ce moment le conseiller se met à regarder Dominique très calmement, satisfait) « … personnalisé pour chaque frère et chaque sœur… Oh, et diffusé au travers des sept continents ! ». Sur ce, l’abbé l’air enjoué et complice, rends à frère Dominique sa page enluminée et complimente encore ce dernier abasourdi (à moins qu’il commente sa propre idée) « Excellent, excellent… ». Le conseiller acquiesce en regardant frère Dominique, ce qui déclenche une sorte de signal chez ce dernier qui s’en va tandis que l’abbé et son second reprennent leur activité.
Frère Dominique avançant face camera en quittant le bureau, hésite un instant puis se met en route d’un pas sûr et décidé, retraversant le corridor, l’escalier, la cour…

Une musique épique monte doucement.

Il arrive alors dans une sorte de grand cagibi reconverti en salle d’impression contemporaine, flambante neuve comparée à ce que l’on a pu observer du reste de l’édifice. Le fond de la salle comporte une grande armoire, un bureau équipé d’un ordinateur au dessus duquel siège une icône de Jésus-christ côtoyant des étagères pleines de boites de cartons de fichiers et de classeurs, trois poubelles de tri blanches empilées. Au centre de la pièce deux tables sur lesquelles sont disposées quelques ramettes et une douzaine de feuilles vierges disposées de manière à être comparées. On peux remarquer une plume et un encrier sur l’une des tables. Au premier plan, là ou Dominique se dirige, est placée une corpulente imprimante. Frère Dominique semble parfaitement savoir ce qu’il fait tandis qu’il passe un badge sur le coté de la machine. Un plan rapproché nous permet alors d’observer le logo de Xerox adjacent au capteur de badge sans contact, on peux également lire sur le badge « Monastère de Saint Augustin ».
Frère Dominique glisse son ouvrage dans le scanner de la machine, sur un écran « home » composé d’une douzaine d’icônes, il touche du doigt un bouton sous-titré « Xerox Easy Translator », il réfléchi quelques instants puis sélectionne English dans la colonne « source Language » d’un nouvel écran « Language Settings »; puis « Spanish » dans la colonne « Target Language ». Nous nous retrouvons alors dans une cuisine ou cinq Nonnes semblent avoir préparer de la pâtisserie. Deux d’entre elles, une tablette à la main commentent en espagnole d’un ton enthousiaste, un manuscrit alors affiché sur l’écran de « l’ordinateur ultra-plat », celle de droite ajoute d’une voix chaude, sous le charme : « es del padre Dominic ! » ce qui semble surprendre et fasciner son interlocutrice. l’instant d’après nous nous retrouvons face à un smart-phone affichant de nouveau le même manuscrit mais encore dans une autre langue, cette fois dans les mains de moines bouddhistes au milieux des montagnes et de la jungle, tout aussi enthousiastes que les sœurs. Le plan suivant, une autre imprimante Xerox crache en série des versions arabes du manuscrit. Juste après qu’une main attrape la dernière, on découvre deux Imams assis sur des tapis en pleine cérémonie du thé, des poussières de sale semblent flotter dans l’air. l’instant d’après, c’est trois évêques qui célèbrent une version imprimé sur mug du manuscrit assis confortablement autour d’une coupe en argent garnie de fruits.
Des prêtres Shintoïstes devant la version web du manuscrit sur un macBook dans un temple japonais traditionnel. Une foule de juifs, la version a4 en main, l’intensité de la musique ne cesse de croître puis s’arrête… La porte de l’abbé s’entrouvre, on tombe sur la face curieuse et inquiète de Frère Dominique. « Encore frère Dominique Monseigneur » déclare d’un ton irrité le conseillé toujours debout aux cotés de l’abbé assis, tandis que le jour s’est couché. c’est cette fois une tablette que frère Dominique tend à l’abbé sous les regards perplexes du conseillé et du père. La lumière beauté de l’écran reflète sur le visage de l’abbé tandis qu’un sourire lui prend au premier coup d’oeil sur la tablette. Un imam Africain en visio conférence dans un marché lui montre l’imprimé du manuscrit « Look Abbot, I got it … It's wonderful » avec un accent très prononcé. l’abbé regarde Dominique et déclare « It's a miracle ».

Le trajet semble être l’occasion pour nous d’explorer l’édifice. Difficile de situer l’époque dans laquelle se déroule l’action. l’austère chambre rappelle fortement le XIXe siècle (on pouvait y trouver un interrupteur), l’édifice à tout d’un moyen-age veilli

# la discorde des mots, le conforme des images

##c’est quoi le graphisme à l’écran ?

###Une religion

#### le graphique est catholique, le textuel est protestant

#### dualité textuel != graphique

#### amalgame graphisme = image

###Un degré de perfection

#### évaluation 

#### mesure du beau

####  tester la beauté

#### évolution

##### le progrès du beau

##### la nostalgie du texte

###Le graphisme c’est naturel.

####Règne graphique

####Idéal graphique

####Un présent graphique

##Dwarf Fortress, au delà de la qualité.

###Le texte, pour le meilleur et pour le pire.

###Apprentissage et révélation

##Matrix, résistance textuelle, obéissance graphique.

###Une humanité divisé entre graphique

####mode graphique par défaut

####un capital graphique

###et textuel

####introduction au secret du texte

#### texte comme résistance

# Pourquoi le texte c’est moche ?

//////////////// début des notes 

- Questionner les termes, les distinctions : Interface textuelle / Interface graphique
- Enseignement du graphique, rapport au texte
- Text-based interface en Français
- Umberto Eco interface textuel : protestant (élitiste) / interface graphique : catho (populiste)
- Habitude au GUI par défaut ou CLI
- texte à l’écran perçut comme manque de design, Ex: Dwarf Fortress, avis des joueurs, 
- Lire la matrice
- Texte métaphore au même titre qu’un icône
- Espace de travail par défaut = bureau virtuel
- Le rapport à l’écrit dans les origine de l’interface graphique rapport à l’écrit

Le fait est que ce qui s’affiche sur les écrans des systèmes informatiques est divisé entre le graphique et le textuel.

La présence même minime d’éléments visuels hors d’une matrice de signes indique que nous sommes en présence de ce qui s’appelle un *environnement graphique* ou une *interface graphique* – de l’anglais GUI, graphical user interface. À l’inverse c’est lorsque que l’ensemble de ce qui nous est donné à voir est conditionné par une grille que nous nous trouvons devant un *environnement textuel*.

l’emploi du terme *graphique* – du grec ancien *graphein* (« écrire ») – en opposition à ce qui est de l’ordre du texte est contradictoire mais ce n’est pas un accident. 

//////////////// début des notes 

Un environnement en mode texte (TUI, de l’anglais « Text User Interface », « Textual User Interface » ou encore «Terminal User Interface » est un rétronyme introduit dans le jargon informatique après l’invention des environnements graphiques pour se distinguer des interfaces en ligne de commande. 
https://fr.wikipedia.org/wiki/Environnement_en_mode_texte

le textuel à l’écran se définit par l’absence de graphisme ?
le textuel à l’écran est nostalgique ?

À l’écran *graphique* tend à signifier hors de la grille du texte.

La distinction environnement graphique, environnement textuel symbolisait la division entre les utilisateurs d’ordinateurs Mac — un système d’exploitation à interface graphique – et les utilisateurs d’ordinateurs compatibles MS-DOS – un système d’exploitation à interface textuelle.
En 1994 Umberto Eco décrit cette opposition comme un déplacement méconnu de l’éthique catholique et protestante.

>Je suis entièrement convaincu que le Mac est Catholique et le DOS Protestant. En effet, le Mac est contre-réformiste et a été influencé par le "ratio studiorum" des Jésuites. c’est un système gai, convivial, amical, il dit au croyant comment il doit procéder étape par étape pour atteindre - sinon le Royaume des Cieux - le moment où le document est imprimé@. c’est une forme de catéchisme : l’essence de la révélation est abordée au moyen de formules simples et d’icônes somptueuses. Chacun a droit au Salut. DOS est Protestant, voire Calviniste. Il permet la libre interprétation des écritures, réclame des décisions personnelles difficiles, impose une herméneutique subtile à l’utilisateur et tient pour acquis que tout le monde ne peut pas atteindre le Salut. Afin de faire fonctionner le système, il faut interpréter soi-même le programme : loin de la communauté baroque des fêtards, l’utilisateur est enfermé à l’intérieur de la solitude de ses propres tourments.[^1]

[^1]:UMBERTO ECO Comment voyager avec un saumon Nouveaux pastiches et postiches TRADUIT DE l’ITALIEN PAR MYRIEM BOUZAHER GRASSET Titre original : IL SECONDO D1ARIO MINIMO Éditions Bompiani, Milan, 1992 

Aujourd’hui l’interface type est *graphique*. Depuis 1995 l’environnement textuel MS-DOS devient invisible pour les utilisateurs car l’interface graphique Windows est directement exécutée au démarrage.

Cette « nouvelle guerre religieuse souterraine qui transforme le monde moderne » décrite par Eco prend alors des allures d’occupation, opposant la résistance textuelle à l’obéissance *graphique*. Ce qui n’est pas *graphique* – c’est à dire ce qui n’est pas convivial,

//////////////// début des notes 

ce qui n’est pas nouveau ?

ce qui ne dit pas à l’utilisateur comment procéder étape par étape – sort de la normalité.

//////////////// début des notes 

l’user experience c’est ce qui est graphique ? 

//////////////// début des notes 

## Pourquoi le texte dans un jeu c’est moche ? 

//////////////// début des notes 

debug
January 14, 2017 at 3:43 pm Reply
It’s a myth that somehow command line interfaces are inferior or outdated. If you develop or manage anything in IT a lot of your day is still spent at the terminal because it is so efficient and easy to extend.

Not saying I don’t use a UI for most of my music making of course, but for updating stuff via midi this is nice. 
http://www.synthtopia.com/content/2017/01/13/sendmidi-brings-midi-to-command-line/ 

Appliqué à ce qui est affiché d’un jeu vidéo, le terme *graphisme* n’exclut pas le texte mais le désigne en tant que plus petite unité d’un système destiné à en mesurer la qualité.

La forte présence du texte dans ce qui s’affiche d’un jeu vidéo est un mauvais signe. c’est un manque de graphisme, un mauvais graphisme, une paresse au mieux une absence volontaire de considération esthétique ou une nostalgie fétiche 

//////////////// début des notes 

un archaïsme 

. Une telle désobéissance tend à provoquer un sentiment de gène au joueur.

//////////////// début des notes 

trouver un commentaire sur texte comme d’un autre temps ? 

Au travers des commentaires vis à vis des *graphismes* du jeu Dwarf Fortress ce statut dépréciatif du texte apparaît comme les stigmates de cette dispute théologique décrite par Eco.

À la fois considéré comme *le meilleur jeu de tout les temps* de part la complexité et la profondeur de ses mécaniques il serait également le pire à cause de ses *graphismes* susceptibles de *faire saigner les yeux*. 

>[…]Laissez-moi commencer par dire que j'adore l’idée derrière ce jeu. Il est juste tellement....... FUN!
Le hic c’est que les graphismes du jeu font saigner mes yeux[…]
Donc, je vous le demande à vous amis redditeurs et joueurs de Dwarf Fortress, avez-vous connaissance d’un mode remplaçant les graphismes du jeu par d’autres plus propres ? […] Qui fonctionne parfaitement avec le jeu et permet d’avoir une meilleure interface graphique (plus intuitive, moins fatiguant)?
https://www.reddit.com/r/dwarffortress/comments/5ohf67/dwarf_fortress_with_better_graphics/?st=jas5klp5&sh=3a5a29ea

Dans Dwarf Fortress le joueur incarne une sorte de dieu architecte chargé de concevoir et d’administrer la cité d’un peuple de nains (nain au sens de la créature fantastique popularisée par J. R. R. Tolkien). 

//////////////// déubt des notes 

Ce qui est caractéristique de ce jeu c’est l’énorme quantité d’information disponible pourtant 

Aucune information n’oblige ou dirige le joueur quant à la manière dont il doit procéder.  
Les pouvoirs qui lui sont accordés semblent tout à la fois illimités et dérisoires. Il peut par exemple accéder à l’historique des pensées de chacun de ses sujets tout comme il accède à la composition minérale des sols. l’ensemble très exhaustif des informations contenues dans le jeu tend à être hiérarchisé de manière horizontale. 

//////////////// début des notes 

Une donnée en soi n’est pas plus précieuse qu’une autre, et son importance très relative est défini par le contexte, exemple : obtenir une information sur la composition minérale du sol est possible dès le démarrage de la partie et n’aura aucune importance jusqu’à ce que par exemple un autre village s’intéresse spécifiquement aux chaises en granite ? 

l’une n’étant pas plus précieuse que l’autre et ayant une influence sur la partie tout à fait contextuelle.

Composé en ASCII le jeu se donne intégralement à voir à travers une grille de glyphes monospace. Il apparaît comme un texte composé d’une suite aléatoire de caractères duquel émerge une image abstraite, texturée proche d’un *bruit de Perlin*. 

Sur cette grille chaque glyphe peut changer de signification en fonction de son contexte d’affichage et de sa couleur. 

Dans l’écran principal du jeu les glyphes s’organisent de façon à représenter la vue aérienne d’un paysage. 

Dans ce contexte, le caractère 'd’ représente un dragon tandis que dans un écran de dialogue le 'd’ représente la lettre 'd’. Ainsi le caractère '•' lorsque les glyphes sont organisés pour représenter la vue cartographique d’une région signale la présence de la source d’une rivière tandis qu’il représentera un rocher sur une vue aérienne.

//////////////// début des notes 

c’est l’inverse de l’user friendliness 
Quel est l’exemple de l’user friendliness 

Autrement dit, dans ce jeu l’utilisateur est libre interprète des écritures, il doit prendre des décisions difficiles. Afin de faire fonctionner le système, il doit interpréter lui-même le programme. c’est l’utilisateur protestant voire Calviniste qu’évoque Umberto Eco.

Dwarf Fortress Beyond Quality

//////////////// début des notes 

au delà de la qualité ? au delà du graphisme ? au delà de l’obéissance ? 

>Le joueur néophyte pourra être récalcitrant compte tenu de la charte graphique résolument rétro, et de l’interface maladroite du jeu. Pourtant, derrière ce côté Old School assumé se cache un bijou rare; La profondeur du gameplay, la grande immersion, la liberté totale du joueur et la richesse de l’univers compensent largement cette phase d’apprentissage ardue.
Le Wiki francophone de Dwarf Fortress http://www.dwarffortress.fr/wiki/index.php?title=Accueil

>Au bout d’une heure, peut-être une heure et demi, j'ai eu comme une révélation à propos des graphismes du jeu. Comme dans Matrix: "Je ne vois même plus le code, tout ce que je vois c’est des blondes, des brunes, des rousses…". d’un coup c’est devenu un lieu réel, avec des travailleurs s’agitant de partout, certains fatigués, assoupis à même le sol. Et je me suis senti mal de ne pas leur avoir fabriquer de lits. Laissez moi vous dire que c’était un moment très bizarre.
https://www.gamerswithjobs.com/node/1131786

Les témoignages de joueurs comparant la soudaine compréhension *graphique* du jeu avec une révélation sont nombreux. Ce phénomène s’est concentré autour d’une analogie avec une scène du film Matrix à propos d’un dialogue particulier.

>Cypher - Whoa, Neo. Tu m'as fichu la trouille!.
>Neo - Désolé.
>Cypher - c’est pas grave.
>Neo - c’est...
>Cypher - La Matrice? Ouais.
>Neo - Tu la regardes toujours codée ?
>Cypher - Bien obligé. Les décodeurs d’images travaillent pour le programme de construction. Mais il y a toujours trop de données pour décoder totalement la Matrice. On s’y fait. Je... Je vois même plus le code. Je vois partout des blondes, des brunes, des rousses. Hum, tu... veux un verre?
>Neo - Oui.

plus que la normalité l’habitude définit l’usage, apprentissage de la lecture ? Langue étrangère ? 
Frigo sur le balcon, on ferme vite la porte comme sis le froid allais partir (adelina)

### l’user experience c’est ce à quoi on est déjà habitué ? 

## Pourquoi le texte c’est moche dans la vie ? 

Dans Matrix l’humanité est divisée entre les utilisateurs de la version *graphique* et les utilisateurs de la version *textuelle* de l’interface d’un même ordinateur appelé «la Matrice». 

qu’est ce qu’un mode ? Voir tunning ?  

Pour l’humanité le mode *graphique* est le mode par défaut, normal.

Pour les utilisateurs du mode *graphique* le texte est invisible au démarrage, c’est à dire dès la naissance. Ils viennent au monde directement branchés à une version de la *Matrice* dans laquelle l’interface *graphique* est exécutée au démarrage. Leur expérience du système est limité à cet environnement qui constitue la forme de leur rapport social, détermine leur existence.

Cet environnement *graphique* récréé somptueusement l’impression de la cité états-unienne stéréotypée avec ses gratte-ciels et ses embouteillages. Il leur apparaît comme un système familier, rassurant, normal, qui les guide naturellement vers - sinon le Royaume des Cieux - le moment où débranchés de la *Matrice* leur corps est recyclé en nourriture pour les autres utilisateurs.

Soleil vert, capital ? 

Contrairement aux utilisateurs du mode *graphique*, on ne naît pas utilisateur du mode *textuel*, on le devient même on est élu.
En effet il est nécessaire d’être contacté par un utilisateur qui nous initie à la vérité du mode textuel.

Il permet la libre interprétation des écritures, réclame des décisions personnelles difficiles, impose une herméneutique subtile à l’utilisateur et tient pour acquis que tout le monde ne peut pas atteindre le Salut. Afin de faire fonctionner le système, il faut interpréter soi-même le programme : loin de la communauté baroque des fêtards, l’utilisateur est enfermé à l’intérieur de la solitude de ses propres tourments.

on apprends dans le film que la matrice à été conçu dans le but de créer un monde dépourvu de souffrance ( c’est à dire dépourvue d’efforts, d’obstacles, un monde user-friendly ) 

association entre vérité et désolation apocalyptique – du monde – de la machine.
Version apocalyptique de la matrice est sa version textuelle.
l’apocalypse est la perte de contrôle ? La perte du langage, c’est l’autre, l’étranger ?
Vérité du monde = version apocalyptique, version non civilisée = non conviviale, sans guide/direction, sans dieu.
Vérité de la machine = version apocalyptique, version non civilisée = non conviviale, sans guide/direction, sans graphismes, textuelle.

01:31:38,565

Ne l’avez-vous jamais contemplée, n’avez-vous jamais été ébloui par sa beauté, son ingéniosité? Des milliards de gens vivent leurs petites vies dans l’ignorance. Saviez-vous que la première matrice avait pour dessein de créer un monde dépourvu de souffrances, où tous seraient heureux? Ce fut un désastre. Le programme n’était pas accepté. On a perdu des récoltes. Certains pensaient qu’on n’était pas à même de créer l’encodage de votre monde parfait. Mais je crois que votre espèce, Le genre humain, se satisfait d’une réalité faite de malheur et de souffrances. Le monde parfait était un rêve que votre cerveau primitif s’évertuait à fuir. c’est pour cela que la Matrice fut remaniée. l’apogée de votre civilisation. Je dis bien de votre civilisation car depuis que votre pensée est nôtre, c’est devenu notre civilisation, ce qui est le cœur du problème. l’évolution, Morpheus. Comme le dinosaure. Regardez par la fenêtre. Vous avez fait votre temps. l’avenir, c’est notre monde, Morpheus. l’avenir, c’est notre temps à nous.

01:33:25,372

Dieu n’existe pas, il n’y a pas de déterminisme

>Si, d’autre part, Dieu n’existe pas, nous ne trouvons pas en face de nous des valeurs ou des ordres qui légitimeront notre conduite. Ainsi, nous n’avons ni derrière nous, ni devant nous, dans le domaine numineux des valeurs, des justifications ou des excuses. Nous sommes seuls, sans excuses. c’est ce que j'exprimerai en disant que l’homme est condamné à être libre. Condamné, parce qu’il ne s’est pas créé lui-même, et par ailleurs cependant libre, parce qu’une fois jeté dans le monde, il est responsable de tout ce qu’il fait. l’existentialiste ne croit pas à la puissance de la passion. Il ne pensera jamais qu’une belle passion est un torrent dévastateur qui conduit fatalement l’homme à certains actes, et qui, par conséquent, est une excuse. Il pense que l’homme est responsable de sa passion.

>l’existentialiste ne pensera pas non plus que l’homme peut trouver un secours dans un signe donné, sur terre, qui l’orientera ; car il pense que l’homme déchiffre lui-même le signe comme il lui plaît. Il pense donc que l’homme, sans aucun appui et sans aucun secours, est condamné à chaque instant à inventer l’homme. Ponge a dit, dans un très bel article : “L’homme est l’avenir de l’homme.” c’est parfaitement exact. Seulement, si on entend par là que cet avenir est inscrit au ciel, que Dieu le voit, alors c’est faux, car ce ne serait même plus un avenir. Si l’on entend que, quel que soit l’homme qui apparaît, il y a un avenir à faire, un avenir vierge qui l’attend, alors ce mot est juste. Mais alors, on est délaissé.

« nous n’avons jamais été aussi libres que sous l’Occupation »

matrice sous forme graphique est programmée, objective.

matrice sous forme de texte est programmable, subjective.

### l’user experience c’est le catholicisme anti existentialiste ? 
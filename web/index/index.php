<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>index</title>
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
    <script src="scripts/scripts.js" defer></script>
    <link rel="stylesheet" type="text/css" href="../COMMON/index.css">
        <link rel="stylesheet" type="text/css" href="../COMMON/index.js">
</head>
<body>

    <!-- NAVIGATION -->
    <?php
    /* include "navigation.php";*/
    ?>


    <!-- ITERATE ITEM -->
    <?php
    include "php/data.php";
    require_once "assets/Spyc/Spyc.php";
    require_once "assets/Parsedown/Parsedown.php";
    ?>
    <div id="items">
        <?php 
        $frontmatter = 0;
        $Parsedown = new Parsedown();

        foreach ($items as $item) { 
            $id = basename($item);
            $file = $item . "/item.md";
            $content = file_get_contents($file);
            if ($frontmatter) {
                $parts = preg_split('/[\n]*[-]{3}[\n]/', $content, 3);
                $header = $parts[1];
                $body = $parts[2];
                $yaml = spyc_load($header);
        #data-entry="<?php echo $yaml[entry];? >"
            }
            ?>
            <div id="<?php echo $id; ?>" class="item">
                <h1><?php echo $id.'/' ?></h1>
                <?php echo $content; ?>
            </div>
            <?php 
        } 
        ?>
    </div>

    <!-- SET PHP ARGUMENTS -->
    <?php
    unset($id);
    if (isset($_GET['id'])) {
        $id=$_GET['id'];
        $url=$_GET['url'];
    } 
    ?>
    <script language="javascript" type="text/javascript">
            var id = "<?php echo $id; ?>";
        var url = "<?php echo $url; ?>";

        if (id) {
            console.log(id);
        }
        if (url) {
            console.log(url);
        }
    </script>

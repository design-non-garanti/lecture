<citation>
	<blockquote>
<p>
“ The demands to technology giants to fix emoji diversity fell on
fertile ground. The protest happened at a moment when US-based
technology companies such as DropBox, Pinterest, Airbnb and Twitter
had published statistics on the lack of women and people of colour
in their workforces, thereby publicly acknowledging their issues
with diversity.7
 Each of the companies had hired so-called diversity
managers that were tasked with correcting these problems.
 The Unicode Consortium, made up of several of these same
companies, was put in charge of responding to the pressure.8

A problem that in essence was caused by an awkward design-decision
from Apple, conveniently became a problem to be solved on the
abstract level of the Unicode standard. In this meta-context it was clear
that the issue could only be addressed through technological means.9
</p>
	[…]
<p>
In March 2016, Facebook proudly announced their use of ethnic affinities
profiling, a thinly veiled form of racial market segregation.18
For the promotion of the Universal motion picture Straight Outta
Compton, two trailers were edited. One was targeting “general population
(non-African American, non-Hispanic)” and another “AfricanAmerican”
audiences. The commercially successful campaign was
the result of a close collaboration between diversity teams in both
companies.19 Despite users’ refusal to provide information on their
ethnic background, Facebook felt entitled to guess their “ethnic
affinity” through analysis and categorisation of the data that they have
access to. Segregation based on personal electronic communication
had become “marketing as usual”.
 Emoji skin tone modifiers have of course been used to construct
racist comments20 and there is a documented case of an Instagram
search that returns different results depending on emoji with the
skin tone modifier applied.21 Should a Unicode compliant search
engine offer to sort results the same way? While Russia investigates
if it can sue Apple for their representation of sexual diversity,  app
stores refuse sex-positive emoji because they do not permit “sexual
content”.23 Activists from Turkey were arrested because of their social
network accounts, while Libya used Big Data to target its opponents
(Manach and Nicoby 2015, 38, 47-48). When social networks can target
ads based on the content of messages and user preferences apparently
representing an ethnic profile, where will the use of modified emoji
lead us? ”</p>


</blockquote>
			<hr>
	<from>
		<title>Modifying the Universal</title>,
		<author>Roel Roscam Abbing, Peggy Pierrot, Femke Snelting</author>, <date>2017</date>, <chapitre>Modifying the Universal, Technologies for segregation</chapitre>
	</from>
</citation>
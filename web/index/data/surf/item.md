<citation>
	<blockquote>
“ Now, in 1996, I think that web-surfing is dead. Sure, users may check out a few new sites every now and then, just as they may buy a new magazine from the newsstand when they are stranded in O'Hare. But to continue the magazine analogy, most users will probably spend the majority of their time with a small number of websites that meet their requirements with respect to quality and content. Hotlists can only grow so big (especially with the lousy user interfaces for bookmark management in current webbrowsers), so only a few websites will be graced with substantial numbers of repeat visitors. ”
	</blockquote>
	<hr>
	<from>
		<title>Jakob Nielsen's Alertbox for January 1996</title>,
		<subtitle>Relationships on the Web</subtitle>, 
		<author>Jakob Nielsen</author>, 
		<date>1996</date>
	</from>
</citation>
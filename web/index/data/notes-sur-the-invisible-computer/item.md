<noteblock>
Quelques extraits du <type>livre</type>
<title>
The Invisible Computer
</title>
<subtitle>Why Good Products Can Fail, the Personal Computer Is So Complex, and Information Appliances Are the Solution</subtitle>
<p>de <author>Don Norman</author> publié en <date>1999</date>.</p>
<h2 id="préface">Préface</h2>
<p>Pour Norman le phonographe est centré sur la technologie est non sur l'utilisateur.</p>
<p>Le but du livre est d'accélérer le jour où la technologie de l'ordinateur disparaisse hors de la vue.</p>
<p>La technologie d'aujourd'hui est intrusive, elle ne nous laisse pas de temps pour nous, moins de contrôle.</p>
<p>Norman prêche une technologie humaine.</p>
<p>L'ordinateur devrait être silencieux, invisible, unobtrusive, mais il est trop visible, trop demandant. Ses complexités et frustrations sont largement due au fait que l'on essaye de fourrer trop de fonctions dans une seule boîte posé sur le bureau.</p>
<blockquote>
<p>In this book I show how to make a new start, how to start with simple devices—information appliances—then slowly establish this new paradigm as the natural successor to today's complexity. The proper way, I argue, is through the user-centered, human-centered, humane technology of appliances where the technology of the computer disappears behind the scenes into task-specific devices that maintain all the power without the difficulties.</p>
</blockquote>
<blockquote>
<p>The original title of this book was Taming Technology, for that is the goal. Then the title changed to Information Appliances, for that is the method. Now it is The Invisible Computer, because that is the end result, hiding the computer, hiding the technology so that it disappears from sight, disappears from consciousness, letting us concentrate upon our activities, upon learning, doing our jobs, and enjoying ourselves. The goal is to move from the current situation of complexity and frustration to one where technology serves human needs invisibly, unobtrusively: the human-centered, customer-centered way.</p>
</blockquote>
<h3 id="the-life-cycle-of-technology">The life cycle of technology</h3>
<p>Norman décrit les consomateurs normaux : ce sont ceux qui veulent juste vivre leur vie, ce sont les gens qui pensent que la technologie devrait être invisible, cachée dans les coulisses, fournissant son avantage sans douleur, angoisse et stress.</p>
<p>Prône une transition de technology centered à humain centered.</p>
<h3 id="the-book">The book</h3>
<p>People sould learn the task, not the technology.</p>
<h2 id="crowing-up-moving-from-technology-centered-to-human-centered-products">Crowing Up: Moving from Technology-Centered to Human-Centered Products</h2>
<p>Pour Don Norman, les produits ont un cycle de vie. Quand une technologie mûrit</p>
<p>Don Norman s'élevant contre la course des industriel vers les performances des ordinateurs s'interroge sur</p>
<blockquote>
<p>Is the consumer well served ?</p>
</blockquote>
<p>p. 23</p>
<blockquote>
<p>In the early days of a technology, it doesn't matter if it is hard to use, expensive, or ungainly. It doesn't matter as long as the benefits are sufficiently great: if the task is important, valuable, and can't be done in any other way.</p>
</blockquote>
<p>p. 23</p>
<p>early days of technology people buy because of functions (early adopters) competion to be faster, smaller, etc (technology-driven) course à la technique does it serves the consumer</p>
<p>Quand une technologie mature on pourrait dire qu'une compagnie mature n'est plus une &quot;technology company&quot; mais une &quot;product company&quot; ou &quot;service company&quot;. On utilise le mot &quot;technology&quot; pour se référer aux choses qui sont nouvelles où la technologie domine par rapport à l'utilisabilité ou l'utilité. We call a digital computer a technology, We call the internet technology.</p>
<p>pencil or paper ? also technology but not called technology pencil and paper are so commonplace that we take them for granted &gt;We assume the technological features are reliable and robust, and so, on the whole, we ignore them.</p>
<p>Quand technologie mature, basic performance for granted and we look for prices, prestige, appearance, convenience, etc. (montres, voitures, etc.)</p>
<p>computer still adolescent</p>
<p>Quand une technologie devient &quot;mature&quot; elle n'est plus associée au mot technologie. Nous utilisons le mot techhnologie pour les choses nouvelles (ordinateur, internet, etc.) mais pas pour un crayon ou du papier.</p>
<p>Quand la technologie devient assez bonne &quot;mature&quot;, alors les caratéristiques intrinsèques ne sont plus une variable qui contrôle l'achat. Les émotions, la fierté de la possession, le plaisir sont des points majeurs pour vendre. Ce n'est plus un technology driven market mais un consumer driven market.</p>
<p>Norman prend l'exemple des montres.</p>
<p>p. 29</p>
<h2 id="the-move-to-information-appliances">The Move to Information Appliances</h2>
<p>p. 51</p>
<p>Jef Raskin coined the term <em>information appliances</em> in 1978.</p>
<blockquote>
<p>Raskin had coined the term in 1978. Personal (email) correspondence with Jef Raskin, December 20, 1997. The term first appeared in an internal Apple document. Also cited in &quot;Special Report on Information Appliances,&quot; Business Week 22 (November 1993): 110. Raskin's company, Information Appliance, still exists, and the name is trademarked. (When I showed Raskin this section, he apologized for his behavior fifteen years ago, explaining that he had just been lectured by his lawyer about his &quot;continued negligence with respect to getting people to sign nondisclosures before discussing things,&quot; and my visit triggered an overreaction.</p>
</blockquote>
<p>Raskin travaillé à Apple Computer où il a commencé le projet Macintosh.</p>
<p>Un des messages soutenu par Norman est qu'un bon produit peut échoué alors qu'un produit inférieur peut réussir. Ce que Norman entend par bon ou mauvais sont les caractéristiques intrinsèques d'un produit, par contre ce qu'il entend par échoué ou réussir ce sont les ventes du produit. Pour Norman un produit qui réussit ou qui a du succès est un produit qui s'est bien vendu.</p>
<blockquote>
<p>To me, the primary motivation behind the information appliance is clear: simplicity. Design the tool to fit the task so well that the tool becomes a part of the task, feeling like a natural extension of the work, a natural extension of the person. This is the essence of the information appliance. It implies specialization of function, thus allowing a customized look, shape, feel, and operation. The primary advantages of appliances come from their ability to provide a close fit with the real needs of their users, combined with the simplicity and elegance that arises from focus upon a single activity.</p>
</blockquote>
<p>Norman évoque un standard afin de permettre aux information appliance de communiquer entre elles.</p>
<blockquote>
<p>The prime goal of the information appliance is to break through the complexity barrier of today's personal computers, the PCs. Computers are complex, difficult to learn, difficult to use, difficult to maintain.</p>
</blockquote>
<p>Norman fait l'analogie avec les moteurs électriques : il y à des moteurs invisibles dans chaque maison cachés dans des appliances : montre, ventilateurs, etc. Le mot moteur n'apparaît pas dans le nom de ces appliances.</p>
<blockquote>
<p>Today, the modern house has dozens of motors, but they are invisible, hidden inside such things as clocks, fans, coffee grinders, food mixers, and blenders. Motors make all of these possible, but note how the word motor doesn't appear in the names of these appliances. The motors are embedded within these specialized tools and appliances so that the user sees a task-specific tool, not the technology of motors. Embedded motors are commonplace, but because they are invisible, the average person doesn't have to know anything about their operation or the details of their technology, or even have to know they are there.</p>
</blockquote>
<p>Norman applique cette idée à l'ordinateur.</p>
<blockquote>
<p>When computers are embedded within information appliances, they can perform their valuable functions without the user necessarily being aware that they are there.</p>
</blockquote>
<blockquote>
<p>They are hidden inside the most recent telephones and television sets. Computers make all of these devices possible, but note how the word computer doesn't appear in the names of the devices; it remains behind the scenes, invisible. Embedded computer systems are becoming commonplace, but because they are invisible, people may not realize they are there.</p>
</blockquote>
<p>Specific-task</p>
<blockquote>
<p>In the appliance model of computing, every different application has its own device especially tailored for the task that is to be done. Each device is specialized for the task it performs, so learning how to use it is indistinguishable from learning the task—which is how it should be. Each device works independently of the others. Switching tasks means switching devices. Resuming a task is as simple as moving back to the appropriate device.</p>
</blockquote>
<p>Comparaison gpc mode / appliance model</p>
<p>Basically, the tradeoff is between ease of use and simplicity on the one hand and convenience on the other.</p>
<p>p. 61</p>
<p>information appliance spécifique et determiné pour 1 usage</p>
<h2 id="whats-wrong-with-the-pc">What's Wrong with the PC?</h2>
<p>p. 69</p>
<p>rebooting - booting derived from bootstrap: small loop at the side or rear of a boot to help the owner pull it on. bootstrap is to pull yourself up by those straps.</p>
<p>This is the best part of Don Norman: explaining something interesting (how and why a computer boot) etymologically and technically, just to say, &quot;you souldn't have to know or care&quot;.</p>
<p>p. 70</p>
<p>temps passé à s'occuper de son ordinateur vs temps passé à s'occuper de sa télé</p>
<p>Don Norman veut technologie cachée</p>
<p>p. 75</p>
<p>Real musicians do not use synthesizer.</p>
<p>p. 78</p>
<p>L'ordinateur tente de tout faire, satisfaire tout le monde, etc. d'être générique ? et comme il y a de plus en plus de possibilités / choses à faire, il devient trop complexe et fait un peu tout mais ne fait rien bien. À la place Don Norman propose de faire des appliances qui soit spécifique, qui fasse une seule chose mais bien. Mais de la même manière que pour l'ordinateur les besoins évoluent. Faudra il autant d'appliances que de besoins ? Si ces appliances sont spécialisées, comment les modifier pour d'autres besoins ? La spécialisation des outils est elle une solution ?</p>
</noteblock>
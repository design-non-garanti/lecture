<citation>
	<blockquote>
		<p>
			“ Every shape exists only because of the space around it. … Hence there is a ‘right’ position for every shape in every situation. If we succeed in finding that position, we have done our job. ”
		</p>
	</blockquote>
	<hr>
	<from>
		<where>Dans : Donald Knuth, TEXbook</where>,
		<title>Typographische Gestaltung</title>,
		<author>JAN TSCHICHOLD</author>, 
		<date>1935</date>
	</from>
</citation>
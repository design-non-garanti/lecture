<citation>
	<blockquote>
		The bible of <i>post-PC thinking — Business Week</i>
	</blockquote>
	<hr>
<from>
	<title>The Invisible Computer: Why Good Products Can Fail, the Personal Computer Is So Complex,
and Information Appliances Are the Solution.</title>,
	<author>Donald Norman</author>, <date>1998</date>,
	<publication>MIT Press</publication>
</from>
</citation>
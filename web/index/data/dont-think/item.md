<citation>
	<from>
		<title>Don't Make Me Think</title>,
		<author>Steve Krug</author>, 
		<date>2000</date>
	</from>
	<hr>
	<blockquote>
		<h2>
			First Rule of Usability? Don't Listen to Users
		</h2>
		<aside>
			<p>
				by JAKOB NIELSEN on August 5, 2001
			</p><p>
			Topics: Research Methods</p>
		</aside>
		<p>
			Summary: To design the best UX, pay attention to what users do, not what they say. Self-reported claims are unreliable, as are user speculations about future behavior. Users do not know what they want.
		</p>
		[…]
		<p>
			So, do users know what they want? No, no, and no. Three times no.
		</p>
		[…]
		<aside>
			<h2>
				Reference
			</h2>
			<p>
				Nielsen, J., and Levy, J. (1994). Measuring usability — preference vs. performance. Communications of the ACM 37, 4 (April), 66–75.
			</p>
		</aside>
	</blockquote>
	<hr>
	<from>
		<a href="https://www.nngroup.com/articles/first-rule-of-usability-dont-listen-to-users/">https://www.nngroup.com/articles/first-rule-of-usability-dont-listen-to-users/</a>
	</from>
</citation>
<citation>
<from>
	<title>Kitsch et avant-garde :</title>
	<subtitle>stratégies culturelles et jugement esthétique</subtitle>,
	<author>Anne BEYAERT-GESLIN</author>, 
	<date>publié en ligne le 24 janvier 2007</date>, 
	<a href="http://epublications.unilim.fr/revues/as/3239">http://epublications.unilim.fr/revues/as/3239</a>
</from>
</citation>
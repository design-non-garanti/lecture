<citation>
	<blockquote>
		“ The use of ornament. in whatever style or quality. comes from an attitude of childish naïvety. It shows a reluctance to use "pure design, " a giving-in to a primitive instinct to decorate - which reveals. in the last resort. a fear of pure appearance. It is so easy to employ ornament to cover up bad design! The important architect Adolf La os. one of the first champions of pure form. wrote already in 1898: "The more primitive a people. the more extravag antly they use ornament and decoration. The Indian overloads everything. every boat. every rudder. every arrow. with ornament. To insist on decoration is to put yourself on the same level as an Indian. The Indian in us all must be overcome. The Indian says: This woman is beautiful because she wears golden rings in her nose and her ears. Men of a higher culture say: This woman is beautiful because she does not wear rings in her nose or her ears. To seek beauty in form itself rather than make it dependent on ornament should be the aim of all mankind." Today we see in a desire for ornament an ignorant tendency which our century must repress. When in earlier periods ornament was used. often in an extravagant degree. it only showed how little the essence of typography. which is communication. was understood. ”
	</blockquote>
	<hr>
	<from>De Adolf Loos dans :</from>
	<title>Typographische Gestaltung</title>,
	<author>Jan Tschichold</author>, 
	<date>1935</date>, 
</citation>


[Adolf Loos, JAN TSCHICHOLD, Typographische Gestaltung (1935)](???)



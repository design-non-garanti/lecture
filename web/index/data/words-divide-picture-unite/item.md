<citation>
	<blockquote>
		“ […] We should remember that Isotype was a European invention during the time of European colonialism. Only then it becomes clear that the rhetoric of Isotype as ‘objective’ and ‘neutral’ simply meant they represented European colonial standards. In the visual examples non-European countries are grouped and categorised as ‘other’. Races are reduced to five, with the white race first, and the non-white races as secondary, depiced as dark, shirtless, and with traditional attire. ”
	</blockquote>
			<hr>
	<from>
		<title>The Politics of design, A (Not so) Global Manual for Visual Communication</title>,
		<author>Ruben Pater</author>, <date>2017</date>, <chapitre>Words Divide, Picture Unite</chapitre>
	</from>
</citation>

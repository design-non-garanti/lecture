<entry><memoire href="21e">L'ordinateur du 21ᵉ siècle</memoire></entry>
<hr>
<from>
	<title>The Computer for the 21st Century</title>,
	<author>Mark Weiser</author>, 
	<parution>Scientific American</parution>,
	<date>Septembre 1991</date>, 
	<a href="http://www.ubiq.com/hypertext/weiser/SciAmDraft3.html">http://www.ubiq.com/hypertext/weiser/SciAmDraft3.html</a>
</from>

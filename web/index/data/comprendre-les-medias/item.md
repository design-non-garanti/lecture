<from>
	<title>Pour comprendre les médias</title>
	<subtitle>Les prolongements technologiques de l’homme</subtitle>,
	<author>Marshall McLuhan</author>, 
	<date>1968</date>
</from>
<citation>
	<blockquote>
“ Patent-Pending PageRankTM Technology: Core to Google's search engine is its patent-pending PageRank technology. PageRank performs an objective measurement of the importance of web pages and is calculated by solving an equation of 500 million variables and more than 2 billion terms. PageRank uses the vast link structure of the web as an organizational tool. In essence, Google interprets a link from Page A to Page B as a "vote" by Page A for Page B. Google assesses a page's importance by the votes it receives. Google also analyzes the page that casts the vote. Votes cast by pages that are themselves "important" weigh more heavily and help to make other pages "important." Important, high-quality pages receive a higher PageRank and are ordered higher in the results. Google's technology uses the collective intelligence of the web to determine a page's importance. Google does not use editors or its own employees to judge a page's importance. ”
	</blockquote>
	<hr>
	<from>
		<title>Google Compagny Info</title>,
		<subtitle>Google's Approach to Searching the Internet</subtitle>, 
		<author>Google</author>, 
		<date>1999</date>
	</from>
</citation>
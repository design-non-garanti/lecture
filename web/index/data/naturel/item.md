<citation>
	<blockquote>
		<p>
			“ La contrefaçon (et la mode du même coup) naît avec la Renaissance, avec la déstructuration de l'ordre féodal par l'ordre bourgeois et l'émergence d'une compétition ouverte au niveau des signes distinctifs. Pas de mode dans une société de castes ou de rangs, puisque l'assignation est totale et la mouvance des classes nulle. Un interdit protège les signes et leur assure une clarté totale : chacun renvoie sans équivoque à un statut. Pas de contrefaçon possible dans le cérémonial — sinon comme magie noire et sacrilège, et c'est bien ainsi que le mélange des signes est puni : comme infraction grave à l'ordre même des choses. Si nous nous prenons encore à rêver — aujourd'hui surtout — d'un monde de signes sûrs, d'un « ordre symbolique » fort, soyons sans illusions : cet ordre a existé, et ce fut celui d'une hierarchie féroce, car la transparence et la cruauté des signes vont de pair.
		</p>
		[…]
		<p>
			Fin du signe <i>obligé</i>, rège du signe émancipé, dont vont pouvoir jouer indifféremment toutes les classes. La démocratie concurrentielle succède à l'endogamie des signes propre aux ordres statutaires. Du même coup on entre, avec le transit de valeurs/signes de prestige d'une classe à l'autre, nécéssairement dans la <i>contrefaçon</i>
		</p>
		[…]
		<p>
			Mais le signe multiplié n'a plus rien à voir avec le signe obligé à diffusion restreinte : Il en est la contrefaçon, non par dénaturation d'un « original », mais par extension d'un materiel dont toute la clarté tenait à la restriction qui le frappait. Non discriminant, (il n'est plus que compétitif), allégé de toute contrainte, disponible dans l'universel, le signe moderne simule pourtant encore la nécessité en se donnant pour lié au monde. Le signe moderne rêve du signe antérieur et voudrait bien, avec sa référence au réel, retrouver une <i>obligation</i> : il ne retrouve qu'une <i>raison</i>: cette raison référentielle, ce réel, ce « naturel » dont il va vivre.
		</p>
		[…]
		<p>
			C'est donc dans le simulacre d'une « nature » que le signe moderne trouve sa valeur. Problématique du « naturel » métaphysique de la réalité et de l'apparence : ce sera celle de toute la bourgeoisie depuis la Renaissance, miroir du signe classique. Encore aujourd'hui la nostalgie d'une référence naturelle du signe est vivace, 
		</p>
		[…]
		<p>
			C'est donc à la Renaissance que le faux est né avec le naturel. ”
		</p>
	</blockquote>
	<hr>
	<from>
		<title>L'échange symbolique et la mort</title>,
		<author>Jean Baudrillard</author>, 
		<chapitre>l'ordre des simulacres</chapitre>,
		<partie>l'ange de stuc</partie>,
		<date>1976</date>, 
	</from>
	<hr>
	<blockquote>
		“ L'illusion de naturel est sans cesse dénoncée (dans les Mythologies, dans le Système de la Mode; dans S/Z même, où il est dit que la dénotation est retournée en Nature du langage). Le naturel n'est nullement un attribut de la Nature physique ; c'est l'alibi dont se pare une majorité sociale : le naturel est une légalité. D'où la nécéssité critique de faire sous ce naturel-là, et, selon le mot de Brecht, « sous la règle l'abus ».
		On peut voir l'origine de cette critique dans la situation minoritaire de R.B. lui-même ; il a toujours appartenu à quelque minorité, à quelque marge — de la société, du langage, du désir, du métier, et même autrefois de la religion (il n'était pas indifférent d'être protestant dans une classe de petits catholiques); situation nullement sévère, mais qui marque un peu toute l'existence sociale : qui ne sent combien il est naturel, en France, d'être catholique, marié et bien diplomé ? La moindre carence introduite dans ce tableau des conformités publiques forme une sorte de pli ténu de ce que l'on pourrait appeler la litière sociale. ”
	</blockquote>
	<hr>
	<from>
		<title>Roland Barthes par Roland Barthes</title>,
		<author>Roland Barthes</author>,
		<date>1975</date>
	</from>
</citation>
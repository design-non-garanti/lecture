<citation>
	<integral>
		<h2>"Intuitive" Software</h2>
		<blockquote>"Oh, SURE the Macintosh interface is Intuitive! 
			I've always thought deep in my heart that command-z should undo things." 
		</blockquote>
		<aside>
			-- Margy Levine
		</aside>
		<p>
			The term "intuitive" for interfaces is a complete misnomer.  I can think of two meanings in software for which the term "intuitive" is presently used:
		</p>
		<ol>
			<li>Almost nobody, looking at a computer system for the first time, has the slightest idea what it will do or how it should work.  What people call an "intuitive interface" is generally one which becomes obvious as soon as it is demonstrated.  But before the demo there was no intuition of what it would be like.  Therefore the real first sense of "intuitive" is retroactively obvious.
			</li>
			<li>Well-designed interactive software gradually unfolds itself, as in the game of Pac-Man, which has many features you don't know about at first.  The best term I've heard for this is self-revealing (term coined by Klavs Landberg).
			</li>
		</ol>
	</integral>
	<hr>
	<from>
		<title>Ted Nelson's Computer Paradigm</title>
		<subtitle>Expressed as One-Liners </subtitle>,
		<author>Ted Nelson</author>, 
		<edition>1999</edition>,
		<a href="http://hyperland.com/TedCompOneLiners">http://hyperland.com/TedCompOneLiners</a>
	</from>
</citation>
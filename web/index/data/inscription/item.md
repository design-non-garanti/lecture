<entry><memoire href="inscription">Inscription</memoire></entry>
<citation>
	<blockquote>
		“ Whether we apply for a visa or fill out a tax form, we obey the questions asked on the form because of its design. If the form used colour photographs and the comic sans font, we would doubt its authenticity. The design of forms is based on authority. Form designers are not just looking at legibility and functionality, they select colours, graphics elements, and typefaces that create the appropriate identity of authority. Forms are a very direct visualization of a power structure. ”
	</blockquote>
			<hr>
	<from>
		<title>The Politics of design, A (Not so) Global Manual for Visual Communication</title>,
		<author>Ruben Pater</author>, <date>2017</date>, <chapitre>Architecture of
                Choices</chapitre>
	</from>
</citation>

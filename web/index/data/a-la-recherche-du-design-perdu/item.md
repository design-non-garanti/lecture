<entry><memoire href="google"> material design,<br> à la recherche du design perdu</memoire></entry>

<citation>
	<blockquote>
		“ The Google User Experience team aims to create designs that are useful, fast, simple, engaging, innovative, universal, profitable, beautiful, trustworthy, and personable. Achieving a harmonious balance of these ten principles is a constant challenge. A product that gets the balance right is "Googley" – and will satisfy and delight people all over the world. ”</p>
	</blockquote>
			<hr>
	<from>
		<url>google.com/corporate/ux</url>,
		<date>2008</date>
	</from>
</citation>
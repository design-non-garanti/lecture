<citation>
	<blockquote>
		<p>
			“ Every year the Pantone Color Institute (known to define Colors of the Year) evaluates the colors shown by fashion designers at the New York Fashion Week.
		</p><p>
		This information is used to create The Pantone Color of the Year and the Pantone Fashion Color Report with the top 10 fashion colors for the comming season : ”</p>
	</blockquote>
	<hr>
	<a  href="https://www.w3schools.com/colors/colors_trends.asp">https://www.w3schools.com/colors/colors_trends.asp</a>
	<hr>
	<blockquote>
		“ In a post-industrial society, every citizen can construct her own custom
		lifestyle and "select" her ideology from a large (but not infinite) number of choices. Rather than pushing the same objects/information to a mass audience, marketing now tries to target each individual separately. The logic of new media technology reflects this new social logic. Every visitor to a Web site automatically gets her own custom version of the site created on the fly from a database. The language of the text, the contents, the ads displayed — all these can be customized by interpreting the information about where on the network the user is coming from; or, if the user previously registered with the site, her personal profile can be used for this customization. According to a report in USA Today (November 9, 1999), “Unlike ads in magazines or other real-world publications, ‘banner’ ads on Web pages change wit every page view. And most of the companies that place the ads on the Web site track your movements across the Net, ‘remembering’ which ads you’ve seen, exactly when you saw them, whether  61 you clicked on them, where you were at the time and the site you have visited just before.” 
	</p>
	<p>
		More generally, every hypertext reader gets her own version of the
	complete text by selecting a particular path through it. Similarly, every user of an interactive installation gets her own version of the work. And so on. In this way new media technology acts as the most perfect realization of the utopia of an ideal society composed from unique individuals. New media objects assure users that their choices — and therefore, their underlying thoughts and desires — are unique, rather than pre-programmed and shared with others. As though trying to compensate for their earlier role in making us all the same, today descendants of the Jacqurd's loom, the Hollerith tabulator and Zuse's cinema-computer are now working to convince us that we are all unique. ”</p>
</blockquote>
<hr>
<from>
	<title>The Language of New Media</title>,
	<author>Lev Manovich</author>, <date>2001</date>, <editeur>MIT Press</editeur>
</from>
</citation>

<citation>
	<blockquote>
		<p>
			“ Job’s point is that it’s not enough to offer customers what they already think they need. He wanted Apple to be a transformational influence, which meant his goal was to expose and then meet needs and desires that customers didn’t realize they had. He was aiming at things that, in the minds and hearts of his customers, were “not yet on the page.”
		</p><p>
			With a little tweaking, I’ve found this Jobsian insight to be really helpful for pushing myself out of the realm of Bible trivia and into the realm of life transformation.
		</p>
		<p>
			THE BIBLE DOESN’T JUST ADDRESS OUR NEEDS—IT DEFINES THEM ”
		</p>
	</blockquote>
	<hr>
	<from>
		<title>Steve Jobs and the Goal of Preaching</title>,
		<author>Matt McCullough</author>, <date>08.28.2014</date>, 
		<a href="https://www.9marks.org/article/steve-jobs-and-the-goal-of-preaching/">https://www.9marks.org/article/steve-jobs-and-the-goal-of-preaching/</a>
	</from>
</citation>
<section>
	<div class="q">
		<h2 id="quel-est-votre-pr-nom-">quel est votre prénom ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="quel-est-votre-nom-">quel est votre nom ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="quel-age-avez-vous-">quel 
		âge avez-vous ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="quelle-est-votre-adresse-mail-">quelle est votre adresse mail ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="choisissez-un-emoji">choisissez un emoji</h2>
		<div>
			<p>
				<input name="emoji" value="h0" type="radio"><img src="emoji-ios-8.3/boy_1f466.png" />
				<input name="emoji" value="h1" type="radio"><img src="emoji-ios-8.3/boy_emoji-modifier-fitzpatrick-type-1-2_1f466-1f3fb_1f3fb.png" />
				<input name="emoji" value="h2" type="radio"><img src="emoji-ios-8.3/boy_emoji-modifier-fitzpatrick-type-3_1f466-1f3fc_1f3fc.png" />
				<input name="emoji" value="h3" type="radio"><img src="emoji-ios-8.3/boy_emoji-modifier-fitzpatrick-type-4_1f466-1f3fd_1f3fd.png" />
				<input name="emoji" value="h4" type="radio"><img src="emoji-ios-8.3/boy_emoji-modifier-fitzpatrick-type-5_1f466-1f3fe_1f3fe.png" />
				<input name="emoji" value="h5" type="radio"><img src="emoji-ios-8.3/boy_emoji-modifier-fitzpatrick-type-6_1f466-1f3ff_1f3ff.png" /></p>

				<p>
					<input name="emoji" value="s0" type="radio"><img src="emoji-ios-8.3/girl_1f467.png" />
					<input name="emoji" value="s1" type="radio"><img src="emoji-ios-8.3/girl_emoji-modifier-fitzpatrick-type-1-2_1f467-1f3fb_1f3fb.png" />
					<input name="emoji" value="s2" type="radio"><img src="emoji-ios-8.3/girl_emoji-modifier-fitzpatrick-type-3_1f467-1f3fc_1f3fc.png" />
					<input name="emoji" value="s3" type="radio"><img src="emoji-ios-8.3/girl_emoji-modifier-fitzpatrick-type-4_1f467-1f3fd_1f3fd.png" />
					<input name="emoji" value="s4" type="radio"><img src="emoji-ios-8.3/girl_emoji-modifier-fitzpatrick-type-5_1f467-1f3fe_1f3fe.png" />
					<input name="emoji" value="s5" type="radio"><img src="emoji-ios-8.3/girl_emoji-modifier-fitzpatrick-type-6_1f467-1f3ff_1f3ff.png" />
				</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="modifying-the-universal">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="choisissez-un-pictogramme">choisissez un pictogramme</h2>
			<div>
				<p>
					<input name="picto" value="p0" type="radio"><img id="picto" src="neurath_0.png" />
					<input name="picto" value="p1" type="radio"><img id="picto" src="neurath_1.png" />
					<input name="picto" value="p2" type="radio"><img id="picto" src="neurath_2.png" />
					<input name="picto" value="p3" type="radio"><img id="picto" src="neurath_3.png" />
					<input name="picto" value="p4" type="radio"><img id="picto" src="neurath_4.png" />
				</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="words-divide-picture-unite">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="quelle-est-votre-couleur-pr-f-r-e-">quelle est votre couleur préférée ?</h2>
			<div>
				<p><input type="radio" data-hex="#88B04B" name="color" value="Greenery">Greenery</p>
				<p><input type="radio" data-hex="#F7CAC9" name="color" value="RoseQuartz">Rose Quartz</p>
				<p><input type="radio" data-hex="#92A8D1" name="color" value="Serenity">Serenity</p>
				<p><input type="radio" data-hex="#955251" name="color" value="Marsala">Marsala</p>
				<p><input type="radio" data-hex="#B565A7" name="color" value="RadiandOrchid">Radiand Orchid</p>
				<p><input type="radio" data-hex="#009B77" name="color" value="Emerald">Emerald</p>
				<p><input type="radio" data-hex="#DD4124" name="color" value="TangerineTango">Tangerine Tango</p>
				<p><input type="radio" data-hex="#D65076" name="color" value="Honeysucle">Honeysucle</p>
				<p><input type="radio" data-hex="#45B8AC" name="color" value="Turquoise">Turquoise</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="color-trends">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="vous-tes-">vous êtes ?</h2>
			<div>
				<p><input name="statut" value="amateur" type="radio">amateur</p>
				<p><input name="statut" value="expert" type="radio">expert</p>
				<p><input name="statut" value="capable de résoudre la plupart des problèmes en un coup de fil" type="radio">capable de résoudre la plupart des problèmes en un coup de fil</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="garantie">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="mais-encore-">mais encore ?</h2>
			<div>
				<p><input name="user-type" value="un scientifique" type="radio">un scientifique</p>
				<p><input name="user-type" value="un ouvrier de la connaissance" type="radio">un ouvrier de la connaissance</p>
				<p><input name="user-type" value="un ouvrier de l'intellect" type="radio">un ouvrier de l&#39;intellect</p>
				<p><input name="user-type" value="un programmeur" type="radio">un programmeur</p>
				<p><input name="user-type" value="un vrai utilisateu" type="radio">un vrai utilisateur</p>
				<p><input name="user-type" value="un utilisateur naïf" type="radio">un utilisateur naïf</p>
				<p><input name="user-type" value="la dame avec une machine à écrire royale" type="radio">la dame avec une machine à écrire royale</p>
				<p><input name="user-type" value="un enfant" type="radio">un enfant</p>
				<p><input name="user-type" value="un artiste" type="radio">un artiste</p>
				<p><input name="user-type" value="un musicien" type="radio">un musicien</p>
				<p><input name="user-type" value="une divinité" type="radio">une divinité</p>
				<p><input name="user-type" value="une machine" type="radio">une machine</p>
				<p><input name="user-type" value="un néophyte complètement paumé" type="radio">un néophyte complètement paumé</p>
				<p><input name="user-type" value="un hacker" type="radio">un hacker</p>
				<p><input name="user-type" value="un nul" type="radio">un nul</p>
				<p><input name="user-type" value="vous-même" type="radio">vous-même</p>
				<p><input name="user-type" value="un individu" type="radio">un individu</p>
				<p><input name="user-type" value="un citoyen" type="radio">un citoyen</p>
				<p><input name="user-type" value="des gens" type="radio">des gens</p>
				<p><input name="user-type" value="un autre" type="radio">un autre</p>
				<p><input name="user-type" value="un client" type="radio">un client</p>
				<p><input name="user-type" value="un interacteur" type="radio">un interacteur</p>
				<p><input name="user-type" value="un acheteur" type="radio">un acheteur</p>
				<p><input name="user-type" value="une cible" type="radio">une cible</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="users-imagined">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="comment-avez-vous-eu-connaissance-de-ce-m-moire-">comment avez-vous eu connaissance de ce mémoire ?</h2>
			<div>
				<p><input name="connaissance" value="publicité" type="radio">publicité</p>
				<p><input name="connaissance" value="magazine" type="radio">magazine</p>
				<p><input name="connaissance" value="ami" type="radio">ami</p>
				<p><input name="connaissance" value="collègue" type="radio">collègue</p>
				<p><input name="connaissance" value="autre" type="radio">autre</p>
			</div>
			<button>Continuer</button>
			<!--<p><a href="???">Monthy&#39;s Python Holy Grail, le jeu vidéo, 1996</a></p>-->
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="qu-attendez-vous-de-ce-m-moire-">qu&#39;attendez-vous de ce mémoire ?</h2>
			<div>
				<p><input name="attente" value="une lecture utile" type="radio">une lecture utile</p>
				<p><input name="attente" value="une lecture rapide" type="radio">une lecture rapide</p>
				<p><input name="attente" value="une problématique simple" type="radio">une problématique simple</p>
				<p><input name="attente" value="un contenu accessible" type="radio">un contenu accessible</p>
				<p><input name="attente" value="une problématique innovante" type="radio">une problématique innovante</p>
				<p><input name="attente" value="une problématique universelle" type="radio">une problématique universelle</p>
				<p><input name="attente" value="une problématique rentable" type="radio">une problématique rentable</p>
				<p><input name="attente" value="de l'élégance" type="radio">de l'élégance</p>
				<p><input name="attente" value="un sentiment de sécurité" type="radio">un sentiment de sécurité</p>
				<p><input name="attente" value="de la convivialité" type="radio">de la convivialité</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="a-la-recherche-du-design-perdu">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="attendez-vous-d-un-objet-qu-il-soit-conforme-">attendez-vous d&#39;un objet qu&#39;il soit conforme ?</h2>
			<div>
				<p><input name="conforme" value="oui" type="radio">oui</p>
				<p><input name="conforme" value="non" type="radio">non</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="garantie">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="un-bon-design-est-">un bon design est :</h2>
			<div>
				<p><input name="bon-design" value="utile" type="radio">utile</p>
				<p><input name="bon-design" value="rapide" type="radio">rapide</p>
				<p><input name="bon-design" value="simple" type="radio">simple</p>
				<p><input name="bon-design" value="accessible" type="radio">accessible</p>
				<p><input name="bon-design" value="innovant" type="radio">innovant</p>
				<p><input name="bon-design" value="universel" type="radio">universel</p>
				<p><input name="bon-design" value="rentable" type="radio">rentable</p>
				<p><input name="bon-design" value="élégant" type="radio">élégant</p>
				<p><input name="bon-design" value="sécuritaire" type="radio">sécuritaire</p>
				<p><input name="bon-design" value="convivial" type="radio">convivial</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="a-la-recherche-du-design-perdu">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="un-bon-design-">un bon design :</h2>
			<div>
				<p><input name="bon-design_besoin" value="répond à un besoin" type="radio">répond à un besoin</p>
				<p><input name="bon-design_besoin" value="définit un besoin" type="radio">définit un besoin</p>
				<p><input name="bon-design_besoin" value="ne se contente pas de répondre à un besoin, il le définit" type="radio">ne se contente pas de répondre à un besoin, il le définit</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="jobs-preaching">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="pourquoi-m-me-un-bon-design-peut-il-chouer-">pourquoi même un bon design peut-il échouer ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="notes-sur-the-invisible-computer">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="un-bon-design-doit-il-avoir-raison-">un bon design doit-il avoir raison ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="raison">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="un-bon-design-est-il-notre-service-">un bon design est-il à notre service ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="a-la-recherche-du-design-perdu">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="cochez-">cochez.</h2>
			<div>
				<p><input name="cochez" value="praticité" type="checkbox"> praticité</p>
				<p><input name="cochez" value="rapidité" type="checkbox"> rapidité</p>
				<p><input name="cochez" value="simplicité" type="checkbox">simplicité</p>
				<p><input name="cochez" value="accessibilité" type="checkbox">accessibilité</p>
				<p><input name="cochez" value="innovation" type="checkbox">innovation</p>
				<p><input name="cochez" value="universalité" type="checkbox">universalité</p>
				<p><input name="cochez" value="rentabilité" type="checkbox">rentabilité</p>
				<p><input name="cochez" value="beauté" type="checkbox">beauté</p>
				<p><input name="cochez" value="sécurité" type="checkbox">sécurité</p>
				<p><input name="cochez" value="convivialité" type="checkbox">convivialité</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="a-la-recherche-du-design-perdu">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">

			<h2 id="choisissez-la-bonne-position-pour-chaque-forme-dans-chaque-situation-">choisissez la bonne position pour chaque forme dans chaque situation.</h2>
			<div>
				<p style="visibility: hidden; position: absolute;"><input id="bauhaus" type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="right-position">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="face-un-choix-le-plus-important-est-d-avoir-">Face à un choix, vous souhaitez :</h2>
			<div>
				<p><input name="choix" value="avoir de bonnes possibilités" type="radio">avoir de bonnes possibilités</p>
				<p><input name="choix" value="faire les bons choix" type="radio">faire les bons choix</p>
				<p><input name="choix" value="pouvoir choisir autre chose que ce qui m'est proposé" type="radio">pouvoir choisir autre chose que ce qui m'est proposé</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="user-rights">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="apprendre-des-nouvelles-mani-res-de-faire-dans-le-seul-but-d-tre-diff-rent-est-">apprendre des nouvelles manières de faire dans le seul but d&#39;être différent est :</h2>
			<div>
				<p><input name="nouvelles-manieres" value="une contrainte" type="radio">une contrainte</p>
				<p><input name="nouvelles-manieres" value="irrespectueux" type="radio">irrespectueux</p>
				<p><input name="nouvelles-manieres" value="incohérent" type="radio">incohérent</p>
			</div>
			<button>Continuer</button>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="trouvez-l-intrus-">Trouvez l&#39;intrus :</h2>
			<div>
				<p><input name="intru" value="innovant" type="radio">innovant</p>
				<p><input name="intru" value="kitsch" type="radio">kitsch</p>
				<p><input name="intru" value="avant-gardiste" type="radio">avant-gardiste</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="kitsch-avant-garde">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="caract-risez-l-usage-de-l-ornement-">caractérisez l&#39;usage de l&#39;ornement :</h2>
			<div>
				<p><input name="ornement" value="enfantin" type="radio">enfantin</p>
				<p><input name="ornement" value="refus de la pureté du design" type="radio">refus de la pureté du design</p>
				<p><input name="ornement" value="instinct primitif" type="radio">instinct primitif</p>
				<p><input name="ornement" value="peur de la beauté réelle" type="radio">peur de la beauté réelle</p>
				<p><input name="ornement" value="maquillage pour un mauvais design" type="radio">maquillage pour un mauvais design</p>
				<p><input name="ornement" value="bon pour les Indiens" type="radio">bon pour les Indiens</p>
				<p><input name="ornement" value="basse culture" type="radio">basse culture</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="ornement">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="qu-est-ce-qui-diff-rencie-le-naturel-de-la-contre-fa-on-">qu&#39;est ce qui différencie le naturel de la contre-façon ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="naturel">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="un-outil-peut-il-tre-con-u-pour-tre-d-tourn-">un outil peut-il être conçu pour être détourné ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="detournement">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="votre-ordinateur-est-il-reprogrammable-">votre ordinateur est-il reprogrammable ?</h2>
			<div>
				<p><input name="reprogrammable" value="oui" type="radio">oui</p>
				<p><input name="reprogrammable" value="non" type="radio">non</p>
			</div>
			<button>Continuer</button>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="votre-interface-est-">votre interface est :</h2>
			<div>
				<p><input name="interface" value="graphique" type="radio">graphique</p>
				<p><input name="interface" value="en ligne de commande" type="radio">en ligne de commande</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="religion-ordinateur">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="vous-poss-dez-un-">vous possédez un :</h2>
			<div>
				<p><input name="mac-windows" value="Mac" type="radio">Mac</p>
				<p><input name="mac-windows" value="Windows" type="radio">Windows</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="religion-ordinateur">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="vous-tes-">vous êtes :</h2>
			<div>
				<p><input name="religion" value="catholique" type="radio">catholique</p>
				<p><input name="religion" value="protestant" type="radio">protestant</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="religion-ordinateur">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="c-est-quoi-un-ordinateur-">c&#39;est quoi un ordinateur ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="c-est-quoi-un-ordinateur">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="quel-est-l-usage-attendu-d-un-ordinateur-">quel est l&#39;usage attendu d&#39;un ordinateur ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<!--<p><a href="???">garantie</a></p>-->
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="d-crivez-un-ordinateur-">décrivez un ordinateur.</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="pourquoi-l-ordinateur-personnel-est-il-si-complexe-">pourquoi l&#39;ordinateur personnel est-il si complexe ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="invisible-computer">?</span></p>
		</div>
	</section>


	<section>
		<div class="q">
			<h2 id="qu-est-ce-qui-ne-va-pas-avec-l-ordinateur-">qu&#39;est ce qui ne va pas avec l&#39;ordinateur ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="invisible-computer">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="l-ordinateur-fait-">l&#39;ordinateur fait :</h2>
			<div>
				<p><input name="ordinateur-fait" value="une seule chose mais bien" type="radio">une seule chose mais bien</p>
				<p><input name="ordinateur-fait" value="plusieurs choses mais mal" type="radio">plusieurs choses mais mal</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="invisible-computer">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="quand-votre-ordinateur-ne-fonctionne-pas-comme-vous-le-voulez-d-o-viens-le-probl-me-">quand votre ordinateur ne fonctionne pas comme vous le voulez, d&#39;où viens le problème ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="myth-of-technology">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="votre-ordinateur-vous-simplifie-t-il-la-vie-">votre ordinateur vous simplifie t-il la vie ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="myth-of-technology">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="un-ordinateur-est-il-un-cran-">un ordinateur est-il un écran ?</h2>
			<div>
				<p><input name="ecran" value="oui" type="radio">oui</p>
				<p><input name="ecran" value="non" type="radio">non</p>
				<p><input name="ecran" value="peut-être" type="radio">peut-être</p>
			</div>
			<button>Continuer</button>
			<!--<p><a href="???">Where did the computer go ?</a></p>-->
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="un-ordinateur-est-il-une-surface-">un ordinateur est-il une surface ?</h2>
			<div>
				<p><input name="surface" value="oui" type="radio">oui</p>
				<p><input name="surface" value="non" type="radio">non</p>
				<p><input name="surface" value="peut-être" type="radio">peut-être</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="a-la-recherche-du-design-perdu">?</span></p> 
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="pourquoi-est-il-impossible-de-mettre-des-notes-marginales-sur-un-fichier-microsoft-word-un-pdf-ou-sur-une-page-web-">pourquoi est-il impossible de mettre des notes marginales sur un fichier Microsoft Word, un Pdf ou sur une page web ?</h2>
			<div>
				<p><input name="notes-marge" value="car les ordinateurs ne le permettent pas" type="radio">car les ordinateurs ne le permettent pas</p>
				<p><input name="notes-marge" value="car c&#39;est une fonctionnalité difficile à trouver" type="radio">car c&#39;est une fonctionnalité difficile à trouver</p>
				<p><input name="notes-marge" value="car Charles Simonyi, John Warnock et Tim Berners-Lee ont considéré <br/>que cela n'était pas nécessaire" type="radio">car Charles Simonyi, John Warnock et Tim Berners-Lee ont considéré <br/>que cela n&#39;était pas nécessaire</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="myth-of-technology">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="pourquoi-le-texte-l-cran-est-il-g-n-ralement-en-noir-sur-fond-blanc-">pourquoi le texte à l&#39;écran est-il généralement en noir sur fond blanc ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="comprendre-les-medias">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="pourquoi-dix-huit-si-cles-de-culture-de-l-imprim-ont-t-r-duits-une-simulation-de-feuille-de-papier-">pourquoi dix-huit siècles de culture de l&#39;imprimé ont été réduits à une simulation de feuille de papier ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="myth-of-technology">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="quelles-sont-les-diff-rences-entre-un-presse-papier-papier-et-un-presse-papier-num-rique-">quelles sont les différences entre un presse-papier et un presse-papier numérique ?</h2>
			<div>
				<p><input name="presse-papier" value="il est invisible" type="radio">il est invisible</p>
				<p><input name="presse-papier" value="il ne peut contenir qu&#39;un objet," type="radio">il ne peut contenir qu&#39;un objet,</p>
				<p><input name="presse-papier" value="quoi-que vous y placiez, il détruit le contenu précédent. À part ça, C'EST JUSTE UN PRESSE-PAPIER NORMAL SOUS TOUT LES ASPECTS – À L'EXEPTION QU&#39;IL NE POSSÈDE AUCUN AUTRE ASPECT." type="radio">quoi-que vous y placiez, il détruit le contenu précédent. À part ça, C&#39;EST JUSTE UN PRESSE-PAPIER NORMAL SOUS TOUT LES ASPECTS – À L&#39;EXEPTION QU&#39;IL NE POSSÈDE AUCUN AUTRE ASPECT.</p>
			</div>
			<button>Continuer</button>
			<p><span class="link" data-id="myth-of-technology">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="avons-nous-des-bureaux-d-ordinateur-ou-des-ateliers-d-ordinateur-">avons-nous des bureaux d&#39;ordinateur ou des ateliers d&#39;ordinateur ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<!--<p><a href="???">???</a></p>-->
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="comment-disposez-vous-les-ic-nes-sur-le-bureau-de-votre-ordinateur-">comment disposez-vous les icônes sur le bureau de votre ordinateur ?</h2>
			<div>
				<p><input type="text"></p>
			</div>
			<button>Continuer</button>
			<!--<p><a href="???">???</a></p>-->
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="-quoi-sert-cette-ic-ne-">à quoi sert cette icône : 🔎</h2>
			<div>
				<p><input name="icone" value="zoomer" type="radio"> zoomer</p>
				<p><input name="icone" value="rechercher" type="radio"> rechercher</p>
			</div>
			<button>Continuer</button>
		<p><span class="link" data-id="behind-the-blip">?</span></p>
		</div>
	</section>

	<section>
		<div class="q">
			<h2 id="la-commande-cmd-z-est-elle-intuitive-">la commande CMD-Z est-elle intuitive ?</h2>
			<div>
				<p><input name="cmdz" value="J'ai toujours profondément pensé au fond de mon cœur que CMD-Z devait annuler les choses." type="radio">J&#39;ai toujours profondément pensé au fond de mon cœur que CMD-Z devait annuler les choses.</p>
			</div>
			<button>Continuer</button>
		<p><span class="link" data-id="intuitive">?</span></p>
	</div>
</section>

<section>
	<div class="q">
		<!--<p>&lt;!-- ## qu&#39;est ce vous trouvez simple --&gt;
		</p>-->
		<h2 id="que-trouvez-vous-simple-">que trouvez-vous simple ?</h2>
		<!--<p>&lt;!-- Qu&#39;est ce vous trouvez compliqué --&gt;
		</p>
		<p>Que trouvez-vous compliqué</p>-->
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<!--<p><a href="???">???</a></p>-->
	</div>
</section>

<section>
	<div class="q">
		<h2 id="qui-est-le-ma-tre-de-la-simplicit-">qui est le maître de la simplicité ?</h2>
		<div>
			<p><input name="maeda" value="John Maeda" type="radio">John Maeda</p>
		</div>
		<button>Continuer</button>
		<p><span class="link" data-id="simplicity">?</span></p>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="quel-pourcentage-du-web-repose-sur-Wordpress-">Quel pourcentage du web repose sur Wordpress ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><span class="link" data-id="wordpress">?</span></p>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="aimez-vous-que-l-on-vous-pousse-r-fl-chir-">Souhaitez-vous que l&#39;on vous pousse à réfléchir ?</h2>
		<div style="visibility: hidden;position: absolute;">
			<p><input type="text" value="done"></p>
		</div>
		<button>Continuer</button>
		<p><span class="link" data-id="dont-think">?</span></p>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="surfez-vous-sur-le-net-">surfez-vous sur le net ?</h2>
		<div>
			<p><input name="surf" value="oui" type="radio">oui</p>
			<p><input name="surf" value="non" type="radio">non</p>
		</div>
		<button>Continuer</button>
		<!--<p><a href="???">Jakob Nielsen, alertbox 1996</a></p>-->
	</div>
</section>

<section>
	<div class="q">
		<h2 id="saviez-vous-que-le-surf-sur-le-net-est-mort-en-1996-">saviez-vous que le surf sur le net est mort en 1996 ?</h2>
		<div>
			<p><input name="surf-is-dead" value="oui" type="radio">oui</p>
			<p><input name="surf-is-dead" value="non" type="radio">non</p>
		</div>
		<button>Continuer</button>
		<p><span class="link" data-id="surf">?</span></p>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="l-ordinateur-est-">l&#39;ordinateur est :</h2>
		<div>
			<p><input name="ordinateur-est" value="une aventure civile" type="radio">une aventure civile</p>
			<p><input name="ordinateur-est" value="une aventure commerciale" type="radio">une aventure commerciale</p>
			<p><input name="ordinateur-est" value="le premier est le deuxième choix signifient la même chose" type="radio">le premier est le deuxième choix signifient la même chose</p>
		</div>
		<button>Continuer</button>
		<!--<p><a href="???">Jakob Nielsen, alertbox 1996</a></p>-->
	</div>
</section>

<section>
	<div class="q">
		<h2 id="parmi-les-termes-suivants-lequel-est-il-le-plus-correct-pour-d-signer-un-contenu-pertinent-">parmi les termes suivants lequel est-il le plus correct pour désigner un contenu pertinent ?</h2>
		<div>
			<p><input name="pertinent" value="populaire" type="radio">populaire</p>
			<p><input name="pertinent" value="démocratique" type="radio">démocratique</p>
			<p><input name="pertinent" value="recommandé" type="radio">recommandé</p>
			<p><input name="pertinent" value="accessible" type="radio">accessible</p>
			<p><input name="pertinent" value="authentique" type="radio">authentique</p>
			<p><input name="pertinent" value="unique" type="radio">unique</p>
			<p><input name="pertinent" value="classique" type="radio">classique</p>
		</div>
		<button>Continuer</button>
		<p><span class="link" data-id="page-ranking-system">?</span></p>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="parmi-les-termes-suivants-choisissez-le-miracle-">parmi les termes suivants, choisissez le miracle :</h2>
		<div>
			<p><input name="miracle" value="Jésus" type="radio">Jésus</p>
			<p><input name="miracle" value="Xerox" type="radio">Xerox</p>
		</div>
		<button>Continuer</button>
		<p><span class="link" data-id="miracle">?</span></p>
	</div>
</section>

<section>
	<div class="q">
		<h2 id="qui-conna-t-le-mieux-vos-produits-">qui connaît le mieux vos produits ?</h2>
		<div>
			<p><input name="connait-produits" value="Adobe" type="radio">Adobe</p>
			<p><input name="connait-produits" value="Apple" type="radio">Apple</p>
			<p><input name="connait-produits" value="Google" type="radio">Google</p>
			<p><input name="connait-produits" value="Microsoft" type="radio">Microsoft</p>
			<p><input name="connait-produits" value="Xerox" type="radio">Xerox</p>
		</div>
		<button>Continuer</button>
		<!--<p><a href="???">garantie</a></p>-->
	</div>
</section>

<section>
	<div class="q">
		<h2 id="qui-invente-l-objet-technique-">qui invente l&#39;objet technique ?</h2>
		<div>
			<p><input name="obj-tech" value="le constructeur" type="radio">le constructeur</p>
			<p><input name="obj-tech" value="l'utilisateur" type="radio">l&#39;utilisateur</p>
		</div>
		<button>Continuer</button>
		<p><span class="link" data-id="garantie">?</span></p> 
	</div>
</section>

<section>
	<div class="q">
		<h2 id="quel-est-le-r-le-d-une-garantie-">quel est le rôle d&#39;une garantie ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><span class="link" data-id="garantie">?</span></p> 
	</div>
</section>

<section>
	<div class="q">
		<h2 id="votre-ordinateur-est-il-honn-te-avec-vous-">votre ordinateur est-il honnête avec vous ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<!--<p><a href="???">???</a></p>-->
	</div>
</section>

<section>
	<div class="q">
		<h2 id="-tes-vous-honn-te-avec-votre-ordinateur-">êtes-vous honnête avec votre ordinateur ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<!--<p><a href="???">???</a></p>-->
	</div>
</section>

<section>
	<div class="q">
		<h2 id="votre-ordinateur-vous-a-t-il-d-j-fait-du-mal-">votre ordinateur vous a t-il déjà fait du mal ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<!--<p><a href="???">???</a></p>-->
	</div>
</section>

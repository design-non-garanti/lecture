[?](url)

<!--

 [The Politics of design, A (Not so) Global Manual for Visual Communication, Ruben Pater, Architecture of Choices, 2017](???)

Whether we apply for a visa or fill out a tax form, we obey the questions asked on the form because of its design. If the form used colour photographs and the comic sans font, we would doubt its authenticity. The design of forms is based on authority. Form designers are not just looking at legibility and functionality, they select colours, graphics elements, and typefaces that create the appropriate identity of authority. Forms are a very direct visualization of a power structure.


-->

---

## quel est votre prénom ?

- [ ]

---

## quel est votre nom ?

- [ ]

---

## quel âge avez-vous ?

- [ ]

---

## choisissez un emoji

- [ ]

[?](url)

<!--

 [The Politics of design, A (Not so) Global Manual for Visual Communication, Ruben Pater, Fake Diversity, 2017](???)

 ‘Diversity is at the very core of our ability to serve our clients well and to maximise return for our shareholders’, says Goldman Sachs on its website.¹ These sorts of statements can be found on the websites of almost all multinationals, usually accompanied by images of a smiling, multi-racial workforce. Three out of four of the largest companies in the U.S. had launched diversity programs by the end of the 1990s².
 Nowadays, diversity of the workforce is considered a valuable asset that increase the net worth of a compagny. How diverse are these companies really? It turns out many companies use the same photography with a calculated mix of races and genders, like they picked from a spreadsheet. […]


-->

[?](url)

<!--

[Modifying the Universal, Roel Roscam Abbing, Peggy Pierrot, Femke Snelting, 2017](https://roelof.info/~r/DB06_Executing_Practices_Modifiying_the_Universal_Pierrot_Roscam_Abbing_Snelting.pdf)

MODIFYING THE UNIVERSAL

 The demands to technology giants to fix emoji diversity fell on
fertile ground. The protest happened at a moment when US-based
technology companies such as DropBox, Pinterest, Airbnb and Twitter
had published statistics on the lack of women and people of colour
in their workforces, thereby publicly acknowledging their issues
with diversity.7
 Each of the companies had hired so-called diversity
managers that were tasked with correcting these problems.
 The Unicode Consortium, made up of several of these same
companies, was put in charge of responding to the pressure.8

A problem that in essence was caused by an awkward design-decision
from Apple, conveniently became a problem to be solved on the
abstract level of the Unicode standard. In this meta-context it was clear
that the issue could only be addressed through technological means.9

	[…]

	Technologies for segregation

In March 2016, Facebook proudly announced their use of ethnic affinities
profiling, a thinly veiled form of racial market segregation.18
For the promotion of the Universal motion picture Straight Outta
Compton, two trailers were edited. One was targeting “general population
(non-African American, non-Hispanic)” and another “AfricanAmerican”
audiences. The commercially successful campaign was
the result of a close collaboration between diversity teams in both
companies.19 Despite users’ refusal to provide information on their
ethnic background, Facebook felt entitled to guess their “ethnic
affinity” through analysis and categorisation of the data that they have
access to. Segregation based on personal electronic communication
had become “marketing as usual”.
 Emoji skin tone modifiers have of course been used to construct
racist comments20 and there is a documented case of an Instagram
search that returns different results depending on emoji with the
skin tone modifier applied.21 Should a Unicode compliant search
engine offer to sort results the same way? While Russia investigates
if it can sue Apple for their representation of sexual diversity,  app
stores refuse sex-positive emoji because they do not permit “sexual
content”.23 Activists from Turkey were arrested because of their social
network accounts, while Libya used Big Data to target its opponents
(Manach and Nicoby 2015, 38, 47-48). When social networks can target
ads based on the content of messages and user preferences apparently
representing an ethnic profile, where will the use of modified emoji
lead us?
-->

---

## choisissez un pictogramme

- [ ] 

[?](url)

<!--

 [The Politics of design, A (Not so) Global Manual for Visual Communication, Ruben Pater, Words Divide, Picture Unite, 2017](???)

[…] We should remember that Isotype was a European invention during the time of European colonialism. Only then it becomes clear that the rhetoric of Isotype as ‘objective’ and ‘neutral’ simply meant they represented European colonial standards. In the visual examples non-European countries are grouped and categorised as ‘other’. Races are reduced to five, with the white race first, and the non-white races as secondary, depiced as dark, shirtless, and with traditional attire. 

-->

---

## quelle est votre couleur préférée ?

- [ ] Greenery
- [ ] Rose Quartz
- [ ] Serenity
- [ ] Marsala
- [ ] Radiand Orchid
- [ ] Emerald
- [ ] Tangerine Tango
- [ ] Honeysucle
- [ ] Turquoise

[?](url)

<!--

[Lev Manovich, The Language of New Media, MIT Press, 2001](http://faculty.georgetown.edu/irvinem/theory/Manovich-LangNewMedia-excerpt.pdf)


In a post-industrial society, every citizen can construct her own custom
lifestyle and "select" her ideology from a large (but not infinite) number of choices. Rather than pushing the same objects/information to a mass audience, marketing now tries to target each individual separately. The logic of new media technology reflects this new social logic. Every visitor to a Web site automatically gets her own custom version of the site created on the fly from a database. The language of the text, the contents, the ads displayed — all these can be customized by interpreting the information about where on the network the user is coming from; or, if the user previously registered with the site, her personal profile can be used for this customization. According to a report in USA Today (November 9, 1999), “Unlike ads in magazines or other real-world publications, ‘banner’ ads on Web pages change wit every page view. And most of the companies that place the ads on the Web site track your movements across the Net, ‘remembering’ which ads you’ve seen, exactly when you saw them, whether  61 you clicked on them, where you were at the time and the site you have visited just before.” 
31

More generally, every hypertext reader gets her own version of the
complete text by selecting a particular path through it. Similarly, every user of an interactive installation gets her own version of the work. And so on. In this way new media technology acts as the most perfect realization of the utopia of an ideal society composed from unique individuals. New media objects assure users that their choices — and therefore, their underlying thoughts and desires — are unique, rather than pre-programmed and shared with others. As though trying to compensate for their earlier role in making us all the same, today descendants of the Jacqurd's loom, the Hollerith tabulator and Zuse's cinema-computer are now working to convince us that we are all unique. 
 -->

---

## vous êtes ?

- [ ] amateur
- [ ] expert
- [ ] capable de résoudre la plupart des problèmes en un coup de fil

[?](url)

<!--

[Apple Care, 2017](https://www.apple.com/fr/support/products/)

Les produits AppleCare offrent une assistance technique téléphonique assurée par des experts ainsi que des options supplémentaires de garantie matérielle proposées par Apple.

Comme Apple est à l’origine du matériel, du système d’exploitation et de nombreuses applications, les produits Apple sont des systèmes particulièrement intégrés. Et seuls les produits AppleCare offrent une assistance technique centralisée, assurée par des experts Apple. Ce qui permet de résoudre la plupart des problèmes en un coup de fil.

Les avantages des produits AppleCare s’ajoutent aux droits prévus par les lois sur la protection des consommateurs. Pour plus de détails, veuillez cliquer ici.

-->

---

## mais encore ?

- [ ] un scientifique
- [ ] un ouvrier de la connaissance
- [ ] un ouvrier de l'intellect
- [ ] un programmeur
- [ ] un vrai utilisateur
- [ ] un utilisateur naïf
- [ ] la dame avec une machine à écrire royale
- [ ] un enfant
- [ ] un artiste
- [ ] un musicien
- [ ] une divinité
- [ ] une machine
- [ ] un néophyte complètement paumé
- [ ] un hacker
- [ ] un nul
- [ ] vous-même
- [ ] un individu
- [ ] un citoyen
- [ ] des gens
- [ ] un autre
- [ ] un client
- [ ] un interacteur
- [ ] un acheteur
- [ ] une cible

[?](url)

<!--
[Turing Complete User, Olia Lialina, October 2012, Appendix B: Users Imagined](http://contemporary-home-computing.org/turing-complete-user/)
-->

---

## comment avez-vous eu connaissance de ce mémoire ?

- [ ] publicité
- [ ] magazine
- [ ] ami
- [ ] collègue
- [ ] autre

[?](url)

<!--
[Monthy's Python Holy Grail, le jeu vidéo, 1996](https://classicreload.com/win3x-monty-python-the-quest-for-the-holy-grail.html)
-->
---

## qu'attendez vous de ce mémoire ?

- [ ] une lecture utile
- [ ] une lecture rapide
- [ ] une problématique simple
- [ ] un contenu accessible
- [ ] une problématique innovante
- [ ] une problématique universelle
- [ ] une problématique rentable
- [ ] de l'élégance
- [ ] un sentiment de sécurité
- [ ] de la convivialité

[?](url)

<!--
[Google, Ten principles that contribute to a Googley user experience, 2008](http://www.google.com:80/corporate/ux.html on webarchive.com)

Principles for Good Design collection*

-->
---

## attendez-vous d'un objet qu'il soit conforme ?

- [ ] oui
- [ ] non


[?](url)

<!--
[^2]: Service Public, « Garantie légale de conformité », consutable à l'adresse :
<https://www.service-public.fr/particuliers/vosdroits/F11094>
-->

---

## un bon design est :

- [ ] utile
- [ ] rapide
- [ ] simple
- [ ] accessible
- [ ] innovant
- [ ] universel
- [ ] rentable
- [ ] élégant
- [ ] sécuritaire
- [ ] convivial

[?](url)

<!--
[Google, Ten principles that contribute to a Googley user experience, 2008](http://www.google.com:80/corporate/ux.html on webarchive.com)

Principles for Good Design collection*

-->

---

## un bon design :

- [ ] répond à un besoin
- [ ] définit un besoin
- [ ] ne se contente pas de répondre à un besoin, il le définit

[?](url)

<!--

[Matt McCullough, Steve Jobs and the Goal of Preaching, 08.28.2014](https://www.9marks.org/article/steve-jobs-and-the-goal-of-preaching/)

-->

---

## pourquoi même un bon design peut-il échouer ?

- [ ] 

[?](url)

<!--

[Don Norman, the invisible computer](???)

-->
---

## un bon design doit-il avoir raison ?

- [ ] 

[?](url)

<!--

[Theodor W. Adorno et Max Horkheimer, dialectique de la raison, 1944](???)

-->
---

## un bon design est-il à notre service ? 

- [ ] 

[?](url)

<!--

[Don Norman, Design of everyday things](???)

-->

---

## cochez.

- [ ] praticité
- [ ] rapidité
- [ ] simplicité
- [ ] accessibilité
- [ ] innovation
- [ ] universalité
- [ ] rentabilité
- [ ] beauté
- [ ] sécurité
- [ ] convivialité

[?](url)

<!--

[Google, Ten principles that contribute to a Googley user experience, 2008](http://www.google.com:80/corporate/ux.html on webarchive.com)

Principles for Good Design collection*

-->

---

## choisissez la bonne position pour chaque forme dans chaque situation.

 O /\ []

[?](url)

<!--

>Every shape exists only because of the space around it. … Hence there is a ‘right’ position for every shape in every situation. If we succeed in finding that position, we have done our job.

[Donald Knuth TEXbook, JAN TSCHICHOLD, Typographische Gestaltung (1935)](???)

-->

---

<!-- ## qu'est ce qui est important lorsque l'on doit choisir-->
## Face à un choix, le plus important est d'avoir :

- de bonnes possibilités
- de bons choix
- aucune des propositions ci-dessus

[?](url)

<!--

[Lialina, User Rights](http://userrights.contemporary-home-computing.org/)

-->

---

## apprendre des nouvelles manières de faire dans le seul but d'être différent est :

- [ ] une contrainte
- [ ] irrespectueux
- [ ] inconsistant

[?](url)

<!--

[Apple Guidelines*](???)

-->

---

## cherchez l'intrus :

- [ ] innovant
- [ ] kitsch
- [ ] avant-gardiste

[?](url)

<!--

[Abraham Moles, Nouveaux actes sémiotiques, kitsch et avant-garde](???)

-->

---

## caractérisez l'usage de l'ornement :

- [ ] Enfantin
- [ ] Refus de la pureté du design
- [ ] Instinct primitif
- [ ] Peur de la beauté réelle
- [ ] Maquillage pour un mauvais design
- [ ] Bon pour les Indiens
- [ ] Basse culture

[?](url)

<!--

[Adolf Loos, JAN TSCHICHOLD, Typographische Gestaltung (1935)](???)

The use of ornament. in whatever style or quality. comes from an attitude of childish na·ivety. It shows a reluctance to use "pure design, " a giving-in to a primitive instinct to decorate - which reveals. in the last resort. a fear of pure appearance. It is so easy to employ ornament to cover up bad design! The important architect Adolf La os. one of the first champions of pure form. wrote already in 1898: "The more primitive a people. the more extravag antly they use ornament and decoration. The Indian overloads everything. every boat. every rudder. every arrow. with ornament. To insist on decoration is to put yourself on the same level as an Indian. The Indian in us all must be overcome. The Indian says: This woman is beautiful because she wears golden rings in her nose and her ears. Men of a higher culture say: This woman is beautiful because she does not wear rings in her nose or her ears. To seek beauty in form itself rather than make it dependent on ornament should be the aim of all mankind." Today we see in a desire for ornament an ignorant tendency which our century must repress. When in earlier periods ornament was used. often in an extravagant degree. it only showed how little the essence of typography. which is communication. was understood. 


-->

---

## qu'est ce qui différencie le naturel de la contre-façon ?

- [ ]

[?](url)

<!--

[Jean Baudrillard, l'échange symbolique et la mort, l'ordre des simulacres, l'ange de stuc](???)

La contrefaçon (et la mode du même coup) naît avec la Renaissance, avec la déstructuration de l'ordre féodal par l'ordre bourgeois et l'émergence d'une compétition ouverte au niveau des signes distinctifs. Pas de mode dans une société de castes ou de rangs, puisque l'assignation est totale et la mouvance des classes nulle. Un interdit protège les signes et leur assure une clarté totale : chacun renvoie sans équivoque à un statut. Pas de contrefaçon possible dans le cérémonial — sinon comme magie noire et sacrilège, et c'est bien ainsi que le mélange des signes est puni : comme infraction grave à l'ordre même des choses. Si nous nous prenons encore à rêver — aujourd'hui surtout — d'un monde de signes sûrs, d'un « ordre symbolique » fort, soyons sans illusions : cet ordre a existé, et ce fut celui d'une hierarchie féroce, car la transparence et la cruauté des signes vont de pair.
[…]
Fin du signe <i>obligé</i>, rège du signe émancipé, dont vont pouvoir jouer indifféremment toutes les classes. La démocratie concurrentielle succède à l'endogamie des signes propre aux ordres statutaires. Du même coup on entre, avec le transit de valeurs/signes de prestige d'une classe à l'autre, nécéssairement dans la <i>contrefaçon</i>
[…]
Mais le signe multiplié n'a plus rien à voir avec le signe obligé à diffusion restreinte : Il en est la contrefaçon, non par dénaturation d'un « original », mais par extension d'un materiel dont toute la clarté tenait à la restriction qui le frappait. Non discriminant, (il n'est plus que compétitif), allégé de toute contrainte, disponible dans l'universel, le signe moderne simule pourtant encore la nécessité en se donnant pour lié au monde. Le signe moderne rêve du signe antérieur et voudrait bien, avec sa référence au réel, retrouver une <i>obligation</i> : il ne retrouve qu'une <i>raison</i>: cette raison référentielle, ce réel, ce « naturel » dont il va vivre.
[…]
C'est donc dans le simulacre d'une « nature » que le signe moderne trouve sa valeur. Problématique du « naturel » métaphysique de la réalité et de l'apparence : ce sera celle de toute la bourgeoisie depuis la Renaissance, miroir du signe classique. Encore aujourd'hui la nostalgie d'une référence naturelle du signe est vivace, 
[…]
C'est donc à la Renaissance que le faux est né avec le naturel.

-->

[?](url)

<!--

[Roland Barthes, Roland Barthes par Roland Barthes](???)

L'illusion de naturel est sans cesse dénoncée (dans les Mythologies, dans le Système de la Mode; dans S/Z même, où il est dit que la dénotation est retournée en Nature du langage). Le naturel n'est nullement un attribut de la Nature physique ; c'est l'alibi dont se pare une majorité sociale : le naturel est une légalité. D'où la nécéssité critique de faire sous ce naturel-là, et, selon le mot de Brecht, « sous la règle l'abus ».
On peut voir l'origine de cette critique dans la situation minoritaire de R.B. lui-même ; il a toujours appartenu à quelque minorité, à quelque marge — de la société, du langage, du désir, du métier, et même autrefois de la religion (il n'était pas indifférent d'être protestant dans une classe de petits catholiques); situation nullement sévère, mais qui marque un peu toute l'existence sociale : qui ne sent combien il est naturel, en France, d'être catholique, marié et bien diplomé ? La moindre carence introduite dans ce tableau des conformités publiques forme une sorte de pli ténu de ce que l'on pourrait appeler la litière sociale.
[…]

-->



---

## un outil peut-il être conçu pour être détourné ?

- [ ] 

[?](url)

<!--

[Guy Debord et Wolman - Mode d'emploi du Détournement](???)

-->

---

## votre ordinateur est-il reprogrammable ?

- [ ] oui
- [ ] non

[?](url)

<!--

[Emile Greis, la reprogrammabilité](???)

-->

---

## votre interface est :

- [ ] graphique
- [ ] en lignes de commandes

[?](url)

<!--
[Umberto Eco, comment voyager avec un saumon, comment reconnaître la religion d'un ordinateur, ](???)
-->

---

## vous possédez un :

- [ ] Mac
- [ ] Windows

[?](url)

<!--
[Umberto Eco, comment voyager avec un saumon, comment reconnaître la religion d'un ordinateur, ](???)
-->

---

## vous êtes :

- [ ] Catholique
- [ ] Protestant

[?](url)

<!--

[Umberto Eco, comment reconnaître la religion d'un ordinateur](???)

-->

---

## c'est quoi un ordinateur ?

- [ ] 

[?](url)

<!--

[Apple, iPad](???)

-->

---

## quel est l'usage attendu d'un ordinateur ?

- [ ]

[?](url)

<!--

[garantie](???)

-->

---

## décrivez un ordinateur.

- [ ]

---

## pourquoi l'ordinateur personnel est-il si complexe ?

- [ ] 

[Don Norman, the invisible computer](???)

---

## qu'est ce qui ne va pas avec l'ordinateur ?

- [ ] 

[What's wrong with the computer ? Norman, The Invisible Computer](???)

---

## l'ordinateur fait :

- [ ] une seule chose mais bien
- [ ] plusieurs choses mais mal

[Information Appliance](???)

---

## quand votre ordinateur ne fonctionne pas comme vous le voulez, d'où viens le problème ? 

- [ ] 

[Ted Nelson, computer for cynics](???)

---

## votre ordinateur vous simplifie t-il la vie ?

- [ ] 

[No one's life has yet been simplified by a computer. Nelson, Computer Lib, 1974](???)

---

## un ordinateur est-il un écran ?
 
- [ ] oui
- [ ] non
- [ ] peut-être

[Where did the computer go ?](???)

---

## un ordinateur est-il une surface ?

- [ ] oui
- [ ] non
- [ ] peut-être

[Paper as metaphor](???)

---

## pourquoi est-il impossible de mettre des notes marginales sur un fichier Microsoft Word, un Pdf ou sur une page web ?

- [ ] car les ordinateurs ne le permettent pas
- [ ] car c'est une fonctionnalité difficile à trouver
- [ ] car Chuck Simoni, John Warnock et Tim Berners-Lee ont considéré que cela n'était pas nécessaire

[Ted Nelson, computer for cynics](???)

---

## pourquoi le texte à l'écran est-il généralement en noir sur fond blanc ?

- [ ] 

[Paper as metaphor](???)

---

## pourquoi dix-huit siècles de culture de l'imprimé ont été réduits à une simulation de feuille de papier ?

- [ ] 

[Ted Nelson, computer for cynics](???)

---

## quelles sont les différences entre un presse papier (papier) et un presse papier numérique ?

- [ ] il est invisible
- [ ] il ne peut contenir qu'un objet,
- [ ] quoi-que vous y placiez, il détruit le contenu précédent. À part ça, C'EST JUSTE UN PRESSE PAPIER NORMAL SOUS TOUT LES ASPECTS-- À L'EXEPTION QU'IL NE POSSÈDE AUCUN AUTRE ASPECT.

>It's just like a regular clipboard, except 
>(a) you can't see it, 
>(b) it holds only one object,
>(c) whatever you put there destroys the previous contents.  Aside from that, IT'S JUST LIKE A REGULAR CLIPBOARD IN EVERY OTHER RESPECT-- EXCEPT THERE AREN'T ANY OTHER RESPECTS!

[Nelson, one liners](???)

---

## avons-nous des bureaux d'ordinateur ou des ateliers d'ordinateur ?

- [ ] 

[???](???)

---

## comment disposez-vous les icônes sur le bureau de votre ordinateur ? 

- [ ] 

[???](???)

---

## à quoi sert cette icône : 🔎 

- [ ]  zoomer
- [ ]  rechercher

ou text ?

[Fuller, Metaphor in The impossibility of the interface in Behind the Blip, ?](???)

---

## la commande CMD-Z est-elle intuitive ?

- [ ] J'ai toujours profondément penser au fond de mon cœur que CMD-Z devait annuler les choses.

>Oh, SURE the Macintosh interface is Intuitive! I've always thought deep in my heart that command-z should undo things. "

[Margy Levine](???)

---

<!-- ## qu'est ce vous trouvez simple -->

## que trouvez-vous simple ?

<!-- Qu'est ce vous trouvez compliqué -->
Que trouvez-vous compliqué

- [ ] 

[???](???)

---

## qui est le maître de la simplicité ?

- [ ] John Maeda

[Rules of simplicity, John Maeda](???)

---

## aimez-vous que l'on vous pousse à réfléchir ?

- [ ] continuer

[Steve Krug, don't make me think](???)

---

## surfez-vous sur le net ?

- [ ] Oui
- [ ] Non

[Jakob Nielsen, alertbox 1996](???)

---

## saviez-vous que le surf sur le net est mort en 1996 ?

- [ ] Oui
- [ ] Non

[Jakob Nielsen, alertbox 1996](???)

---

## l'ordinateur est : 

- [ ] une aventure civile
- [ ] une aventure commerciale
- [ ] la première et le deuxième question signifient la même chose

[Jakob Nielsen, alertbox 1996](???)

---

## parmi les termes suivants lequel est-il le plus correct pour désigner un contenu pertinent ?

- [ ] Populaire
- [ ] Démocratique
- [ ] Recommandé
- [ ] Accessible
- [ ] Authentique
- [ ] Unique
- [ ] Classique

[Google PageRank System](???)

---

## parmi les termes suivants, choisissez le miracle :

- [ ] Jésus
- [ ] Xerox

[Xerox, It's a miracle](???)

---

## qui connaît le mieux vos produits ? 

- [ ] Adobe
- [ ] Apple
- [ ] Google
- [ ] Microsoft
- [ ] Xerox

[garantie](???)

---

## qui invente l'objet technique ?

- [ ] Le constructeur
- [ ] L'utilisateur

---

## quel est le rôle d'une garantie ?

- [ ]

---

## votre ordinateur est-il honnête avec vous ?

- [ ] 

[???](???)

---

## êtes-vous honnête avec votre ordinateur ?

- [ ] 

[???](???)

---

## votre ordinateur vous a t-il déjà fait du mal ?

- [ ] 

[???](???)

---

<<<<<<< HEAD
---




Dans le but d'éviter de forcer ses utilisateurs à apprendre de nouvelles façons de faire les choses
et donc de respecter ses utilisateurs une applications doit être consistente

le principe de consistente

=======
>>>>>>> 33aa636b85e744df521f26d2bdf2d083a940dc16

function tschichold_event(id){
    console.log('BAUHAUSEVENT');
    var bauhausSection=document.createElement('div');
    bauhausSection.style.left=window.scrollX+'px';
    bauhausSection.position="fixed";
    bauhausSection.display="block";
    bauhausSection.width='100vw';
    bauhausSection.height='100vh';

    function addShape(string,parent){

        var shape=document.createElement('img');
        shape.style.maxWidth='5vw';
        shape.name=string;
        shape.className='shape';
        shape.src=string+".png";
        shape.style.position='absolute';
        shape.style.cursor='pointer';
        bauhausSection.appendChild(shape);

    }

    function addEmplacement(string){

        var emplacement=document.createElement('img');
        emplacement.style.maxWidth='5vw';
        emplacement.name=string;
        emplacement.className='emplacement';
        emplacement.src=string+"_emplacement.png";
        emplacement.style.position='absolute';
        emplacement.style.border='solid';
        emplacement.style.borderWidth='0.5vw';
        emplacement.style.borderColor='rgb(196,196,196)';
        emplacement.style.margin='1vw';
        emplacement.style.padding='1vw';
        bauhausSection.appendChild(emplacement);

    }

    addShape('carre');
    addShape('cercke');
    addShape('triangle');

    addEmplacement('carre');
    addEmplacement('cercke');
    addEmplacement('triangle');
    document.body.appendChild(bauhausSection);


    var shapes = document.getElementsByClassName('shape');

    for(var i=0; i<shapes.length; i++){
        shapes[i].style.left=window.scrollX+window.innerWidth+Math.random()*window.innerWidth/1.125-shapes[i].getBoundingClientRect().width/2+'px';
        shapes[i].style.top=Math.random()*window.innerHeight/1.125-shapes[i].getBoundingClientRect().height/2+'px';
        shapes[i].onclick=function(e){
            if(!this.select){
                this.style.zIndex=998;
                this.select=true;
            }else{
                this.style.zIndex=0;
                this.select=false;
            }
        }

    }

    var emplacements = document.getElementsByClassName('emplacement');

    for(var i = 0 ; i < emplacements.length; i++){
        emplacements[i].style.left=window.scrollX+window.innerWidth+(emplacements[i].getBoundingClientRect().width*3)*i+'px';
        emplacements[i].style.top=emplacements[i].getBoundingClientRect().width*1.125+'px';
    }

    document.onmousemove=function(e){
        for(var i=0; i<shapes.length; i++){
            if(shapes[i].select){
                shapes[i].style.left=window.scrollX+e.clientX-shapes[i].getBoundingClientRect().width/2+'px';
                shapes[i].style.top=e.clientY-shapes[i].getBoundingClientRect().height/2+'px';
            }
        }
        for(var i = 0 ; i < emplacements.length; i++){

            if(e.clientX > emplacements[i].getClientRects()[0].x && e.clientX < emplacements[i].getClientRects()[0].x+emplacements[i].getClientRects()[0].width && e.clientY > emplacements[i].getClientRects()[0].y && e.clientY < emplacements[i].getClientRects()[0].y+emplacements[i].getClientRects()[0].height
                ){
                for( var s in shapes){
                    if(shapes[s].select ){
                        if(shapes[s].name == emplacements[i].name){

                            for(var e = 0 ; e < emplacements.length; e++){
                                if(!emplacements[e].fill){
                                    emplacements[e].style['border-color']='rgb(196,196,196)';
                                }
                            }
                            emplacements[i].style['border-color']='rgb(196,255,196)';
                            console.log('right');
                            shapes[s].select=false;
                            shapes[s].style.left='calc('+window.scrollX+'px + '+emplacements[i].getClientRects()[0].x+'px + 1.5vw)';
                            shapes[s].style.top='calc('+emplacements[i].getClientRects()[0].y+'px + 1.5vw)';
                            shapes[s].style.pointerEvents = "none";
                            emplacements[i].fill=true;
                            shapes[s].style.zIndex=999;
                            var end=true;
                            for(var e = 0 ; e < emplacements.length; e++){
                                if(!emplacements[e].fill){
                                 end=false;
                             }
                         }
                         if(end){
                            document.getElementById(id).value='done';
                        }
                    }else{
                        if(!emplacements[i].fill){
                            for(var e = 0 ; e < emplacements.length; e++){
                                if(!emplacements[e].fill){
                                    emplacements[e].style['border-color']='rgb(196,196,196)';
                                }
                            }

                            emplacements[i].style['border-color']='rgb(196,255,196)';
                            emplacements[i].style['border-color']='rgb(255,196,196)';
                            console.log('wrong');
                        }
                    }
                }
            }
        }       
    }
}

window.bauhausEvent=true;
}
console.log('events_ok');

async function cartoon_transition(){

    var dom = document.getElementById('cartoon_overlay');
    dom.style.visibility= 'visible';
    var size = dom.getClientRects()[0].width;
    var left = dom.getClientRects()[0].x;

    while(size>window.innerWidth){
        await sleep(17);

        size-=2000;
        left+=1000;

        if(size<window.innerWidth){
            size=window.innerWidth;
            left=0;
            dom.style.backgroundColor='black';
        }

        dom.style.left=left+'px';
        dom.style.width=size+'px';
        dom.style.height=size+'px';

        console.log(size);
    }

    window.cartoonEvent=true;
    window.location.assign("../ordinateur_du_21e_siecle");

}
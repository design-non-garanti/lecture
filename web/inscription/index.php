<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8">-->
    <title></title>
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
    <link rel="stylesheet" type="text/css" href="fonts/myriad/myriad.css">
    <link rel="stylesheet" type="text/css" href="../COMMON/index.css">
    <script src="assets/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="scripts/events.js" ></script>
    <script type="text/javascript" src="scripts/scripts.js" defer></script>
    <script type="text/javascript" src="../COMMON/index.js" defer></script>
</head>
<body>
   <!-- <div id="travaux"></div> -->
    <div id="cartoon_overlay"></div>
    <div id="overlay"></div>
    <section>
        <!-- start -->
        <div id='start'>
            <h2>Design non-garanti.</h2>
            <button onclick="commencer()">Commencer</button>
            <p id="conditions">
                En cliquant sur Commencer, vous acceptez nos <a href="conditions.php">Conditions</a> et indiquez
                que vous avez lu notre <a href="utilisation_des_donnees.php">Politique d’utilisation des
                données</a>, y compris notre <a href="utilisation_des_cookies.php">Utilisation des cookies</a>. Vous
                pourrez recevoir des notifications par mail de notre part et pouvez vous désabonner à
                tout moment.
            </p>
        </div>
    </section>
    <?php include "questions.php"; ?>
</body>
</html>

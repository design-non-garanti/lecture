<!--

Choisissez un schéma :

centralisé
décentralisé
mesh

-->

Définissez simplicité

-truc à dit
-truc à dit





En France, le consommateur bénéficie de deux garanties obligatoires dont les conditions sont fixées
par la loi : la garantie légale de conformité et la garantie des vices cachés. Une
garantie commerciale peut être proposée par le vendeur ou le fabricant et dépend des conditions
fixées par celui-ci. Elle est facultative et s'ajoute aux garanties légales obligatoires[^1].  

La garantie légale de conformité stipule que le produit doit être conforme à l'usage attendu et à la
description du vendeur. La garantie des vices cachés protège contre les défauts cachés. Gratuite ou
payante et d'une durée variable la garantie commerciale permet à l'acquéreur, pendant la durée de
couverture, de faire réparer, remplacer ou rembourser par le fournisseur un produit en panne
(appareil électroménager, ordinateur, voiture, fauteuil, etc.). 

Généralement comprise dans l'achat de l'objet la garantie s'impose comme une étiquette, un label,
qui transporte une confiance à l'acquéreur. De plus la garantie propose un service, une assistance
par ceux, qui sont à l'origine de l'objet, les experts qui connaissent le mieux le produit.

- service et une assistance par **ceux qui connaissent le mieux vos produits** Apple
- Les produits AppleCare offrent une assistance technique téléphonique **assurée par des experts** ainsi
  que des options supplémentaires de garantie matérielle proposées par Apple.
- Comme Apple est **à l’origine du matériel**, du système d’exploitation et de nombreuses applications,
  les produits Apple sont des systèmes particulièrement intégrés. Et **seuls** les produits AppleCare
  offrent une assistance technique **centralisée, assurée par des experts** Apple. Ce qui permet de
  résoudre la plupart des problèmes en un coup de fil.

remettre dans les mains d'experts afin qu'ils assurent la réparation
garantir c'est donc 


Pour Simondon la garantie renforce une rupture entre le constructeur qui s'identifie comme
l'inventeur et l'utilisateur qui acquiert l'usage de l'objet technique uniquement par un procédé
économique.


la garantie concrétise le caractère économique pur de cette relation

contexte
différenciation constructeur/inventeur et utilisateur
règle, modifie, 
continue la vie de la machine règlant, modifiant
tendent 
méconaissance

>Le plombage des organes délicats indique cette coupure entre le constructeur, qui s'identifie à
>l'inventeur, et l'utilisateur, qui acquiert l'usage de l'objet technique uniquement par un procédé
>économique; 


>La garantie concrétise le caractère économique pur de cette relation entre le constructeur et
>l'utilisateur ; l'utilisateur ne prolonge en aucune manière l'acte du constructeur ; par la garantie
>il achète le droit d'imposer au constructeur une reprise de son activité si le besoin s'en fait
>sentir.

>Au contraire, les objets techniques qui ne sont pas soumis à un pareil statut de séparation entre
>la construction et l'utilisation ne se dégradent pas dans le temps : ils sont conçus pour que les
>différents organes qui les constituent puissent être remplacés et réparés au cours de l'utilisation
>de manière continue : l'entretien ne se sépare pas de la construction il la prolonge [...]






garantie auprès du fournisseur qui procédera, soit à la réparation, au remplacement ou au
remboursement.





:w







[^1]: Service Public, « Garanties », consultable à l'adresse :
<https://www.service-public.fr/particuliers/vosdroits/N31164>

[^2]: Service Public, « Garantie légale de conformité », consutable à l'adresse :
<https://www.service-public.fr/particuliers/vosdroits/F11094>

[^3]: Service Public, « Garantie des vices cachés », consultable à l'adresse :
<https://www.service-public.fr/particuliers/vosdroits/F11007>

[^4]: Service Public, « Garantie commerciale », consultable à l'adresse :
<https://www.service-public.fr/particuliers/vosdroits/F11093>

## cherchez l’intrus :

- [ ] Informatique
- [ ] Digital
- [ ] Numérique
- [ ] Technologique
- [ ] Ordinateur

[???](???)

---








<!--

---

## quel est votre typo préférée ?

- [ ] 

[???](???)
-->


<!--
## choisissez un mot de passe :

 - [ ] Face ID 

---

-->
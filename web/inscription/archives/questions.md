
---

## Quel est votre nom ?

- [ ]

---

## Quel est votre prénom ?

- [ ]

---

## Quel est votre age ?

- [ ]

---

## Quelle est votre adresse mail ?

- [ ]

---

## Quelle est votre sexe ?

- [ ] Homme
- [ ] Femme
- [ ] Autre

---

## Choisissez un mot de passe :

- [ ] Face ID

---

## Vous êtes ?

- [ ] amateur
- [ ] professionel
- [ ] expert

[???](???)

---

## Mais encore ?

- [ ] un scientifique
- [ ] un ouvrier de la connaissance
- [ ] un programmeur
- [ ] un vrai utilisateur
- [ ] un utilisateur naïf
- [ ] une dame avec une machine à écrire royale
- [ ] un enfant
- [ ] un artiste
- [ ] un musicien
- [ ] une divinité
- [ ] un néophyte complètement paumé
- [ ] un hacker
- [ ] un nul
- [ ] vous-même
- [ ] un individu
- [ ] un citoyen
- [ ] un autre
- [ ] un client
- [ ] un interacteur
- [ ] un acheteur
- [ ] une cible

[Turing Complete User, Olia Lialina, October 2012, Appendix B: Users Imagined](http://contemporary-home-computing.org/turing-complete-user/)

---

## Comment avez-vous eu connaissance de ce mémoire ?

- [ ] publicité
- [ ] magazine
- [ ] ami
- [ ] collègue
- [ ] autre

[Monthy's Python Holy Grail, le jeu vidéo, 1996](???)

---

## Quelle est votre couleur préférée ?

- [ ] Greenery
- [ ] Rose Quartz
- [ ] Serenity
- [ ] Marsala
- [ ] Radiand Orchid
- [ ] Emerald
- [ ] Tangerine Tango
- [ ] Honeysucle
- [ ] Turquoise

[Monthy's Python Holy Grail, le jeu vidéo, 1996](???)

---

## Qu'attendez vous de ce mémoire ?

- [ ] une lecture facile
- [ ] une lecture rapide
- [ ] un contenu accessible
- [ ] un sentiment de sécurité
- [ ] un sentiment d'élégance
- [ ] une problématique simple
- [ ] une problématique innovante

[user experience principles](???)

---

## Vous possédez un :

- [ ] Mac
- [ ] Windows

[Umberto Eco, comment reconnaître la religion d'un ordinateur](???)

---

## Votre interface est :

- [ ] graphique
- [ ] en lignes de commandes

[Umberto Eco, comment reconnaître la religion d'un ordinateur](???)

---

## Vous êtes :

- [ ] Catholique
- [ ] Protestant

[Umberto Eco, comment reconnaître la religion d'un ordinateur](???)

---

## C'est quoi un ordinateur ?

- [ ] 

---

## Pourquoi l'ordinateur personnel est-il si complexe ?

- [ ] 

[Don Norman, the invisible computer](???)

---

## Quand votre ordinateur ne fonctionne pas comme vous le voulez d'où viens le problème ? 

- [ ] 

[Ted Nelson, computer for cynics](???)

---

## Pourquoi ne peut-on pas mettre des notes marginales sur un fichier Microsoft Word, un Pdf ou sur une page web ?

- [ ] car les ordinateurs ne le permettent pas
- [ ] car Chuck Simoni, John Warnock et Tim Berne-Lee ont considéré que cela n'était pas nécessaire

[Ted Nelson, computer for cynics](???)

---

## Pourquoi avons-nous des bureaux d'ordinateur et pas des ateliers d'ordinateur ?

- [ ] 

[???](???)

---

## Comment disposez-vous les îcones sur le bureau de votre ordinateur ? 

- [ ] 

[???](???)

---

## Pourquoi le texte à l'écran est-il généralement en noir sur fond blanc ?

- [ ] 

[Paper as metaphor](???)

---

## Pourquoi dix-huit siècles de culture de l'imprimé ont été réduit à une simulation de feuille de papier ?

- [ ] 

[Ted Nelson, computer for cynics](???)

---

## Un ordinateur est-il un écran ?
 
- [ ] oui
- [ ] non
- [ ] peut-être

[Apple](???)

---

## Un ordinateur est-il une surface ?

- [ ] oui
- [ ] non
- [ ] peut-être

[Paper as metaphor](???)

---

## Surfez-vous sur le net ?

- [ ] Oui
- [ ] Non

[Jakob Nielsen, alertbox 1996](???)

---

## Saviez-vous que le surf sur le net est mort en 1996 ?

- [ ] Oui
- [ ] Non

[Jakob Nielsen, alertbox 1996](???)

---

## L'ordinateur est : 

- [ ] une aventure civile
- [ ] une aventure commerciale
- [ ] la première et le deuxième question signifient la même chose

[Jakob Nielsen, alertbox 1996](???)

---

## Un bon design est :

- [ ] pratique
- [ ] élégant
- [ ] intelligent
- [ ] naturel
- [ ] facile d'utilisation
- [ ] accesible
- [ ] innovant

[user experience principles](???)

---

## Un bon design :

- [ ] répond à un besoin
- [ ] définit un besoin
ne se contente pas de répondre à un besoin, il le définit
<!-- - [ ] répond à une attente
- [ ] définit un attente
- [ ] ne se contente pas de répondre à une attente, il la définit-->

[Matt McCullough, Steve Jobs and the Goal of Preaching, 08.28.2014](???)

---

## Pourquoi même un bon design peut-il échouer ?

- [ ] 

[Don Norman, the invisible computer](???)

---

## Un bon design doit-il avoir raison ?

- [ ] 

[???](???)

---

## Un bon design doit-il servir ? 

- [ ] 

[Don Norman, Design of everyday things](???)

---

## Cochez.

- [ ] Sécurité
- [ ] Facilité
- [ ] Rapidité
- [ ] Accessibilité
- [ ] Élégance
- [ ] Simplicité
- [ ] Innovation

[user experience principles](???)

---

## Choisissez la bonne position pour chaque forme dans chaque situation.

 O /\ []

>Every shape exists only because of the space around it. … Hence there is a ‘right’ position for every shape in every situation. If we succeed in finding that position, we have done our job.

[Donald Knuth TEXbook, JAN TSCHICHOLD, Typographische Gestaltung (1935)](???)

---

## Caractérisez l'usage de l'ornement :

- [ ] Enfantin
- [ ] Refus de la pureté du design
- [ ] Instinct primitif
- [ ] Peur de la beauté réelle
- [ ] Maquillage pour un mauvais design
- [ ] Bon pour les Indiens
- [ ] Basse culture

[Adolf Loos, JAN TSCHICHOLD, Typographische Gestaltung (1935)](???)

---

## Parmi les termes suivants lequel est-il le plus correct pour désigner un contenu pertinent ?

- [ ] Populaire
- [ ] Démocratique
- [ ] Recommandé
- [ ] Accessible
- [ ] Authentique
- [ ] Unique
- [ ] Classique

[Google PageRank System](???)

---

## Qui est le maître de la simplicité ?

- [ ] John Maeda

[Rules of simplicity, John Maeda](???)

---

## Parmi les termes suivants, choisissez le miracle :

- [ ] Apple
- [ ] Xerox
- [ ] Google
- [ ] Microsoft

[Xerox, It's a miracle](???)

---

## Quelle est la différence entre innovant et novateur ?

- [ ]

[???](???)

---

## Cherchez l’intrus :

- [ ] Informatique
- [ ] Digital
- [ ] Numérique

[???](???)

---

## Un outil peut-il être conçu pour être détourné ?

- [ ] 

[Guy Debord et Wolman - Mode d'emploi du Détournement](???)

---

## Votre ordinateur vous simplifie t-il la vie ?

- [ ] 

[No one's life has yet been simplified by a computer. Nelson, Computer Lib, 1974](???)

---

## à quoi set cet icône

- [ ]  zoomer
- [ ]  rechercher

ou text ?

[Fuller, Metaphor in The impossibility of the interface in Behind the Blip, ?](???)

---

## Qu'est ce qui est important lorsque que l'on doit choisir

- de bonnes possibilités
- de bon choix
- choose none of the above

[Lialina, User Rights](http://userrights.contemporary-home-computing.org/)

---

## Quel est votre typo préférée ?

- [ ] 

[???](???)

---

## Quelle sont les différence entre un presse papier papier numérique

- [ ] you can't see it, 
- [ ] it holds only one object,
- [ ] whatever you put there destroys the previous contents.  Aside from that, IT'S JUST LIKE A REGULAR CLIPBOARD IN EVERY OTHER RESPECT-- EXCEPT THERE AREN'T ANY OTHER RESPECTS!

>It's just like a regular clipboard, except 
>(a) you can't see it, 
>(b) it holds only one object,
>(c) whatever you put there destroys the previous contents.  Aside from that, IT'S JUST LIKE A REGULAR CLIPBOARD IN EVERY OTHER RESPECT-- EXCEPT THERE AREN'T ANY OTHER RESPECTS!

[Nelson, one liners](???)

---

## Trouvez-vous la commande CTRL-Z intuitive ?

>Oh, SURE the Macintosh interface is Intuitive! I've always thought deep in my heart that command-z should undo things. "

[Margy Levine](???)

---

## Qu'est ce qui ne va pas avec l'ordinateur

- [ ] 

[What's wrong with the computer ? Norman, The Invisible Computer](???)

---

## L'ordinateur fait-il ?
Préférez vous que votre ordinateur fasse ?

- [ ] une seule chose mais bien
- [ ] plusieurs choses mais mal

[Information Appliance](???)

---

## pour quoi est fait l'ordinateur

- [ ] 

[Information Appliance](???)

---

## Qu'est ce vous trouvez simple
Qu'est ce vous trouvez compliqué

- [ ] 

[???](???)

---

## Votre ordinateur est-il honnête avec vous ?

- [ ] 

[???](???)

---

## êtes vous honnête avec votre ordinateur ?

- [ ] 

[???](???)

---

## votre ordinauter vous a  til déjà fait du mal

- [ ] 

[???](???)

---

## Ne me poussez pas à réfléchir…

- [ ] 

[Steve Krug, don't make me think](???)

---
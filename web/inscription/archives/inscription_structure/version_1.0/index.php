<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8">-->
    <title></title>
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
    <script src="assets/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="scripts/scripts.js" defer></script>
    <style type="text/css">

    <?php
    $dir = "fonts/";
    $fonts = scandir($dir);


    foreach ($fonts as $key => $value) {
        if($value == "." || $value == ".."){}else{
            echo "@font-face{\r\nfont-family: \"".$value."\";\r\nsrc: url(\"".$dir.$value."\");\r\n}\r\n";
        }
    }
    ?>

    *{
        font-weight: normal;
        margin: 0;
        padding: 0;
        border-collapse: collapse;
    }

    html{
        font-family: 'MYRIADAT.ttf';
        color: rgb(32,32,32);
        background-color: rgb();
        overflow: hidden;
    }

    article{
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin:auto;
        width: 100%;
        display: table;
    }
    section{
        margin:auto;
        display: table;
    }
    section h1, h2, h3{
        font-family: 'MYRIADAM.ttf';
        display: table; 
    }
    section h1{
        font-size: 4rem;
    }
    section h2{
        font-size: 3rem;
    }
    section h3{
        font-size: 2rem;
    }
    section * {
        margin-bottom: 1rem;
    }
    section h4{
        font-size: 1.25rem;
    }
    .button{
        cursor: pointer;
        font-family: 'MYRIADAT.ttf';
        box-sizing: border-box;
        display: block;
        padding: 0.33rem;
        background-color: rgb(240,240,240);
        text-align: center;
        width: 20rem;
        margin:auto;
        margin-bottom: 1rem;
    }
    section aside p {
        font-size: 2rem;
        margin:auto;
        width: 20rem;
        margin-bottom: 1rem;
    }
    section aside{
        margin-top: 1.5rem;
        margin-bottom: 2rem;
    }
    section input{
        font-size: 1.25rem;
        box-sizing: border-box;
        margin:auto;
        width: 20rem;
        margin-bottom: 1rem;
        min-height: 2.5rem;
        display: block;
        text-align: center;
    }

    section input[type=radio]{
        vertical-align: middle;
        display: inline !important;
        min-height:1.5rem !important;
        width: auto !important;
        min-width: 1.25rem !important;
        margin: auto;
    }

    #overlay{
        width: 100%;
        height: 100%;
        background-color: white;
        position: fixed;
        z-index: 999;
    }

</style>
</head>
<body>

    <div id="overlay">
    </div>

    <main>

        <article>
            <section class='q'>
                <h1>
                    Design non garanti.
                </h1>
                <h3 class="button" onclick="commencer();">Commencer</h3>
            </section>
        </article>



        <article>
            <section class='q'>
                <h2>
                    Quel est votre nom ?
                </h2>
                <input placeholder="" >

            </input>
            <h3 class="button">suivant</h3>
        </section>
    </article>

    <article>
        <section class='q'>
            <h2>
                Quel est votre prénom ?
            </h2>
            <input placeholder="">

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Quel est votre age ?
        </h2>
        <input placeholder="">

    </input>
    <h3 class="button">suivant</h3>
</section>
</article>

<!--
    <article>
        <section class='q'>
            <h2>
                Quelle est votre adresse postale ?
            </h2>
            <input placeholder="exemple : michel">

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>
-->

<article>
    <section class='q'>
        <h2>
            Quelle est votre adresse mail ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Quelle est votre sexe ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="gender" value="Homme">
                    Homme
                </input>
            </p>
            <p>
                <input type="radio" name="gender" value="Femme">
                    Femme
                </input>
            </p>
            <p>
                <input type="radio" name="gender" value="Autre">
                    Autre
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>


<article>
    <section class='q'>
        <h2>
            Choisissez un mot de passe :
        </h2>
        <aside>
            <!--
            <p>
                <input type="radio" name="key" value="mot de passe">
                mot de passe
            </input>
        </p>
        <p>
            <input type="radio" name="key" value="pas de mot de passe">
            pas de mot de passe
        </input>
    </p>-->
    <p>
        <input type="radio" name="key" value="Face ID">
        Face ID
    </input>
</p>
</aside>
<h3 class="button">suivant</h3>
</section>
</article>

<article>
    <section class='q'>
        <h2>
            Vous êtes ?
        </h2>
        <aside>
            <p>
                <input type="radio"  name="job" value="amateur">
                    amateur
                </input>
            </p>
            <p>
                <input type="radio" name="job" value="expert">
                    expert
                </input>
            </p>
        </aside>
        <h3 class="button">
            suivant
        </h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
           Mais encore ?
        </h2>
        <aside>
            <p>
                <input type="radio"  name="and" value="Un individu">
                    Un individu
                </input>
            </p>
            <p>
                <input type="radio" name="and" value="Un utilisateur">
                    Un utilisateur
                </input>
            </p>
        </aside>
        <h3 class="button">
            suivant
        </h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Comment avez-vous eu connaissance de ce mémoire ?
        </h2>
        <aside>
            <p>
                <input type="radio"  name="provenance" value="publicité">
                    publicité
                </input>
            </p>
            <p>
                <input type="radio" name="provenance" value="magazine">
                    magazine
                </input>
            </p>
            <p>
                <input type="radio" name="provenance" value="ami">
                    ami
                </input>
            </p>
            <p>
                <input type="radio" name="provenance" value="collègue">
                    collègue
                </input>
            </p>
            <p>
                <input type="radio" name="provenance" value="autre">
                    autre
                </input>
            </p>
        </aside>
        <h3 class="button">
            suivant
        </h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Quelle est votre couleur préférée ?
        </h2>
        <aside>
            <p>
                <input type="radio" hex="#88B04B" name="color" value="Greenery">
                Greenery
            </input>
        </p>
        <p>
            <input type="radio" hex="#F7CAC9" name="color" value="Rose Quartz">
            Rose Quartz
        </input>
    </p>
    <p>
        <input type="radio" hex="#92A8D1" name="color" value="Serenity">
        Serenity
    </input>
</p>
<p>
    <input type="radio" hex="#955251" name="color" value="Marsala">
    Marsala
</input>
</p>
<p>
    <input type="radio" hex="#B565A7" name="color" value="Radiand Orchid">
    Radiand Orchid
</input>
</p>
<p>
    <input type="radio" hex="#009B77" name="color" value="Emerald">
    Emerald
</input>
</p>
<p>
    <input type="radio" hex="#DD4124" name="color" value="Tangerine Tango">
    Tangerine Tango
</input>
</p>
<p>
    <input type="radio" hex="#D65076" name="color" value="Honeysucle">
    Honeysucle
</input>
</p>
<p>
    <input type="radio" hex="#45B8AC" name="color" value="Turquoise">
    Turquoise
</input>
</p>
</aside>
<h3 class="button">suivant</h3>
</section>

</article>


<article>
    <section class='q'>
        <h2>
            Qu'attendez vous de ce mémoire ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="goal" value="une lecture facile">
                    une lecture facile
                </input>
            </p>
            <p>
                <input type="radio" name="goal" value="une lecture rapide">
                    une lecture rapide
                </input>
            </p>
            <p>
                <input type="radio" name="goal" value="un contenu accessible">
                    un contenu accessible
                </input>
            </p>
            <p>
                <input type="radio" name="goal" value="un sentiment de sécurité">
                    un sentiment de sécurité
                </input>
            </p>
            <p>
                <input type="radio" name="goal" value="un sentiment d'élégance">
                    un sentiment d'élégance
                </input>
            </p>
            <p>
                <input type="radio" name="goal" value="une problématique simple">
                    une problématique simple
                </input>
            </p>
            <p>
                <input type="radio" name="goal" value="une problématique innovante">
                    une problématique innovante
                </input>
            </p>
        </aside>
<h3 class="button">
    suivant
</h3>
</section>
</article>

<article>
    <section class='q'>
        <h2>
            Vous possédez un :
        </h2>
        <aside>
            <p>
                <input type="radio" name="macpc" value="de vous-même">
                    Mac
                </input>
            </p>
            <p>
                <input type="radio" name="macpc" value="de l'ordinateur">
                    Windows
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Vous votre interface est :
        </h2>
        <aside>
            <p>
                <input type="radio" name="guicmd" value="graphique">
                    graphique
                </input>
            </p>
            <p>
                <input type="radio" name="guicmd" value="en lignes de commandes">
                    en lignes de commandes
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Vous êtes :
        </h2>
        <aside>
            <p>
                <input type="radio" name="religion" value="Catholique">
                    Catholique
                </input>
            </p>
            <p>
                <input type="radio" name="religion" value="Protestant">
                    Protestant
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            C'est quoi un ordinateur ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Pourquoi l'ordinateur personnel est-il si complexe ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Quand votre ordinateur ne fonctionne pas comme vous le voulez d'où viens le problème ? 
        </h2>
        <input>

        </input>
        <!--
        <aside>
            <p>
                <input type="radio" name="problem" value="de vous-même">
                    de vous-même
                </input>
            </p>
            <p>
                <input type="radio" name="problem" value="de l'ordinateur">
                    de l'ordinateur
                </input>
            </p>
            <p>
                <input type="radio" name="problem" value="de l'ordinateur">
                    de l'ordinateur
                </input>
            </p>
        </aside>
    -->
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Pourquoi ne peut-on pas mettre des notes marginales sur un fichier Microsoft Word, un Pdf ou sur une page web ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="why" value="car les ordinateurs ne le permettent pas">
                    car les ordinateurs ne le permettent pas
                </input>
            </p>
            <p>
                <input type="radio" name="why" value="car Chuck Simoni, John Warnock et Tim Berne-Lee ont considéré que cela ne nous ai pas nécessaire">
                    car Chuck Simoni, John Warnock et Tim Berne-Lee ont considéré que cela ne nous ai pas nécessaire
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Pourquoi avons-nous des bureaux d'ordinateur et pas des ateliers d'ordinateur ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Comment disposez-vous les îcones sur le bureau de votre ordinateur ? 
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Pourquoi le texte à l'écran est-il généralement en noir sur fond blanc ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Pourquoi dix-huit siècles de culture de l'imprimé ont été réduit à une simulation de feuille de papier ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Un ordinateur est-il une surface ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="surface" value="oui">
                    oui
                </input>
            </p>
            <p>
                <input type="radio" name="surface" value="non">
                    non
                </input>
            </p>
            <p>
                <input type="radio" name="surface" value="peut-être">
                    peut-être
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Un ordinateur est-il un écran ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="surface" value="oui">
                    oui
                </input>
            </p>
            <p>
                <input type="radio" name="surface" value="non">
                    non
                </input>
            </p>
            <p>
                <input type="radio" name="surface" value="peut-être">
                    peut-être
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Surfez-vous sur le net ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="surf" value="Oui">
                    Oui
                </input>
            </p>
            <p>
                <input type="radio" name="surf" value="Non">
                    Non
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Saviez-vous que le surf sur le net est mort en 1996 ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="surfisdead" value="Oui">
                    Oui
                </input>
            </p>
            <p>
                <input type="radio" name="surfisdead" value="Non">
                    Non
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            L'ordinateur est-il
        </h2>
        <aside>
            <p>
                <input type="radio" name="whatiscomputer" value="une aventure civile">
                    une aventure civile
                </input>
            </p>
            <p>
                <input type="radio" name="whatiscomputer" value="une aventure commerciale">
                    une aventure commerciale
                </input>
            </p>
            <p>
                <input type="radio" name="whatiscomputer" value="la première et le deuxième question signifient la même chose">
                    la première et le deuxième question signifient la même chose
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>


<article>
    <section class='q'>
        <h2>
            Un bon design est :
        </h2>
        <aside>
            <p>
                <input type="radio" name="gooddesign" value="pratique">
                    pratique
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesign" value="élégant">
                    élégant
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesign" value="intelligent">
                    intelligent
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesign" value="naturel">
                    naturel
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesign" value="facile d'utilisation">
                    facile d'utilisation
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesign" value="compréhensible par tous">
                    compréhensible par tous
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesign" value="innovant">
                    innovant
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Un bon design :
        </h2>
        <aside>
            <p>
                <input type="radio" name="gooddesigndo" value="répond à un besoin">
                    répond à un besoin
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="définit un besoin">
                    définit un besoin
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="ne se contente pas de répondre à un besoin, il le définit">
                    ne se contente pas de répondre à un besoin, il le définit
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="répond à une attente">
                    répond à une attente
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="définit un attente">
                    définit un attente
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="ne se contente pas de répondre à une attente, il la définit">
                    ne se contente pas de répondre à une attente, il la définit
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Pourquoi même un bon design peut-il échouer ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Un bon design doit-il avoir raison ? 
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Un bon design doit-il servir ? 
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Cochez.
        </h2>
        <aside>
            <p>
                <input type="radio" name="gooddesigndo" value="Sécurité">
                    Sécurité
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="Facilité">
                    Facilité
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="Rapidité">
                    Rapidité
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="Accessibilité">
                    Accessibilité
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="Élégance">
                    Élégance
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="Simplicité">
                    Simplicité
                </input>
            </p>
            <p>
                <input type="radio" name="gooddesigndo" value="Innovation">
                    Innovation
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Choisissez la bonne position pour chaque forme dans chaque situation.
        </h2>
        <input placeholder="(jeu du carré, triangle, rond)">

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Caractérisez l'usage de l'ornement :
        </h2>
        <aside>
            <p>
                <input type="radio" name="pertinent" value="Enfantin">
                    Enfantin
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Refus de la pureté du design">
                    Refus de la pureté du design
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Instinct primitif">
                    Instinct primitif
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Peur de la beauté réelle">
                    Peur de la beauté réelle
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Maquillage pour un mauvais design">
                    Maquillage pour un mauvais design
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Bon pour les Indiens">
                    Bon pour les Indiens
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Basse culture">
                    Basse culture
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Parmi les termes suivants lequel est-il le plus correct pour désigner un contenu pertinent ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="pertinent" value="Populaire">
                    Populaire
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Démocratique">
                    Démocratique
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Recommandé">
                    Recommandé
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Accessible">
                    Accessible
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Authentique">
                    Authentique
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Unique">
                    Unique
                </input>
            </p>
            <p>
                <input type="radio" name="pertinent" value="Classique">
                    Classique
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Qui est le maître de la simplicité ?
        </h2>
        <aside>
            <p>
                <input type="radio" name="simple" value="John Maeda">
                    John Maeda
                </input>
            </p>
        </aside>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Parmi les termes suivants, choisissez un miracle :
        </h2>
        <aside>
            <p>
                <input type="radio" name="miracle" value="Apple">
                    Apple
                </input>
            </p>
            <p>
                <input type="radio" name="miracle" value="Xerox">
                    Xerox
                </input>
            </p>
            <p>
                <input type="radio" name="miracle" value="Google">
                    Google
                </input>
            </p>
            <p>
                <input type="radio" name="miracle" value="Miracle">
                    Miracle
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Quelle est la différence entre innovant et novateur ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Cherchez l’intrus :
        </h2>
        <aside>
            <p>
                <input type="radio" name="intru" value="Informatique">
                    Informatique
                </input>
            </p>
            <p>
                <input type="radio" name="intru" value="Digital">
                    Digital
                </input>
            </p>
            <p>
                <input type="radio" name="intru" value="Numérique">
                    Numérique
                </input>
            </p>
        </aside>
        <h3 class="button">suivant</h3>
    </section>
</article>

<article>
    <section class='q'>
        <h2>
            Un outil peut-il être conçu pour être détourné ?
        </h2>
        <input>

        </input>
        <h3 class="button">suivant</h3>
    </section>
</article>

</main>

<script type="text/javascript">

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    var overlay = document.getElementById("overlay");

    window.onload=function(){
        var articles = document.getElementsByTagName('article');
        for(var i in articles){
            articles[i].style.marginLeft=i*100+'vw';
        }
      overlay = document.getElementById('overlay');
}

window.repeat=setInterval(function(){


    var all = document.getElementsByTagName('*');
    var done=true;
    for(var i in all){
        console.log(all[i].scrollLeft);
        while(all[i].scrollLeft>0){
            done=false;
            console.log(all[i].scrollLeft);
            all[i].scrollTo(0,0);
            console.log(all[i].scrollLeft);
        }
    }
    if(done){
        console.log('done');
            overlay.parentNode.removeChild(overlay);
            window.clearInterval(window.repeat);
        }
        
    });

var scrollTo=0;
var scroll=0;
var speed=10;
var prevScroll=0;
var scrolled=0;
var async=0;

var buttons= document.getElementsByClassName('button');

async function scrollLeft(){
    if(async>0){
        return
    }
    async++;
    scrollTo=window.innerWidth;
    while(window.scrollX-scrolled<scrollTo){
        scroll+=(scrollTo-scroll)/20+4;
        speed=scroll-prevScroll;
        if(scrollTo-scroll<1){
            speed=4;
        }
        prevScroll=scroll;
        window.scrollBy(speed,0);
        await sleep(17);
    }


    scrolled=window.scrollX;
    scroll=0;
    prevScroll=0;
    async--;
}

for(var i in buttons){
    buttons[i].onmouseup=function(){
        scrollLeft();
    }
}




document.onmouseup=function(){  
        //document.body.scrollTo(1000, 0);
        //window.scrollTo(0,0);
    }



</script>
</body>
</html>





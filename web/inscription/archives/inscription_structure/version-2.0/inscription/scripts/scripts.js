// # DATA
// cette partie récupère des données sur le client 

navigator.browserInfo= (function(){
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return { 'browser': M[0], 'version': M[1] };
})();


var data = {};

data.date = new Date();

data.screen = {};
data.screen.width = screen.width
data.screen.height = screen.height;
data.screen.colors = screen.colorDepth;
data.client = {};
data.client.height = document.documentElement.clientHeight;
data.client.width = document.documentElement.clientWidth;
data.scroll = {};
data.scroll.height = document.documentElement.scrollHeight;
data.scroll.width = document.documentElement.scrollWidth;
data.offset = {};
data.offset.height = document.documentElement.offsetHeight;
data.offset.width = document.documentElement.offsetWidth;
data.window = {};
data.window.innerWidth = window.innerWidth;
data.window.innerHeight = window.innerHeight;


data.navigator = {};
data.navigator.language = navigator.languages || navigator.language;
data.navigator.appVersion = navigator.appVersion;
data.navigator.browser = navigator.browser;
data.navigator.userAgent = navigator.userAgent;
data.navigator.cookieEnabled = navigator.cookieEnabled;

data.browserInfo = {};
data.browserInfo.browser = navigator.browserInfo.browser;
data.browserInfo.version = navigator.browserInfo.version;

//console.log(data);

// # ITEM
// ici c'est la création de l'item associé à un id en json au moment du clic sur commencer 

function commencer() {
    console.log('commencer');
    nouv(data);
    document.getElementById('start').style.display = 'none';
    qDivs = document.getElementsByClassName('q');
    qShow(0);
}

function nouv(data){
    var request = $.ajax({
        url: "php/nouv.php",
        type: "POST",
        //processData: false,
        //contentType: false,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        data: data,
        async: false, // grado mais permet d'assigner result
        success: function(result){
            console.log(result);
            key = result;
            newData = {};
            newData[result] = {} 
            newData[result]['info'] = data; 
            newData[key]['questions'] = {};
        }
    });
}

// # QUESTIONS
// ici c'est la navigation dans les questions ainsi que la récupération des données et l'update du
// fichier json. le json fait des allers-retours tout au long des questions.

function qShow(id) {
    console.log('qShow');
    qDivs[id].style.display = 'block';
    qDivs[id].getElementsByTagName('button')[0].addEventListener("click", function() {
                 qNext(id);
            }, false);
}

function qHide(id) {
    console.log('qHide');
    qDivs[id].style.display = 'none';
}

function qNext(id) {
    console.log('qNext');
    qData(id);
    qEvents();
    if (qDivs[id+1]) {
        qShow(id+1);
        qHide(id);   
    } else {
        qHide(id);   
        document.getElementById('end').style.display = 'block';
    }
}

function qData(id) {
    console.log('qData');

    question = qDivs[id].getElementsByTagName('p')[0].innerText;
    console.log(question);

    newData[key]['questions'][question] = '';
    //console.log(newData);
    
    inputs =  qDivs[id].getElementsByTagName('input');
    for (i = 0; i < inputs.length; i ++) {
        if (inputs[i].type == "text") {
            newData[key]['questions'][question] = inputs[i].value;
        } else if (inputs[i].type == "radio") {
            if (inputs[i].checked) {
                newData[key]['questions'][question] = inputs[i].value;
                console.log(inputs[i]);
            }
        } 
    }
    //console.log(newData);

    edit(newData);
}

function edit(data){
    var request = $.ajax({
        url: "php/edit.php",
        type: "POST",
        //processData: false,
        //contentType: false,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        data: data,
        success: function(result){
            console.log(result);
        }
    });
}

// # EVENTS
//aici on check pour des données partculières afin de lancer des events en fonction 

function qEvents() {
    console.log(newData[key]['questions']);

    //quelle est votre couleur préférée
    if (newData[key]['questions']['Quel est votre couleur préférée ?']) {
        var color = newData[key]['questions']['Quel est votre couleur préférée ?'];
        var hex = document.querySelectorAll("input[value=" +color+"]")[0].getAttribute("data-hex");
        document.body.style.backgroundColor = hex;
    }

    //ctrl-z
    if (newData[key]['questions']['Quel est votre couleur préférée ?']==1) {
        for (var i = 0; i < qDivs.length; i ++) {
            qDivs[id].getElementsByTagName('button')[0].innerText = 'CTRL-Z';
        }

    }




}

var map = {}; // You could also use an array
onkeydown = onkeyup = function(e){
    e = e || event; // to deal with IE
    map[e.keyCode] = e.type == 'keydown';
    /* insert conditional here */
    if(map[17] && map[90]){ // CTRL+Z
        console.log('ctrl-z');
    }
}


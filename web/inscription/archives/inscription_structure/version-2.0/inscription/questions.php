<div class="q">
    <p>Quel est votre prénom ?</p>
    <div>
        <p><input type="text"></p>
    </div>
    <button>Continuer</button>
</div>

<div class="q">
    <p>Quel est votre nom ?</p>
    <div>
        <p><input type="text"></p>
    </div>
    <button>Continuer</button>
</div>

<div class="q">
    <p>Quel est votre adresse mail ?</p>
    <div>
        <p><input type="mail"></p>
    </div>
    <button>Continuer</button>
</div>

<div class="q">
    <p>Quel est votre sexe ?</p>
    <div>
        <p><input name="sexe" value="Homme" type="radio">Homme</p>
        <p><input name="sexe" value="Femme" type="radio">Femmme</p>
        <p><input name="sexe" value="Autre" type="radio">Autre</p>
    </div>
    <button>Continuer</button>
</div>

<div class="q">
    <p>Quel est votre couleur préférée ?</p>
    <div>
        <p><input type="radio" data-hex="#88B04B" name="color" value="Greenery">Greenery</p>
	<p><input type="radio" data-hex="#F7CAC9" name="color" value="RoseQuartz">Rose Quartz</p>
	<p><input type="radio" data-hex="#92A8D1" name="color" value="Serenity">Serenity</p>
	<p><input type="radio" data-hex="#955251" name="color" value="Marsala">Marsala</p>
        <p><input type="radio" data-hex="#B565A7" name="color" value="RadiandOrchid">Radiand Orchid</p>
        <p><input type="radio" data-hex="#009B77" name="color" value="Emerald">Emerald</p>
        <p><input type="radio" data-hex="#DD4124" name="color" value="TangerineTango">Tangerine Tango</p>
        <p><input type="radio" data-hex="#D65076" name="color" value="Honeysucle">Honeysucle</p>
        <p><input type="radio" data-hex="#45B8AC" name="color" value="Turquoise">Turquoise</p>
    </div>
    <button>Continuer</button>
</div>

<div class="q">
    <p>Trouvez-vous la commande CTRL-Z intuitive ?</p>
    <div>
        <p><input name="z" value="Oui" type="radio">Oui</p>
        <p><input name="z" value="Non" type="radio">Non</p>
    </div>
    <button>Continuer</button>
</div>

<div class="q">
    <p>Qu'est ce qui ne va pas avec l'ordinateur ?</p>
    <div>
        <p><input type="text"></p>
    </div>
    <button>Continuer</button>
</div>

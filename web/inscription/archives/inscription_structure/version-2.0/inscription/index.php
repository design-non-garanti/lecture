<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8">-->
        <title></title>
        <link rel="stylesheet" type="text/css" href="styles/styles.css">
        <link rel="stylesheet" type="text/css" href="fonts/myriad/myriad.css">
	<script src="assets/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="scripts/scripts.js" defer></script>
    </head>
    <body>
<?php
echo file_put_contents("test.txt","Hello World. Testing!");
?>
        <!-- start -->
        <div id='start'>
            <p>Design non-garanti.</p>
            <button onclick="commencer()">Commencer</button>
            <p id="conditions">
                En cliquant sur Commencer, vous acceptez nos <a href="conditions.php">Conditions</a> et indiquez
                que vous avez lu notre <a href="utilisation_des_donnees.php">Politique d’utilisation des
                données</a>, y compris notre <a href="utilisation_des_cookies.php">Utilisation des cookies</a>. Vous
                pourrez recevoir des notifications par mail de notre part et pouvez vous désabonner à
                tout moment.
            </p>
        </div>

        <?php include "questions.php"; ?>

        <!-- end -->
        <div id='end'>
            <p>Bravo</p>
            <button>Recommencer</button>
        </div>
       

    </body>
</html>

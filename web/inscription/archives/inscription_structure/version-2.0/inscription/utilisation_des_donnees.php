<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title></title>
        <style>
            * {
                margin: 0;
                padding: 0; 
            }
            body {
            padding: 4px;

            }
            div {
                margin: 4px;
                min-width: 10px;
                min-height: 10px;
                background-color: black;
                display: inline-block;
                vertical-align: top;
            }
        </style>
    </head>
    <body>

<?php
    $dataFile = "data/data.json";
    include "php/data.php";
?>

<?php foreach (array_reverse($dataJson) as $item) { ?><div style="width:<?php echo $item['info']['screen']['width']/10; ?>px; height:<?php echo $item['info']['screen']['height']/10; ?>px"></div><?php }; ?>

    </body>
</html>

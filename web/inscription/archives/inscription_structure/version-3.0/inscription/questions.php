<section>
	<div class="q">
		<h2 id="quel-est-votre-pr-nom-">quel est votre prénom ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quel-est-votre-nom-">quel est votre nom ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quel-est-votre-age-">quel est votre age ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quelle-est-votre-adresse-mail-">quelle est votre adresse mail ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quelle-est-votre-sexe-">quelle est votre sexe ?</h2>
		<div>
			<p><input name="sexe" value="Homme" type="radio">Homme</p>
			<p><input name="sexe" value="Femme" type="radio">Femme</p>
			<p><input name="sexe" value="Autre" type="radio">Autre</p>
		</div>
		<button>Continuer</button>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="choisissez-un-mot-de-passe-">choisissez un mot de passe :</h2>
		<div>
			<p><input name="mdp" value="Face ID" type="radio">Face ID</p>
		</div>
		<button>Continuer</button>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="vous-tes-">vous êtes ?</h2>
		<div>
			<p><input name="statut" value="amateur" type="radio">amateur</p>
			<p><input name="statut" value="professionel" type="radio">professionel</p>
			<p><input name="statut" value="expert" type="radio">expert</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="mais-encore-">mais encore ?</h2>
		<div>
			<p><input name="user" value="un scientifique" type="radio">un scientifique</p>
			<p><input name="user" value="un ouvrier de la connaissance" type="radio">un ouvrier de la connaissance</p>
			<p><input name="user" value="un programmeur" type="radio">un programmeur</p>
			<p><input name="user" value="un vrai utilisateur" type="radio">un vrai utilisateur</p>
			<p><input name="user" value="un utilisateur naïf" type="radio">un utilisateur naïf</p>
			<p><input name="user" value="une dame avec une machine à écrire royale" type="radio">une dame avec une machine à écrire royale</p>
			<p><input name="user" value="un enfant" type="radio">un enfant</p>
			<p><input name="user" value="un artiste" type="radio">un artiste</p>
			<p><input name="user" value="un musicien" type="radio">un musicien</p>
			<p><input name="user" value="une divinité" type="radio">une divinité</p>
			<p><input name="user" value="un néophyte complètement paumé" type="radio">un néophyte complètement paumé</p>
			<p><input name="user" value="un hacker" type="radio">un hacker</p>
			<p><input name="user" value="un nul" type="radio">un nul</p>
			<p><input name="user" value="vous-même" type="radio">vous-même</p>
			<p><input name="user" value="un individu" type="radio">un individu</p>
			<p><input name="user" value="un citoyen" type="radio">un citoyen</p>
			<p><input name="user" value="un autre" type="radio">un autre</p>
			<p><input name="user" value="un client" type="radio">un client</p>
			<p><input name="user" value="un interacteur" type="radio">un interacteur</p>
			<p><input name="user" value="un acheteur" type="radio">un acheteur</p>
			<p><input name="user" value="une cible" type="radio">une cible</p>
		</div>
		<button>Continuer</button>
		<p><a href="http://contemporary-home-computing.org/turing-complete-user/">Turing Complete User, Olia Lialina, October 2012, Appendix B: Users Imagined</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="comment-avez-vous-eu-connaissance-de-ce-m-moire-">comment avez-vous eu connaissance de ce mémoire ?</h2>
		<div>
			<p><input name="know" value="publicité" type="radio">publicité</p>
			<p><input name="know" value="magazine" type="radio">magazine</p>
			<p><input name="know" value="ami" type="radio">ami</p>
			<p><input name="know" value="collègue" type="radio">collègue</p>
			<p><input name="know" value="autre" type="radio">autre</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Monthy&#39;s Python Holy Grail, le jeu vidéo, 1996</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quelle-est-votre-couleur-pr-f-r-e-">quelle est votre couleur préférée ?</h2>
		<div>
			<p><input type="radio" data-hex="#88B04B" name="color" value="Greenery">Greenery</p>
			<p><input type="radio" data-hex="#F7CAC9" name="color" value="RoseQuartz">Rose Quartz</p>
			<p><input type="radio" data-hex="#92A8D1" name="color" value="Serenity">Serenity</p>
			<p><input type="radio" data-hex="#955251" name="color" value="Marsala">Marsala</p>
			<p><input type="radio" data-hex="#B565A7" name="color" value="RadiandOrchid">Radiand Orchid</p>
			<p><input type="radio" data-hex="#009B77" name="color" value="Emerald">Emerald</p>
			<p><input type="radio" data-hex="#DD4124" name="color" value="TangerineTango">Tangerine Tango</p>
			<p><input type="radio" data-hex="#D65076" name="color" value="Honeysucle">Honeysucle</p>
			<p><input type="radio" data-hex="#45B8AC" name="color" value="Turquoise">Turquoise</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Most trending Color 2010-2017</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="qu-attendez-vous-de-ce-m-moire-">qu&#39;attendez vous de ce mémoire ?</h2>
		<div>
			<p><input name="attente" value="une lecture facile" type="radio">une lecture facile</p>
			<p><input name="attente" value="une lecture rapide" type="radio">une lecture rapide</p>
			<p><input name="attente" value="un contenu accessible" type="radio">un contenu accessible</p>
			<p><input name="attente" value="un sentiment de sécurité" type="radio">un sentiment de sécurité</p>
			<p><input name="attente" value="un sentiment d'élégance" type="radio">un sentiment d&#39;élégance</p>
			<p><input name="attente" value="une problématique simple" type="radio">une problématique simple</p>
			<p><input name="attente" value="une problématique innovante" type="radio">une problématique innovante</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">user experience principles</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="vous-poss-dez-un-">vous possédez un :</h2>
		<div>
			<p><input name="name" value="value" type="radio">Mac</p>
			<p><input name="name" value="value" type="radio">Windows</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Umberto Eco, comment reconnaître la religion d&#39;un ordinateur</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="votre-interface-est-">votre interface est :</h2>
		<div>
			<p><input name="name" value="value" type="radio">graphique</p>
			<p><input name="name" value="value" type="radio">en lignes de commandes</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Umberto Eco, comment reconnaître la religion d&#39;un ordinateur</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="vous-tes-">vous êtes :</h2>
		<div>
			<p><input name="name" value="value" type="radio">Catholique</p>
			<p><input name="name" value="value" type="radio">Protestant</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Umberto Eco, comment reconnaître la religion d&#39;un ordinateur</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="c-est-quoi-un-ordinateur-">c&#39;est quoi un ordinateur ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="pourquoi-l-ordinateur-personnel-est-il-si-complexe-">pourquoi l&#39;ordinateur personnel est-il si complexe ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Don Norman, the invisible computer</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quand-votre-ordinateur-ne-fonctionne-pas-comme-vous-le-voulez-d-o-viens-le-probl-me-">quand votre ordinateur ne fonctionne pas comme vous le voulez d&#39;où viens le problème ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Ted Nelson, computer for cynics</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="pourquoi-ne-peut-on-pas-mettre-des-notes-marginales-sur-un-fichier-microsoft-word-un-pdf-ou-sur-une-page-web-">pourquoi ne peut-on pas mettre des notes marginales sur un fichier Microsoft Word, un Pdf ou sur une page web ?</h2>
		<div>
			<p><input name="name" value="value" type="radio">car les ordinateurs ne le permettent pas</p>
			<p><input name="name" value="value" type="radio">car Chuck Simoni, John Warnock et Tim Berne-Lee ont considéré que cela n&#39;était pas nécessaire</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Ted Nelson, computer for cynics</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="pourquoi-avons-nous-des-bureaux-d-ordinateur-et-pas-des-ateliers-d-ordinateur-">pourquoi avons-nous des bureaux d&#39;ordinateur et pas des ateliers d&#39;ordinateur ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="comment-disposez-vous-les-cones-sur-le-bureau-de-votre-ordinateur-">comment disposez-vous les îcones sur le bureau de votre ordinateur ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="pourquoi-le-texte-l-cran-est-il-g-n-ralement-en-noir-sur-fond-blanc-">pourquoi le texte à l&#39;écran est-il généralement en noir sur fond blanc ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Paper as metaphor</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="pourquoi-dix-huit-si-cles-de-culture-de-l-imprim-ont-t-r-duit-une-simulation-de-feuille-de-papier-">pourquoi dix-huit siècles de culture de l&#39;imprimé ont été réduit à une simulation de feuille de papier ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Ted Nelson, computer for cynics</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="un-ordinateur-est-il-un-cran-">un ordinateur est-il un écran ?</h2>
		<div>
			<p><input name="name" value="value" type="radio">oui</p>
			<p><input name="name" value="value" type="radio">non</p>
			<p><input name="name" value="value" type="radio">peut-être</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Apple</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="un-ordinateur-est-il-une-surface-">un ordinateur est-il une surface ?</h2>
		<div>
			<p><input name="name" value="value" type="radio">oui</p>
			<p><input name="name" value="value" type="radio">non</p>
			<p><input name="name" value="value" type="radio">peut-être</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Paper as metaphor</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="surfez-vous-sur-le-net-">surfez-vous sur le net ?</h2>
		<div>
			<p><input name="name" value="value" type="radio">Oui</p>
			<p><input name="name" value="value" type="radio">Non</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Jakob Nielsen, alertbox 1996</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="saviez-vous-que-le-surf-sur-le-net-est-mort-en-1996-">saviez-vous que le surf sur le net est mort en 1996 ?</h2>
		<div>
			<p><input name="name" value="value" type="radio">Oui</p>
			<p><input name="name" value="value" type="radio">Non</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Jakob Nielsen, alertbox 1996</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="l-ordinateur-est-">l&#39;ordinateur est :</h2>
		<div>
			<p><input name="name" value="value" type="radio">une aventure civile</p>
			<p><input name="name" value="value" type="radio">une aventure commerciale</p>
			<p><input name="name" value="value" type="radio">la première et le deuxième question signifient la même chose</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Jakob Nielsen, alertbox 1996</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="un-bon-design-est-">un bon design est :</h2>
		<div>
			<p><input name="name" value="value" type="radio">pratique</p>
			<p><input name="name" value="value" type="radio">élégant</p>
			<p><input name="name" value="value" type="radio">intelligent</p>
			<p><input name="name" value="value" type="radio">naturel</p>
			<p><input name="name" value="value" type="radio">facile d&#39;utilisation</p>
			<p><input name="name" value="value" type="radio">accesible</p>
			<p><input name="name" value="value" type="radio">innovant</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">user experience principles</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="un-bon-design-">un bon design :</h2>
		<div>
			<p><input name="name" value="value" type="radio">répond à un besoin</p>
			<p><input name="name" value="value" type="radio">définit un besoin<br>ne se contente pas de répondre à un besoin, il le définit<br>&lt;!-- - répond à une attente</p>
			<p><input name="name" value="value" type="radio">définit un attente</p>
			<p><input name="name" value="value" type="radio">ne se contente pas de répondre à une attente, il la définit--&gt;</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Matt McCullough, Steve Jobs and the Goal of Preaching, 08.28.2014</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="pourquoi-m-me-un-bon-design-peut-il-chouer-">pourquoi même un bon design peut-il échouer ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Don Norman, the invisible computer</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="un-bon-design-doit-il-avoir-raison-">un bon design doit-il avoir raison ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="un-bon-design-doit-il-servir-">un bon design doit-il servir ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Don Norman, Design of everyday things</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="cochez-">cochez.</h2>
		<div>
			<p><input name="name" value="value" type="radio">Sécurité</p>
			<p><input name="name" value="value" type="radio">Facilité</p>
			<p><input name="name" value="value" type="radio">Rapidité</p>
			<p><input name="name" value="value" type="radio">Accessibilité</p>
			<p><input name="name" value="value" type="radio">Élégance</p>
			<p><input name="name" value="value" type="radio">Simplicité</p>
			<p><input name="name" value="value" type="radio">Innovation</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">user experience principles</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="choisissez-la-bonne-position-pour-chaque-forme-dans-chaque-situation-">choisissez la bonne position pour chaque forme dans chaque situation.</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<blockquote>
			<p>Every shape exists only because of the space around it. … Hence there is a ‘right’ position for every shape in every situation. If we succeed in finding that position, we have done our job.</p>
		</blockquote>
		<p><a href="???">Donald Knuth TEXbook, JAN TSCHICHOLD, Typographische Gestaltung (1935)</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="caract-risez-l-usage-de-l-ornement-">caractérisez l&#39;usage de l&#39;ornement :</h2>
		<div>
			<p><input name="name" value="value" type="radio">Enfantin</p>
			<p><input name="name" value="value" type="radio">Refus de la pureté du design</p>
			<p><input name="name" value="value" type="radio">Instinct primitif</p>
			<p><input name="name" value="value" type="radio">Peur de la beauté réelle</p>
			<p><input name="name" value="value" type="radio">Maquillage pour un mauvais design</p>
			<p><input name="name" value="value" type="radio">Bon pour les Indiens</p>
			<p><input name="name" value="value" type="radio">Basse culture</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Adolf Loos, JAN TSCHICHOLD, Typographische Gestaltung (1935)</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="parmi-les-termes-suivants-lequel-est-il-le-plus-correct-pour-d-signer-un-contenu-pertinent-">parmi les termes suivants lequel est-il le plus correct pour désigner un contenu pertinent ?</h2>
		<div>
			<p><input name="name" value="value" type="radio">Populaire</p>
			<p><input name="name" value="value" type="radio">Démocratique</p>
			<p><input name="name" value="value" type="radio">Recommandé</p>
			<p><input name="name" value="value" type="radio">Accessible</p>
			<p><input name="name" value="value" type="radio">Authentique</p>
			<p><input name="name" value="value" type="radio">Unique</p>
			<p><input name="name" value="value" type="radio">Classique</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Google PageRank System</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="qui-est-le-ma-tre-de-la-simplicit-">qui est le maître de la simplicité ?</h2>
		<div>
			<p><input name="name" value="value" type="radio">John Maeda</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Rules of simplicity, John Maeda</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="parmi-les-termes-suivants-choisissez-le-miracle-">parmi les termes suivants, choisissez le miracle :</h2>
		<div>
			<p><input name="name" value="value" type="radio">Apple</p>
			<p><input name="name" value="value" type="radio">Xerox</p>
			<p><input name="name" value="value" type="radio">Google</p>
			<p><input name="name" value="value" type="radio">Microsoft</p>
			<p><input name="name" value="value" type="radio">Adobe</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Xerox, It&#39;s a miracle</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quelle-est-la-diff-rence-entre-innovant-et-novateur-">quelle est la différence entre innovant et novateur ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="cherchez-l-intrus-">cherchez l’intrus :</h2>
		<div>
			<p><input name="name" value="value" type="radio">Informatique</p>
			<p><input name="name" value="value" type="radio">Digital</p>
			<p><input name="name" value="value" type="radio">Numérique</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="un-outil-peut-il-tre-con-u-pour-tre-d-tourn-">un outil peut-il être conçu pour être détourné ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Guy Debord et Wolman - Mode d&#39;emploi du Détournement</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="votre-ordinateur-vous-simplifie-t-il-la-vie-">votre ordinateur vous simplifie t-il la vie ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">No one&#39;s life has yet been simplified by a computer. Nelson, Computer Lib, 1974</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="-quoi-set-cet-ic-ne">à quoi set cet icône</h2>
		<div>
			<p><input name="name" value="value" type="radio"> zoomer</p>
			<p><input name="name" value="value" type="radio"> rechercher</p>
		</div>
		<button>Continuer</button>
		<p>ou text ?</p>
		<p><a href="???">Fuller, Metaphor in The impossibility of the interface in Behind the Blip, ?</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="qu-est-ce-qui-est-important-lorsque-que-l-on-doit-choisir">qu&#39;est ce qui est important lorsque que l&#39;on doit choisir</h2>
		<div>
			<p><input name="name" value="value" type="radio">de bonnes possibilités</p>
			<p><input name="name" value="value" type="radio">de bon choix</p>
			<p><input name="name" value="value" type="radio">choose none of the above</p>
		</div>
		<button>Continuer</button>
		<p><a href="http://userrights.contemporary-home-computing.org/">Lialina, User Rights</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quel-est-votre-typo-pr-f-r-e-">quel est votre typo préférée ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="quelle-sont-les-diff-rence-entre-un-presse-papier-papier-num-rique">quelle sont les différence entre un presse papier papier numérique</h2>
		<div>
			<p><input name="name" value="value" type="radio">you can&#39;t see it, </p>
			<p><input name="name" value="value" type="radio">it holds only one object,</p>
			<p><input name="name" value="value" type="radio">whatever you put there destroys the previous contents.  Aside from that, IT&#39;S JUST LIKE A REGULAR CLIPBOARD IN EVERY OTHER RESPECT-- EXCEPT THERE AREN&#39;T ANY OTHER RESPECTS!</p>
		</div>
		<button>Continuer</button>
		<blockquote>
			<p>It&#39;s just like a regular clipboard, except<br>(a) you can&#39;t see it,<br>(b) it holds only one object,<br>(c) whatever you put there destroys the previous contents.  Aside from that, IT&#39;S JUST LIKE A REGULAR CLIPBOARD IN EVERY OTHER RESPECT-- EXCEPT THERE AREN&#39;T ANY OTHER RESPECTS!</p>
		</blockquote>
		<p><a href="???">Nelson, one liners</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="trouvez-vous-la-commande-ctrl-z-intuitive-">trouvez-vous la commande CTRL-Z intuitive ?</h2>
		<button>Continuer</button>
		<blockquote>
			<p>Oh, SURE the Macintosh interface is Intuitive! I&#39;ve always thought deep in my heart that command-z should undo things. &quot;</p>
		</blockquote>
		<p><a href="???">Margy Levine</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="qu-est-ce-qui-ne-va-pas-avec-l-ordinateur">qu&#39;est ce qui ne va pas avec l&#39;ordinateur</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">What&#39;s wrong with the computer ? Norman, The Invisible Computer</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="l-ordinateur-fait-il-">l&#39;ordinateur fait-il ?</h2>
		<p>Préférez vous que votre ordinateur fasse ?</p>
		<div>
			<p><input name="name" value="value" type="radio">une seule chose mais bien</p>
			<p><input name="name" value="value" type="radio">plusieurs choses mais mal</p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Information Appliance</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="pour-quoi-est-fait-l-ordinateur">pour quoi est fait l&#39;ordinateur</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Information Appliance</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="qu-est-ce-vous-trouvez-simple">qu&#39;est ce vous trouvez simple</h2>
		<p>Qu&#39;est ce vous trouvez compliqué</p>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="votre-ordinateur-est-il-honn-te-avec-vous-">votre ordinateur est-il honnête avec vous ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="-tes-vous-honn-te-avec-votre-ordinateur-">êtes vous honnête avec votre ordinateur ?</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="votre-ordinauter-vous-a-til-d-j-fait-du-mal">votre ordinauter vous a  til déjà fait du mal</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">???</a></p>
	</div>
</section>
<section>
	<div class="q">
		<h2 id="ne-me-poussez-pas-r-fl-chir-">ne me poussez pas à réfléchir…</h2>
		<div>
			<p><input type="text"></p>
		</div>
		<button>Continuer</button>
		<p><a href="???">Steve Krug, don&#39;t make me think</a></p>
	</div>
</section>
---

## quel est votre prénom ?

- [ ]

---

## quel est votre nom ?

- [ ]

---

## quel age avez-vous ?

- [ ]

---

## quelle est votre adresse mail ?

- [ ]

---

## quel est votre sexe ? Choisissez un emoji

- [ ] Homme
- [ ] Femme
- [ ] Autre

---

## quelle est votre couleur préférée ?

- [ ] Greenery
- [ ] Rose Quartz
- [ ] Serenity
- [ ] Marsala
- [ ] Radiand Orchid
- [ ] Emerald
- [ ] Tangerine Tango
- [ ] Honeysucle
- [ ] Turquoise

<!-- Lev Manovitch customisation personnalisation 
[Monthy's Python Holy Grail, le jeu vidéo, 1996](???) -->

---

## vous êtes ?

- [ ] amateur
- [ ] expert
- [ ] capable de résoudre la plupart des problèmes en un coup de fil

[garantie](???)

---

## mais encore ?

- [ ] un scientifique
- [ ] un ouvrier de la connaissance
- [ ] un ouvrier de l'intellect
- [ ] un programmeur
- [ ] un vrai utilisateur
- [ ] un utilisateur naïf
- [ ] la dame avec une machine à écrire royale
- [ ] un enfant
- [ ] un artiste
- [ ] un musicien
- [ ] une divinité
- [ ] une machine
- [ ] un néophyte complètement paumé
- [ ] un hacker
- [ ] un nul
- [ ] vous-même
- [ ] un individu
- [ ] un citoyen
- [ ] des gens
- [ ] un autre
- [ ] un client
- [ ] un interacteur
- [ ] un acheteur
- [ ] une cible

[Turing Complete User, Olia Lialina, October 2012, Appendix B: Users Imagined](http://contemporary-home-computing.org/turing-complete-user/)

---

## comment avez-vous eu connaissance de ce mémoire ?

- [ ] publicité
- [ ] magazine
- [ ] ami
- [ ] collègue
- [ ] autre

[Monthy's Python Holy Grail, le jeu vidéo, 1996](???)

---

## qu'attendez vous de ce mémoire ?

- [ ] une lecture facile
- [ ] une lecture rapide
- [ ] un contenu accessible
- [ ] un sentiment de sécurité
- [ ] un sentiment d'élégance
- [ ] une problématique simple
- [ ] une problématique innovante

[user experience principles](???)

---

## attendez-vous d'un objet qu'il soit conforme ?

- [ ] oui
- [ ] non

---

## un bon design est :

- [ ] pratique
- [ ] élégant
- [ ] intelligent
- [ ] naturel
- [ ] facile d'utilisation
- [ ] accessible
- [ ] innovant

[user experience principles](???)

---

## un bon design :

- [ ] répond à un besoin
- [ ] définit un besoin
- [ ] ne se contente pas de répondre à un besoin, il le définit

[Matt McCullough, Steve Jobs and the Goal of Preaching, 08.28.2014](???)

---

## pourquoi même un bon design peut-il échouer ?

- [ ] 

[Don Norman, the invisible computer](???)

---

## un bon design doit-il avoir raison ?

- [ ] 

[dialectique de la raison](???)

---

## un bon design est-il à notre service ? 

- [ ] 

[Don Norman, Design of everyday things](???)

---

## cochez.

- [ ] Sécurité
- [ ] Facilité
- [ ] Rapidité
- [ ] Accessibilité
- [ ] Élégance
- [ ] Simplicité
- [ ] Innovation

[user experience principles](???)

---

## choisissez la bonne position pour chaque forme dans chaque situation.

 O /\ []

>Every shape exists only because of the space around it. … Hence there is a ‘right’ position for every shape in every situation. If we succeed in finding that position, we have done our job.

[Donald Knuth TEXbook, JAN TSCHICHOLD, Typographische Gestaltung (1935)](???)

---

<!-- ## qu'est ce qui est important lorsque l'on doit choisir-->
## Face à un choix, le plus important est d'avoir :

- de bonnes possibilités
- de bons choix
- aucune des propositions ci-dessus

[Lialina, User Rights](http://userrights.contemporary-home-computing.org/)

---

## apprendre des nouvelles manières de faire dans le seul but d'être différent est :

- [ ] une contrainte
- [ ] irrespectueux
- [ ] inconsistant

---

## cherchez l'intrus :

- [ ] innovant
- [ ] kitsch
- [ ] avant-gardiste

[kitsch et avant-garde](???)

---

## caractérisez l'usage de l'ornement :

- [ ] Enfantin
- [ ] Refus de la pureté du design
- [ ] Instinct primitif
- [ ] Peur de la beauté réelle
- [ ] Maquillage pour un mauvais design
- [ ] Bon pour les Indiens
- [ ] Basse culture

[Adolf Loos, JAN TSCHICHOLD, Typographische Gestaltung (1935)](???)

---

## qu'est ce qui différencie le naturel de la contre-façon ?

- [ ]

---

## un outil peut-il être conçu pour être détourné ?

- [ ] 

[Guy Debord et Wolman - Mode d'emploi du Détournement](???)

---

## votre ordinateur est-il reprogrammable ?

- [ ] oui
- [ ] non

---

## votre interface est :

- [ ] graphique
- [ ] en lignes de commandes

[Umberto Eco, comment reconnaître la religion d'un ordinateur](???)

---

## vous possédez un :

- [ ] Mac
- [ ] Windows

[Umberto Eco, comment reconnaître la religion d'un ordinateur](???)

---

## vous êtes :

- [ ] Catholique
- [ ] Protestant

[Umberto Eco, comment reconnaître la religion d'un ordinateur](???)

---

## c'est quoi un ordinateur ?

- [ ] 

[Apple, iPad](???)

---

## quel est l'usage attendu d'un ordinateur ?

- [ ]

[garantie](???)

---

## décrivez un ordinateur.

- [ ]

---

## pourquoi l'ordinateur personnel est-il si complexe ?

- [ ] 

[Don Norman, the invisible computer](???)

---

## qu'est ce qui ne va pas avec l'ordinateur ?

- [ ] 

[What's wrong with the computer ? Norman, The Invisible Computer](???)

---

## l'ordinateur fait :

- [ ] une seule chose mais bien
- [ ] plusieurs choses mais mal

[Information Appliance](???)

---

## quand votre ordinateur ne fonctionne pas comme vous le voulez, d'où viens le problème ? 

- [ ] 

[Ted Nelson, computer for cynics](???)

---

## votre ordinateur vous simplifie t-il la vie ?

- [ ] 

[No one's life has yet been simplified by a computer. Nelson, Computer Lib, 1974](???)

---

## un ordinateur est-il un écran ?
 
- [ ] oui
- [ ] non
- [ ] peut-être

[Where did the computer go ?](???)

---

## un ordinateur est-il une surface ?

- [ ] oui
- [ ] non
- [ ] peut-être

[Paper as metaphor](???)

---

## pourquoi est-il impossible de mettre des notes marginales sur un fichier Microsoft Word, un Pdf ou sur une page web ?

- [ ] car les ordinateurs ne le permettent pas
- [ ] car c'est une fonctionnalité difficile à trouver
- [ ] car Chuck Simoni, John Warnock et Tim Berners-Lee ont considéré que cela n'était pas nécessaire

[Ted Nelson, computer for cynics](???)

---

## pourquoi le texte à l'écran est-il généralement en noir sur fond blanc ?

- [ ] 

[Paper as metaphor](???)

---

## pourquoi dix-huit siècles de culture de l'imprimé ont été réduits à une simulation de feuille de papier ?

- [ ] 

[Ted Nelson, computer for cynics](???)

---

## quelles sont les différences entre un presse papier (papier) et un presse papier numérique ?

- [ ] il est invisible
- [ ] il ne peut contenir qu'un objet,
- [ ] quoi-que vous y placiez, il détruit le contenu précédent. À part ça, C'EST JUSTE UN PRESSE PAPIER NORMAL SOUS TOUT LES ASPECTS-- À L'EXEPTION QU'IL NE POSSÈDE AUCUN AUTRE ASPECT.

>It's just like a regular clipboard, except 
>(a) you can't see it, 
>(b) it holds only one object,
>(c) whatever you put there destroys the previous contents.  Aside from that, IT'S JUST LIKE A REGULAR CLIPBOARD IN EVERY OTHER RESPECT-- EXCEPT THERE AREN'T ANY OTHER RESPECTS!

[Nelson, one liners](???)

---

## avons-nous des bureaux d'ordinateur ou des ateliers d'ordinateur ?

- [ ] 

[???](???)

---

## comment disposez-vous les icônes sur le bureau de votre ordinateur ? 

- [ ] 

[???](???)

---

## à quoi sert cette icône : 🔎 

- [ ]  zoomer
- [ ]  rechercher

ou text ?

[Fuller, Metaphor in The impossibility of the interface in Behind the Blip, ?](???)

---

## la commande CMD-Z est-elle intuitive ?

- [ ] J'ai toujours profondément penser au fond de mon cœur que CMD-Z devait annuler les choses.

>Oh, SURE the Macintosh interface is Intuitive! I've always thought deep in my heart that command-z should undo things. "

[Margy Levine](???)

---

<!-- ## qu'est ce vous trouvez simple -->

## que trouvez-vous simple ?

<!-- Qu'est ce vous trouvez compliqué -->
Que trouvez-vous compliqué

- [ ] 

[???](???)

---

## qui est le maître de la simplicité ?

- [ ] John Maeda

[Rules of simplicity, John Maeda](???)

---

## aimez-vous que l'on vous pousse à réfléchir ?

- [ ] continuer

[Steve Krug, don't make me think](???)

---

## surfez-vous sur le net ?

- [ ] Oui
- [ ] Non

[Jakob Nielsen, alertbox 1996](???)

---

## saviez-vous que le surf sur le net est mort en 1996 ?

- [ ] Oui
- [ ] Non

[Jakob Nielsen, alertbox 1996](???)

---

## l'ordinateur est : 

- [ ] une aventure civile
- [ ] une aventure commerciale
- [ ] la première et le deuxième question signifient la même chose

[Jakob Nielsen, alertbox 1996](???)

---

## parmi les termes suivants lequel est-il le plus correct pour désigner un contenu pertinent ?

- [ ] Populaire
- [ ] Démocratique
- [ ] Recommandé
- [ ] Accessible
- [ ] Authentique
- [ ] Unique
- [ ] Classique

[Google PageRank System](???)

---

## parmi les termes suivants, choisissez le miracle :

- [ ] Jésus
- [ ] Xerox

[Xerox, It's a miracle](???)

---

## qui connaît le mieux vos produits ? 

- [ ] Adobe
- [ ] Apple
- [ ] Google
- [ ] Microsoft
- [ ] Xerox

[garantie](???)

---

## qui invente l'objet technique ?

- [ ] Le constructeur
- [ ] L'utilisateur

---

## quel est le rôle d'une garantie ?

- [ ]

---

## votre ordinateur est-il honnête avec vous ?

- [ ] 

[???](???)

---

## êtes-vous honnête avec votre ordinateur ?

- [ ] 

[???](???)

---

## votre ordinateur vous a t-il déjà fait du mal ?

- [ ] 

[???](???)

---


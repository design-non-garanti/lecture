// # DATA
// cette partie récupère des données sur le client 

navigator.browserInfo= (function(){
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return { 'browser': M[0], 'version': M[1] };
})();


var data = {};

data.date = new Date();

data.screen = {};
data.screen.width = screen.width
data.screen.height = screen.height;
data.screen.colors = screen.colorDepth;
data.client = {};
data.client.height = document.documentElement.clientHeight;
data.client.width = document.documentElement.clientWidth;
data.scroll = {};
data.scroll.height = document.documentElement.scrollHeight;
data.scroll.width = document.documentElement.scrollWidth;
data.offset = {};
data.offset.height = document.documentElement.offsetHeight;
data.offset.width = document.documentElement.offsetWidth;
data.window = {};
data.window.innerWidth = window.innerWidth;
data.window.innerHeight = window.innerHeight;


data.navigator = {};
data.navigator.language = navigator.languages || navigator.language;
data.navigator.appVersion = navigator.appVersion;
data.navigator.browser = navigator.browser;
data.navigator.userAgent = navigator.userAgent;
data.navigator.cookieEnabled = navigator.cookieEnabled;

data.browserInfo = {};
data.browserInfo.browser = navigator.browserInfo.browser;
data.browserInfo.version = navigator.browserInfo.version;

//console.log(data);

// # SWIPE
// ici c'est la configuration du swipe entre chaque question

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

qSections = document.getElementsByTagName('section');

for(var s = 0 ; s < qSections.length ; s++){
    qSections[s].style.left=s*100+'vw';
}

window.repeat=setInterval(function(){

    var all = document.getElementsByTagName('*');
    var done=true;
    for(var i in all){
        while(all[i].scrollLeft>0){
            done=false;
            all[i].scrollTo(0,0);
        }
    }
    if(done){
        window.clearInterval(window.repeat);
    }

});


var scrollTo=0;
var scroll=0;
var speed=10;
var prevScroll=0;
var scrolled=0;
var async=0;

async function scrollRight(){
    if(async>0){
        return
    }
    async++;
    scrollTo=window.innerWidth;
    while(window.scrollX-scrolled<scrollTo){
        scroll+=(scrollTo-scroll)/20+4;
        speed=scroll-prevScroll;
        if(scrollTo-scroll<1){
            speed=4;
        }
        prevScroll=scroll;
        window.scrollBy(speed,0);
        await sleep(17);
    }

    scrolled=window.scrollX;
    scroll=0;
    prevScroll=0;
    async--;
}

// # ITEM
// ici c'est la création de l'item associé à un id en json au moment du clic sur commencer 

var qDivs;

function commencer() {
    console.log('commencer');
    nouv(data);
    //document.getElementById('start').style.display = 'none';
    qDivs = document.getElementsByClassName('q');
    for(var i = 0 ; i < qDivs.length ; i++){
        qDivs[i].getElementsByTagName('button')[0].id=i;
        console.log(this);
        qDivs[i].getElementsByTagName('button')[0].addEventListener("click", function() {
            qNext(this.id);
        }, false);
    }
    scrollRight();
}

function nouv(data){
    var request = $.ajax({
        url: "php/nouv.php",
        type: "POST",
        //processData: false,
        //contentType: false,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        data: data,
        async: false, // grado mais permet d'assigner result
        success: function(result){
            console.log(result);
            key = result;
            newData = {};
            newData[result] = {} 
            newData[result]['info'] = data; 
            newData[key]['questions'] = {};
        }
    });
}

// # QUESTIONS
// ici c'est la navigation dans les questions ainsi que la récupération des données et l'update du
// fichier json. le json fait des allers-retours tout au long des questions.

function qShow(id) {
    console.log('qShow');
    //qDivs[id].style.display = 'block';
    qDivs[id].getElementsByTagName('button')[0].addEventListener("click", function() {
     qNext(id);
 }, false);
}

function qHide(id) {
    console.log('qHide');
    //qDivs[id].style.display = 'none';
}

function qNext(id) {
    if(qData(id)){
        scrollRight();
    }
    qEvents();
    
    /*
    if (qDivs[id+1]) {
        qShow(id+1);
        qHide(id);   
    } else {
        qHide(id);   
        //document.getElementById('end').style.display = 'block';
    }*/
}

var name='';

function qData(id) {
    console.log('qData',id);

    question = qDivs[id].getElementsByTagName('h2')[0].id;
    //console.log(question);

    newData[key]['questions'][question] = '';
    console.log(newData);
    
    inputs =  qDivs[id].getElementsByTagName('input');

    var answer='';

    for (i = 0; i < inputs.length; i ++) {
        if (inputs[i].type == "text") {
            newData[key]['questions'][question] = inputs[i].value;
            answer+=inputs[i].value;
        } else if (inputs[i].type == "radio") {
            if (inputs[i].checked) {
                newData[key]['questions'][question] = inputs[i].value;
                //console.log(inputs[i]);
                answer+=inputs[i].value;
            }
        } 
    }

    console.log('Answer',answer.length);
    if(answer.length>0){
        edit(newData);
        return true
    }else{
        //return true
        if(name.length>0){
            alert(name+", pour continuer veuillez remplir correctement le formulaire");
        }else{
            alert("Pour continuer veuillez remplir correctement le formulaire");
        }
        return false
    }
}

function edit(data){
    var request = $.ajax({
        url: "php/edit.php",
        type: "POST",
        //processData: false,
        //contentType: false,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        data: data,
        success: function(result){
            console.log(result);
        }
    });
}

// # EVENTS
//aici on check pour des données partculières afin de lancer des events en fonction 

async function qEvents() {

    console.log('qEvents');
    //console.log(newData[key]['questions']);
    //quelle est votre couleur préférée
    if (newData[key]['questions']['quelle-est-votre-couleur-pr-f-r-e-'] && !window.colorEvent) {
        var color = newData[key]['questions']['quelle-est-votre-couleur-pr-f-r-e-'];
        var hex = document.querySelectorAll("input[value=" +color+"]")[0].getAttribute("data-hex");
        document.body.style.backgroundColor = hex;
        var overlay = document.getElementById('overlay');
        var opacity = 1;

        while(opacity>0){
            console.log('opacity--',opacity);
            await sleep(17);
            overlay.style.opacity=opacity;
            opacity-=0.05;
        }
        //console.log('event_ok');
        window.colorEvent=true;
    }

    if (newData[key]['questions']['quel-est-votre-pr-nom-'] && !window.nameEvent) {
        name = newData[key]['questions']['quel-est-votre-pr-nom-'];
        for(var i = 0 ; i < qDivs.length ; i++){
            var question = qDivs[i].getElementsByTagName('h2')[0];
            question.textContent=name+', '+question.textContent;
        }
        //console.log('event_ok');
        window.nameEvent=true;
    }

    if (newData[key]['questions']['choisissez-la-bonne-position-pour-chaque-forme-dans-chaque-situation-'] && !window.bauhausEvent) {
        tschichold_event();
    }
}

var map = {}; // You could also use an array
onkeydown = onkeyup = function(e){
    e = e || event; // to deal with IE
    map[e.keyCode] = e.type == 'keydown';
    /* insert conditional here */
    if(map[17] && map[90]){ // CTRL+Z
        console.log('ctrl-z');
    }
}


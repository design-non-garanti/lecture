<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<style type="text/css">

	<?php
	$dir = "fonts/";
	$fonts = scandir($dir);


	foreach ($fonts as $key => $value) {
		if($value == "." || $value == ".."){}else{
			echo "@font-face{\r\nfont-family: \"".$value."\";\r\nsrc: url(\"".$dir.$value."\");\r\n}\r\n";
		}
	}
	?>

	*{
		font-weight: normal;
		margin: 0;
		padding: 0;
		border-collapse: collapse;
	}

	html{
		font-family: 'MYRIADAT.ttf';
		color: rgb(32,32,32);
		background-color: rgb();
		overflow: hidden;
	}

	article{
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		margin:auto;
		width: 100%;
		display: table;
	}
	section{
		margin:auto;
		display: table;
	}
	section h1, h2, h3{
		font-family: 'MYRIADAM.ttf';
		display: table; 
	}
	section h1{
		font-size: 4rem;
	}
	section h2{
		font-size: 3rem;
	}
	section h3{
		font-size: 2rem;
	}
	section * {
		margin-bottom: 1rem;
	}
	section h4{
		font-size: 1.25rem;
	}
	.button{
		cursor: pointer;
		font-family: 'MYRIADAT.ttf';
		box-sizing: border-box;
		display: block;
		padding: 0.33rem;
		background-color: rgb(240,240,240);
		text-align: center;
		width: 20rem;
		margin:auto;
		margin-bottom: 1rem;
	}
	section aside p {
		font-size: 2rem;
		margin:auto;
		width: 20rem;
		margin-bottom: 1rem;
	}
	section aside{
		margin-top: 1.5rem;
		margin-bottom: 2rem;
	}
	section input{
		font-size: 1.25rem;
		box-sizing: border-box;
		margin:auto;
		width: 20rem;
		margin-bottom: 1rem;
		min-height: 2.5rem;
		display: block;
		text-align: center;
	}

	section input[type=radio]{
		vertical-align: middle;
		display: inline !important;
		min-height:1.5rem !important;
		width: auto !important;
		min-width: 1.25rem !important;
		margin: auto;
	}
</style>
</head>
<body>
	<main>

		<article>
			<section>
				<h1>
					Design non garanti.
				</h1>
				<h3 class="button">Commencer</h3>
			</section>
		</article>

		<article>
			<section>
				<h2>
					Quel est votre prénom ?
				</h2>
				<input placeholder="">

			</input>
			<h3 class="button">suivant</h3>
		</section>
	</article>

	<article>
		<section>
			<h2>
				Quel est votre nom ?
			</h2>
			<input placeholder="" >

		</input>
		<h3 class="button">suivant</h3>
	</section>
</article>
<!--
	<article>
		<section>
			<h2>
				Quelle est votre adresse postale ?
			</h2>
			<input placeholder="exemple : michel">

		</input>
		<h3 class="button">suivant</h3>
	</section>
</article>
-->
<article>
	<section>
		<h2>
			Quelle est votre adresse mail ?
		</h2>
		<input>

	</input>
	<h3 class="button">suivant</h3>
</section>
</article>

<article>
	<section>
		<h2>
			Quelle est votre sexe ?
		</h2>
		<aside>
			<p>
				<input type="radio" name="gender" value="Homme">
				Homme
			</input>
		</p>
		<p>
			<input type="radio" name="gender" value="Femme">
			Femme
		</input>
	</p>
	<p>
		<input type="radio" name="gender" value="Autre">
		Autre
	</input>
</p>
</aside>
<h3 class="button">suivant</h3>
</section>
</article>


<article>
	<section>
		<h2>
			Choisissez un mot de passe :
		</h2>
		<aside>
			<p>
				<input type="radio" name="key" value="mot de passe">
				mot de passe
			</input>
		</p>
		<p>
			<input type="radio" name="key" value="pas de mot de passe">
			pas de mot de passe
		</input>
	</p>
	<p>
		<input type="radio" name="key" value="Face ID">
		Face ID
	</input>
</p>
</aside>
<h3 class="button">suivant</h3>
</section>
</article>

<article>
	<section>
		<h2>
			Quelle est votre profession ?
		</h2>
		<aside>
			<p>
				<input type="radio"  name="job" value="designer">
				designer
			</input>
		</p>
		<p>
			<input type="radio" hex="#F7CAC9" name="job" value="Rose Quartz">
			étudiant.e
		</input>
	</p>
	<p>
		<input type="radio" hex="#92A8D1" name="job" value="Serenity">
		chercheur.se
	</input>
</p>
<p>
			<input type="radio" hex="#F7CAC9" name="job" value="Rose Quartz">
			enseignant.e
		</input>
	</p>
</aside>
<h3 class="button">suivant</h3>
</section>
</article>

<article>
	<section>
		<h2>
			Quelle est votre couleur préférée ?
		</h2>
		<aside>
			<p>
				<input type="radio" hex="#88B04B" name="color" value="Greenery">
				Greenery
			</input>
		</p>
		<p>
			<input type="radio" hex="#F7CAC9" name="color" value="Rose Quartz">
			Rose Quartz
		</input>
	</p>
	<p>
		<input type="radio" hex="#92A8D1" name="color" value="Serenity">
		Serenity
	</input>
</p>
<p>
	<input type="radio" hex="#955251" name="color" value="Marsala">
	Marsala
</input>
</p>
<p>
	<input type="radio" hex="#B565A7" name="color" value="Radiand Orchid">
	Radiand Orchid
</input>
</p>
<p>
	<input type="radio" hex="#009B77" name="color" value="Emerald">
	Emerald
</input>
</p>
<p>
	<input type="radio" hex="#DD4124" name="color" value="Tangerine Tango">
	Tangerine Tango
</input>
</p>
<p>
	<input type="radio" hex="#D65076" name="color" value="Honeysucle">
	Honeysucle
</input>
</p>
<p>
	<input type="radio" hex="#45B8AC" name="color" value="Turquoise">
	Turquoise
</input>
</p>
</aside>
<h3 class="button">suivant</h3>
</section>

</article>


<article>
	<section>
		<h2>
			Qu'attendez vous de ce mémoire ?
		</h2>
		<aside>
			<p>
				<input type="radio" name="goal" value="un moment de lecture agréable">
					une lecture agréable
				</input>
			</p>
			<p style="margin-left:11.4rem; width: 30rem">
				<input type="radio" name="goal" value="pas de mot de passe">
					une bonne découverte
				</input>
			</p>
			<p style="margin-left:11.4rem; width: 30rem">
				<input type="radio" name="goal" value="un académisme exemplaire">
					un académisme exemplaire
				</input>
			</p>
			<p style="margin-left:11.4rem; width: 30rem">
				<input type="radio" name="goal" value="un académisme exemplaire">
					une expérience intéressante
				</input>
			</p>
		</aside>
		<h3 class="button">
			suivant
		</h3>
	</section>
</article>

<article>
	<section>
		<h2>
			Qu'attendez vous de ce mémoire ?
		</h2>
		<aside>
			<p>
				<input type="radio" name="goal" value="un moment de lecture agréable">
					une lecture agréable
				</input>
			</p>
			<p style="margin-left:11.4rem; width: 30rem">
				<input type="radio" name="goal" value="pas de mot de passe">
					une bonne découverte
				</input>
			</p>
			<p style="margin-left:11.4rem; width: 30rem">
				<input type="radio" name="goal" value="un académisme exemplaire">
					un académisme exemplaire
				</input>
			</p>
			<p style="margin-left:11.4rem; width: 30rem">
				<input type="radio" name="goal" value="un académisme exemplaire">
					une expérience intéressante
				</input>
			</p>
		</aside>
		<h3 class="button">
			suivant
		</h3>
	</section>
</article>




</main>

<script type="text/javascript">

	function sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}

	var overlay;

	window.onload=function(){
		var articles = document.getElementsByTagName('article');
		for(var i in articles){
			articles[i].style.marginLeft=i*100+'vw';
		}
	//	overlay = document.getElementById('overlay');
}

window.repeat=setInterval(function(){
	

	var all = document.getElementsByTagName('*');
	var done=true;
	for(var i in all){
		console.log(all[i].scrollLeft);
		while(all[i].scrollLeft>0){
			done=false;
			console.log(all[i].scrollLeft);
			all[i].scrollTo(0,0);
			console.log(all[i].scrollLeft);
		}
	}
	if(done){
			//overlay.parentNode.removeChild(overlay);
			window.clearInterval(window.repeat);
		}
		
	});

var scrollTo=0;
var scroll=0;
var speed=10;
var prevScroll=0;
var scrolled=0;
var async=0;

var buttons= document.getElementsByClassName('button');

async function scrollLeft(){
	if(async>0){
		return
	}
	async++;
	scrollTo=window.innerWidth;
	while(window.scrollX-scrolled<scrollTo){
		scroll+=(scrollTo-scroll)/20+4;
		speed=scroll-prevScroll;
		if(scrollTo-scroll<1){
			speed=4;
		}
		prevScroll=scroll;
		window.scrollBy(speed,0);
		await sleep(17);
	}


	scrolled=window.scrollX;
	scroll=0;
	prevScroll=0;
	async--;
}

for(var i in buttons){
	buttons[i].onmousedown=function(){
		scrollLeft();
	}
}




document.onmouseup=function(){	
		//document.body.scrollTo(1000, 0);
		//window.scrollTo(0,0);
	}



</script>
</body>
</html>
Comparant la difficulté de régler l'heure sur une montre électronique à 4 boutons permettant
l'incrémenter et de décrémenter les heures et les minutes à celle d'une montre mécanique, Raskin
affirme que même les tâches simples se sont embourbées dans la complexité.  Il montre alors le
design d'une montre électronique présente sur un VCR qu'il qualifie de facile à régler. Cependant,
il soutient qu'un design encore meilleur serait une horloge qui se règle elle même en fonction de
signaux horaires.

![« An easy-to-set digital clock on a VCR » présenté par Raskin.](../items/easy-to-set_digital_clock/easy-to-set_digital_clock_orig.png "easy-to-set_digita_clock_orig.png")

On peut se demander si le design de la montre électronique imaginée par Raskin serait celle-ci :

![« An easy-to-set digital clock on a VCR » réglable par incrémentation et décrémentation ou par synchronisation au signal horaire.](../items/easy-to-set_digital_clock/easy-to-set_digital_clock_time.png "easy-to-set_digita_clock_time.png")

Ou celle-là :

![« An easy-to-set digital clock on a VCR » réglée par synchronisation au signal horaire.](../items/easy-to-set_digital_clock/easy-to-set_digital_clock_auto.png "easy-to-set_digita_clock_auto.png")

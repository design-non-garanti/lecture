function reset() {
    window.scrollTo(0, 0);
}

function init() {
    speed=10;
    autoscroll=true;
}

function scroll() {
        window.scrollBy(0,1);
}

function check() {
    if (autoscroll === true) {
        console.log("var autoscroll=true;");
        scroll();
    }
    // check if bottom
    checkDelay = setTimeout(check,speed);
}

window.onload = function() {
    reset();
    init();
    check();
}

#REDACTION

# À la recherche du design perdu, le material design

* [X] L'enquête
    * [X] Pourquoi cette enquête ?
      - [X] expérience utilisateur
      - [X] Résumé des courants de design graphique du web (s'il existent)
      <!--
      - [ ] design moderne & informatique
      - [ ] design moderne & web -->
    * [X] Comment ?
      - [X] Compagny Info
      - [X] Web archives
      <!--
    * [ ] Google ?
      - [ ] culture du design d’expérience, culture google ?
    -->

* [X] 05may1999
  * [X] Le site de google
    - [X] un moteur de recherche 
    - [X] où est le design ?
    - [X] The Name

* [X] 08nov1999
  - [X] changement de logo
  - [X] complexification de la page (8 parties)
  - [X] apparition du terme experience (3)
  - [X] apparition du terme user (19)
  - [X] apparition du terme design (5)
  - [X] le design comme une approche, une mission
  * [X] Jakob Nielsen dans la team
    * [X] pensée Ux et Google
      - [X] Web as a business tool
      - [X] Business exist to serve people
    - [X] Je pense donc je quitte
    - [X] La télé du web

* [X] 2001
  - [X] complexification de la page (11 parties) <!-- le contenu principal précédant est séparé dans une page google today de la section in Depth, en profondeur -->
  - [X] changement d'adresse <!-- (compagny -> corporate/today) -->
  - [X] formule user experience (1)
  - [X] apparition terme people (3)
  - [X] terme experience (2)
  - [X] terme user (20)
  - [X] disparition du terme design

* [X] 2008
  - [X] complexification de la page (15 parties) <!-- la page s'appelle désormais Design principles dans la section Our Philosophy -->
  - [X] changement d'adresse, apparition du terme UX <!-- (corporate/today -> corporate/ux) -->
  - [X] 10 vérités deviennent 10 principes résumés en dix mots <!-- top 10, mojo, dieter rams, bible -->
  - [X] formule user experience (8) <!-- titre -->
  - [X] terme people (22) <!-- explosion -->
  - [X] terme experience (10)  <!-- montée en force -->
  - [X] terme user (33) <!-- montée stable -->
  - [X] retour en force du terme design (27) <!-- associé au terme principe, nom de cette page dans l'index -->
  * [X] L'influence de Don Norman
    - [X] explosion du terme people même année de sa déclaration, adressage ux
    - [X] L'UX les origines de la peur

* [X] 2008 - 2016
  - [X] dix vérités
  - [X] dix principes de design
  - [X] la philosophie l'emporte

* [X]  2011
  - [X] version française (tardive, a vérifier) google.fr/corporate/ux
  - [X] pas de terme people car ça veux rien dire en français donc utilisateur
  - [X] google traduction ?
    …

* [ ] 25jun2014
  - [ ] création d'une page design <!-- qui existait depuis des années mais vide -->
  - [ ] veille de l'officialisation du material design
  - [ ] formules sont déplacées 
  * Un Design pour tous les guider
    - [ ] une arme pour rivaliser avec l'app store
    - [ ] la soumission graphique librement consentie 
  * [ ] Praise to be Duarte
    - [ ] Un messie chez google
    - [ ] Mathias le roi de la surface
    - [ ] Mathias le moderne
    - [ ] Un phénix en papier
    …

* [ ] 29jun2014
  - [ ] Un document en avant-première
  * [ ] la nouvelle typographie du web
    - [ ] Do / Don't
    …

* [ ] 2017
  * [ ] Le material design, le nouveau bauhaus
    - [ ] un fac-similé moderne
    - [ ] une révolte devenue tradition

# Xerox, la bible et les grecs

* [ ] Aux origines de l'interface graphique ( du graphisme à l'écran ? )
  * [ ] La culture des interfaces graphiques
    - [ ] le bureau, 
    - [ ] le travailleur en col blanc
    - [ ] le scribe
  * [ ] Courte histoire de Xerox
    - [ ] origines du terme Xerox
    - [ ] un papier WYSIWIG aux origines de l'interface graphique
* [ ] Dominique 1977
  - [ ] Le travail manuel, une corvée archaïque
  * [ ] L'âge sombre
    - [ ] Représentation pessimiste du moyen-age
    - [ ] Le moyen-age comme anti-thèse de la modernité

* [ ] Dominique 2017
  - [ ] Esthétique post-moderne
  * [ ] Xerox missionnaire
    - [ ] diffusion des textes sacrés
    - [ ] caricature culturelle
  - [ ] Un message résolument universel

# la discorde des mots, le conforme des images

* [ ] c'est quoi le graphisme à l'écran ?
  * [ ] Une religion
    - [ ] le graphique est catholique, le textuel est protestant
    - [ ] dualité textuel != graphique
    - [ ] amalgame graphisme = image
  * [ ] Un degré de perfection
    * [ ] évaluation 
    - [ ] mesure du beau
    - [ ] tester la beauté
    * [ ] évolution
      - [ ] le progrès du beau
      - [ ] la nostalgie du texte
<!--    * [ ] spectaculaire
      - [ ] superproduction graphique -->
  * [ ] Le graphisme c'est naturel.
    - [ ] Règne graphique
    - [ ] Idéal graphique
    - [ ] Un présent graphique

* [ ] Dwarf Fortress, au delà de la qualité.
  - [ ] Le texte, pour le meilleur et pour le pire.
  - [ ] Apprentissage et révélation
  <!-- - [ ] Un jeu protestant-->

* [ ] Matrix, résistance textuelle, obéissance graphique.
  * [ ] Une humanité divisé entre graphique
    - [ ] mode graphique par défaut
    - [ ] un capital graphique
  * [ ] et textuel
    - [ ] introduction au secret du texte
    - [ ] texte comme résistance

    &nbsp;